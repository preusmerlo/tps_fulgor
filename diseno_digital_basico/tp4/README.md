# Trabajo Practico Nº 4: Punto Fijo

El presente práctico tiene como objetivo aplicar los conceptos de aritmética de Punto Fijo usando Python. Luego, construir operaciones básicas en Verilog y comparar los resultados de estas con los obtenidos en Python.

Los enunciados del práctico se encuentran en ```tp4.pdf```.

## Contenido
[[_TOC_]]

## Modelado de Punto Fijo en Python

Este punto es resuelto por el script de Python ```ej1.py``` que se encuentra en el directorio ```ej1/```. Antes de ejecutar el programa, deben configurarse algunos parámetros de entrada del mismo. Por ejemplo, suponga que se desea analizar el efecto de la representación en un sistema ```S(6,4)``` por redondeo para tres valores de rolloff del filtro conformador de espectro, una posible configuración resulta:

```python
    ### ------------------------ Configurables -----------------------
    # Generales
    BR = 1e9                # Symbol Rate               [Bd]
    N  = 16                 # Factor de sobremuestreo
    L  = 1e3                # Cantidad de símbolos a simular
    
    # FFT
    NFFT = 256              # Cantidad de puntos de fft
    
    # Pulse shaping
    rolloff = [0.0,0.5,1]   # Roll-Off vector (len = 3)
    NT = 16                 # Cantidad de ceros de h(t)
    
    # Mapper
    M  = 2                  # Cantidad de niveles del mapper (M = 2^n)

    # RX
    T0 = 2                  # Fase de muestreo rx

    # Punto Fijo
    tipo       = 2          # (0) float: (1): truncado (2) redondeo
    sign       = 'S'        # (S) signado : (U) no signado
    totalWidth = 6          # Cantidad de bits totales
    fractWidth = 4          # Cantidad de bits fraccionales

    # Archivos de salida
    svg   = 1               # Guarda plots en svg : (0) NO: (1) SI
    show  = 0               # Ver plots           : (0) NO: (1) SI
    clear = 0               # Borrar dir imagenes : (0) NO: (1) SI
    ### -----------------------------------------------------------------
```

Definidas las entradas del script, lo ejecutamos abriendo una terminal en el mejor OS del mundo:

```bash
$ cd /ej1                   # Se mueve al directorio del ejercicio
$ ./ej1.py                  # Corre el script con Python3
```

Si se fijó el parámetro ```show = 1``` se desplegaran un conjunto de figuras:

1. Comparativa entre respuestas al impulso $`h(t)`$ del filtro de pulse shaping para los distintos valores de $`\beta`$ y con el formato en punto fijo definido. El plot se presenta como si de una señal continua se tratase. 
2. Respuesta al impulso $`h[n]`$ individual del filtro de pulse shaping para los distintos valores de $`\beta`$ y con el formato de punto fijo definido.
3. Respuesta en frecuencia $`H(\omega)`$ del filtro de pulse shaping.
4. Diagramas de ojo.
5. Diagramas de constelación. El parámetro  ```T0``` de la configuración define la fase de muestreo para la construcción de las constelaciones.
6. Señales de salida del transmisor para cada filtro.
7. Cálculo de $`SNR_{dB}`$ y comparativa entre señales resultantes de un filtrado en punto flotante y equivalente en punto fijo para los distintos $`\beta`$.

Si  se selecciona ```show = 0```  pero  ```svg = 1``` los plots no se muestran, pero se almacenan en formato ```svg``` en un directorio creado por el mismo script dentro de ```img/``` cuyo nombre se construye a partir de los parámetros de entrada asociados al formato de la representación. Por ejemplo, al ejecutar el script con la configuración mostrada anteriormente se genera el directorio ```ej1/img/round_S_6_4```.


### Ejercicio 1 - Filtro coseno realzado

Se desea generar tres filtros con ```rolloff = [0.0,0.5,1.0]``` en punto flotante con ```NT = 16```, ```BR = 1e9``` y ```N = 8```. Para ello, haciendo uso del módulo ```tool``` de Python provisto, se obtienen los coeficientes de tres filtros coseno realzado en formato punto flotante:

```python
    import tool.DSPtools as dsp
    ...
    BR = 1e9                # Symbol Rate [Bd]
    N  = 8                  # Factor de sobremuestreo
    rolloff = [0.0,0.5,1]   # Roll-Off vector (len = 3)
    NT = 16                 # Cantidad de ceros de h(t)
    ...
    # Respuesta al impulso
    (t, h0_float) = dsp.rcosine(rolloff[0], T, N, NT, Norm = False)
    (t, h1_float) = dsp.rcosine(rolloff[1], T, N, NT, Norm = False)
    (t, h2_float) = dsp.rcosine(rolloff[2], T, N, NT, Norm = False)
    ...
```

La función ```dsp.rcosine()``` implementa la siguiente expresión:

```math
        h_{rc}(t) = \left\lbrace
        \begin{matrix}
            \frac{\pi}{4} \cdot sinc\left(\frac{1}{2\beta}\right), & t = \pm \frac{T}{2\beta} \\
            sinc\left( \frac{t}{T}\right) \cdot \frac{cos\left( \frac{\pi\beta t}{T}\right)}{1-\left(\frac{2\beta t}{T}\right)^2}, & \forall \ otro \ t 
        \end{matrix}\right.
```

![respuesta_al_impulso_plot](ej1/img/float/cos_realzado_tiempo.svg)
![respuesta_al_impulso_stem](ej1/img/float/cos_realzado_tiempo_stem.svg)

### Ejercicio 2 - Respuestas en frecuencia

Nuevamente, mediante el módulo ```tool``` se obtiene la $`H(\omega)`$ de los tres filtros y luego se normaliza el módulo para que los filtros sean fácilmente comparables:

```python
    ...
    # Respuesta en frecuencia
    [H0,A0,F0] = dsp.resp_freq(h0, Ts, NFFT)
    [H1,A1,F1] = dsp.resp_freq(h1, Ts, NFFT)
    [H2,A2,F2] = dsp.resp_freq(h2, Ts, NFFT)

    # Normalización
    H0 = H0 / H0[0]
    H1 = H1 / H1[0]
    H2 = H2 / H2[0]
    ...
```
Por último, se computa la respuesta en frecuencia de los tres filtros. El resultado se ilustra a continuación. El ancho de banda de todos los filtros es el mismo, sin embargo, la frecuencia máxima varía en función de $`\beta`$:

```math
        f_{max} = \frac{1+\beta}{2T} = \frac{1}{2T} + \frac{\beta}{2T}
```

![respuesta_en_frec](ej1/img/float/cos_realzado_frec.svg)


### Ejercicio 3 - Pulse shaping

Para evaluar el efecto de $`\beta`$ en la señal de salida del transmisor, se convoluciona la respuesta en al impulso $`h(t)`$ de los filtros con una versión sobremuestreada de una secuencia de símbolos generados aleatoriamente. Si bien los símbolos generados en el transmisor son digitales, estos se sobremuestrean para modelar un comportamiento analógico, luego se filtran con el filtro conformador. El resultado busca modelar al filtro conformador como un filtro analógico. 

En primer lugar, se agrega al modulo ```tool``` un generador de PAM-M y una función de sobremuestreo:

```python
    import numpy as np
    ...
    def pamm(M,L):
        ak = 2 * np.random.randint(M, size = int(L)) - (M-1)
        return ak

    def upsample(ak,N):
        ak_up = np.zeros(N * len(ak), dtype = int)
        ak_up[1:len(ak_up):int(N)] = ak
        return ak_up
```
Hecho esto, se generan dos secuencias aleatorias que van a modelar los canales $`I`$ y $`Q`$ del sistema, luego se sobremuestrean y posteriormente se calcula la convolución de estas secuencias  con las  $`h(t)`$:

```python
    import numpy as np
    ...
    L  = 1e3                # Cantidad de símbolos a simular
    M  = 4                  # Cantidad de niveles del mapper (M = 2^n)
    ...
    # Genera secuencias de símbolos PAM
    ak_I = dsp.pamm(M,L)
    ak_Q = dsp.pamm(M,L)

    # Sobremuestrea para modelar continua
    ak_up_I = dsp.upsample(ak_I,N)
    ak_up_Q = dsp.upsample(ak_Q,N)
    
    # Convolucion con filtro rolloff0
    tx_0I = np.convolve(h0,ak_up_I, mode = 'full')
    tx_0Q = np.convolve(h0,ak_up_Q, mode = 'full')

    # Convolucion con filtro rolloff1
    tx_1I = np.convolve(h1,ak_up_I, mode = 'full')
    tx_1Q = np.convolve(h1,ak_up_Q, mode = 'full')

    # Convolucion con filtro rolloff2
    tx_2I = np.convolve(h2,ak_up_I, mode = 'full')
    tx_2Q = np.convolve(h2,ak_up_Q, mode = 'full')
    ...
```

Las señales de salida de la convolución junto a los símbolos transmitidos se ilustran a continuación:

![respuesta_en_frec](ej1/img/float/tx_banda_base.svg)

Puede verse que independientemente de $`\beta`$ los tres filtros generan tres señales "continuas" que coinciden cada vez que alcanzan un símbolo. Esto quiere decir que no existe *interferencia inter símbolos ISI* en los símbolos enviados.

La ausencia de ISI puede verse con claridad muestreando nuevamente las señales continuas a la salida del transmisor y así  obtener nuevamente los símbolos enviados. Básicamente esto es lo que hace el receptor. Este proceso se modela con diezmado, para ello se agrega la función ```downsample()``` al módulo ```tool```:

```python
    def downsample(ak_up,N,offset):
        ak= np.zeros(len(ak_up/N), dtype = int)
        ak = ak_up[offset:len(ak_up):int(N)]
        return ak
```

Asumiendo que el canal es ideal, la señal de entrada del transmisor es igual a la salida del receptor:

```python
    ...
    N  = 16                 # Factor de sobremuestreo
    T0 = 2                  # Fase de muestreo del rx
    ...
    # Downsample caso filtro 0
    rx_0I_muestreada = dsp.downsample(tx_0I, N, T0)
    rx_0Q_muestreada = dsp.downsample(tx_0Q, N, T0)

    # Downsample caso filtro 1
    rx_1I_muestreada = dsp.downsample(tx_1I, N, T0)
    rx_1Q_muestreada = dsp.downsample(tx_1Q, N, T0)

    # Downsample caso filtro 2
    rx_2I_muestreada = dsp.downsample(tx_2I, N, T0)
    rx_2Q_muestreada = dsp.downsample(tx_2Q, N, T0)  
    ...
```

Recuperados los símbolos, se construye el diagrama de constelación:

![respuesta_en_frec](ej1/img/float/rx_constelaciones.svg)

Una manera más práctica de evaluar la interferencia inter símbolos del transmisor consiste en realizar el *diagramas de ojo* de la señal transmitida o recibida. Este diagrama se construye a partir de la superposición de múltiples intervalos de $`T=1/BR`$ segundos de la señal analógica que contiene de cierta forma a los símbolos. Si los símbolos son aleatorios e independientes, el resultado resume todas las transiciones posibles entre símbolos existentes en la señal transmitida.

![respuesta_en_frec](ej1/img/float/ojos.svg)

La reducción de la apertura vertical del ojo minimiza el nivel de ruido aditivo que puede agregarse a la señal en el canal de comunicación antes de causar errores de discriminación entre símbolos en receptor. Cuanto más ancha sea la apertura vertical, mayor es la inmunidad al ruido. Al no existir ISI en estos casos, la apertura del ojo es total.


### Ejercicio 4 - Comparativa Punto Fijo Vs Punto Flotante

Cuando los coeficientes del filtro conformador se pasan a punto fijo se introduce *error de representación*. Este error puede ser irrelevante o puede afectar tanto al sistema como para evitar que funcione. A fin de evaluar como afecta la representación a la performance del sistema, se ejecutó el script ```ej1.py``` con las siguientes representaciones en punto fijo:

 - $`S(8,7)`$ por truncado.
 - $`S(8,7)`$ por redondeo.
 - $`S(3,2)`$ por truncado.
 - $`S(3,2)`$ por redondeo.
 - $`S(6,4)`$ por truncado.
 - $`S(6,4)`$ por redondeo.

En función del valor del parámetro ```tipo```, el script recorta los coeficientes del filtro en punto flotante. Para ello hace uso del  módulo ```tool```, específicamente de la clase ```_fixedInt```:

```python
    import tool._fixedInt as fxd
    ...
    tipo       = 2          # (0) float: (1): truncado (2) redondeo
    sign       = 'S'        # (S) signado : (U) no signado
    totalWidth = 6          # Cantidad de bits totales
    fractWidth = 4          # Cantidad de bits fraccionales
    ...
    if   tipo == 0:
        # Punto flotante
        h0 = h0_float
        h1 = h1_float
        h2 = h2_float

        img_path = 'img/' + 'float'

    elif tipo == 1:
        # Punto fijo por truncado
        h0_f = fxd.arrayFixedInt(totalWidth, fractWidth,h0_float, signedMode=sign,roundMode='trunc')
        h1_f = fxd.arrayFixedInt(totalWidth, fractWidth,h1_float, signedMode=sign,roundMode='trunc')
        h2_f = fxd.arrayFixedInt(totalWidth, fractWidth,h2_float, signedMode=sign,roundMode='trunc')
        img_path = 'img/' + 'trunc_' + sign + '_' + str(totalWidth) + '_' + str(fractWidth)

    elif tipo == 2:
        # Punto fijo por redondeo
        h0_f = fxd.arrayFixedInt(totalWidth, fractWidth,h0_float, signedMode=sign,roundMode='round')
        h1_f = fxd.arrayFixedInt(totalWidth, fractWidth,h1_float, signedMode=sign,roundMode='round')
        h2_f = fxd.arrayFixedInt(totalWidth, fractWidth,h2_float, signedMode=sign,roundMode='round')
        img_path = 'img/' + 'round_' + sign + '_' + str(totalWidth) + '_' + str(fractWidth)
    else:
        print('Error en configuracion de Punto Fijo')
        exit()

```
Luego de estas líneas, los coeficientes de los filtros conformadores (para los tres valores de $`\beta`$) se almacenan en las listas ```hn_f```. Sin embargo, la función ```fxd.arrayFixedInt()``` retorna un objeto propio del módulo ```_fixedInt``` por lo que, luego de aplicar la representación deseada, es más cómodo volver a convertir los coeficientes recortados a un objeto de tipo ```float``` nativo de Python:

```python
    if tipo != 0:
        h0 = []
        h1 = []
        h2 = []
        for i in range(len(h0_float)):
            h0.append(h0_f[i].fValue)
            h1.append(h1_f[i].fValue)
            h2.append(h2_f[i].fValue)
```

A partir de los coeficientes recortados se obtienen las siguientes curvas:

#### Filtro coseno realzado

##### $`S(8,7)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_8_7/cos_realzado_tiempo_stem.svg)

##### $`S(8,7)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_8_7/cos_realzado_tiempo_stem.svg)

##### $`S(3,2)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_3_2/cos_realzado_tiempo_stem.svg)

##### $`S(3,2)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_3_2/cos_realzado_tiempo_stem.svg)

##### $`S(6,4)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_6_4/cos_realzado_tiempo_stem.svg)

##### $`S(6,4)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_6_4/cos_realzado_tiempo_stem.svg)

#### Respuestas en frecuencia

##### $`S(8,7)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_8_7/cos_realzado_frec.svg)

##### $`S(8,7)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_8_7/cos_realzado_frec.svg)

##### $`S(3,2)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_3_2/cos_realzado_frec.svg)

##### $`S(3,2)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_3_2/cos_realzado_frec.svg)

##### $`S(6,4)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_6_4/cos_realzado_frec.svg)

##### $`S(6,4)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_6_4/cos_realzado_frec.svg)

#### Pulse shaping

##### $`S(8,7)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_8_7/tx_banda_base.svg)

##### $`S(8,7)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_8_7/tx_banda_base.svg)

##### $`S(3,2)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_3_2/tx_banda_base.svg)

##### $`S(3,2)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_3_2/tx_banda_base.svg)

##### $`S(6,4)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_6_4/tx_banda_base.svg)

##### $`S(6,4)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_6_4/tx_banda_base.svg)

#### Constelaciones

##### $`S(8,7)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_8_7/rx_constelaciones.svg)

##### $`S(8,7)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_8_7/rx_constelaciones.svg)

##### $`S(3,2)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_3_2/rx_constelaciones.svg)

##### $`S(3,2)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_3_2/rx_constelaciones.svg)

##### $`S(6,4)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_6_4/rx_constelaciones.svg)

##### $`S(6,4)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_6_4/rx_constelaciones.svg)

### Ejercicio 5 - SNR de representación

Una métrica que se utiliza para evaluar cuanto se degrada el sistema debido al pasaje de punto flotante a punto fijo es la relación señal ruido $`SNR_{dB}`$. Aquí se considera como ruido a la diferencia entre la señal resultante del transmisor construido con un filtro en punto flotante y la señal resultante del transmisor construido con un filtro cuyos coeficientes están representados en punto fijo:

```math
    SNR_{dB} = 10 \cdot log_{10}\left( \frac{P_s}{P_n}\right)
```
Donde:

```math
    P_s = \frac{1}{T} \cdot \int_{T} S_{int}(t)^2  \\
    P_n = \frac{1}{T} \cdot \int_{T} N(t)^2       \\
    N(t)= S_{float}(t)-S_{int}(t)
```
Siendo $`P_s`$ la potencia de señal, $`P_n`$ la potencia de ruido, $` S_{float}(t)`$ la señal a la salida del transmisor en punto flotante y $`S_{int}(t)`$ la señal a la salida del transmisor en punto fijo. 

Debido a que todo el sistema se modela de manera discreta, la integral se convierte en una sumatoria y las señales se representan en tiempo discreto. Antes de calcular las potencias, es necesario calcular la salida *ideal* correspondiente al transmisor en punto flotante para cada valor de $`\beta`$ simulado:

```python
    ...
    # Convolucion con filtro 0 en float
    tx_0I_float = np.convolve(h0_float,ak_up_I, mode = 'full')
    tx_0Q_float = np.convolve(h0_float,ak_up_Q, mode = 'full')

    # Convolucion con filtro 1 en float
    tx_1I_float = np.convolve(h1_float,ak_up_I, mode ='full')
    tx_1Q_float = np.convolve(h1_float,ak_up_Q, mode ='full')

    # Convolucion con filtro 2 en float
    tx_2I_float = np.convolve(h2_float,ak_up_I, mode ='full')
    tx_2Q_float = np.convolve(h2_float,ak_up_Q, mode ='full')
    ...
```

Las potencias de ruido se calculan como:

```python
    ...
    # potencia de ruido
    p_noise_0I = np.sum((tx_0I_float - tx_0I)**2) / len(tx_0I)
    p_noise_0Q = np.sum((tx_0Q_float - tx_0Q)**2) / len(tx_0Q) 
    
    p_noise_1I = np.sum((tx_1I_float - tx_1I)**2) / len(tx_1I)
    p_noise_1Q = np.sum((tx_1Q_float - tx_1Q)**2) / len(tx_1Q) 

    p_noise_2I = np.sum((tx_2I_float - tx_2I)**2) / len(tx_2I)
    p_noise_2Q = np.sum((tx_2Q_float - tx_2Q)**2) / len(tx_2Q) 
    ...
```

Y las potencias de señal:

```python
    ...
    # Potencia de señal
    p_signal_0I = np.sum(tx_0I**2) / len(tx_0I)
    p_signal_0Q = np.sum(tx_0Q**2) / len(tx_0Q)

    p_signal_1I = np.sum(tx_1I**2) / len(tx_1I)
    p_signal_1Q = np.sum(tx_1Q**2) / len(tx_1Q)

    p_signal_2I = np.sum(tx_2I**2) / len(tx_2I)
    p_signal_2Q = np.sum(tx_2Q**2) / len(tx_2Q)
    ...
```
La relación señal ruido de cada canal y para cada $`\beta`$ es:

```python
    ...
    SNR_dB_0I = float(10 * np.log10(p_signal_0I/p_noise_0I))
    SNR_dB_0Q = float(10 * np.log10(p_signal_0Q/p_noise_0Q))
    SNR_dB_1I = float(10 * np.log10(p_signal_1I/p_noise_1I))
    SNR_dB_1Q = float(10 * np.log10(p_signal_1Q/p_noise_1Q))
    SNR_dB_2I = float(10 * np.log10(p_signal_2I/p_noise_2I))
    SNR_dB_2Q = float(10 * np.log10(p_signal_2Q/p_noise_2Q))
    ...
```

Para comparar las distintas representaciones, se grafica la salida del transmisor ideal, la salida del transmisor en punto fijo y el ruido (o diferencia entre ambas salidas). Solo se muestra el canal $`I`$. 

##### $`S(8,7)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_8_7/SNR_I.svg)

##### $`S(8,7)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_8_7/SNR_I.svg)

##### $`S(3,2)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_3_2/SNR_I.svg)

##### $`S(3,2)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_3_2/SNR_I.svg)

##### $`S(6,4)`$ por truncado
![respuesta_al_impulso_plot](ej1/img/trunc_S_6_4/SNR_I.svg)

##### $`S(6,4)`$ por redondeo
![respuesta_al_impulso_stem](ej1/img/round_S_6_4/SNR_I.svg)

### Conclusión

Evaluando las curvas comparativas entre los distintos tipos de representación queda claro que los casos $`S(3,2)`$ son extremos y destruyen el sistema. La representación intermedia $`S(6,4)`$ en versión redondeo es aceptable, sin embargo, su  contrapuesta en truncado no es tan aceptable. Por último, las variantes $`S(8,7)`$ presentan una $`SNR_{dB}`$ alta, no presentan error significante respecto al transmisor ideal.

Independientemente de la representación, queda demostrado por los diagramas de constelación que el truncado genera ISI en mayor grado que el redondeo. Para completar esta idea, observe los diagramas de ojo los casos $`S(6,4)`$.

#### $`S(6,4)`$ por redondeo
![respuesta_en_frec](ej1/img/round_S_6_4/ojos.svg)

#### $`S(6,4)`$ por truncado
![respuesta_en_frec](ej1/img/trunc_S_6_4/ojos.svg)

## Modelado de Punto Fijo en Verilog

Cuando se hace una operación en punto fijo en Python, la clase ```_fixedInt``` se encarga de que todo salga bien. Sin embargo, esto no es tan simple en Verilog. Aquí el diseñador es responsable de que las operaciones aritméticas que se realicen tengan coherencia, ya que para el lenguaje las entradas y salidas son simplemente un conjunto de bits.  

Las operaciones aritméticas que forman las bases para las demás operaciones a nivel de hardware son la suma y la multiplicación. Debido a ello, se estudian los aspectos a tener en cuenta para realizar estas operaciones en módulos Verilog. Los segmentos de código que se muestran las próximas secciones corresponden a los archivos ```ej2/src/fixedPointSum.v``` y ```ej2/tb/tb_fixedPointSum.v``` para el caso de la suma y a los archivos  ```ej2/src/fixedPointMul.v``` y ```ej2/tb/tb_fixedPointMul.v``` para el producto.

### Ejercicio 1 - Suma

Se desea un módulo en Verilog que permita obtener la suma de dos entradas en punto fijo con los formatos $`S(16,14)`$ y $`S(12,11)`$. Mediante un parámetro del módulo, la salida puede variar entre las siguientes representaciones:
    
1. Full-Resolution
2. $`S(11,10)`$ con overflow y truncado
3. $`S(11,10)`$ con saturación y truncado
4. $`S(9,8)`$ con saturación y redondeo

La estructura básica del módulo que implementa la suma se muestra a continuación:


```verilog
module fixedPointSum #(             /* Implementa  C = A + B  */
    /* --- Sumando A --- */
    parameter NB_IN_A  = 16 ,       /* Bits totales de A      */
    parameter NBF_IN_A = 14 ,       /* Bits fraccionales de A */

    /* --- Sumando B --- */
    parameter NB_IN_B  = 12 ,       /* Bits totales de B      */
    parameter NBF_IN_B = 11 ,       /* Bits fraccionales de B */
    
    /* --- Resultado C --- */
    parameter NB_OUT   = 11 ,       /* Bits totales de C      */
    parameter NBF_OUT  = 10 ,       /* Bits fraccionales de C */

    /* Recorte de  C */
    parameter MODE_OUT = 3          /* Modo de recorte de o_c */
)
(
    /* Entradas */
    input  [NB_IN_A - 1:0] i_a,
    input  [NB_IN_B - 1:0] i_b,

    /* Salidas */        
    output [NB_OUT  - 1:0] o_c
);
... 
```

La cantidad de bits de las entradas y la salida se parametrizan para lograr un bloque flexible y reutilizable. Los parámetros ```NB``` y ```NBF``` definen la representación de una entrada o salida de la siguiente manera: ```S(NB,NBF)```. Las entradas y la salida se definen signadas por defecto. El parámetro  ```MODE_OUT``` define la representación de la salida con la siguiente codificación:

```verilog
    ...
    /* Codigo de MODO_OUT */
    localparam OUT_FULL_RES = 0;  /* o_c en full resolution      */
    localparam OUT_TRN_OVF  = 1;  /* o_c truncado con overlow    */
    localparam OUT_TRN_SAT  = 2;  /* o_c truncado con saturacion */
    localparam OUT_RND_SAT  = 3;  /* o_c redondeo con saturacion */
    ...
```
Lógicamente, la representación de salida definida como ```S(NB_OUT,NBF_OUT)``` debe ser coherente con el valor de ```MODO_OUT``` seleccionado.

La suma de dos números binarios de $`n`$ bits genera un resultado de $`n+1`$ bits. Cuando se trata de dos entradas de distinta cantidad de bits, digamos $`n_1`$ y $`n_2`$, la cantidad de bits resultante depende del número con mayor cantidad de bits, por lo tanto, la salida tendrá $`max(n_1,n_2)+1`$ bits. Independientemente del valor fijado a  ```MODO_OUT```, siempre es necesario calcular el valor de ```i_a + i_b``` en full resolution y luego en función del formato de salida despreciar bits o no. Por lo tanto, como es importante la cantidad de bits en full resolution, se definen algunos parámetros útiles:

```verilog
    ...
    /* Cantidad de bits para full resolution */
    localparam NB_FR  = ( NB_IN_A  > NB_IN_B  )? NB_IN_A + 1 : NB_IN_B + 1 ; 
    localparam NBF_FR = ( NBF_IN_A > NBF_IN_B )? NBF_IN_A    : NBF_IN_B    ;
    localparam NBI_FR = NB_FR - NBF_FR ;
    ...
```
En punto fijo, la operacion de suma requiere la alineación del punto de los sumandos. Aquí se presentan dos posibilidades:

1.  ```NBF_IN_A >= NBF_IN_B``` : para alinear es necesario desplazar la entrada ```i_b``` hacia la izquierda ```(NBF_IN_A - NBF_IN_B)``` posiciones rellenando con ceros los bits menos significativos.
2.  ```NBF_IN_A <  NBF_IN_B``` : para alinear hay que realizar la misma operación que en el caso anterior, pero esta vez sobre ```i_a```, esta debe desplazarse ```(NBF_IN_B - NBF_IN_A)``` bits hacia la izquierda.

Estas operaciones no son más que cruzar un par de líneas. En Verilog la suma puede resolverse en un bloque ```always```:

```verilog
    ...
    /* Define entradas como signadas */
    wire signed [NB_IN_A -1     : 0] a ;
    wire signed [NB_IN_B -1     : 0] b ;

    /* Salida para full resolution */
    reg  signed [NB_FR   -1     : 0] c ;
    ...
    /* Renombra lineas */
    assign a = i_a ;
    assign b = i_b ;

    /* Suma en bloque always */
    always @(*) begin
        if(NBF_IN_A >= NBF_IN_B)
            c = a + $signed( {b, { NBF_IN_A - NBF_IN_B {1'b0}}} );
        else
            c = b + $signed( {a, { NBF_IN_B - NBF_IN_A {1'b0}}} );
    end
    
    /* Asignación de puertos */
    assign o_c = c;
```

 O también puede implementarse mediante la sentencia ```assign```:


```verilog
    ...
    /* Define entradas como signadas */
    wire signed [NB_IN_A -1     : 0] a ;
    wire signed [NB_IN_B -1     : 0] b ;

    /* Salida para full resolution */
    wire signed [NB_FR   -1     : 0] c ;
    ...
    
    /* Renombra lineas */
    assign a = i_a ;
    assign b = i_b ;

    /* Suma con assign */
    assign c = (NBF_IN_A >= NBF_IN_B)?
                a + $signed( {b, { NBF_IN_A - NBF_IN_B {1'b0}}}):
                b + $signed( {a, { NBF_IN_B - NBF_IN_A {1'b0}}});
    
    /* Asignación de puertos */
    assign o_c = c;
```
Ambas opciones cruzan cables e implementan un multiplexor. Tener en cuenta que la variable ```c``` debe ser de tipo ```reg``` al usar ```always``` y de tipo ```wire``` al usar ```assign```. En hardware, el resultado es:

![suma_full_res_A](ej2/img/suma_full_res.png)

Como en este caso ```NBF_IN_A > NBF_IN_B```, la entrada ```i_b``` se completa con ceros hasta alinear el punto con ```i_a``` y luego se extiende el signo de ambas entradas hasta completar los ```BN_FR``` bits de la salida en full resolution. Las entradas al sumador quedan como sigue:

![suma_full_res_A](ej2/img/suma_full_res_A.png)
![suma_full_res_A](ej2/img/suma_full_res_B.png)

Para lograr la extensión de ambas entradas es ***muy importante*** que estas estén definidas como signadas. Además, si sobre uno o ambos sumandos se realiza una concatenación, como es el caso mostrado anteriormente, es necesario reafirmar que el resultado de la concatenación es signado con ```$signed()```.

Hasta ahora, la salida ```o_c``` está en full resolution, es decir, el caso ```MODO_OUT = 0```. Resta implementar la descripción para las demás opciones.

#### Truncado y overflow ```MODO_OUT = 1```

Para implementar este modo simplemente hay que descartar bits de la variable  ```c``` cuando esta se asigna al puerto de salida  ```o_c```. Para simplificar el código y hacerlo más entendible se define un nuevo parámetro, la cantidad de bits enteros de la salida ```NBI_OUT```:

```verilog
    ...
    /* Cantidad de bits de la parte entera */
    localparam NBI_OUT = (NB_OUT > NBF_OUT)? NB_OUT-NBF_OUT : 0;
    
    ...
    
    /* Salida recortada con overflow y truncado */
    reg  signed [NB_OUT - 1 : 0] c_trn_ovf;
    ...
    
    always @(*) begin
        ...
        
        /* --- Truncado y overflow --- */
        c_trn_ovf = c [(NB_FR -1)-(NBI_FR-NBI_OUT) -: NB_OUT];
    end

    /* Asignacion de puertos */
    assign o_c = c_trn_ovf;
```
En hardware el resultado es similar al caso ```MODO_OUT = 0``` pero con ```o_c``` de menos lineas:

![suma_full_res_A](ej2/img/suma_trn_ovf.png)


#### Saturación y truncado ```MODO_OUT = 2```

Los dos casos anteriores no requieren hardware adicional al sumador para ser implementados. Sin embargo, la saturación no tiene ese beneficio, es necesario agregar lógica extra para implementarla. 

La lógica para determinar cuando es necesario saturar una variable consiste en evaluar el estado de los bits más significativos que se van a eliminar y uno más. Por ejemplo, si se desea recortar y  saturar el número ```S(9,4) = 11101.0101``` para obtener una representación ```S(6,4)``` se deben analizar los primero cuatro bits, ```1110```. Si estos bits son todos iguales no es necesario saturar y se aplica un recorte de bits como en el caso de truncado y overflow. Caso contrario, si los bits no son todos iguales, se debe saturar al máximo número negativo o positivo según el bit de signo. Para el ejemplo anterior, es necesario saturar y como el bit más significativo es ```1``` se debe saturar al número más negativo resultando ```S(6,4) = 10.0000```. En Verilog:

```verilog
    ...
    /* Salida recortada con saturacion y truncado */
    reg  signed [NB_OUT - 1 : 0] c_trn_sat;
    ...

    always @(*) begin
        ...

        /* --- Truncado y saturacion --- */
        if ( &c[(NB_FR -1)-:(NBI_FR-NBI_OUT)+1] || ~|c[(NB_FR -1)-: (NBI_FR-NBI_OUT)+1])
            c_trn_sat = c[ (NB_FR -1) - (NBI_FR - NBI_OUT) -: NB_OUT ];

        else if ( c[(NB_FR -1)] )
            c_trn_sat = { 1'b1 , { NB_OUT-1 {1'b0} }};

        else
            c_trn_sat = { 1'b0 , { NB_OUT-1 {1'b1} }};
    end

    /* Asignacion de puertos */
    assign o_c = c_trn_sat;
```

El circuito resultante es:

![suma_full_res_A](ej2/img/suma_trn_sat.png)

Cuando los bits a descartar más uno son iguales, la ```and``` o la ```nor``` generan un uno a la salida por lo que se selecciona la entrada uno del multiplexor de salida que, simplemente, conecta solo algunas lineas del resultado de la suma en full resolution, es decir algunos bits de ```c```. Caso contrario, cuando los bits no son iguales, se selecciona la entrada cero del multiplexor de salida que, en función del bit de signo, satura negativamente o positivamente la salida.

#### Saturación y redondeo ```MODO_OUT = 3```

Este es el caso que más recursos de hardware requiere pero, como vimos en Python, es el que da los mejores resultados. La implementación consiste en sumar un uno al primer bit descartado del lado de los bits menos significativos. En Verilog la implementación es muy similar al caso anterior ya que despues de sumar este bit, se aplica saturación:

```verilog
    ...
    /* Cantidad de bits de variable AUX para redondeo */
    localparam NB_RND = (1 + NBI_FR + NBF_OUT + 1) ;
    ...
    
    /* Salida recortada con saturacion y redondeo */
    reg  signed [NB_RND - 1 : 0] c_rnd;
    reg  signed [NB_OUT - 1 : 0] c_rnd_sat;
    ...

    always @(*) begin
        ...

        /* --- Redondeo y saturacion ---  */
        c_rnd = $signed(c[(NB_FR -1) -: (NB_RND -1)]) + $signed(2'b01);
        
        if ( &c_rnd[(NB_RND -1)-:(NBI_FR-NBI_OUT)+2] || ~|c_rnd[(NB_RND -1)-:(NBI_FR-NBI_OUT) +2])
            c_rnd_sat = c_rnd[(NB_RND -1) - (NBI_FR-NBI_OUT) -1 -: NB_OUT];

        else if ( c_rnd[(NB_RND -1)] )
            c_rnd_sat = { 1'b1 , { NB_OUT-1 {1'b0} }};
        
        else
            c_rnd_sat = { 1'b0 , { NB_OUT-1 {1'b1} }};
    end

    /* Asignacion de puertos */
    assign o_c = c_rnd_sat;
```

El resultado en hardware:

![suma_full_res_A](ej2/img/suma_rnd_sat.png)

Este circuito requiere un sumador adicional de ```NB_OUT``` bits. En comparación con los demás circuitos, el redondeo es sumamente costoso.

### Ejercicio 2 - Multiplicación

A diferencia de la suma, la multiplicación de dos números en punto fijo **no** requiere la alineación de los puntos. En este sentido, multiplicar requiere menos análisis por parte del diseñador en comparación con la suma. Sin embargo, la salida en full resolution de un producto es considerablemente más grande en bits, siendo este el aspecto a evaluar en esta operación. 

La estructura para el módulo que implementa el producto en Verilog es prácticamente igual al caso de la suma, la única diferencia es el nombre del bloque: 

```verilog

module fixedPointMul #(             /* Implementa  C = A * B  */
    /* --- Multiplicando A --- */
    parameter NB_IN_A  = 8  ,       /* Bits totales de A      */
    parameter NBF_IN_A = 6  ,       /* Bits fraccionales de A */

    /* --- Multiplicando B --- */
    parameter NB_IN_B  = 12 ,       /* Bits totales de B      */
    parameter NBF_IN_B = 11 ,       /* Bits fraccionales de B */
    
    /* --- Resultado C --- */
    parameter NB_OUT   = 12 ,       /* Bits totales de C      */
    parameter NBF_OUT  = 11 ,       /* Bits fraccionales de C */

    /* Recorte de  C */
    parameter MODE_OUT = 0          /* Modo de recorte de o_c */
)
(
    /* Entradas */
    input  [NB_IN_A - 1:0] i_a,
    input  [NB_IN_B - 1:0] i_b,

    /* Salidas */        
    output [NB_OUT  - 1:0] o_c
);
... 

```
En este caso analizaremos el producto de dos entradas en punto fijo con los formatos $`S(16,14)`$ y $`S(12,11)`$. La salida variará entre las siguientes representaciones:
    
1. Full-Resolution
2. $`S(12,11)`$ con overflow y truncado
3. $`S(12,11)`$ con saturación y truncado
4. $`S(10,9)`$ con saturación y redondeo

La forma de aplicar truncado, overflow , saturación y redondeo son exactamente iguales al caso de la suma por lo que no evaluaremos esas situaciones en esta sección.

Cuando se multiplica una entrada de $`n_1`$ bits con otra de $`n_2`$ bits, el resultado en full resolution tendrá  $`n_1 + n_2`$ bits. Si estas entradas están representadas en punto fijo, la cantidad de bits fraccionarios del resultado es la suma de la cantidad de bits fraccionarios de ambas entradas. En resumen:

```verilog
    ...
    /* Cantidad de bits para full resolution */
    localparam NB_FR  = NB_IN_A  + NB_IN_B  ; 
    localparam NBF_FR = NBF_IN_A + NBF_IN_B ;
    localparam NBI_FR = NB_FR - NBF_FR ;
    ...
```
Debido a que la cantidad de bits de salida del módulo es función de ```MODO_OUT``` se define una variable ```c``` para el resultado de producto en full resolution para luego operar sobre esta y generar la representación de salida deseada. Hecho esto, resta multiplicar las entradas:

```verilog
...
    /* Salida para full resolution */
    wire  signed [NB_FR  -1  : 0] c ;

    /* Producto */
    assign c = $signed(i_a) * $signed(i_b) ;
...
```

Nuevamente, es importante que las entradas estén definidas como variables signadas. Obtenido el producto, se trunca, satura,  o redondea según corresponda y luego se asigna al puerto de salida. Para el caso ```MODO_OUT = 0``` simplemente:

```verilog
    ...
    /* Asignación de puertos */
    assign o_c = c ;
```
Obteniendose:

![suma_full_res_A](ej2/img/prod_full_res.png)


El código para redondeo, truncado, saturación y overflow es igual al utilizado en la suma. A continuación se ilustra el circuito obtenido para cada uno de los casos planteados, todos se obtienen a partir de archivo ```fixedPointMul.v``` ubicado en ```ej2/src/```. 

#### Truncado y overflow ```MODO_OUT = 1```

![suma_full_res_A](ej2/img/prod_trn_ovf.png)

#### Saturación y truncado ```MODO_OUT = 2```

![suma_full_res_A](ej2/img/prod_trn_sat.png)

#### Saturación y redondeo ```MODO_OUT = 3```

![suma_full_res_A](ej2/img/prod_rnd_sat.png)

### Ejercicio 3 - Entorno de verificación

A fin de evaluar de forma rápida e intensiva los circuitos descritos en Verilog, se construye un entorno de verificación que consiste en los siguientes archivos:

1. ```ej2_vector_creator.py``` : generador de estímulos, es un script en Python que, en función de algúnos parámetros de configuración, exporta archivos que después se inyectan al circuito descrito en Verilog.

2. ```ej2_vector_comparator.py``` : el comparador es un Script que levanta dos archivos, uno proveniente de la salida del circuito y otro proveniente del generador de estímulos, los compara e indica cuantas salidas no coinciden. Además, cuando se presenta una diferencia entre los archivos de entrada, el script genera un archivo ```.log``` con los valores de entrada que difieren para luego evaluar el problema. Este script es simple y no tiene parámetros de configuración por lo que no se evalua en profunidad. 

3. ```tb_fixedPointSum.v``` y ```tb_fixedPointMul.v``` : son los archivos de simulación en Verilog que instancian los módulos ```fixedPointSum.v``` y ```fixedPointMul.v```, respectivamente. Estos levantan los archivos de estimulos generados por ```ej2_vector_creator.py```, los inyectan al módulo en cuestion y con las salidas del mismo genera archivos que luego ```ej2_vector_comparator.py``` usa como entradas.

#### Generador de estímulos

Este script genera un conjunto de objetos de la clase ```DeFixedInt```, realiza con ellos las operaciones de suma y producto y exporta el resultado a un archivo. Asumiendo que la clase ```DeFixedInt``` no tiene errores, los resultados obtenidos sirven para evaluar el circuito descrito en Verilog. Antes de ejecutar este script es necesario definir algunos parámetros de configuración:

```python
    ### ------------- Configurables -------------------------
    # Puntos de simulacion
    lenght         = 10000      # Cantidad de puntos de simulacion 

    # Punto fijo
    operation      = 'P'        # (S) suma : (P) producto

    # Sumando A  (entrada)
    A_type         = 'round'    # (trunc) truncado : (round) redondeo
    A_sign         = 'S'        # (U) no signado : (S) signado
    A_totalWidth   = 16         # Cantidad de bits totales
    A_fractWidth   = 14         # Cantidad de bits fraccionales
    A_waveform     = 'rand'     # (rand) aleatoria : (sin) seno : (cos) coseno 
    A_waveform_T   = 255        # Cantidad de muestras por periodo para sin y cos
    
    # Sumando B  (entrada)
    B_type         = 'round'    # (trunc) truncado : (round) redondeo
    B_sign         = 'S'        # (U) no signado : (S) signado
    B_totalWidth   = 12         # Cantidad de bits totales
    B_fractWidth   = 11         # Cantidad de bits fraccionales
    B_waveform     = 'rand'     # (rand) aleatoria : (sin) seno : (cos) coseno 
    B_waveform_T   = 255        # Cantidad de muestras por periodo para sin y cos

    # Resultado C (punto b)
    Cb_type         = 'trunc'   # (trunc) truncado : (round) redondeo
    Cb_sign         = 'S'       # (U) no signado : (S) signado
    Cb_totalWidth   = 11        # Cantidad de bits totales
    Cb_fractWidth   = 10        # Cantidad de bits fraccionales
    Cb_saturateMode = 'wrap'    # (wrap) overflow : (saturate) satura 

    # Resultado C (punto c)
    Cc_type         = 'trunc'   # (trunc) truncado : (round) redondeo
    Cc_sign         = 'S'       # (U) no signado : (S) signado
    Cc_totalWidth   = 11        # Cantidad de bits totales
    Cc_fractWidth   = 10        # Cantidad de bits fraccionales
    Cc_saturateMode = 'saturate'# (wrap) overflow : (saturate) satura 

    # Resultado C (punto d)
    Cd_type         = 'round'   # (trunc) truncado : (round) redondeo
    Cd_sign         = 'S'       # (U) no signado : (S) signado
    Cd_totalWidth   = 10        # Cantidad de bits totales
    Cd_fractWidth   = 9         # Cantidad de bits fraccionales
    Cd_saturateMode = 'saturate'# (wrap) overflow : (saturate) satura 
    ### -----------------------------------------------------

```

A partir de estas definiciones, el script crea (si no existe) el directorio ```sum_vectors/``` o ```mul_vectors/``` según corresponda y dentro de este exporta los siguientes archivos:

1. ```A.in``` : estímulos para ```i_a```.
2. ```B.in``` : estímulos para ```i_b```.
3. ```Ca_py.out``` : salidas patrón para suma o producto en full resolution.
4. ```Cx_py.out``` : salidas patrón para suma o producto con la representación definida por las variables ```Cx_*```.

Estos archivos tienen ```lenght``` lineas y cada una de ellas hay un número binario que corresponde al valor en punto fijo de las entradas o salidas.  

**Dato de color:** hice una función que recibe como parámetro el entero equivalente que puede entregar un objeto ```DeFixedInt```, formatea el resultado (con algunos chiches configurables) y lo devuelve como una un ```str``` que representa al binario: 

```python

    def formatForTestbenchFile(intValue,nBits,flip=False,separator='',end='\n'):
        '''
        Converts a number (int) to its binary representation (str) and
        formats it  for the  testbench. The input  integer  must be in 
        equivalent integer format.
        
        @type   intValue    : integer
        @param  intValue    : integer to convert to binary

        @type   nBits       : integer
        @param  nBits       : number of bits at the output

        @type   flip        : bool 
        @param  flip        : true to read from LSB to MSB in file

        @type   separator   : str
        @param  separator   : separator to add between bits

        @rtype              : str
        @return             : bin formatted to write to testbench file
        '''
        
        ...
```

Con esta función,  escribir en el archivo de testbench es más sintético. Suponiendo un objeto ```DeFixedInt``` llamado ```A```, escribir el valor binario de este en un archivo es tan simple como:

```python
    fileA.write (formatForTestbenchFile( A.intvalue, A.width))
```
Poniendo esto dentro de un bucle y cargando con valores aleatorios a ```A``` resulta:

```python
    0101100100101000
    0110101000001001
    1101111101010101
    1000000001101010
    1110101101111001
    0100110111010000
    ...
```

De esta manera y con este formato se generan todos los archivos comentados anteriormente. Para correr el script abrir una terminal en el mejor OS del planeta, ubicarse en el directorio ```ej2/``` y ejecutar:

```bash
    $ ./ej2_vector_creator.py
```

#### Testbench 

Los archivos de simulación en Verilog se encuentran en el directorio ```ej2/tb/```. Para ambos módulos (```fixedPointSum``` y ```fixedPointMul```) el ```tb``` es prácticamente igual, cambian los archivos de salida, los parámetros locales que definen el tamaño de las salidas en full resolution y lógicamente, instancian módulos diferentes. 

En el ```tb``` se crea un ```clock```, que nada tiene que ver con los módulos a simular, pero que permite  con un bloque ```always``` fijar las entradas de los módulos leyendo los archivos en los flancos ascendentes de ```clock``` y, con otro bloque ```always```, extraer la salida y guardarla en los archivos en los flancos descendentes de ```clock```:

```verilog
    ...
    initial begin
        ...
        
        /* Abre archivo para entradas */
        fileA = $fopen("<path_absoluto>/ej2/sum_vectors/A.in","r");
        fileB = $fopen("<path_absoluto>/ej2/sum_vectors/B.in","r");

        /* Abre o crea archivo para salidas */
        fileCa = $fopen("<path_absoluto>/ej2/sum_vectors/Ca_tb.out","w");
        fileCb = $fopen("<path_absoluto>/ej2/sum_vectors/Ca_tb.out","w");
        ...
    end

    /* Generacion de clock */
    always begin
        #10 clock = ~clock ;
    end

    /* Inyecta entradas */
    always @(posedge clock) begin
        errorA <= $fscanf(fileA,"%b", i_a);
        errorB <= $fscanf(fileB,"%b", i_b);
        ... 
    end

    /* Captura salidas */
    always @(negedge clock) begin
        $fdisplay( fileCa,"%b", o_c_full_res );
        $fdisplay( fileCb,"%b", o_c_trn_ovf  );
        ...
    end

```
Generados los estímulos y capturadas las salidas provocadas por los mismos, resta instanciar los módulos a testear. Como pudo haber deducido del código anterior, se instancia el módulo sumador o multiplicador cuatro veces, uno para cada posible valor de ```MODE_OUT```. De este modo, con una sola corrida de simulación, todas las posibilidades del bloque quedan testeadas para un formato determinado de representación:

```verilog
    /* Instanciacion del DUT en modo FULL RESOLUTION */
    fixedPointSum #(
        .NB_IN_A  ( NB_IN_A      ),
        .NBF_IN_A ( NBF_IN_A     ),
        .NB_IN_B  ( NB_IN_B      ),
        .NBF_IN_B ( NBF_IN_B     ),
        .NB_OUT   ( NB_FR        ),
        .NBF_OUT  ( NBF_FR       ),
        .MODE_OUT ( OUT_FULL_RES )
    )
    DUT_OUT_FULL_RES
    (
        .i_a( i_a          ),
        .i_b( i_b          ),        
        .o_c( o_c_full_res )
    );

    /* Instanciacion del DUT en modo TRUNCADO Y OVERFLOW */
    fixedPointSum #(
        .NB_IN_A  ( NB_IN_A      ),
        .NBF_IN_A ( NBF_IN_A     ),
        .NB_IN_B  ( NB_IN_B      ),
        .NBF_IN_B ( NBF_IN_B     ),
        .NB_OUT   ( NB_OUT       ),
        .NBF_OUT  ( NBF_OUT      ),
        .MODE_OUT ( OUT_TRN_OVF  )
    )
    DUT_OUT_TRN_OVF
    (
        .i_a( i_a          ),
        .i_b( i_b          ),        
        .o_c( o_c_trn_ovf  )
    );
    ...
```

En Vivado se obtiene:

![suma_full_res_A](ej2/img/vivado.png)

Para chequear los archivos generados por el testbench y compararlos con los generados en Python hay que ubicarse en el directorio ```ej2/``` y ejecutar:

```bash
    $ ./ej2_vector_comparator.py
```
El resultado es:

```bash
    Resultados para operacion SUMA:
    (a) Errores : 0
    (b) Errores : 0
    (c) Errores : 0
    (d) Errores : 72
    Archivos exportados al directorio "sum_vectors/"
    Salu2
```

**Dato de color:** el módulo ```DeFixedInt``` no implementa correctamente el redondeo y genera un error de un bit para algunas entradas particulares. En ```sum_vectors/``` el archivo ```Cd_error.log``` generado por ```ej2_vector_comparator.py``` ilustra el error:

```python
    # C en full res      C de Python   C de Testbench
    00011101010001000 	 01110101000 	 01110101001
    00011011110001000 	 01101111000 	 01101111001
    00000110101101000 	 00011010110 	 00011010111
    00010011000101000 	 01001100010 	 01001100011
    00000011000101000 	 00001100010 	 00001100011
    00011110000001000 	 01111000000 	 01111000001
    00010000101101000 	 01000010110 	 01000010111
    00011111000001000 	 01111100000 	 01111100001
    ...
```
A papel y lápiz puede demostrarse que el LSB de la salida generada en Python tiene un error y debería ser uno como indica la salida del testbench. En Python el error se presenta al calcular el redondeo cuando se suma un uno al primer bit menos significativos a eliminar, si este es uno el simulador da un resultado erroneo.

### Ejercicio 4 - Indicador de saturación y módulo ```bitTrimmer()```

Para no tener que evaluar como recortar bits cada vez que es necesario hacerlo se describe un módulo Verilog que reciba algunos parámetros de configuración y recorte los bits en función de estos. Además, puede ser útil que en los casos de saturación exista una *alarma* que da aviso de la situación. La estructura del módulo es la siguiente :

```verilog
    module bitTrimmer #(
        /* --- Representacion de entrada --- */
        parameter NB_FR    = 16 ,
        parameter NBF_FR   = 14 ,
        
        /* --- Representacion de salida  --- */
        parameter NB_OUT   = 9  ,
        parameter NBF_OUT  = 8  ,

        /* ---  Tipo de recorde de bits  --- */
        parameter MODE_OUT = 3
    )
    (
        /* Inputs  */
        input  [NB_FR  - 1:0] i_full     ,

        /* Outputs */        
        output                o_sat_flag ,
        output [NB_OUT - 1:0] o_trim     
    );
```
Con este bloque de hardware el recorte de bits es más simple y consiste simplemente en hacer una instancia y configurar parámetros. Por ejemplo, para el producto resulta:

```verilog
    ...
    /* Salida para full resolution */
    wire  signed [NB_FR  -1  : 0] c ;

    /* Salida para full resolution */
    wire  signed [NB_OUT -1  : 0] c_trimmed ;
    
    /* Salida para indicar saturacion */
    wire                          sat_flag;

    /* Producto */
    assign c = $signed(i_a) * $signed(i_b) ;

    /* Recorte de bits */
    bitTrimmer #(
        .NB_FR      ( NB_FR     ),
        .NBF_FR     ( NBF_FR    ),
        .NB_OUT     ( NB_OUT    ),
        .NBF_OUT    ( NBF_OUT   ),
        .MODE_OUT   ( MODE_OUT  )
    )
    u_bitTrimmer
    (
        .i_full     ( c         ),    
        .o_trim     ( c_trimmed ),
        .o_sat_flag ( sat_flag  )
    );

    /* Asignacion de puertos */
    assign  o_c         = c_trimmed ;
    assign  o_sat_flag  = sat_flag  ;
```

El módulo está descrito en el archivo ```bitTimmer.v``` ubicado en ```ej2/src```. Básicamente, este archivo condensa las descripciones de cada uno de los modos de recorte de bits comentados anteriormente.

