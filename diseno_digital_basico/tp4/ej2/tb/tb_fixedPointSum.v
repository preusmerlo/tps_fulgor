/*
************************************************************************************************
*	        						    Patricio Reus Merlo
*			        			  patricio.reus.merlo@gmail.com
*
* Filename		: tb_fixedPointSum.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 29 jun. 2021
* Description 	: tp4 of digital design - ej2 
************************************************************************************************
*/

/* Timescale */
`timescale 1ns/100ps

module tb_fixedPointSum ();
    
    /* Local parameters */

    /* Sumando a */
    localparam  NB_IN_A  = 16 ;
    localparam  NBF_IN_A = 14 ;
    
    /* Sumando b */
    localparam  NB_IN_B  = 12 ;
    localparam  NBF_IN_B = 11 ;
    
    /* Resultado c */
    localparam  NB_OUT   = 11 ;
    localparam  NBF_OUT  = 10 ;

    /* Salida Full Resolution */
    localparam NB_FR  = ( NB_IN_A  > NB_IN_B  )? NB_IN_A + 1 : NB_IN_B + 1 ; 
    localparam NBF_FR = ( NBF_IN_A > NBF_IN_B )? NBF_IN_A    : NBF_IN_B    ;

    /* Codigo de MODO_OUT */
    localparam  OUT_FULL_RES = 0 ;
    localparam  OUT_TRN_OVF  = 1 ;
    localparam  OUT_TRN_SAT  = 2 ;
    localparam  OUT_RND_SAT  = 3 ;

    /* Entradas y salidas */
    reg  [NB_IN_A - 1:0] i_a  ;
    reg  [NB_IN_B - 1:0] i_b  ;
    
    wire [NB_FR   - 1:0] o_c_full_res ;  /* a) salida DUT en modo FULL RESOLUTION       */
    wire [NB_OUT  - 1:0] o_c_trn_ovf  ;  /* b) salida DUT en modo TRUNCADO Y OVERFLOW   */
    wire [NB_OUT  - 1:0] o_c_trn_sat  ;  /* c) salida DUT en modo TRUNCADO Y SATURACION */
    wire [NB_OUT  - 1:0] o_c_rnd_sat  ;  /* d) salida DUT en modo REDONDEO Y SATURACION */
 
    /* Variables */
    reg     clock ;

    integer fileA  ;
    integer fileB  ;
    integer fileCa ;
    integer fileCb ;
    integer fileCc ;
    integer fileCd ;

    integer  errorA;
    integer  errorB;

    /* Estimulos */
    initial begin
        /* Inicializacion de entradas */
        clock = 1'b0;
        i_a   = {NB_IN_A{1'b0}} ;
        i_b   = {NB_IN_B{1'b0}} ;

        /* ARCHIVOS DE ENTRADA */
        /* Entrada i_a */
        fileA = $fopen("/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp4/ej2/sum_vectors/A.in","r");
        if(fileA == 0) begin
           $display("Error abriendo el archivo de A");
           $stop; 
        end       

        /* Entrada i_b */
        fileB = $fopen("/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp4/ej2/sum_vectors/B.in","r");
        if(fileB == 0) begin
           $display("Error abriendo el archivo de B");
           $stop; 
        end  

        /* ARCHIVOS DE SALIDA */
        
        /* Salida o_c_full_res */
        fileCa = $fopen("/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp4/ej2/sum_vectors/Ca_tb.out","w");
        if(fileCa == 0) begin
           $display("Error abriendo el archivo de Ca");
           $stop; 
        end

        /* Salida o_c_trn_ovf */
        fileCb = $fopen("/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp4/ej2/sum_vectors/Cb_tb.out","w");
        if(fileCb == 0) begin
           $display("Error abriendo el archivo de Cb");
           $stop; 
        end

        /* Salida o_c_trn_sat */
        fileCc = $fopen("/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp4/ej2/sum_vectors/Cc_tb.out","w");
        if(fileCc == 0) begin
           $display("Error abriendo el archivo de Cc");
           $stop; 
        end

        /* Salida o_c_rnd_sat */
        fileCd = $fopen("/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp4/ej2/sum_vectors/Cd_tb.out","w");
        if(fileCd == 0) begin
           $display("Error abriendo el archivo de Cd");
           $stop; 
        end

    end

    always @(posedge clock) begin
        /* Levanta nuevo valor de i_a*/
        errorA <= $fscanf(fileA,"%b", i_a);
        
        /* Levanta nuevo valor de i_b*/
        errorB <= $fscanf(fileB,"%b", i_b);

        /* Cierra archivos por error o EOF */
        if( errorA != 1 || errorB != 1 ) begin
            $fclose(fileA );
            $fclose(fileB );
            $fclose(fileCa);
            $fclose(fileCb);
            $fclose(fileCc);
            $fclose(fileCd);
            $finish;
        end  
    end

    always @(negedge clock) begin
        /* Guarda valor de o_c */
        $fdisplay( fileCa,"%b", o_c_full_res );
        $fdisplay( fileCb,"%b", o_c_trn_ovf  );
        $fdisplay( fileCc,"%b", o_c_trn_sat  );
        $fdisplay( fileCd,"%b", o_c_rnd_sat  );
    end

    /* Generacion de clock */
    always begin
        #10 clock = ~clock ;
    end

    /* Instanciacion del DUT en modo FULL RESOLUTION */
    fixedPointSum #(
        .NB_IN_A  ( NB_IN_A      ),
        .NBF_IN_A ( NBF_IN_A     ),
        .NB_IN_B  ( NB_IN_B      ),
        .NBF_IN_B ( NBF_IN_B     ),
        .NB_OUT   ( NB_FR        ),
        .NBF_OUT  ( NBF_FR       ),
        .MODE_OUT ( OUT_FULL_RES )
    )
    DUT_OUT_FULL_RES
    (
        .i_a( i_a          ),
        .i_b( i_b          ),        
        .o_c( o_c_full_res )
    );

    /* Instanciacion del DUT en modo TRUNCADO Y OVERFLOW */
    fixedPointSum #(
        .NB_IN_A  ( NB_IN_A      ),
        .NBF_IN_A ( NBF_IN_A     ),
        .NB_IN_B  ( NB_IN_B      ),
        .NBF_IN_B ( NBF_IN_B     ),
        .NB_OUT   ( NB_OUT       ),
        .NBF_OUT  ( NBF_OUT      ),
        .MODE_OUT ( OUT_TRN_OVF  )
    )
    DUT_OUT_TRN_OVF
    (
        .i_a( i_a          ),
        .i_b( i_b          ),        
        .o_c( o_c_trn_ovf  )
    );

    /* Instanciacion del DUT en modo TRUNCADO Y SATURACION */
    fixedPointSum #(
        .NB_IN_A  ( NB_IN_A      ),
        .NBF_IN_A ( NBF_IN_A     ),
        .NB_IN_B  ( NB_IN_B      ),
        .NBF_IN_B ( NBF_IN_B     ),
        .NB_OUT   ( NB_OUT       ),
        .NBF_OUT  ( NBF_OUT      ),
        .MODE_OUT ( OUT_TRN_SAT  )
    )
    DUT_OUT_TRN_SAT
    (
        .i_a( i_a          ),
        .i_b( i_b          ),        
        .o_c( o_c_trn_sat  )
    );

    /* Instanciacion del DUT en modo REDONDEO Y SATURACION */
    fixedPointSum #(
        .NB_IN_A  ( NB_IN_A      ),
        .NBF_IN_A ( NBF_IN_A     ),
        .NB_IN_B  ( NB_IN_B      ),
        .NBF_IN_B ( NBF_IN_B     ),
        .NB_OUT   ( NB_OUT       ),
        .NBF_OUT  ( NBF_OUT      ),
        .MODE_OUT ( OUT_RND_SAT  )
    )
    DUT_OUT_RND_SAT
    (
        .i_a( i_a          ),
        .i_b( i_b          ),        
        .o_c( o_c_rnd_sat  )
    );

endmodule /* tb_fixedPointSum */