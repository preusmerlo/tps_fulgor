#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: ej2_suma.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 29 jun. 2021
# Description 	: tp4 of digital design - ej2
######################################################################

######################################################################
#                               MODULES
######################################################################

import os
import numpy as np
import matplotlib.pyplot as plt

import tool._fixedInt as fxd
import tool.DSPtools as dsp

######################################################################
#                               MAIN
######################################################################

def main():
    ''' Main function '''

    ### ------------- Configurables -------------------------
    operation      = 'S'        # (S) suma : (P) producto
    ### -----------------------------------------------------

    # Contadores de errores (guardan los indices)
    Ca_neq_idx = []
    Cb_neq_idx = []
    Cc_neq_idx = []
    Cd_neq_idx = []
    
    # Selecciona directorio de archivos
    if operation == 'S':
        file_path = 'sum_vectors/'
 
    else:
        file_path = 'mul_vectors/'

    # Elimina .log viejos
    os.system('rm ' + file_path +'*.log')   

    # Arreglos con salidas de Python y el Tb
    Ca_out = [0,0]
    Cb_out = [0,0]
    Cc_out = [0,0]
    Cd_out = [0,0]    
    
    # Levanta datos de archivos de Python
    PY = 0
    TB = 1

    Ca_out[PY] = np.genfromtxt( file_path + 'Ca_py.out', usecols = 0, dtype = str )
    Cb_out[PY] = np.genfromtxt( file_path + 'Cb_py.out', usecols = 0, dtype = str )
    Cc_out[PY] = np.genfromtxt( file_path + 'Cc_py.out', usecols = 0, dtype = str )
    Cd_out[PY] = np.genfromtxt( file_path + 'Cd_py.out', usecols = 0, dtype = str )     

    # Levanta datos de archivos del Testbench y alinea (de forma muy rudimentaria)
    Ca_out[TB] = np.genfromtxt( file_path + 'Ca_tb.out', usecols = 0, dtype = str )[1:-1]
    Cb_out[TB] = np.genfromtxt( file_path + 'Cb_tb.out', usecols = 0, dtype = str )[1:-1]
    Cc_out[TB] = np.genfromtxt( file_path + 'Cc_tb.out', usecols = 0, dtype = str )[1:-1]
    Cd_out[TB] = np.genfromtxt( file_path + 'Cd_tb.out', usecols = 0, dtype = str )[1:-1]

    # Compara arreglos
    for i in range(len(Ca_out[0])):
        
        if(Ca_out[PY][i] != Ca_out[TB][i]):
            Ca_neq_idx.append(i)

        if(Cb_out[PY][i] != Cb_out[TB][i]):
            Cb_neq_idx.append(i)

        if(Cc_out[PY][i] != Cc_out[TB][i]):
            Cc_neq_idx.append(i)

        if(Cd_out[PY][i] != Cd_out[TB][i]):
            Cd_neq_idx.append(i)
    
    # Exporta errores a un log para evaluar

    if len(Ca_neq_idx):
        file_err = open(file_path + 'Ca_error.log','w')
        for i in Ca_neq_idx:
            file_err.write(f'{Ca_out[PY][i]}\t{Ca_out[TB][i]}\n')
        file_err.close()

    if len(Cb_neq_idx):
        file_err = open(file_path + 'Cb_error.log','w')
        for i in Cb_neq_idx:
            file_err.write(f'{Ca_out[PY][i]}\t{Cb_out[PY][i]}\t{Cb_out[TB][i]}\n')
        file_err.close()
    
    if len(Cc_neq_idx):
        file_err = open(file_path + 'Cc_error.log','w')
        for i in Cc_neq_idx:
            file_err.write(f'{Ca_out[PY][i]}\t{Cc_out[PY][i]}\t{Cc_out[TB][i]}\n')
        file_err.close()

    if len(Cd_neq_idx):
        file_err = open(file_path + 'Cd_error.log','w')
        for i in Cd_neq_idx:
            file_err.write(f'{Ca_out[PY][i]} \t {Cd_out[PY][i]} \t {Cd_out[TB][i]}\n')
        file_err.close()

    # Titulo con operacion
    if operation == 'S':
        print('Resultados para operacion SUMA:')
    else:
        print('Resultados para operacion PRODUCTO:')

    print(f'(a) Errores : {len(Ca_neq_idx)}')
    print(f'(b) Errores : {len(Cb_neq_idx)}')
    print(f'(c) Errores : {len(Cc_neq_idx)}')
    print(f'(d) Errores : {len(Cd_neq_idx)} <- error del modulo py ')

    if len(Ca_neq_idx) or len(Cb_neq_idx) or len(Cc_neq_idx) or len(Cd_neq_idx):
        print(f'Archivos exportados al directorio "{file_path}"\nSalu2')

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()