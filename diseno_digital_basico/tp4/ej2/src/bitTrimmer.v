/*
************************************************************************************************
*	        						    Patricio Reus Merlo
*			        			  patricio.reus.merlo@gmail.com
*
* Filename		: bitTrimmer.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 4 jul. 2021
* Description 	: tp4 of digital design - ej2 
************************************************************************************************
*/

/*
************************************************************************************************
*									      bitTrimmer()
*
* Description 	: recorta bits de distintas maneras 
* File		    : bitTrimmer.v
* Notes			: ...
*
************************************************************************************************
*/
    
module bitTrimmer #(
        /* --- Representacion de entrada --- */
        parameter NB_FR    = 16 ,
        parameter NBF_FR   = 14 ,
        
        /* --- Representacion de salida  --- */
        parameter NB_OUT   = 9 ,
        parameter NBF_OUT  = 8 ,

        /* ---  Tipo de recorde de bits  --- */
        parameter MODE_OUT = 0

        /* MODE_OUT:
         *  (0) - FULL RESOLUTION (sin recorte)
         *  (1) - OVERFLOW   CON TRUNCADO
         *  (2) - SATURACION CON TRUNCADO
         *  (3) - SATURACION CON REDONDEO
         *  (x) - NB_OUT BITS EN CERO
         */
    )
    (
        /* Inputs  */
        input  [NB_FR  - 1:0] i_full     ,

        /* Outputs */        
        output                o_sat_flag ,
        output [NB_OUT - 1:0] o_trim     
    );
    
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Codigo de MODO_OUT */
    localparam OUT_FULL_RES = 0 ;
    localparam OUT_TRN_OVF  = 1 ;
    localparam OUT_TRN_SAT  = 2 ;
    localparam OUT_RND_SAT  = 3 ;

    /* Cantidad de bits para enteros i_full resolution */
    localparam NBI_FR = NB_FR - NBF_FR ;

    /* Cantidad de bits parte entera de OUT */
    localparam NBI_OUT = ( NB_OUT  > NBF_OUT  )? NB_OUT  - NBF_OUT  : 0;

    /* Cantidad de bits de variable AUX para redondeo */
    localparam NB_RND  = (1 + NBI_FR + NBF_OUT + 1) ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Salida para indicar saturacion */
    reg                           sat_flag;

    /* Salida recortada con overflow y truncado */
    reg  signed [NB_OUT - 1 : 0]  full_trn_ovf;

    /* Salida recortada con saturacion y truncado */
    reg  signed [NB_OUT - 1 : 0]  full_trn_sat;

    /* Salida recortada con saturacion y redondeo */
    reg  signed [NB_RND - 1 : 0]  full_rnd;
    reg  signed [NB_OUT - 1 : 0]  full_rnd_sat;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/
    
    always @(*) begin
        
        case (MODE_OUT)
            /* --- Truncado y overflow --- */
            OUT_TRN_OVF : begin
                    full_trn_ovf = i_full [(NB_FR -1) - (NBI_FR - NBI_OUT) -: NB_OUT];
                    sat_flag = 1'b0;        
                end
            
            /* --- Truncado y saturacion --- */           
            OUT_TRN_SAT  : begin
                    if ( &i_full[(NB_FR -1) -: (NBI_FR - NBI_OUT) + 1] || ~|i_full[(NB_FR -1) -: (NBI_FR - NBI_OUT) + 1] ) begin
                        full_trn_sat = i_full[ (NB_FR -1) - (NBI_FR - NBI_OUT) -: NB_OUT ];
                        sat_flag = 1'b0;
                    end
                    else if ( i_full[(NB_FR -1)] ) begin
                        full_trn_sat = { 1'b1 , { NB_OUT-1 {1'b0} } } ;
                        sat_flag = 1'b1;
                    end
                    else begin
                        full_trn_sat = { 1'b0 , { NB_OUT-1 {1'b1} } } ;
                        sat_flag = 1'b1;
                    end
                end
            
            /* --- Redondeo y saturacion ---  */
            OUT_RND_SAT  : begin
                    /* Redondeo */
                    full_rnd = $signed(i_full[(NB_FR -1) -: (NB_RND -1)]) + $signed(2'b01);

                    /* Saturacion */
                    if ( &full_rnd[(NB_RND -1) -: (NBI_FR - NBI_OUT) + 2] || ~|full_rnd[(NB_RND -1) -: (NBI_FR - NBI_OUT) + 2] ) begin
                        full_rnd_sat = full_rnd[ (NB_RND -1) - (NBI_FR - NBI_OUT) -1 -: NB_OUT ];
                        sat_flag = 1'b0;
                    end
                        
                    else if ( full_rnd[(NB_RND -1)] ) begin
                        full_rnd_sat = { 1'b1 , { NB_OUT-1 {1'b0} } } ;
                        sat_flag = 1'b1;
                    end
                    else begin
                        full_rnd_sat = { 1'b0 , { NB_OUT-1 {1'b1} } } ;
                        sat_flag = 1'b1;
                    end
                end
            
            default      : begin
                    sat_flag     = 1'b0             ;
                    full_trn_ovf = {NB_OUT{1'b0}}   ;
                    full_trn_sat = {NB_OUT{1'b0}}   ;
                    full_rnd     = {NB_RND{1'b0}}   ;
                    full_rnd_sat = {NB_OUT{1'b0}}   ;
                end
        endcase
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/
    /* Salida recortada */
    assign o_trim = ( MODE_OUT == OUT_FULL_RES )? i_full       :
                    ( MODE_OUT == OUT_TRN_OVF  )? full_trn_ovf :
                    ( MODE_OUT == OUT_TRN_SAT  )? full_trn_sat :
                    ( MODE_OUT == OUT_RND_SAT  )? full_rnd_sat : {NB_OUT{1'b0} } ;

    /* Indicador de saturacion */
    assign o_sat_flag = sat_flag;

endmodule