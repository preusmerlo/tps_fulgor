/*
************************************************************************************************
*	        						    Patricio Reus Merlo
*			        			  patricio.reus.merlo@gmail.com
*
* Filename		: fixedPointMul.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 29 jun. 2021
* Description 	: tp4 of digital design - ej2 
************************************************************************************************
*/

/*
************************************************************************************************
*									      fixedPointMul()
*
* Description 	: calcula el productoo de dos numeros representados en punto fijo
* File		    : fixedPointMul.v
* Notes			: ...
*
************************************************************************************************
*/
    
module fixedPointMul #(
        /* --- Multiplicando a --- */
        parameter NB_IN_A  = 16 ,
        parameter NBF_IN_A = 14 ,

        /* --- Multiplicando b --- */
        parameter NB_IN_B  = 12 ,
        parameter NBF_IN_B = 11 ,
        
        /* --- Resultado c --- */
        parameter NB_OUT   = 11 ,
        parameter NBF_OUT  = 10 ,
        parameter MODE_OUT = 3

        /* MODE_OUT:
         *  (0) - FULL RESOLUTION
         *  (1) - OVERFLOW   CON TRUNCADO
         *  (2) - SATURACION CON TRUNCADO
         *  (3) - SATURACION CON REDONDEO 
         */
    )
    (
        /* Inputs */
        input  [NB_IN_A - 1:0] i_a        ,
        input  [NB_IN_B - 1:0] i_b        ,

        /* Outputs */
        output                 o_sat_flag ,            
        output [NB_OUT  - 1:0] o_c
    );
    
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Cantidad de bits para full resolution */
    localparam NB_FR  = NB_IN_A  + NB_IN_B  ; 
    localparam NBF_FR = NBF_IN_A + NBF_IN_B ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Salida para full resolution */
    wire  signed [NB_FR  -1  : 0] c ;

    /* Salida para full resolution */
    wire  signed [NB_OUT -1  : 0] c_trimmed ;
    
    /* Salida para indicar saturacion */
    wire                          sat_flag;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    /* Producto */
    assign c = $signed(i_a) * $signed(i_b) ;

    /* Recorte de bits */
    bitTrimmer #(
        .NB_FR      ( NB_FR     ),
        .NBF_FR     ( NBF_FR    ),
        .NB_OUT     ( NB_OUT    ),
        .NBF_OUT    ( NBF_OUT   ),
        .MODE_OUT   ( MODE_OUT  )
    )
    u_bitTrimmer
    (
        .i_full     ( c         ),    
        .o_trim     ( c_trimmed ),
        .o_sat_flag ( sat_flag  )
    );

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/
    assign  o_c         = c_trimmed ;
    assign  o_sat_flag  = sat_flag  ;

endmodule