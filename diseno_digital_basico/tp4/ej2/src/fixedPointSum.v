/*
************************************************************************************************
*	        						    Patricio Reus Merlo
*			        			  patricio.reus.merlo@gmail.com
*
* Filename		: fixedPointSum.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 29 jun. 2021
* Description 	: tp4 of digital design - ej2 
************************************************************************************************
*/

/*
************************************************************************************************
*									      fixedPointSum()
*
* Description 	: calcula la suma de dos numeros representados en punto fijo
* File		    : fixedPointSum.v
* Notes			: ...
*
************************************************************************************************
*/
    
module fixedPointSum #(
        /* --- Sumando a --- */
        parameter NB_IN_A  = 16 ,
        parameter NBF_IN_A = 14 ,

        /* --- Sumando b --- */
        parameter NB_IN_B  = 12 ,
        parameter NBF_IN_B = 11 ,
        
        /* --- Resultado c --- */
        parameter NB_OUT   = 11 ,
        parameter NBF_OUT  = 10 ,
        parameter MODE_OUT = 3

        /* MODE_OUT:
         *  (0) - FULL RESOLUTION
         *  (1) - OVERFLOW   CON TRUNCADO
         *  (2) - SATURACION CON TRUNCADO
         *  (3) - SATURACION CON REDONDEO 
         */
    )
    (
        /* Inputs */
        input  [NB_IN_A - 1:0] i_a        ,
        input  [NB_IN_B - 1:0] i_b        ,

        /* Outputs */
        output                 o_sat_flag ,            
        output [NB_OUT  - 1:0] o_c
    );
    
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Cantidad de bits para full resolution */
    localparam NB_FR  = ( NB_IN_A  > NB_IN_B  )? NB_IN_A + 1 : NB_IN_B + 1 ; 
    localparam NBF_FR = ( NBF_IN_A > NBF_IN_B )? NBF_IN_A    : NBF_IN_B    ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Defino entradas como signadas */
    wire signed [NB_IN_A -1: 0] a ;
    wire signed [NB_IN_B -1: 0] b ;

    /* Salida para full resolution */
    reg  signed [NB_FR  -1: 0] c  ;

    /* Salida para full resolution */
    wire signed [NB_OUT -1: 0] c_trimmed ;
    
    /* Salida para indicar saturacion */
    wire                       sat_flag;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    assign a = i_a ;
    assign b = i_b ;

    always @(*) begin
        /* Sea w = S(x,y):
         *      - NBI < NBF requiere x < 2 * NBF
         *      - NBI > NBF requiere x > 2 * NBF
         */

        /* Ej sin bits enteros y ambos NBI < NBF: A = S(5,9)  y B = S(6,8)  [  NO ANDA error > 50% ] */
        /* Ej con bits enteros y ambos NBI < NBF: A = S(10,7) y B = S(8,6)  [   ANDA!  error = 0 % ] */
        /* Ej con bits enteros y ambos NBI > NBF: A = S(10,4) y B = S(8,3)  [   ANDA!  error = 0 % ] */
        /* Ej A con  NBI > NBF y B con NBI < NBF: A = S(10,4) y B = S(7,5)  [ A MEDIAS error ~ 5 % ] */
        if(NBF_IN_A >= NBF_IN_B)
            c = a + $signed( {b , { NBF_IN_A - NBF_IN_B {1'b0} } } );
        
        /* Ej sin bits enteros y ambos NBI < NBF: A = S(6,8)  y B = S(5,9)  [  NO ANDA error > 50% ] */
        /* Ej con bits enteros y ambos NBI < NBF: A = S(8,6)  y B = S(10,7) [   ANDA!  error = 0 % ] */
        /* Ej con bits enteros y ambos NBI > NBF: A = S(8,3)  y B = S(10,4) [   ANDA!  error = 0 % ] */
        /* Ej A con  NBI < NBF y B con NBI > NBF: A = S(7,5)  y B = S(10,4) [ A MEDIAS error ~ 5 % ] */
        else
            c = b + $signed( {a , { NBF_IN_B - NBF_IN_A {1'b0} } } );
    end

    /* Recorte de bits */
    bitTrimmer #(
        .NB_FR      ( NB_FR     ),
        .NBF_FR     ( NBF_FR    ),
        .NB_OUT     ( NB_OUT    ),
        .NBF_OUT    ( NBF_OUT   ),
        .MODE_OUT   ( MODE_OUT  )
    )
    u_bitTrimmer
    (
        .i_full     ( c         ),    
        .o_trim     ( c_trimmed ),
        .o_sat_flag ( sat_flag  )
    );

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign  o_c         = c_trimmed ;
    assign  o_sat_flag  = sat_flag  ;

endmodule