#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: ej1.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 28 jun. 2021
# Description 	: tp4 of digital design 
######################################################################

######################################################################
#                               MODULES
######################################################################

import os
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import size

import tool._fixedInt as fxd
import tool.DSPtools as dsp
    
######################################################################
#                               MAIN
######################################################################

def main():
    ''' Main function '''
    
    ### ------------------------ Configurables -----------------------
    # Generales
    BR = 1e9             # Symbol Rate               [Bd]
    N  = 16               # Factor de sobremuestreo
    L  = 1e3             # Cantidad de simbolos a simular (numero par)
    
    # FFT
    NFFT = 256           # Cantidad de puntos de fft
    
    # Pulse shaping
    rolloff = [0.0,0.5,1]# Roll-Off vector (len = 3)
    NT = 16              # Cantidad de cruces por cero (simbolos) de h(t)
    
    # Mapper
    M  = 2               # Cantidad de niveles del mapper (M = 2^n)

    # RX
    T0 = 1               # Fase de muestreo rx

    # Punto Fijo
    tipo       = 1       # (0) float: (1): truncado (2) redondeo
    sign       = 'S'     # (S) signado : (U) no signado
    totalWidth = 3       # Cantidad de bits totales
    fractWidth = 2       # Cantidad de bits fraccionales

    # Archivos de salida
    svg   = 1             # Guarda plots en svg: (0) NO; (1) SI
    show  = 0             # Ver plots          : (0) NO; (1) SI
    clear = 0             # Limpiar (rm -rf img/*) : (0) NO; (1) SI
                          # Usar clear = 1 puede romper el README.md
    ### -----------------------------------------------------------------

    ### --------------- Constantes --------------------------
    
    T  = 1 / BR          # Tiempo entre simbolos     [seg]
    fs = N * BR          # Frecuencia de muestreo    [Hz]
    Ts = 1/fs            # Perdiodo de muestreo      [seg]
    
    # Plots
    H = 6                 # Alto  minimo de los plots
    W = 6                 # Ancho minimo de los plots
    ### -----------------------------------------------------

    # Limpia directorio de plots exportados
    if clear:
        os.system('rm -rf img/*')    

    ### --------------- Ejercicio 1 -------------------------
    
    # Genera tres filtros coseno realzado
    (t, h0_float) = dsp.rcosine(rolloff[0], T, N, NT, Norm = False)
    (t, h1_float) = dsp.rcosine(rolloff[1], T, N, NT, Norm = False)
    (t, h2_float) = dsp.rcosine(rolloff[2], T, N, NT, Norm = False)

    filter_delay  = int(len(h0_float)/2)
    ### --------------- Ejercicio 4 -------------------------
    if   tipo == 0:
        # Punto flotante
        h0 = h0_float
        h1 = h1_float
        h2 = h2_float

        img_path = 'img/' + 'float'

    elif tipo == 1:
        # Punto fijo por truncado
        h0_f = fxd.arrayFixedInt(totalWidth, fractWidth,h0_float, signedMode=sign,roundMode='trunc')
        h1_f = fxd.arrayFixedInt(totalWidth, fractWidth,h1_float, signedMode=sign,roundMode='trunc')
        h2_f = fxd.arrayFixedInt(totalWidth, fractWidth,h2_float, signedMode=sign,roundMode='trunc')
        img_path = 'img/' + 'trunc_' + sign + '_' + str(totalWidth) + '_' + str(fractWidth)

    elif tipo == 2:
        # Punto fijo por redondeo
        h0_f = fxd.arrayFixedInt(totalWidth, fractWidth,h0_float, signedMode=sign,roundMode='round')
        h1_f = fxd.arrayFixedInt(totalWidth, fractWidth,h1_float, signedMode=sign,roundMode='round')
        h2_f = fxd.arrayFixedInt(totalWidth, fractWidth,h2_float, signedMode=sign,roundMode='round')
        img_path = 'img/' + 'round_' + sign + '_' + str(totalWidth) + '_' + str(fractWidth)
    else:
        print('Error en configuracion de Punto Fijo')
        exit()

    # Chequea si existe directorio de imagenes y si no existe lo crea
    os.system(f'[ ! -d "{img_path}" ] && mkdir -p "{img_path}"')

    # Define coeficientes fixeados
    if tipo != 0:
        h0 = []
        h1 = []
        h2 = []
        for i in range(len(h0_float)):
            # print(h0[i],h0_f[i].fValue)
            h0.append(h0_f[i].fValue)
            h1.append(h1_f[i].fValue)
            h2.append(h2_f[i].fValue)
    
    ### --------------- Ejercicio 2 -------------------------
    # Plot de h(t) para distintos roll-off - con plot
    f = plt.figure(figsize = (2*W,H))
    plt.plot(t*1e9,h0,'r-o',linewidth = 1, label = r'$\beta = {}$'.format(rolloff[0]))
    plt.plot(t*1e9,h1,'b-o',linewidth = 1, label = r'$\beta = {}$'.format(rolloff[1]))
    plt.plot(t*1e9,h2,'k-o',linewidth = 1, label = r'$\beta = {}$'.format(rolloff[2]))
    plt.grid(linestyle='--', linewidth=0.5)
    plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
    plt.xlim(t[0]*1e9,t[-1]*1e9)
    plt.ylim(-0.3,1.1)
    plt.title (r'Coseno realzado para distintos valores de $\beta$')
    plt.xlabel('Tiempo [ns]')
    plt.ylabel('Amplitud')

    if svg:
        f.savefig(f"{img_path}/cos_realzado_tiempo.svg", bbox_inches='tight')
    

    # Plot de h(t) para distintos roll-off - con stem
    x = np.linspace(0,len(h0),len(h0))

    f = plt.figure(figsize = (4*W,1.33*H))    
    
    for i in range(3):
        plt.subplot(1,3,i+1)
        if i == 0:
            plt.stem(x,h0,linefmt='r:',markerfmt='r.', basefmt='k-',label = r'$\beta = {}$'.format(rolloff[0]))
        
        if i == 1:
            plt.stem(x,h1,linefmt='b:',markerfmt='b.', basefmt='k-',label = r'$\beta = {}$'.format(rolloff[1]))
    
        if i == 2:
            plt.stem(x,h2,linefmt='k:',markerfmt='k.', basefmt='k-',label = r'$\beta = {}$'.format(rolloff[2]))
        plt.xlim(0+20,len(h0)-20)
        plt.ylim(-0.3,1.1)
        plt.grid(linestyle='--', linewidth=0.5)
        plt.xlabel('Muestras',fontsize=15)
        plt.ylabel('Amplitud',fontsize=15)
        plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
    
    if svg:
        f.savefig(f"{img_path}/cos_realzado_tiempo_stem.svg", bbox_inches='tight')
    
    # Calcula respuesta en frecuencia de los filtros
    [H0,A0,F0] = dsp.resp_freq(h0, Ts, NFFT)
    [H1,A1,F1] = dsp.resp_freq(h1, Ts, NFFT)
    [H2,A2,F2] = dsp.resp_freq(h2, Ts, NFFT)
    
    # Normaliza a 0dB en DC
    H0 = H0 / H0[0]
    H1 = H1 / H1[0]
    H2 = H2 / H2[0]

    # Plot de H(w)
    if tipo == 0:
        f = plt.figure(figsize=[3*W,2*H])
    else:
        f = plt.figure(figsize=[3*W,H])

    for i in range(3):
        if tipo == 0:
            plt.subplot(2,3,i+1)
        else:
            plt.subplot(1,3,i+1)

        plt.axhline(y = 20*np.log10(0.5),color = 'k',linewidth = 1, linestyle='-.', label = '- 6dB')
        plt.axvline(x = (1./T)/2.       ,color = 'k',linewidth = 1, linestyle='--', label = 'BR/2')
        
        if i == 0:
            plt.semilogx(F0, 20*np.log10(H0),'r-', linewidth = 1.5, label = r'$\beta={}$'.format(rolloff[0]))
        
        if i == 1:
            plt.semilogx(F1, 20*np.log10(H1),'b-', linewidth = 1.5, label = r'$\beta={}$'.format(rolloff[1]))
    
        if i == 2:
            plt.semilogx(F2, 20*np.log10(H2),'k-', linewidth = 1.5, label = r'$\beta={}$'.format(rolloff[2]))

        plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
        plt.grid(linestyle='--', linewidth=0.5)
        plt.xlim(F0[1],F0[-1])
        plt.ylim(-20,10)
        plt.xlabel('Frequencia [Hz]',fontsize=15)
        plt.ylabel('Magnitud [dB]'  ,fontsize=15)
    
    if tipo == 0:
        # Plot de arg(H(w))
        for i in range(3,6):
            plt.subplot(2,3,i+1)
            plt.axvline(x = (1./T)/2.       ,color = 'k',linewidth = 1, linestyle='--', label = 'BR/2')
            
            if i == 3:
                plt.semilogx(F0, A0,'r-', linewidth = 1.5, label = r'$\beta={}$'.format(rolloff[0]))
            
            if i == 4:
                plt.semilogx(F1, A1,'b-', linewidth = 1.5, label = r'$\beta={}$'.format(rolloff[1]))
        
            if i == 5:
                plt.semilogx(F2, A2,'k-', linewidth = 1.5, label = r'$\beta={}$'.format(rolloff[2]))

            plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
            plt.grid(linestyle='--', linewidth=0.5)
            plt.xlim(F0[1],F0[-1])
            plt.ylim(-np.pi,np.pi)
            plt.xlabel('Frequencia [Hz]',fontsize=15)
            plt.ylabel('Fase [deg]'  ,fontsize=15)

    if svg:
        f.savefig(f"{img_path}/cos_realzado_frec.svg", bbox_inches='tight')

    ### --------------- Ejercicio 3 ------------------------- 
    # Genera secuencias de simbolos PAM
    ak_I = dsp.pamm(M,L)
    ak_Q = dsp.pamm(M,L)

    # Sobremuestreo para modelar continua
    ak_up_I = dsp.upsample(ak_I,N)
    ak_up_Q = dsp.upsample(ak_Q,N)
    
    # Convolucion con filtro 0
    tx_0I = np.convolve(h0,ak_up_I, mode = 'full')
    tx_0Q = np.convolve(h0,ak_up_Q, mode = 'full')

    # Convolucion con filtro 1
    tx_1I = np.convolve(h1,ak_up_I, mode ='full')
    tx_1Q = np.convolve(h1,ak_up_Q, mode ='full')

    # Convolucion con filtro 2
    tx_2I = np.convolve(h2,ak_up_I, mode ='full')
    tx_2Q = np.convolve(h2,ak_up_Q, mode ='full')

    # Plot parte real
    x_start = 1000
    x_end   = 1300

    f = plt.figure(figsize = [3*W,2*H])
    plt.subplot(2,1,1)
    plt.plot(tx_0I,'r-',linewidth = 1.5,label = r'$\beta = {}$'.format(rolloff[0]))
    plt.plot(tx_1I,'b-',linewidth = 1.5,label = r'$\beta = {}$'.format(rolloff[1]))
    plt.plot(tx_2I,'k-',linewidth = 1.5,label = r'$\beta = {}$'.format(rolloff[2]))

    for idx in range(x_start - filter_delay ,x_end - filter_delay):
        if(ak_up_I[idx] != 0):
            plt.scatter(idx + filter_delay,ak_up_I[idx], color = 'tab:orange',s=100)

    plt.xlim(x_start,x_end)
    plt.ylim(-M,M)
    plt.grid(linestyle='--', linewidth=0.5)
    plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
    plt.title (r'Simbolos $a_k^I$ transmitidos',fontsize=15)
    plt.xlabel('Muestras',fontsize=15)
    plt.ylabel('Magnitud',fontsize=15)

    # Plot parte imag
    plt.subplot(2,1,2)
    plt.plot(tx_0Q,'r-',linewidth = 1.5,label = r'$\beta = {}$'.format(rolloff[0]))
    plt.plot(tx_1Q,'b-',linewidth = 1.5,label = r'$\beta = {}$'.format(rolloff[1]))
    plt.plot(tx_2Q,'k-',linewidth = 1.5,label = r'$\beta = {}$'.format(rolloff[2]))

    for idx in range(x_start - filter_delay ,x_end - filter_delay):
            if(ak_up_Q[idx] != 0):
                plt.scatter(idx + filter_delay,ak_up_Q[idx], color = 'tab:orange',s=100)

    plt.xlim(x_start,x_end)
    plt.ylim(-M,M)
    plt.grid(linestyle='--', linewidth=0.5)
    plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
    plt.title (r'Simbolos $a_k^Q$ transmitidos',fontsize=15)
    plt.xlabel('Muestras',fontsize=15)
    plt.ylabel('Magnitud',fontsize=15)

    if svg:
        f.savefig(f"{img_path}/tx_banda_base.svg", bbox_inches='tight')

    # Construccion de las constelaciones
    # Downsample caso filtro 0
    rx_0I_muestreada = dsp.downsample(tx_0I, N, T0)
    rx_0Q_muestreada = dsp.downsample(tx_0Q, N, T0)

    # Downsample caso filtro 1
    rx_1I_muestreada = dsp.downsample(tx_1I, N, T0)
    rx_1Q_muestreada = dsp.downsample(tx_1Q, N, T0)

    # Downsample caso filtro 2
    rx_2I_muestreada = dsp.downsample(tx_2I, N, T0)
    rx_2Q_muestreada = dsp.downsample(tx_2Q, N, T0)  
 
    # Plot de constelaciones

    f = plt.figure(figsize = [3*W,H])
    
    for i in range(3):
        plt.subplot(1,3,i+1)
        
        if i == 0:
            plt.scatter(rx_0I_muestreada[filter_delay:-filter_delay], rx_0Q_muestreada[filter_delay:-filter_delay], color='red'  )
        if i == 1:
            plt.scatter(rx_1I_muestreada[filter_delay:-filter_delay], rx_1Q_muestreada[filter_delay:-filter_delay], color='blue' )
        if i == 2:
            plt.scatter(rx_2I_muestreada[filter_delay:-filter_delay], rx_2Q_muestreada[filter_delay:-filter_delay], color='black')
        
        plt.grid(linestyle='--', linewidth=0.2)
        plt.xlim(-M,M)
        plt.ylim(-M,M)
        plt.title (r'$\beta = {}$'.format(rolloff[i]),fontsize=15)
        plt.xlabel('I',fontsize=15)
        plt.ylabel('Q',rotation=0,fontsize=15)

    if svg:
        f.savefig(f"{img_path}/rx_constelaciones.svg", bbox_inches='tight')


    ### ------------- Plus ejercicio 3 ---------------------- 
    # Diagramas de OJO
    period = 6

    f = plt.figure(figsize = [3*W,2*H])

    plt.subplot(2,3,1)
    plt.ylabel('I',rotation=0,fontsize=15)
    plt.title (r'$\beta = {}$'.format(rolloff[0]),fontsize=15)
    dsp.eyediagram(tx_0I[filter_delay:-filter_delay],N,period,NT, color='r')
    plt.grid(linestyle='--', linewidth=0.5)

    plt.subplot(2,3,2)
    plt.title (r'$\beta = {}$'.format(rolloff[1]),fontsize=15)
    dsp.eyediagram(tx_1I[filter_delay:-filter_delay],N,period,NT, color='b')
    plt.grid(linestyle='--', linewidth=0.5)

    plt.subplot(2,3,3)
    plt.title (r'$\beta = {}$'.format(rolloff[2]),fontsize=15)
    dsp.eyediagram(tx_2I[filter_delay:-filter_delay],N,period,NT, color='k')
    plt.grid(linestyle='--', linewidth=0.5)

    plt.subplot(2,3,4)
    plt.ylabel('Q',rotation=0,fontsize=15)
    dsp.eyediagram(tx_0Q[filter_delay:-filter_delay],N,period,NT, color='r')
    plt.grid(linestyle='--', linewidth=0.5)

    plt.subplot(2,3,5)
    dsp.eyediagram(tx_1Q[filter_delay:-filter_delay],N,period,NT, color='b')
    plt.grid(linestyle='--', linewidth=0.5)

    plt.subplot(2,3,6)
    dsp.eyediagram(tx_2Q[filter_delay:-filter_delay],N,period,NT, color='k')
    plt.grid(linestyle='--', linewidth=0.5)
    

    if svg:
        f.savefig(f"{img_path}/ojos.svg", bbox_inches='tight')

    
    ### --------------- Ejercicio 6 ------------------------- 
    # Convolucion con filtro 0 en float
    tx_0I_float = np.convolve(h0_float,ak_up_I, mode = 'full')
    tx_0Q_float = np.convolve(h0_float,ak_up_Q, mode = 'full')

    # Convolucion con filtro 1 en float
    tx_1I_float = np.convolve(h1_float,ak_up_I, mode ='full')
    tx_1Q_float = np.convolve(h1_float,ak_up_Q, mode ='full')

    # Convolucion con filtro 2 en float
    tx_2I_float = np.convolve(h2_float,ak_up_I, mode ='full')
    tx_2Q_float = np.convolve(h2_float,ak_up_Q, mode ='full')

    # Potencia de señal
    p_signal_0I = np.sum(tx_0I**2) / len(tx_0I)
    p_signal_0Q = np.sum(tx_0Q**2) / len(tx_0Q)

    p_signal_1I = np.sum(tx_1I**2) / len(tx_1I)
    p_signal_1Q = np.sum(tx_1Q**2) / len(tx_1Q)

    p_signal_2I = np.sum(tx_2I**2) / len(tx_2I)
    p_signal_2Q = np.sum(tx_2Q**2) / len(tx_2Q)

    # potencia de ruido
    p_noise_0I = np.sum((tx_0I_float - tx_0I)**2) / len(tx_0I)
    p_noise_0Q = np.sum((tx_0Q_float - tx_0Q)**2) / len(tx_0Q) 
    
    p_noise_1I = np.sum((tx_1I_float - tx_1I)**2) / len(tx_1I)
    p_noise_1Q = np.sum((tx_1Q_float - tx_1Q)**2) / len(tx_1Q) 

    p_noise_2I = np.sum((tx_2I_float - tx_2I)**2) / len(tx_2I)
    p_noise_2Q = np.sum((tx_2Q_float - tx_2Q)**2) / len(tx_2Q) 

    if tipo != 0:
        # Calculo de SNR
        SNR_dB_0I = float(10 * np.log10(p_signal_0I/p_noise_0I))
        SNR_dB_0Q = float(10 * np.log10(p_signal_0Q/p_noise_0Q))
        SNR_dB_1I = float(10 * np.log10(p_signal_1I/p_noise_1I))
        SNR_dB_1Q = float(10 * np.log10(p_signal_1Q/p_noise_1Q))
        SNR_dB_2I = float(10 * np.log10(p_signal_2I/p_noise_2I))
        SNR_dB_2Q = float(10 * np.log10(p_signal_2Q/p_noise_2Q))

    # Plot de curvas ideales y curvas cuantizadas canal I
    bbox = dict(boxstyle ="round",ec = "0",fc ="1")

    f = plt.figure(figsize = [3*W,2*H])
    for i in range(3):
        plt.subplot(3,1,i+1)
        if i == 0:
            plt.plot(tx_0I      ,'b-' ,linewidth = 1.5,label = 'Punto Fijo')
            plt.plot(tx_0I_float,'r--',linewidth = 1.5,label = 'Punto Flotante')
            plt.plot(tx_0I_float - tx_0I , 'k--',linewidth = 0.8,label = 'Error')
            if tipo != 0:
                plt.title (r'CANAL I - $\beta$ = {}'.format(rolloff[i]),fontsize=15)
                plt.annotate('SNR = {:.2f} dB'.format(SNR_dB_0I),(x_start + 5,-M+0.5),bbox = bbox, fontsize=15)
        
        if i == 1:
            plt.plot(tx_1I      ,'b-' ,linewidth = 1.5,label = 'Punto Fijo')
            plt.plot(tx_1I_float,'r--',linewidth = 1.5,label = 'Punto Flotante')
            plt.plot(tx_1I_float - tx_1I , 'k--',linewidth = 0.8,label = 'Error')
            plt.ylabel('Amplitud',fontsize=15)
            if tipo != 0:
                plt.title (r'CANAL I - $\beta$ = {}'.format(rolloff[i]),fontsize=15)
                plt.annotate('SNR = {:.2f} dB'.format(SNR_dB_1I),(x_start + 5,-M+0.5),bbox = bbox, fontsize=15)
        
        if i == 2:
            plt.plot(tx_2I      ,'b-' ,linewidth = 1.5,label = 'Punto Fijo')
            plt.plot(tx_2I_float,'r--',linewidth = 1.5,label = 'Punto Flotante')
            plt.plot(tx_2I_float - tx_2I , 'k--',linewidth = 0.8,label = 'Error')
            if tipo != 0:
                plt.title (r'CANAL I - $\beta$ = {}'.format(rolloff[i]),fontsize=15)
                plt.annotate('SNR = {:.2f} dB'.format(SNR_dB_2I),(x_start + 5,-M+0.5),bbox = bbox, fontsize=15)

        plt.xlim(x_start,x_end)
        plt.ylim(-M,M)
        plt.grid(linestyle='--', linewidth=0.5)
        plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
        if tipo == 0:
            plt.title (r'CANAL I - $\beta$ = {}'.format(rolloff[i]),fontsize=15)
    
    plt.xlabel('Muestras',fontsize=15)
    
    if svg:
        f.savefig(f"{img_path}/SNR_I.svg", bbox_inches='tight')

    # Plot de curvas ideales y curvas cuantizadas canal Q
    f = plt.figure(figsize = [3*W,2*H])
    for i in range(3):
        plt.subplot(3,1,i+1)
        if i == 0:
            plt.plot(tx_0Q      ,'b-' ,linewidth = 1.5,label = 'Punto Fijo')
            plt.plot(tx_0Q_float,'r--',linewidth = 1.5,label = 'Punto Flotante')
            plt.plot(tx_0Q_float - tx_0Q , 'k--',linewidth = 0.8,label = 'Error')
            if tipo != 0:
                plt.title (r'CANAL Q - $\beta$ = {}'.format(rolloff[i]),fontsize=15)
                plt.annotate('SNR = {:.2f} dB'.format(SNR_dB_0Q),(x_start + 5,-M+0.5),bbox = bbox, fontsize=15)
        
        if i == 1:
            plt.plot(tx_1Q      ,'b-' ,linewidth = 1.5,label = 'Punto Fijo')
            plt.plot(tx_1Q_float,'r--',linewidth = 1.5,label = 'Punto Flotante')
            plt.plot(tx_1Q_float - tx_1Q , 'k--',linewidth = 0.8,label = 'Error')
            plt.ylabel('Amplitud',fontsize=15)
            if tipo != 0:
                plt.title (r'CANAL Q - $\beta$ = {}'.format(rolloff[i]),fontsize=15)
                plt.annotate('SNR = {:.2f} dB'.format(SNR_dB_1Q),(x_start + 5,-M+0.5),bbox = bbox, fontsize=15)
        
        if i == 2:
            plt.plot(tx_2Q      ,'b-' ,linewidth = 1.5,label = 'Punto Fijo')
            plt.plot(tx_2Q_float,'r--',linewidth = 1.5,label = 'Punto Flotante')
            plt.plot(tx_2Q_float - tx_2Q , 'k--',linewidth = 0.8,label = 'Error')
            if tipo != 0:
                plt.title (r'CANAL Q - $\beta$ = {}'.format(rolloff[i]),fontsize=15)
                plt.annotate('SNR = {:.2f} dB'.format(SNR_dB_2Q),(x_start + 5,-M+0.5),bbox = bbox, fontsize=15)

        plt.xlim(x_start,x_end)
        plt.ylim(-M,M)
        plt.grid(linestyle='--', linewidth=0.5)
        plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)

        if tipo == 0:
            plt.title (r'CANAL Q - $\beta$ = {}'.format(rolloff[i]),fontsize=15)
    
    plt.xlabel('Muestras',fontsize=15)
    
    if svg:
        f.savefig(f"{img_path}/SNR_Q.svg", bbox_inches='tight')

    ### -------------------- End ----------------------------

    # Muestra todos los plots
    if show:
        plt.show()

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        # Cierra plots con ctrl + c
        plt.close()