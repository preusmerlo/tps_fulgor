#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: tp7.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 17 Aug. 2021
# Description 	: tp7 of digital design 
######################################################################

######################################################################
#                               MODULES
######################################################################

import argparse

from framer import framerMarcos as fmr
import serial
import time

######################################################################
#                               MAIN
######################################################################

def main():

    # ---------- Script args ----------
    parser = argparse.ArgumentParser()
    parser.add_argument( "port" , help = "FPGA ttyUSBX" )
    args   = parser.parse_args()
    # ---------------------------------

    # ---------- Serial port ----------
    tty = serial.Serial(
            port     = args.port            ,
            # port     = '/dev/ttyUSB7'            ,
            baudrate = 115200               ,
            parity   = serial.PARITY_NONE   ,
            stopbits = serial.STOPBITS_ONE  ,
            bytesize = serial.EIGHTBITS
        )

    if tty.isOpen() == False:
        raise Exception('Serial port is not open.')
    # ---------------------------------

    # ------------ Framer -------------
    framer = fmr.framerMarcos(framerPort = tty)
    # ---------------------------------

    # -------- Tablas de Hash  --------
    cmd = {                       # Bytes
        'reset'          :  1 << 24,  # 0x01
        'enable_tx'      :  2 << 24,  # 0x02
        'enable_rx'      :  3 << 24,  # 0x03
        'phase'          :  4 << 24,  # 0x04
        'run_log'        :  5 << 24,  # 0x05
        'read_log'       :  6 << 24,  # 0x06
        'mem_addr'       :  7 << 24,  # 0x07
        'mem_data'       :  8 << 24,  # 0x08
        'mem_status'     :  9 << 24,  # 0x09
        'ber_samp_i'     : 10 << 24,  # 0x0A \n (genera break en el micro)
        'ber_samp_q'     : 11 << 24,  # 0x0B
        'ber_err_i'      : 12 << 24,  # 0x0C
        'ber_err_q'      : 13 << 24,  # 0x0D \r (genera break en el micro)
        'ber_idx'        : 14 << 24,  # 0x0E
    }

    cmd_type = {
        'get' : 0 ,
        'set' : 1
    }

    data = {
        'off' : 0 ,
        'on'  : 1 ,
        '0'   : 0 ,
        '1'   : 1 ,
        '2'   : 2 ,
        '3'   : 3 ,
        'low' : 0 ,
        'high': 1 
    }

    # ---------------------------------

    # ------------- Menu --------------
    print('Hi! Commands list:')
    print('------------------------')
    print(' - set reset     [ on / off ]')
    print(' - set enable_tx [ on / off ]')
    print(' - set enable_rx [ on / off ]')
    print(' - set phase     [ 0/1/2/3  ]')
    print(' - set run_log   [ on / off ]')
    print(' - set read_log  [ on / off ]')
    print(' - set mem_addr  [ 0x  ...  ]')
    print(' - get mem_data')
    print(' - get mem_status')
    print(' - get ber_samp_i')
    print(' - get ber_samp_q')
    print(' - set ber_idx   [ low/high ]')
    print(' - get ber_err_i')
    print(' - get ber_err_q')
    print(' - exit')
    print('------------------------')
    # ---------------------------------

    while True :
        
        # Wait command
        inputData = input("<< ").lower()
        
        #  Close script
        if inputData == 'exit':
            tty.close()
            print('>> Bye!')
            exit()
        else:

            inputData = inputData.split()

            try:
                # Setter command
                if inputData[0] == 'set':

                    if inputData[1] == 'mem_addr':
                        p   = cmd[inputData[1]] | int(inputData[2])
                    
                    else:
                        p   = cmd[inputData[1]] | data[inputData[2]]
                    
                    framer.encode(data=p,device=1,type='frame')
                    framer.send()

                # Getter command
                else:
                    p   = cmd[inputData[1]] | data['1']
                    framer.send(p, device = 0)
                    time.sleep(0.5)
                    framer.receive()
                    print('>> {}'.format(framer.decode(type='int'))) 
            except:
                print('>> Unknown command')

            
######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()