#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: tp7.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 17 Aug. 2021
# Description 	: tp7 of digital design 
######################################################################

######################################################################
#                               MODULES
######################################################################

import time
import serial
import argparse
import tool._fixedInt as fxd
from framer import framerMarcos as fmr

######################################################################
#                              CONSTANTS
######################################################################

sys_cmd = {                       # Bytes
    'reset'          :  1 << 24,  # 0x01
    'enable_tx'      :  2 << 24,  # 0x02
    'enable_rx'      :  3 << 24,  # 0x03
    'phase'          :  4 << 24,  # 0x04
    'run_log'        :  5 << 24,  # 0x05
    'read_log'       :  6 << 24,  # 0x06
    'mem_addr'       :  7 << 24,  # 0x07
    'mem_data'       :  8 << 24,  # 0x08
    'mem_status'     :  9 << 24,  # 0x09
    'ber_samp_i'     : 10 << 24,  # 0x0A
    'ber_samp_q'     : 11 << 24,  # 0x0B
    'ber_err_i'      : 12 << 24,  # 0x0C
    'ber_err_q'      : 13 << 24,  # 0x0D
    'ber_idx'        : 14 << 24,  # 0x0E
}

cmd_type = {
    'get' : 0 ,
    'set' : 1
}

data = {
    'off' : 0 ,
    'on'  : 1 ,
    '0'   : 0 ,
    '1'   : 1 ,
    '2'   : 2 ,
    '3'   : 3 ,
    'low' : 0 ,
    'high': 1 
}

######################################################################
#                               MAIN
######################################################################

def main():

    # # ---------- Script args ----------
    # parser = argparse.ArgumentParser()
    # parser.add_argument( "port" , help = "FPGA ttyUSBX" )
    # args   = parser.parse_args()
    # # ---------------------------------

    # ---------- Serial port ----------
    tty = serial.Serial(
            # port     = args.port            ,
            port     = '/dev/ttyUSB9'            ,
            baudrate = 115200               ,
            parity   = serial.PARITY_NONE   ,
            stopbits = serial.STOPBITS_ONE  ,
            bytesize = serial.EIGHTBITS
        )

    if tty.isOpen() == False:
        raise Exception('Serial port is not open.')
    # ---------------------------------

    # ------------ Framer -------------
    framer = fmr.framerMarcos(framerPort = tty)
    # ---------------------------------

    # ------ Command structure --------
    idxCmdType = 0
    idxCmd     = 1
    idxCmdData = 2
    # ---------------------------------

    # --------- Fixed point -----------
    sign       = 'S'
    totalWidth =  11
    fractWidth =  4
    # ---------------------------------

    # ------------- Menu --------------
    print('Hi! Commands list:')
    print('------------------------')
    print(' - set reset       [ on / off ]')
    print(' - set enable_tx   [ on / off ]')
    print(' - set enable_rx   [ on / off ]')
    print(' - set phase       [ 0/1/2/3  ]')
    print(' - get ber_i')
    print(' - get ber_q')
    print(' - get mem_data    [  length  ]')
    print(' - exit')
    print('------------------------')
    # ---------------------------------

    while True :
        
        # Wait command
        inputData = input("<< ").lower()
        
        #  Close script
        if inputData == 'exit':
            tty.close()
            print('>> Bye!')
            exit()
        else:

            inputData = inputData.split()

            try:
                # Setter command
                if inputData[idxCmdType] == 'set':            
                    p   = sys_cmd[ inputData[idxCmd] ] | data[ inputData[idxCmdData] ]
                    framer.send(p, device = cmd_type['set'])

                # Getter command
                elif inputData[idxCmdType] == 'get':
                    
                    if   inputData[idxCmd] == 'ber_i':
                        ber = getBer(framer, 'i')
                        print('>> {}'.format(ber))
                        
                    elif inputData[idxCmd] == 'ber_q':
                        ber = getBer(framer, 'q')
                        print('>> {}'.format(ber))
                    
                    elif inputData[idxCmd] == 'mem_data':
                        # Read data
                        mem_i, mem_q = getMemoryData(framer, inputData[idxCmdData])                        

                        # Save log
                        fd_mem_i  = open('log/mem_i.log','w')
                        fd_mem_q  = open('log/mem_q.log','w')

                        for i in range(len(mem_i)):
                            fd_mem_i.write(str(mem_i[i]) + '\n')
                            fd_mem_q.write(str(mem_q[i]) + '\n')
                        
                        fd_mem_i.close()
                        fd_mem_q.close()

                        print('>> Done! See /log/ ...')
                    else:
                        print('>> Unknown command')                
            except:
                print('>> Unknown command')

######################################################################
#                              GET BER
######################################################################

def getBer(framer, channel):
    
    # Get samples
    framer.send(sys_cmd['ber_idx'] | data['0'], device = cmd_type['set'])
    framer.send(sys_cmd['ber_samp_' + channel], device = cmd_type['get'])
    time.sleep(0.2)
    framer.receive()
    
    sampLow = framer.decode(type='int')

    framer.send(sys_cmd['ber_idx'] | data['1'], device = cmd_type['set'])
    framer.send(sys_cmd['ber_samp_' + channel], device = cmd_type['get'])
    time.sleep(0.2)
    framer.receive()

    sampHigh = framer.decode(type='int')

    samp = sampHigh << 32 | sampLow 

    if samp == 0:
        return 'First run system'
    
    # Get errors
    framer.send(sys_cmd['ber_idx'] | data['0'], device = cmd_type['set'])    
    framer.send(sys_cmd['ber_err_' + channel ], device = cmd_type['get'])
    time.sleep(0.2)
    framer.receive()

    errorLow = framer.decode(type='int')

    framer.send(sys_cmd['ber_idx'] | data['1'], device = cmd_type['set'])    
    framer.send(sys_cmd['ber_err_' + channel ], device = cmd_type['get'])
    time.sleep(0.2)
    framer.receive()

    errorHigh = framer.decode(type='int')

    err =  errorHigh << 32 | errorLow
    
    # Return ber
    ber = err / samp
    return ber

######################################################################
#                          GET MEMORY DATA
######################################################################

def getMemoryData(framer, memLength):
    
    # Run log
    framer.send( sys_cmd['run_log'] | data['on'] , device = cmd_type['set'])    
    
    # Wait full memory
    while True:
        time.sleep(0.1)
        framer.send(sys_cmd['mem_status'], device = cmd_type['get'])
        framer.receive()
        if framer.decode(type='int') == 1:
            framer.send( sys_cmd['run_log'] | data['off'] , device = cmd_type['set'])
            break
    
    mem_i = []
    mem_q = []
    
    # Get memory data
    framer.send(sys_cmd['read_log'] | data['on'], device = cmd_type['set'])
    
    for i in range(0,int( memLength)):
        framer.send(sys_cmd['mem_addr'] | (i & int('0x1FFF',16)), device = cmd_type['set'])
        framer.send(sys_cmd['mem_data'] , device = cmd_type['get'])
        time.sleep(0.05)
        
        framer.receive()
        framer.receive()
        
        # print('Frame: {}'.format(framer.getAttribute('dataFrame').hex()))

        mem_data = framer.decode(type='int')

        mem_i.append(signedBinToFloat( mem_data        & int('0x7FF',16), 11, 4))
        mem_q.append(signedBinToFloat((mem_data >> 11) & int('0x7FF',16), 11, 4))
    
    framer.send(sys_cmd['read_log'] | data['off'], device = cmd_type['set'])            
    return mem_i , mem_q

######################################################################
#                        BITS TO FLOAT
######################################################################

def signedBinToFloat( bin:int , totalWidth:int, fractWidth:int ):
    intWidth = totalWidth - fractWidth
    # Sign bit
    sign = (-1*((bin >> fractWidth+  intWidth - 1) & 1)) * 2**(intWidth - 1) 

    # Integer part
    integer = 0
    for pos in range(1,intWidth):
        integer +=  ((bin >> totalWidth-pos-1) & int('0x1',16)) * 2**(intWidth-pos-1)

    # Fractional part
    fractional = 0
    for pos in range(fractWidth):
        fractional +=  ((bin >> fractWidth-pos-1) & int('0x1',16)) * 2**(-pos-1)

    return sign + integer + fractional

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()