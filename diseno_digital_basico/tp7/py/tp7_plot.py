#! /bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: tp7_plot.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 4 Sep. 2021
# Description 	: tp7 of digital design 
######################################################################

######################################################################
#                               MODULES
######################################################################

import matplotlib.pyplot as plt

######################################################################
#                               MAIN
######################################################################

def main():
    fd_mem_i  = open('log/mem_i.log','r')
    fd_mem_q  = open('log/mem_q.log','r')

    length = int(input('Samples to plot: '))

    mem_i = []
    mem_q = []

    for i in range(length-1):
        mem_i.append(float(fd_mem_i.readline()))
        mem_q.append(float(fd_mem_q.readline()))
    
    
    f = plt.figure(figsize = [600, 400])
    
    plt.subplot(2,1,1)
    plt.plot(mem_i,'r-',linewidth = 3)
    plt.grid(linestyle='--', linewidth=1)
    plt.title ('Channel I',fontsize = 15)
    plt.ylabel('Amplitude')
    plt.xlim([0,length-2])

    plt.subplot(2,1,2)
    plt.plot(mem_q,'r-',linewidth = 3)
    plt.grid(linestyle='--', linewidth=1)
    plt.title ('Channel Q',fontsize = 15)
    plt.xlabel('Samples')
    plt.ylabel('Amplitude')
    plt.xlim([0,length-2])

    plt.show()
    f.savefig('../img/mem_plots.svg', bbox_inches='tight')

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()