#!/bin/bash
# Creamos dos terminales virtuales ttyVP (tty Virtual Port)
echo "Creando puertos virtuales ···"
sudo gnome-terminal --command="socat -d -d pty,raw,echo=0,link=/dev/ttyVPA pty,raw,echo=0,link=/dev/ttyVPB" --title="Puertos corriendo - NO CERRAR" &> /dev/null

# Agregamos permisos (a lo bruto)
echo "Agregando permisos ···"
sudo chmod 666 /dev/ttyVPA
sudo chmod 666 /dev/ttyVPB

# Data de puertos
echo "Puertos creados:"
echo " + [virtual port A] /dev/ttyVPA"
echo " + [virtual port B] /dev/ttyVPB"
