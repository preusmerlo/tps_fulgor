/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: tb_registerFile.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 24 Aug. 2021
* Description 	: tp7 of digital design 
************************************************************************************************
*/

/* Timescale */
`timescale 1ns/100ps

module tb_registerFile();
    
    /* DUT parameters */
    localparam NB_IO_PORTS = 32 ; 
    localparam NB_CMD      = 8  ;
    localparam NB_DATA     = 23 ;
    localparam NB_MEM_ADDR = 13 ;
    localparam NB_MEM_DATA = 12 ;  
    localparam NB_BER_REG  = 64 ;

    /* Inputs */
    reg                      i_clock      ;
    reg [NB_BER_REG  -1 : 0] i_ber_samp_I ;
    reg [NB_BER_REG  -1 : 0] i_ber_samp_Q ;
    reg [NB_BER_REG  -1 : 0] i_ber_err_I  ;
    reg [NB_BER_REG  -1 : 0] i_ber_err_Q  ;
    reg [NB_MEM_DATA -1 : 0] i_mem_data   ;
    reg                      i_mem_state  ;
    reg [NB_IO_PORTS-1 : 0 ] i_input_port ;

    /* Outputs to system */
    wire                     o_reset       ;
    wire                     o_en_tx       ;
    wire                     o_en_rx       ;
    wire [2          -1 : 0] o_phase_sel   ;
    wire                     o_run_log     ;
    wire                     o_read_log    ;
    wire [NB_MEM_ADDR-1 : 0] o_mem_addr    ;
    wire [NB_IO_PORTS-1 : 0] o_output_port ;      


    /* Commands code */
    localparam CMD_NULL           = {NB_CMD{1'b0}};
    
    localparam CMD_RST            = {NB_CMD{1'b0}} | 4'h1;  /* Reset sys = 0x01 */
    localparam CMD_EN_TX          = {NB_CMD{1'b0}} | 4'h2;  /* Enable tx = 0x02 */
    localparam CMD_EN_RX          = {NB_CMD{1'b0}} | 4'h3;  /* Enable rx = 0x03 */
    localparam CMD_PAHSE          = {NB_CMD{1'b0}} | 4'h4;  /* Phase     = 0x04 */
    localparam CMD_RUN_LOG        = {NB_CMD{1'b0}} | 4'h5;  /* Run log   = 0x05 */
    localparam CMD_READ_LOG       = {NB_CMD{1'b0}} | 4'h6;  /* Read log  = 0x06 */
    
    localparam CMD_SET_MEM_ADDR   = {NB_CMD{1'b0}} | 4'h7;  /* Read log  = 0x07 */
    localparam CMD_GET_MEM_DATA   = {NB_CMD{1'b0}} | 4'h8;  /* Read log  = 0x08 */
    localparam CMD_GET_MEM_STATUS = {NB_CMD{1'b0}} | 4'h9;  /* Read log  = 0x09 */

    localparam CMD_GET_BER_SAMP_I = {NB_CMD{1'b0}} | 4'hA;  /* Read log  = 0x0A */
    localparam CMD_GET_BER_SAMP_Q = {NB_CMD{1'b0}} | 4'hB;  /* Read log  = 0x0B */
    localparam CMD_GET_BER_ERR_I  = {NB_CMD{1'b0}} | 4'hC;  /* Read log  = 0x0C */
    localparam CMD_GET_BER_ERR_Q  = {NB_CMD{1'b0}} | 4'hD;  /* Read log  = 0x0D */

    localparam CMD_SET_BER_IDX    = {NB_CMD{1'b0}} | 4'hE;  /* Read log  = 0x0E */

    localparam WRITE_ENABLE_ON    = 1'b1;
    localparam WRITE_ENABLE_OFF   = 1'b0;
    
    localparam DATA_ZEROS         = {NB_DATA{1'b0}};
    localparam DATA_ONES          = {NB_DATA{1'b1}};
    /* Probes */
    
    /* Stimuli generation */

    initial begin
        i_clock       = 1'b0;
        
        i_ber_samp_I  = 64'hAAAA_EEEE_FFFF_8888;
        i_ber_samp_Q  = 64'hAAAA_EEEE_FFFF_8888;
        i_ber_err_I   = 64'hAAAA_EEEE_FFFF_8888;
        i_ber_err_Q   = 64'hAAAA_EEEE_FFFF_8888;
        
        i_mem_data    = 12'hEEF;
        i_mem_state   =  1'b0;
        
        i_input_port  = 32'b0;
        
        /* Reset all */
        #20 i_input_port  = {CMD_RST , WRITE_ENABLE_OFF , DATA_ZEROS};
        #20 i_input_port  = {CMD_RST , WRITE_ENABLE_ON  , DATA_ZEROS};
        #20 i_input_port  = {CMD_RST , WRITE_ENABLE_OFF , DATA_ZEROS};

        /* Enable tx */
        #20 i_input_port  = {CMD_EN_TX , WRITE_ENABLE_OFF , DATA_ONES};
        #20 i_input_port  = {CMD_EN_TX , WRITE_ENABLE_ON  , DATA_ONES};
        #20 i_input_port  = {CMD_EN_TX , WRITE_ENABLE_OFF , DATA_ONES};

        /* Enable rx */
        #20 i_input_port  = {CMD_EN_RX , WRITE_ENABLE_OFF , DATA_ONES};
        #20 i_input_port  = {CMD_EN_RX , WRITE_ENABLE_ON  , DATA_ONES};
        #20 i_input_port  = {CMD_EN_RX , WRITE_ENABLE_OFF , DATA_ONES};

        /* Set sampler phase = 11 */
        #20 i_input_port  = {CMD_PAHSE , WRITE_ENABLE_OFF , DATA_ONES};
        #20 i_input_port  = {CMD_PAHSE , WRITE_ENABLE_ON  , DATA_ONES};
        #20 i_input_port  = {CMD_PAHSE , WRITE_ENABLE_OFF , DATA_ONES};

        /* Run log */
        #20 i_input_port  = {CMD_RUN_LOG , WRITE_ENABLE_OFF , DATA_ONES};
        #20 i_input_port  = {CMD_RUN_LOG , WRITE_ENABLE_ON  , DATA_ONES};
        #20 i_input_port  = {CMD_RUN_LOG , WRITE_ENABLE_OFF , DATA_ONES};

        /* Read log */
        #20 i_input_port  = {CMD_READ_LOG , WRITE_ENABLE_OFF , DATA_ONES};
        #20 i_input_port  = {CMD_READ_LOG , WRITE_ENABLE_ON  , DATA_ONES};
        #20 i_input_port  = {CMD_READ_LOG , WRITE_ENABLE_OFF , DATA_ONES};

        /* Set memory address */
        #20 i_input_port  = {CMD_SET_MEM_ADDR , WRITE_ENABLE_OFF , DATA_ONES};
        #20 i_input_port  = {CMD_SET_MEM_ADDR , WRITE_ENABLE_ON  , DATA_ONES};
        #20 i_input_port  = {CMD_SET_MEM_ADDR , WRITE_ENABLE_OFF , DATA_ONES};

        /* Get memory data */
        #20 i_input_port  = {CMD_GET_MEM_DATA , WRITE_ENABLE_OFF , DATA_ONES};
        #20 i_input_port  = {CMD_GET_MEM_DATA , WRITE_ENABLE_ON  , DATA_ONES};
        #20 i_input_port  = {CMD_GET_MEM_DATA , WRITE_ENABLE_OFF , DATA_ONES};

        /* Get memory status */
        #20 i_input_port  = {CMD_GET_MEM_STATUS , WRITE_ENABLE_OFF , DATA_ZEROS};
        #20 i_input_port  = {CMD_GET_MEM_STATUS , WRITE_ENABLE_ON  , DATA_ZEROS};
        #20 i_input_port  = {CMD_GET_MEM_STATUS , WRITE_ENABLE_OFF , DATA_ZEROS};

        /* Set ber index */
        #20 i_input_port  = {CMD_SET_BER_IDX , WRITE_ENABLE_OFF , DATA_ZEROS};
        #20 i_input_port  = {CMD_SET_BER_IDX , WRITE_ENABLE_ON  , DATA_ZEROS};
        #20 i_input_port  = {CMD_SET_BER_IDX , WRITE_ENABLE_OFF , DATA_ZEROS};

        /* Get ber samp */
        #20 i_input_port  = {CMD_GET_BER_SAMP_I , WRITE_ENABLE_OFF , DATA_ZEROS};
        #20 i_input_port  = {CMD_GET_BER_SAMP_I , WRITE_ENABLE_ON  , DATA_ZEROS};
        #20 i_input_port  = {CMD_GET_BER_SAMP_I , WRITE_ENABLE_OFF , DATA_ZEROS};

        /* Set ber index */
        #20 i_input_port  = {CMD_SET_BER_IDX , WRITE_ENABLE_OFF , DATA_ONES};
        #20 i_input_port  = {CMD_SET_BER_IDX , WRITE_ENABLE_ON  , DATA_ONES};
        #20 i_input_port  = {CMD_SET_BER_IDX , WRITE_ENABLE_OFF , DATA_ONES};

        /* Get ber samp */
        #20 i_input_port  = {CMD_GET_BER_SAMP_Q , WRITE_ENABLE_OFF , DATA_ZEROS};
        #20 i_input_port  = {CMD_GET_BER_SAMP_Q , WRITE_ENABLE_ON  , DATA_ZEROS};
        #20 i_input_port  = {CMD_GET_BER_SAMP_Q , WRITE_ENABLE_OFF , DATA_ZEROS};

        #20 $finish();
    end

    /* Clock generation */

    always  begin
        #1 i_clock = ~i_clock;
    end

    /* DUT instance */
    registerFile #(
        .NB_IO_PORTS  ( NB_IO_PORTS   ), 
        .NB_CMD       ( NB_CMD        ),
        .NB_DATA      ( NB_DATA       ),
        .NB_MEM_ADDR  ( NB_MEM_ADDR   ),
        .NB_MEM_DATA  ( NB_MEM_DATA   ),  
        .NB_BER_REG   ( NB_BER_REG    )
    )
    DUT (
        .i_clock      ( i_clock       ),
        .i_ber_samp_I ( i_ber_samp_I  ),
        .i_ber_samp_Q ( i_ber_samp_Q  ),
        .i_ber_err_I  ( i_ber_err_I   ),
        .i_ber_err_Q  ( i_ber_err_Q   ),
        .i_mem_data   ( i_mem_data    ),
        .i_mem_state  ( i_mem_state   ),
        .i_input_port ( i_input_port  ),
        .o_reset      ( o_reset       ),
        .o_en_tx      ( o_en_tx       ),
        .o_en_rx      ( o_en_rx       ),
        .o_phase_sel  ( o_phase_sel   ),
        .o_run_log    ( o_run_log     ),
        .o_read_log   ( o_read_log    ),
        .o_mem_addr   ( o_mem_addr    ),
        .o_output_port( o_output_port )
    );

endmodule /* tb_registerFile */