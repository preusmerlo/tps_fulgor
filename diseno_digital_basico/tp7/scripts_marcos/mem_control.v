module mem_control 
#(
    parameter NB_ADDRESS = 11,
    parameter NB_DATA    = 16
)
(
    output [NB_DATA - 1 : 0]                    o_rf_data,
    output                                      o_rf_mem_full,
    input                                       i_rf_load_mem_en,
    input                                       i_rf_reset_mem,
    input [NB_ADDRESS - 1 : 0]                  i_rf_read_address,

    input [NB_DATA - 1 : 0]                     i_data,
    input                                       i_clock
);

localparam [NB_ADDRESS-1 : 0] MEM_LAST_ADD = 2**NB_ADDRESS-1;

reg wr_en;
reg load_mem_en_d;
reg mem_full_flag;
reg [NB_ADDRESS-1:0] mem_address;

always@(posedge i_clock)
begin
    if(i_rf_reset_mem)
    begin
        mem_full_flag <= 1'b0;
        load_mem_en_d <= 1'b0;
        mem_address <= {NB_ADDRESS{1'b0}};
        wr_en <= 1'b0;
    end
    else 
    begin
        load_mem_en_d <= i_rf_load_mem_en;

        if(load_mem_en_d == 1'b0 && i_rf_load_mem_en == 1'b1)
        begin
            mem_address <= {NB_ADDRESS{1'b0}};
            wr_en <= 1'b1;
        end
        else if (wr_en == 1'b1)
        begin
            if (mem_address == MEM_LAST_ADD)
            begin
                mem_address <= i_rf_read_address; //from RF: address to read
                wr_en <= 1'b0;
                mem_full_flag <= 1'b1;
            end
            else
            begin
                mem_address <= mem_address + 1'b1;
                wr_en <= 1'b1;
            end       
        end
        else
        begin
            mem_address <= i_rf_read_address; //from RF: address to read
            wr_en <= 1'b0;
        end
    end
end

assign o_rf_mem_full = mem_full_flag;

log_mem #(
    .RAM_WIDTH(NB_DATA),                  // Specify RAM data width
    .RAM_DEPTH(2**NB_ADDRESS),            // Specify RAM depth (number of entries)
    .RAM_PERFORMANCE("HIGH_PERFORMANCE"), // Select "HIGH_PERFORMANCE" or "LOW_LATENCY" 
    .INIT_FILE("")                        // Specify name/location of RAM initialization file if using one (leave blank if not)
) 
u_log_mem (
    .addra(mem_address),    // Address bus, width determined from RAM_DEPTH
    .dina(i_data),          // RAM input data, width determined from RAM_WIDTH
    .clka(i_clock),         // Clock
    .wea(wr_en),            // Write enable
    .ena(1'b1),             // RAM Enable, for additional power savings, disable port when not in use
    .rsta(i_rf_reset_mem),  // Output reset (does not affect memory contents)
    .regcea(1'b1),          // Output register enable
    .douta(o_rf_data)       // RAM output data, width determined from RAM_WIDTH
);

endmodule