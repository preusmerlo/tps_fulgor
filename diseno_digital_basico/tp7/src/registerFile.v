/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: registerFile.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 24 Aug. 2021
* Description 	: ...
************************************************************************************************
*/

module registerFile #(
        /* Parameters */
        parameter NB_IO_PORTS = 32 , 
        
        parameter NB_CMD      = 8  ,
        parameter NB_DATA     = 23 ,

        parameter NB_MEM_ADDR = 13 ,
        parameter NB_MEM_DATA = 12 ,  

        parameter NB_BER_REG  = 64 

    )
    (
        /* Inputs */
        input  wire                     i_clock     ,

        /* Inputs from system */
        input wire [NB_BER_REG  -1 : 0] i_ber_samp_I,
        input wire [NB_BER_REG  -1 : 0] i_ber_samp_Q,
        input wire [NB_BER_REG  -1 : 0] i_ber_err_I ,
        input wire [NB_BER_REG  -1 : 0] i_ber_err_Q ,

        input wire [NB_MEM_DATA -1 : 0] i_mem_data  ,
        input wire                      i_mem_state ,

        /* Inputs from uP */
        input  wire [NB_IO_PORTS-1 : 0] i_input_port,
            
        /* Outputs to system */
        output wire                     o_reset     ,
        output wire                     o_en_tx     ,
        output wire                     o_en_rx     ,
        output wire [2          -1 : 0] o_phase_sel ,

        output wire                     o_run_log   ,
        output wire                     o_read_log  ,

        output wire [NB_MEM_ADDR-1 : 0] o_mem_addr  ,
    
        /* Outputs to uP */
        output wire [NB_IO_PORTS-1 : 0] o_output_port      
    );

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Commands code */
    localparam CMD_RST            = {NB_CMD{1'b0}} | 4'h1;  /* Reset sys = 0x01 */
    localparam CMD_EN_TX          = {NB_CMD{1'b0}} | 4'h2;  /* Enable tx = 0x02 */
    localparam CMD_EN_RX          = {NB_CMD{1'b0}} | 4'h3;  /* Enable rx = 0x03 */
    localparam CMD_PAHSE          = {NB_CMD{1'b0}} | 4'h4;  /* Phase     = 0x04 */
    
    localparam CMD_RUN_LOG        = {NB_CMD{1'b0}} | 4'h5;  /* Run log   = 0x05 */
    localparam CMD_READ_LOG       = {NB_CMD{1'b0}} | 4'h6;  /* Read log  = 0x06 */
    
    localparam CMD_SET_MEM_ADDR   = {NB_CMD{1'b0}} | 4'h7;  /* Read log  = 0x07 */
    localparam CMD_GET_MEM_DATA   = {NB_CMD{1'b0}} | 4'h8;  /* Read log  = 0x08 */
    localparam CMD_GET_MEM_STATUS = {NB_CMD{1'b0}} | 4'h9;  /* Read log  = 0x09 */

    localparam CMD_GET_BER_SAMP_I = {NB_CMD{1'b0}} | 4'hA;  /* Read log  = 0x0A */
    localparam CMD_GET_BER_SAMP_Q = {NB_CMD{1'b0}} | 4'hB;  /* Read log  = 0x0B */
    localparam CMD_GET_BER_ERR_I  = {NB_CMD{1'b0}} | 4'hC;  /* Read log  = 0x0C */
    localparam CMD_GET_BER_ERR_Q  = {NB_CMD{1'b0}} | 4'hD;  /* Read log  = 0x0D */
    localparam CMD_SET_BER_IDX    = {NB_CMD{1'b0}} | 4'hE;  /* Read log  = 0x0E */

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Input fields */
    wire [NB_CMD     -1 : 0] i_command ;
    wire [NB_DATA    -1 : 0] i_data    ;
    wire                     i_enable  ;
    
    /* Output registers */
    reg [2          -1 : 0] phase_sel ;
    reg                     en_tx     ;
    reg                     en_rx     ;
    reg                     reset     ;

    reg                     run_log   ;
    reg                     read_log  ;
    reg [NB_MEM_ADDR-1 :0 ] mem_addr  ;

    reg [NB_IO_PORTS-1 :0 ] output_port;
    
    
    reg                     ber_idx   ;


/*
************************************************************************************************
*								        INPUT SLICE
************************************************************************************************
*/

    /*                                i_input_port                           */
    /*     ----------------------------------------------------------        */
    /*     |       Command         |     Enable     |      Data     |        */
    /*     ----------------------------------------------------------        */
    /*     NB_IO_PORTS-1   NB_DATA+1     NB_DATA    NB_DATA-1       0        */


    assign i_data    = i_input_port[NB_DATA-1 : 0]; 
    assign i_enable  = i_input_port[NB_DATA ]; 
    assign i_command = i_input_port[(NB_CMD+NB_DATA+1)-1 : NB_DATA+1]; 

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    /* Write enable */
    posEdgeDetector #(
        .NB_IN   (     1     )
    )
    u_posEdgeDetector (
        .i_in    ( i_enable  ) ,
        .o_edge  ( w_enable  ) ,
        .i_clock ( i_clock   )
    );

    /* Write register file */
    always @(posedge i_clock) begin
        if(w_enable) begin
            case (i_command)
                /* Comunications system */
                CMD_RST: 
                    reset     <= i_data[0]  ;
                CMD_EN_TX:
                    en_tx     <= i_data[0]  ; 
                CMD_EN_RX:
                    en_rx     <= i_data[0]  ;
                CMD_PAHSE:
                    phase_sel <= i_data[1:0];
                
                /* Memory */
                CMD_RUN_LOG:
                    run_log   <= i_data[0]  ;
                CMD_READ_LOG:
                    read_log  <= i_data[0]  ;
                CMD_SET_MEM_ADDR:
                    mem_addr        <= i_data[NB_MEM_ADDR-1:0];
                CMD_GET_MEM_STATUS:
                    output_port     <= i_mem_state;
                CMD_GET_MEM_DATA:
                    output_port     <= i_mem_data;
                
                /* BER checker */
                CMD_SET_BER_IDX:
                    ber_idx   <= i_data[0]  ;
                CMD_GET_BER_SAMP_I:
                    if (ber_idx)
                        output_port <= i_ber_samp_I[NB_BER_REG-1:NB_IO_PORTS];
                    else
                        output_port <= i_ber_samp_I[NB_IO_PORTS-1:0];
                CMD_GET_BER_SAMP_Q:
                    if (ber_idx)
                        output_port <= i_ber_samp_Q[NB_BER_REG-1:NB_IO_PORTS];
                    else
                        output_port <= i_ber_samp_Q[NB_IO_PORTS-1:0];
                CMD_GET_BER_ERR_I:
                    if (ber_idx)
                        output_port <= i_ber_err_I[NB_BER_REG-1:NB_IO_PORTS];
                    else
                        output_port <= i_ber_err_I[NB_IO_PORTS-1:0];
                CMD_GET_BER_ERR_Q:
                    if (ber_idx)
                        output_port <= i_ber_err_Q[NB_BER_REG-1:NB_IO_PORTS];
                    else
                        output_port <= i_ber_err_Q[NB_IO_PORTS-1:0];

            endcase
        end
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign o_reset       =  reset      ;
    
    assign o_en_tx       =  en_tx      ;
    assign o_en_rx       =  en_rx      ;
    assign o_phase_sel   =  phase_sel  ;
    
    assign o_run_log     =  run_log    ;
    assign o_read_log    =  read_log   ;
    assign o_mem_addr    =  mem_addr   ;

    assign o_output_port = output_port ;

endmodule