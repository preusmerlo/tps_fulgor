/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: top.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 29 Aug. 2021
* Description 	: tp7 of digital design
************************************************************************************************
*/

module top #(
        /* --- Register File settings --- */
        parameter NB_IO_PORTS = 32      , 
        parameter NB_CMD      = 8       ,
        parameter NB_DATA     = 23      ,
        parameter NB_MEM_ADDR = 13      ,
        parameter NB_BER_REG  = 64      ,
        
        /* --- Com. system settings --- */
        parameter OVRSAMPLE = 4         ,
        
        /* - Control settings - */
        parameter NB_COUNTER = 2        ,

        /* - PRBS settings - */
        parameter PRBS_ORDER   = 9      ,
        parameter PRBS_IMPULSE = 0      ,
        parameter PRBS_I_SEED  = 9'h1AA ,
        parameter PRBS_Q_SEED  = 9'h1FE ,

        /* - FIR  settings - */
        parameter NTAPS        = 24     ,
        parameter NB_TAPS      = 6      ,
        parameter NBF_TAPS     = 4      ,
        parameter MAPPER       = 1
    )
    (
        /* Inputs */
        input        i_clock            ,
        input        i_reset            , 
        input        i_rx_uart          ,

        /* Outputs */        
        output       o_tx_uart          ,
        output [3:0] o_leds             ,
        output [3:0] o_leds_rgb          
    );
    
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam NB_FIR_OUT = (2 + NB_TAPS) + $clog2(NTAPS / OVRSAMPLE);
    localparam NB_GPIOS   = 32 ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    /* MicroBlaze outputs */
    wire                      uC_lock_clock  ;
    wire                      uC_clock_100   ;
    wire [NB_IO_PORTS -1 : 0] uC_gp_output   ;
    wire [NB_IO_PORTS -1 : 0] uC_gp_input    ;

    /* Comunications system outputs */
    wire [NB_FIR_OUT  -1 : 0] com_fir_I      ;
    wire [NB_FIR_OUT  -1 : 0] com_fir_Q      ;
    wire [NB_BER_REG  -1 : 0] com_ber_samp_I ;
    wire [NB_BER_REG  -1 : 0] com_ber_samp_Q ;
    wire [NB_BER_REG  -1 : 0] com_ber_err_I  ;
    wire [NB_BER_REG  -1 : 0] com_ber_err_Q  ;
    wire [3              : 0] com_leds       ;
      
    /* Memory outputs  */
    wire [2*NB_FIR_OUT-1 : 0] mem_data       ;
    wire                      mem_state      ;

    /* Register file outputs */
    wire                     rf_soft_reset   ;
    wire                     rf_en_tx        ;
    wire                     rf_en_rx        ;
    wire [2          -1 : 0] rf_phase_sel    ;
    wire                     rf_run_log      ;
    wire                     rf_read_log     ;
    wire [NB_MEM_ADDR-1 : 0] rf_addr         ;
    wire [3             : 0] rf_switch       ;
 
/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    // /* ----  Temporal ----  */
    // assign mem_data  = {NB_MEM_DATA{1'b1}};
    // assign mem_state = 1'b1;
    // /* ----  Temporal ----  */

    MicroBlaze u_MicroBlaze(
        .gpio_io_o_0    ( uC_gp_output   ) , /* Already Connected  */
        .gpio_io_i_0    ( uC_gp_input    ) , /* Already Connected  */
        .i_reset        ( i_reset        ) , /* Already Connected  */
        .i_sys_clock    ( i_clock        ) , /* Already Connected  */
        .o_clock_100    ( uC_clock_100   ) , /* Already Connected  */
        .o_lock_clock   ( uC_lock_clock  ) , /* Already Connected  */
        .usb_uart_rxd   ( i_rx_uart      ) , /* Already Connected  */
        .usb_uart_txd   ( o_tx_uart      )   /* Already Connected  */
    );

    registerFile #(
        .NB_IO_PORTS  ( NB_IO_PORTS  ), 
        .NB_CMD       ( NB_CMD       ),
        .NB_DATA      ( NB_DATA      ),
        .NB_MEM_ADDR  ( NB_MEM_ADDR  ),
        .NB_MEM_DATA  ( 2*NB_FIR_OUT ),  
        .NB_BER_REG   ( NB_BER_REG   )
    )
    u_registerFile (
        .i_clock      ( uC_clock_100   ), /* Already Connected  */
        .i_input_port ( uC_gp_output   ), /* Already Connected  */
        
        .i_ber_samp_I ( com_ber_samp_I ), /* Already Connected  */
        .i_ber_samp_Q ( com_ber_samp_Q ), /* Already Connected  */ 
        .i_ber_err_I  ( com_ber_err_I  ), /* Already Connected  */
        .i_ber_err_Q  ( com_ber_err_Q  ), /* Already Connected  */
        .i_mem_data   ( mem_data       ),
        .i_mem_state  ( mem_state      ),
        .o_reset      ( rf_soft_reset  ), /* Already Connected  */
        .o_en_tx      ( rf_en_tx       ), /* Already Connected  */
        .o_en_rx      ( rf_en_rx       ), /* Already Connected  */
        .o_phase_sel  ( rf_phase_sel   ), /* Already Connected  */        
        .o_run_log    ( rf_run_log     ),
        .o_read_log   ( rf_read_log    ),
        .o_mem_addr   ( rf_addr        ),
        .o_output_port( uC_gp_input    )  /* Already Connected  */
    );

    assign rf_switch = { rf_phase_sel , rf_en_rx , rf_en_tx};

    comSystem #(
        .OVRSAMPLE    ( OVRSAMPLE    ),
        .NB_COUNTER   ( NB_COUNTER   ),
        .PRBS_ORDER   ( PRBS_ORDER   ),
        .PRBS_IMPULSE ( PRBS_IMPULSE ),
        .PRBS_I_SEED  ( PRBS_I_SEED  ),
        .PRBS_Q_SEED  ( PRBS_Q_SEED  ),
        .NTAPS        ( NTAPS        ),
        .NB_TAPS      ( NB_TAPS      ),
        .NBF_TAPS     ( NBF_TAPS     ),
        .MAPPER       ( MAPPER       )
    )
    u_comSystem (
        .i_clock      ( uC_clock_100   ), /* Already Connected  */
        .i_reset      ( rf_soft_reset  ), /* Already Connected  */
        .i_sw         ( rf_switch      ), /* Already Connected  */
        .o_leds       ( com_leds       ), /* Already Connected  */
        .o_fir_I      ( com_fir_I      ),
        .o_fir_Q      ( com_fir_Q      ),
        .o_ber_samp_I ( com_ber_samp_I ), /* Already Connected  */ 
        .o_ber_samp_Q ( com_ber_samp_Q ), /* Already Connected  */ 
        .o_ber_error_I( com_ber_err_I  ), /* Already Connected  */ 
        .o_ber_error_Q( com_ber_err_Q  )  /* Already Connected  */
    );

    memoryControl #(
        .NB_ADDRESS         ( NB_MEM_ADDR  ),
        .NB_DATA            ( 2*NB_FIR_OUT )
    )
    u_memoryControl
    (
        .i_clock            (   uC_clock_100        ),
        .i_rf_reset_mem     (   rf_soft_reset       ),
        .i_rf_load_mem_en   (   rf_run_log          ),
        .i_rf_read_address  (   rf_addr             ),
        .i_data             ({com_fir_I , com_fir_Q}),
        .o_rf_data          (   mem_data            ),
        .o_rf_mem_full      (   mem_state           )
    );


    /*
            VIO Probe               Signal                  Bits
            PROBE_IN0               rf_soft_reset           1
            PROBE_IN1               rf_en_tx                1
            PROBE_IN2               rf_en_rx                1
            PROBE_IN3               rf_phase_sel            2
            PROBE_IN4               rf_run_log              1
            PROBE_IN5               rf_read_log             1
            PROBE_IN6               rf_addr                 13
            PROBE_IN7               mem_data                11
            PROBE_IN8               mem_state               1
    */

    VIO u_vio (
        .i_clock            ( uC_clock_100  ),
        .i_enable_rx        ( rf_en_rx      ),
        .i_enable_tx        ( rf_en_tx      ),
        .i_leds             ( com_leds      ),
        .i_mem_address      ( rf_addr       ),
        .i_mem_data         ( mem_data      ),
        .i_mem_state        ( mem_state     ),
        .i_phase_selector   ( rf_phase_sel  ),
        .i_read_log         ( rf_read_log   ),
        .i_run_log          ( rf_run_log    ),
        .i_soft_reset       ( rf_soft_reset ),
        .i_uc_port_in       ( uC_gp_input   ),
        .i_uc_port_out      ( uC_gp_output  )
    );

    /*
            ILA Probe               Signal                  Bits
            PROBE_IN0               com_fir_I               11
            PROBE_IN1               com_fir_Q               11
    */

    ILA u_ila (
        .i_clock            ( uC_clock_100  ) ,
        .i_fir_out_I        ( com_fir_I     ) ,
        .i_fir_out_Q        ( com_fir_Q     )     
    );

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign o_leds        = com_leds      ;
    assign o_leds_rgb[0] = uC_lock_clock ;
    assign o_leds_rgb[1] = rf_run_log    ;
    assign o_leds_rgb[2] = rf_read_log   ;
    assign o_leds_rgb[3] = mem_state     ;

endmodule