/*
 ************************************************************************************************
 *										Patricio Reus Merlo
 *									patricio.reus.merlo@gmail.com
 *
 * Filename		: main.c
 * Programmer(s): Patricio Reus Merlo
 * Created on	: 30 Aug. 2021
 * Description 	: tp7 of digital design
 ************************************************************************************************
 */

/*
 ************************************************************************************************
 *								 		INCLUDE FILES
 ************************************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xgpio.h"
#include "platform.h"
#include "xuartlite.h"
#include "microblaze_sleep.h"

/*
 ************************************************************************************************
 *								  PRIVATE CONSTANTS & MACROS
 ************************************************************************************************
 */

/* GPIO ID */
#define PORT_IN	 		XPAR_AXI_GPIO_0_DEVICE_ID
#define PORT_OUT 		XPAR_AXI_GPIO_0_DEVICE_ID

/* FRAME PARAMETERS */
#define FRAMELENGTH 	9
#define HEADLENGTH  	4
#define FRAMEOVERHEAD 	HEADLENGTH + 1

#define FRAMEINIT   	0x05
#define FRAMETAIL   	0x02

#define LSIZEH_OFFSET 	1
#define LSIZEL_OFFSET 	2
#define DEVICE_OFFSET 	3
#define DATA_OFFSET 	4

#define GET_INIT(data)	((data >> 5) & 0x7)
#define GET_TYPE(data)	((data >> 4) & 0x1)
#define GET_SSIZE(data)	((data >> 0) & 0xF)
#define GET_TAIL(data)	((data >> 5) & 0x7)

#define SET_INIT(data)	((data & 0x7) << 5)
#define SET_TYPE(data)	((data & 0x1) << 4)
#define SET_SSIZE(data)	((data & 0xF) << 0)
#define SET_TAIL(data)	((data & 0x7) << 5)

/* Commands */
#define CMD_RST             0x01
#define CMD_EN_TX           0x02
#define CMD_EN_RX           0x03
#define CMD_PAHSE           0x04
#define CMD_RUN_LOG         0x05
#define CMD_READ_LOG        0x06
#define CMD_SET_MEM_ADDR    0x07
#define CMD_GET_MEM_DATA    0x08
#define CMD_GET_MEM_STATUS  0x09
#define CMD_GET_BER_SAMP_I  0x0A
#define CMD_GET_BER_SAMP_Q  0x0B
#define CMD_GET_BER_ERR_I   0x0C
#define CMD_GET_BER_ERR_Q   0x0D
#define CMD_SET_BER_IDX     0x0E


/* Port frame */

/*                                i_input_port                           */
/*     ----------------------------------------------------------        */
/*     |       Command         |     Enable     |      Data     |        */
/*     ----------------------------------------------------------        */
/*     31   					24     23 		22 				0        */

#define POS_ENABLE			23

#define SET_EN_OFF(d)		(d & (~(0x1 << POS_ENABLE)))
#define SET_EN_ON(d)		(d | ( (0x1 << POS_ENABLE)))


/*
 ************************************************************************************************
 *								  NEW DATA TYPE
 ************************************************************************************************
 */

union buffer{
	u32 port_int;
	u8 port_char [4];
};

/*
 ************************************************************************************************
 *								  GLOBAL VARIABLES
 ************************************************************************************************
 */

XGpio 		GpioOutput	  ;
XGpio 		GpioInput	  ;
XGpio 		GpioParameter ;
XUartLite 	uart_module	  ;

u32 		GPO_Value = 0x00000000 ;
u32 		GPO_Param = 0x00000000 ;

/*
 ************************************************************************************************
 *								  FUNCTIONS PROTOTIPES
 ************************************************************************************************
 */

s32 read (s32 fd, char8* buf, s32 nbytes);
char8 receiveFrame (char8 *frame);
char8 sendFrame (u8 *data, u16 bytes, u8 device);


/*
 ************************************************************************************************
 *										MAIN
 ************************************************************************************************
 */

int main()
{
	char8  	frame [FRAMELENGTH];
	char8 *	data  ;
	char8 *	device;

	union buffer buff;

	init_platform();
	XUartLite_Initialize(&uart_module, 0);

	/* Init inputs */
	if(XGpio_Initialize(&GpioInput, PORT_IN) != XST_SUCCESS){
		return XST_FAILURE;
	}

	/* Init outputs*/
	if(XGpio_Initialize(&GpioOutput, PORT_OUT) != XST_SUCCESS){
		return XST_FAILURE;
	}

	/* The order its important, i don't know why, but this runs :D */
	XGpio_SetDataDirection(&GpioOutput, 1, 0x00000000); /* ALL OUTPUT */
	XGpio_SetDataDirection(&GpioInput , 1, 0xFFFFFFFF); /* ALL INPUT  */

	while(1){

		if (receiveFrame((char8*)frame))
		{
			data   = frame + HEADLENGTH;
			device = frame + HEADLENGTH - 1;

			/* Write port */
			buff.port_char[3] = *(data+0);
			buff.port_char[2] = *(data+1);
			buff.port_char[1] = *(data+2);
			buff.port_char[0] = *(data+3);

			XGpio_DiscreteWrite(&GpioOutput,1, (u32)  SET_EN_OFF(buff.port_int));
			XGpio_DiscreteWrite(&GpioOutput,1, (u32)  SET_EN_ON(buff.port_int));
			XGpio_DiscreteWrite(&GpioOutput,1, (u32)  SET_EN_OFF(buff.port_int));

			if (*device == 0){

				/* Read port */
				buff.port_int = XGpio_DiscreteRead(&GpioInput, 1);
				sendFrame(buff.port_char, 4 , 0);
			}
		}
	}
	cleanup_platform();
	return 0;
}

char8 receiveFrame(char8 *frame)
{
	/* Reads a byte, check if a frame init and if it is, read all head */
	if (read((s32)stdin, frame, 1 ) && (GET_INIT(*frame) == FRAMEINIT) && \
			(read((s32)stdin, (frame + 1), HEADLENGTH - 1) == HEADLENGTH -1))
	{
		/* Short frame */
		if (!GET_TYPE(*frame))
		{
			/* Read data and tail fields and then, check tail */
			if ((read((s32)stdin, (frame + HEADLENGTH), GET_SSIZE(*frame) + 1) == GET_SSIZE(*frame) + 1) && \
					(GET_TAIL(*(frame + HEADLENGTH + GET_SSIZE(*frame))) == FRAMETAIL))
			{
				return 1;
			}
		}
		/* Long frame */
		/* Long frame is not supported */
	}

	return 0;
}

char8 sendFrame(u8 *data, u16 bytes, u8 device){
	u8  frame  [FRAMELENGTH];
	u16 idx;

	/* Short frame */
	if (bytes <= 16)
	{
		*frame       = SET_INIT(FRAMEINIT) | SET_TYPE(0) | SET_SSIZE(bytes);
		*(frame + 1) = 0;
		*(frame + 2) = 0;
	}
	/* Long frame */
	else
	{
		*frame  = SET_INIT(FRAMEINIT) | SET_TYPE(1) | SET_SSIZE(0);
		*(frame + LSIZEH_OFFSET) = (uint8_t)((bytes >> 8) & 0xFF);
		*(frame + LSIZEL_OFFSET) = (uint8_t) (bytes & 0xFF);
	}

	/* Device field*/
	*(frame + DEVICE_OFFSET) = device & 0xFF;;

	/* Set data field */
	for (idx = 0; idx < bytes ; idx++)
		*(frame + DATA_OFFSET + idx) = *(data + idx);

	/* Add tail */
	if (bytes <= 16)
		*(frame + HEADLENGTH + bytes) = SET_TAIL(FRAMETAIL) | SET_TYPE(0) | SET_SSIZE(bytes);
	else
		*(frame + HEADLENGTH + bytes) = SET_TAIL(FRAMETAIL) | SET_TYPE(1) | SET_SSIZE(0);

	while(XUartLite_IsSending(&uart_module)){}

	return XUartLite_Send(&uart_module, frame , bytes + FRAMEOVERHEAD);
}

s32 read (s32 fd, char8* buf, s32 nbytes){
  s32 i;
  s32 numbytes = 0;
  char8* LocalBuf = buf;

  (void)fd;
  if(LocalBuf != NULL) {
	for (i = 0; i < nbytes; i++) {
		numbytes++;
		*(LocalBuf + i) = inbyte();
	}
  }
  return numbytes;
}
