/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: top.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 01 Aug. 2021
* Description 	: tp5 of digital design - ej2 
************************************************************************************************
*/

module comSystem #(
        
        /* --- System settings --- */
        parameter OVRSAMPLE = 4         ,
        
        /* --- Control settings --- */
        parameter NB_COUNTER = 2        ,

        /* --- PRBS settings --- */
        parameter PRBS_ORDER   = 9      ,
        parameter PRBS_IMPULSE = 0      ,
        parameter PRBS_I_SEED  = 9'h1AA ,
        parameter PRBS_Q_SEED  = 9'h1FE ,

        /* --- FIR  settings --- */
        parameter NTAPS        = 24     ,
        parameter NB_TAPS      = 6      ,
        parameter NBF_TAPS     = 4      ,
        parameter MAPPER       = 1
    )
    (
        /* ----- Inputs ----- */
        input        i_clock            ,
        input        i_reset            , 
        input  [3:0] i_sw               ,

        /* ----- Outputs ---- */
        output [((2 + NB_TAPS) + $clog2(NTAPS / OVRSAMPLE))-1 : 0] o_fir_I,
        output [((2 + NB_TAPS) + $clog2(NTAPS / OVRSAMPLE))-1 : 0] o_fir_Q,
        
        output [64-1:0] o_ber_samp_I  ,
        output [64-1:0] o_ber_samp_Q  ,
        output [64-1:0] o_ber_error_I ,
        output [64-1:0] o_ber_error_Q ,
        
        output [3:0] o_leds
    );
    
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam NB_FIR_OUT = (2 + NB_TAPS) + $clog2(NTAPS / OVRSAMPLE);

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Renamed inputs */
    wire       enable_tx   ;
    wire       enable_rx   ;
    wire [1:0] dwphase     ;

    /* Connections between Instances */
    wire                    control_out    ;

    wire                    prbs_enable    ;
    wire                    prbs_I_out     ;
    wire                    prbs_Q_out     ;

    wire [NB_FIR_OUT-1 : 0] fir_I_out      ;
    wire [NB_FIR_OUT-1 : 0] fir_Q_out      ;

    wire                    dwsample_I_out ;
    wire                    dwsample_Q_out ;

    wire                    slicer_I_out   ;
    wire                    slicer_Q_out   ;

    wire                    checker_enable ;
    wire                    checker_I_out  ;
    wire                    checker_Q_out  ;

    wire [64-1         : 0] errors_count_I ;
    wire [64-1         : 0] bits_count_I   ;
    wire [64-1         : 0] errors_count_Q ;
    wire [64-1         : 0] bits_count_Q   ;


    /*  */

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    /* Rename inputs */
    assign enable_tx = i_sw[ 0 ] ;
    assign enable_rx = i_sw[ 1 ] ;
    assign dwphase   = i_sw[3:2] ;

    /* System control */
    control #(
        .NB_COUNTER( NB_COUNTER )
    )
    u_control
    (
        .i_reset ( i_reset     ) ,
        .i_clock ( i_clock     ) ,
        .o_valid ( control_out )
    );

    assign prbs_enable = enable_tx &  control_out;

    /* PRBS I Channel */
    prbs #(
        .PRBSX      ( PRBS_ORDER   ) ,
        .SEED       ( PRBS_I_SEED  ) ,
        .IMPULSE    ( PRBS_IMPULSE )
    )
    u_prbs_I
    (
        .i_enable   ( prbs_enable  ) ,
        .i_reset    ( i_reset      ) ,
        .i_clock    ( i_clock      ) ,
        .o_sequence ( prbs_I_out   )
    );
    
    /* PRBS Q Channel */
    prbs #(
        .PRBSX      ( PRBS_ORDER   ) ,
        .SEED       ( PRBS_Q_SEED  ) ,
        .IMPULSE    ( PRBS_IMPULSE )
    )
    u_prbs_Q
    (
        .i_enable   ( prbs_enable  ) ,
        .i_reset    ( i_reset      ) ,
        .i_clock    ( i_clock      ) ,
        .o_sequence ( prbs_Q_out   )
    );

    /* FIR I Channel */
    fir #(
        .OVRSAMPLE  ( OVRSAMPLE   ) ,
        .NTAPS      ( NTAPS       ) ,
        .NB_TAPS    ( NB_TAPS     ) ,
        .NBF_TAPS   ( NBF_TAPS    ) , 
        .MAPPER     ( MAPPER      )
    )
    u_fir_I
    (
        .i_in       ( prbs_I_out  ) ,
        .i_enable   ( control_out ) ,
        .i_clock    ( i_clock     ) ,
        .i_reset    ( i_reset     ) ,
        .o_out      ( fir_I_out   )
    );

    /* FIR Q Channel */
    fir #(
        .OVRSAMPLE  ( OVRSAMPLE   ) ,
        .NTAPS      ( NTAPS       ) ,
        .NB_TAPS    ( NB_TAPS     ) ,
        .NBF_TAPS   ( NBF_TAPS    ) , 
        .MAPPER     ( MAPPER      )
    )
    u_fir_Q
    (
        .i_in       ( prbs_Q_out  ) ,
        .i_enable   ( control_out ) ,
        .i_clock    ( i_clock     ) ,
        .i_reset    ( i_reset     ) ,
        .o_out      ( fir_Q_out   )
    );

    /* Slicer I */
    slicer #()
    u_slicer_I
    (
        .i_in   ( fir_I_out[NB_FIR_OUT-1] ),
        .o_out  ( slicer_I_out            )
    );

    /* Slicer Q */
    slicer #()
    u_slicer_Q
    (
        .i_in   ( fir_Q_out[NB_FIR_OUT-1] ),
        .o_out  ( slicer_Q_out            )
    );

    /* Downsampler I */
    dwsampler #(
        .OVRSAMPLE (   OVRSAMPLE   )
    )
    u_dwsampler_I
    (
        .i_in      ( slicer_I_out  ) ,
        .i_ctrl    ( dwphase       ) ,
        .i_valid   ( control_out   ) ,
        .i_clock   ( i_clock       ) ,
        .i_reset   ( i_reset       ) ,
        .o_out     ( dwsample_I_out)
    );

    /* Downsampler Q */
    dwsampler #(
        .OVRSAMPLE (   OVRSAMPLE   )
    )
    u_dwsampler_Q
    (
        .i_in      ( slicer_Q_out  ) ,
        .i_ctrl    ( dwphase       ) ,
        .i_valid   ( control_out   ) ,
        .i_clock   ( i_clock       ) ,
        .i_reset   ( i_reset       ) ,
        .o_out     ( dwsample_Q_out)
    );


    assign checker_enable = enable_rx &  control_out;

    /* BER counter I */
    prbs_checker #(
        .N_PRBS         (   PRBS_ORDER   )
    )
    u_prbs_checker_I
    (
        .i_clock        ( i_clock        ) ,
        .i_reset        ( i_reset        ) ,
        .i_enable       ( checker_enable ) ,
        .i_ref_bit      ( prbs_I_out     ) ,
        .i_bit          ( dwsample_I_out ) ,
        .o_errors_count ( errors_count_I ) ,
        .o_bits_count   ( bits_count_I   ) ,
        .o_led          ( checker_I_out  )
    );

    /* BER counter Q */
    prbs_checker #(
        .N_PRBS         ( PRBS_ORDER     )
    )
    u_prbs_checker_Q
    (
        .i_clock        ( i_clock        ) ,
        .i_reset        ( i_reset        ) ,
        .i_enable       ( checker_enable ) ,
        .i_ref_bit      ( prbs_Q_out     ) ,
        .i_bit          ( dwsample_Q_out ) ,
        .o_errors_count ( errors_count_Q ) ,
        .o_bits_count   ( bits_count_Q   ) ,
        .o_led          ( checker_Q_out  )
    );

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    /* To leds */
    assign o_leds[0]  = enable_tx     ;
    assign o_leds[1]  = enable_rx     ;
    assign o_leds[2]  = checker_I_out ;
    assign o_leds[3]  = checker_Q_out ;

    /* To memory */
    assign o_fir_I    = fir_I_out ;
    assign o_fir_Q    = fir_Q_out ;

    /* To Register file */
    assign o_ber_error_I = errors_count_Q ;
    assign o_ber_samp_I  = bits_count_Q   ;
    assign o_ber_error_Q = errors_count_I ;
    assign o_ber_samp_Q  = bits_count_I   ;

endmodule