/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: fir.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 21 Jul. 2021
* Description 	: tp5 of digital design - ej2 
************************************************************************************************
*/

/*
************************************************************************************************
*									      fir()
*
* Description 	: implements a fir filter
* File		    : fir.v
* Notes			: ...
*
************************************************************************************************
*/

module fir #(
        parameter OVRSAMPLE = 4  ,
        parameter NTAPS     = 24 ,
        parameter NB_TAPS   = 6  ,
        parameter NBF_TAPS  = 4  ,
        parameter MAPPER    = 1
    )
    (
        /* ----- Inputs ----- */
        input  i_in     ,
        input  i_enable  ,
        input  i_clock  ,
        input  i_reset  , 

        /* ----- Outputs ---- */
        output [((2 + NB_TAPS) + $clog2(NTAPS / OVRSAMPLE))-1 : 0] o_out

    );
    
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam FILTERLENGTH   = NTAPS / OVRSAMPLE ;
    localparam MULLENGTH      = (2+NB_TAPS)       ;
    localparam SUMLENGTH      =  MULLENGTH + $clog2(FILTERLENGTH);
    localparam COUNTERLENGTH  = $clog2(OVRSAMPLE) ;
    
/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Input buffer */
    reg         [FILTERLENGTH -1: 0] buffer                        ;
    
    /* Mapper output */
    reg  signed [1              : 0] symbols  [0 : FILTERLENGTH-1] ;

    /* Taps MUX counter */
    reg         [COUNTERLENGTH-1: 0] counter                       ;

    /* Products */
    reg  signed [MULLENGTH    -1: 0] products [0 : FILTERLENGTH-1] ;

    /* Sum */
    reg signed  [SUMLENGTH    -1: 0] sums                          ;

    /* Filter taps */
    wire signed [NB_TAPS-1      : 0] taps     [0:OVRSAMPLE-1][0:FILTERLENGTH-1] ;
    `include "taps.v"

    /* Loop indices */
    integer i ;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    /* Input buffer */
    always @(posedge i_clock or posedge i_reset) begin
        if (i_reset)
            buffer   <= {FILTERLENGTH{1'b0}};
        else begin
            if (i_enable)
                buffer <= {buffer[FILTERLENGTH -2:0], i_in};
            else
                buffer <= buffer;
        end
    end

    /* Mapper MUX */
    always @(*) begin
        for (i = 0; i < FILTERLENGTH; i = i + 1)
            if (MAPPER)
                symbols[i] = $signed({~buffer[i], 1'b1}); /* Map 0 to 11 and 1 to 01 */
            else
                symbols[i] = $signed({1'b0, buffer[i] }); /* Map 0 to 00 and 1 to 01 */
    end
    
    /* Taps counter */
    always @(posedge i_clock or posedge i_reset) begin
        if (i_reset || i_enable)
            counter <= {COUNTERLENGTH{1'b0}};
        else 
            counter <= counter + {{COUNTERLENGTH-1{1'b0}}, 1'b1};
    end

    /* Taps MUX and product */
    always @(*) begin
        for (i = 0; i < FILTERLENGTH; i = i + 1) begin
            products[i] = $signed(symbols[i]) * $signed(taps[counter][i]);
        end
    end

    /* Sums */
    always @(*) begin
        sums = 0;
        for (i = 0; i < FILTERLENGTH; i = i + 1) begin
            sums = sums + products[i];
        end
    end    
/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/
    
    assign o_out = sums; 

endmodule