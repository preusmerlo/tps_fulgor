# Trabajo Práctico Nº 7: Integración DSP y Microprocesador

---


## Objetivo

El objetivo es integrar el bloque DSP desarrollado en el tp5 con el microprocesador utilizado en el tp6. Se busca que el mini sistema de comunicaciones  sea controlado desde el microprocesador usando como interfaz entre estos dos bloques un Register File (RF).

<img src="img/diagrama.png" alt="rf_diagram" width="800"/>


El Register File consiste en un bloque de lógica que recibe comandos de 32bits desde el uP, los decodifica y en función de si se trata de un comando de escritura o lectura, actúa sobre el DSP o extrae información desde el mismo, respectivamente.

<img src="img/diagrama_2.png" alt="rf_diagram" width="400"/>

---

## Desarrollo

El problema planteado se resolvió siguiendo las siguientes etapas:

  1. Implementación del RF y test.
  2. Corrección del DSP (cantidad de bits de salida del filtro).
  3. Integración RF + uP + DSP y test.
  5. Desarrollo de Firmware.
  6. Desarrollo de Python - uP.
  7. Integración de la memoria.
  8. Ajustes del Firmware y la interfaz.
  9. Desarrollo de script de post-procesamiento.

### Register File 

Este bloque es el encargado de interactuar con el uP. Para tal fin, cuenta con un puerto de escritura y otro de lectura de 32bits que se conectan a los GPIO de uP. Este último envía comandos al register file con la siguiente estructura:
 
<img src="img/frame.png" alt="register_file" width="400"/>

Para que un comando sea detectado por el register, el uP debe escribirlo en el GPIO con `E = 0` y luego debe invertir el bit de enable `E = 1` para generar un flanco positivo. En ese instante el comando es procesado por el bloque. Por último, volver a escribir el comando con `E = 0`. El circuito detector de flanco implementado es:

<img src="img/edge_detector.png" alt="register_file" width="800"/>


La descripción de este bloque se encuentra en `src/registerFile.v`. El resultado es el siguiente:

<img src="img/register_file.png" alt="register_file" width="800"/>

---

### Register File, MicroBlaze, DSP y VIO

Con el register file terminado y el DSP corregido, se interconectaron estos módulos junto al microprocesador para desarrollar el firmware y al VIO para debuggear el sistema. 

#### MicroBlaze

<img src="img/micro.png" alt="micro" width="800"/>

#### DSP

<img src="img/dsp.png" alt="dsp" width="800"/>

#### Conjunto RF + uP + DSP + VIO

<img src="img/reg_dsp_vio_up.png" alt="register_file" width="800"/>

El firmware consta principalmente de dos funciones encargadas de entramar y desentramar la comunicación con la aplicación en Python y un bloque condicional que decodifica los comandos. El listado de comandos soportados por el register file se listan a continuación:

```bash
    Descripcion     Tipo    Comando      Datos
    --------------------------------------------
    reset            set     0x01        1 o 0
    enable_tx        set     0x02        1 o 0
    enable_rx        set     0x03        1 o 0
    phase            set     0x04        0 a 3
    run_log          set     0x05        1 o 0
    read_log         set     0x06        1 o 0
    mem_addr         set     0x07        0 a 2**13
    mem_data         get     0x08         ---
    mem_status       get     0x09         ---
    ber_samp_i       get     0x0A         ---
    ber_samp_q       get     0x0B         ---
    ber_err_i        get     0x0C         ---
    ber_err_q        get     0x0D         ---
    ber_idx          set     0x0E        1 o 0
```

Los comandos ***set*** y ***get*** se diferencian en el campo *device* de la trama. Cuando se trata de un comando setter se envía `device = 0` mientras que para un comando getter se utiliza `device=1`.


 La esencia del programa del microporcesador se resume en las siguientes líneas:

```C
...
union buffer{
	u32 port_int;
	u8 port_char [4];
};
...

void main(void){
...
  union buffer buff;
...
  while(1){
    if (receiveFrame((char8*)frame)) {
      data   = frame + HEADLENGTH;
      device = frame + HEADLENGTH - 1;

      /* Write port */
      buff.port_char[3] = *(data+0);
      buff.port_char[2] = *(data+1);
      buff.port_char[1] = *(data+2);
      buff.port_char[0] = *(data+3);

      XGpio_DiscreteWrite(&GpioOutput,1, (u32)  SET_EN_OFF(buff.port_int));
      XGpio_DiscreteWrite(&GpioOutput,1, (u32)  SET_EN_ON(buff.port_int));
      XGpio_DiscreteWrite(&GpioOutput,1, (u32)  SET_EN_OFF(buff.port_int));

      if (*device == 0){
          /* Read port */
          buff.port_int = XGpio_DiscreteRead(&GpioInput, 1);
          sendFrame(buff.port_char, 4 , 0);
      }
    }
  }
}
...
```

Como puede verse, el microporcesador recibe y verifica que los bytes de entrada conformen una trama completa con `receiveFrame()`, luego escribe en el puerto de salida el comando recibido haciendo uso de la unión `buffer`. Las funciones in-line `SET_EN_OFF()` y `SET_EN_ON()` toman el bit 23 y lo ponen a cero o uno, respectivamente.

El script `py/tp7_test.py` permite enviar estos comandos en crudo al microprocesador para evaluar el comportamiento de este y el register file. El script recibe como parámetro el puerto en el que está conectada la FPGA en cuestión:

<img src="img/test.png" alt="register_file" width="800"/>

La lectura de BER requiere un bus de 64bits mientras que el uP solo tiene 32bits para leer información desde el RF. Debido a ello, el RF posee un registro que en función de su estado, multiplexa la parte alta de las líneas provenientes del ber checker o la parte baja. La función del comando `ber_idx` es definir el estado de este registro de 1bit. El proceso de lectura es el siguiente:
 
 ```C
  << set ber_idx low
  << get ber_samp_i
  >> xxxx
  << get ber_err_i
  >> xxxx
  << set ber_idx high
  << get ber_samp_i
  >> xxxx
  << get ber_err_i
  >> xxxx
 ```

### Memoria, ILA e interfaz Python-uP

Luego de que todo lo anterior funcione, se agregó la memoria que almacena la salida de los filtros I y Q. Para verificar que el DSP funciona correctamente (y para implementar algo nuevo) se incorporó el ILA. Esto permitió verificar que los datos a la entrada de la memoria eran los esperados.  

<img src="img/mem_data.png" alt="vio_ila" width="800"/>

Esta etapa incorpora la interfaz en python (`py/tp7_app.py`) que permite interactuar con el microprocesador sin tener que enviar los comandos uno por uno:

<img src="img/menu.png" alt="python_microblaze" width="800"/>

Al ejecutar los comandos `get ber_i`, `get ber_q` o `get mem_data` el script envía al procesador todos los comandos necesarios para calcular ber, lanzar la lectura de datos en la memoria y luego extraer los datos de la misma. Los datos enviados desde la memoria se almacenan en dos archivos `.log` y luego, con el script `py/tp7_plot.py`, se obtiene el siguiente plot:

<img src="img/mem_plots.svg" alt="curva_memoria" width="800"/>

### Esquema completo

<img src="img/all.png" alt="all" width="800"/>


![videito](video/test.mp4)