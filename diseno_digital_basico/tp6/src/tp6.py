#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: tp6.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 17 Aug. 2021
# Description 	: tp6 of digital design 
######################################################################

######################################################################
#                               MODULES
######################################################################

import argparse

from framer import framerMarcos as fmr
import serial
import time

######################################################################
#                               MAIN
######################################################################

def main():

    # ---------- Script args ----------
    parser = argparse.ArgumentParser()
    parser.add_argument( "port" , help = "FPGA ttyUSBX" )
    args   = parser.parse_args()
    # ---------------------------------

    # ---------- Serial port ----------
    tty = serial.Serial(
            port     = args.port            ,
            baudrate = 115200               ,
            parity   = serial.PARITY_NONE   ,
            stopbits = serial.STOPBITS_ONE  ,
            bytesize = serial.EIGHTBITS
        )

    if tty.isOpen() == False:
        raise Exception('Serial port is not open.')
    # ---------------------------------

    # ------------ Framer -------------
    framer = fmr.framerMarcos(framerPort=tty)
    # ---------------------------------

    # -------- Tablas de Hash  --------
    id    = {               # Bits
                '0' : 0 ,   # 00
                '1' : 1 ,   # 01
                '2' : 2 ,   # 10
                '3' : 3     # 11
            }
    
    color = {               # Bits
                'R' : 4 ,   # 0100
                'G' : 8 ,   # 1000
                'B' : 12    # 1100
            }

    state = {               # Bits
                '0' : 0 ,   # 00000
                '1' : 16    # 10000
            }
    # ---------------------------------

    # ------------- Menu --------------
    print('Para prender leds enviar:')
    print('\t$ <  led ID >< color >< on/off >')
    print('\t$ <[0 1 2 3]><[R G B]>< [1 0]  >')
    print('Para leer estado de los switch enviar:')
    print('\t$ <switch ID>')
    print('\t$ <[0 1 2 3]>')
    print('Exit para salir')
    # ---------------------------------

    while True :
        
        inputData = input("<< ")
        
        #  Close script
        if str.lower(inputData) == 'exit':
            tty.close()
            exit()

        else:
            inputData = list(inputData)
            
            # Write (device frame filed = 1)
            if len(inputData) == 3:
                data   = id[inputData[0]] | color[inputData[1]] | state[inputData[2]]
                framer.send(data, device = 1)
                dataframeTX = framer.getAttribute('dataFrame')
                print('Enviado  :{}'.format(dataframeTX))


            # Read (device filed = 1)
            elif len(inputData) == 1:
                framer.send(id[inputData[0]], device = 0)
                time.sleep(1)
                framer.receive()
                dataframeRX = framer.getAttribute('dataFrame')
                print('Recibido :{}'.format(dataframeRX))
                data = framer.decode(type='list')
                print('>> {}'.format(data))

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()