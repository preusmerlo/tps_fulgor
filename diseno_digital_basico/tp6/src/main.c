/*
 ************************************************************************************************
 *										Patricio Reus Merlo
 *									patricio.reus.merlo@gmail.com
 *
 * Filename		: main.c
 * Programmer(s): Patricio Reus Merlo
 * Created on	: 16 Aug. 2021
 * Description 	: tp6 of digital design
 ************************************************************************************************
 */

/*
 ************************************************************************************************
 *								 		INCLUDE FILES
 ************************************************************************************************
 */

#include <stdio.h>
#include <string.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xgpio.h"
#include "platform.h"
#include "xuartlite.h"
#include "microblaze_sleep.h"

/*
 ************************************************************************************************
 *								  PRIVATE CONSTANTS & MACROS
 ************************************************************************************************
 */

#define PORT_IN	 		XPAR_AXI_GPIO_0_DEVICE_ID
#define PORT_OUT 		XPAR_AXI_GPIO_0_DEVICE_ID

/* FRAME PARAMETERS */
#define FRAMELENGTH 	6
#define HEADLENGTH  	4
#define FRAMEOVERHEAD 	HEADLENGTH + 1

#define FRAMEINIT   	0x05
#define FRAMETAIL   	0x02

#define LSIZEH_OFFSET 	1
#define LSIZEL_OFFSET 	2
#define DEVICE_OFFSET 	3
#define DATA_OFFSET 	4

#define GET_INIT(data)	((data >> 5) & 0x7)
#define GET_TYPE(data)	((data >> 4) & 0x1)
#define GET_SSIZE(data)	((data >> 0) & 0xF)
#define GET_TAIL(data)	((data >> 5) & 0x7)

#define SET_INIT(data)	((data & 0x7) << 5)
#define SET_TYPE(data)	((data & 0x1) << 4)
#define SET_SSIZE(data)	((data & 0xF) << 0)
#define SET_TAIL(data)	((data & 0x7) << 5)

/* 		  ID	BIT MASK			RTL 	       MICRO GPIO	*/
#define LED0B 	0x00000001	/* o_leds_rgb0[0] 		gpo0[ 0]	*/
#define LED0G 	0x00000002	/* o_leds_rgb0[1] 		gpo0[ 1]	*/
#define LED0R 	0x00000004	/* o_leds_rgb0[2] 		gpo0[ 2]	*/

#define LED1B 	0x00000008	/* o_leds_rgb1[0] 		gpo0[ 3]	*/
#define LED1G 	0x00000010	/* o_leds_rgb1[1] 		gpo0[ 4]	*/
#define LED1R 	0x00000020	/* o_leds_rgb1[2] 		gpo0[ 5]	*/

#define LED2B 	0x00000040	/* o_leds_rgb2[0] 		gpo0[ 6]	*/
#define LED2G 	0x00000080	/* o_leds_rgb2[1] 		gpo0[ 7]	*/
#define LED2R 	0x00000100	/* o_leds_rgb2[2] 		gpo0[ 8]	*/

#define LED3B 	0x00000200	/* o_leds_rgb3[0] 		gpo0[ 9]	*/
#define LED3G 	0x00000400	/* o_leds_rgb3[1] 		gpo0[10]	*/
#define LED3R 	0x00000800	/* o_leds_rgb3[2] 		gpo0[11]	*/

#define SW0 	0x00000001	/* i_sw[0] 				gpi0[0]		*/
#define SW1		0x00000002	/* i_sw[1] 				gpi0[1]		*/
#define SW2		0x00000004	/* i_sw[2] 				gpi0[2]		*/
#define SW3		0x00000008	/* i_sw[3] 				gpi0[3]		*/

/* Data codification */
#define D_LED0B	12
#define D_LED0G	8
#define D_LED0R	4
#define D_LED1B	13
#define D_LED1G	9
#define D_LED1R	5
#define D_LED2B	14
#define D_LED2G	10
#define D_LED2R	6
#define D_LED3B	15
#define D_LED3G	11
#define D_LED3R	7

#define D_SW0	0
#define D_SW1 	1
#define D_SW2	2
#define D_SW3	3


#define GET_ID(data)	(data & 0xF)
#define GET_STATE(data)	((data >> 4) & 0x1)
/*
 ************************************************************************************************
 *								  GLOBAL VARIABLES
 ************************************************************************************************
 */

XGpio 		GpioOutput	  ;
XGpio 		GpioInput	  ;
XGpio 		GpioParameter ;
XUartLite 	uart_module	  ;

u32 		GPO_Value = 0x00000000 ;
u32 		GPO_Param = 0x00000000 ;

/*
 ************************************************************************************************
 *								  FUNCTIONS PROTOTIPES
 ************************************************************************************************
 */

unsigned char receiveFrame(unsigned char *frame);
unsigned char sendFrame(unsigned char *data, uint16_t bytes, uint8_t device);

/*
 ************************************************************************************************
 *										MAIN
 ************************************************************************************************
 */

int main()
{
	unsigned char  	frame [FRAMELENGTH];
	unsigned char *	data  ;
	unsigned char *	device;

	unsigned char	aux   = 0;
	uint32_t		mask  ;

	init_platform();
	XUartLite_Initialize(&uart_module, 0);

	/* Init inputs */
	if(XGpio_Initialize(&GpioInput, PORT_IN) != XST_SUCCESS){
		return XST_FAILURE;
	}

	/* Init outputs*/
	if(XGpio_Initialize(&GpioOutput, PORT_OUT) != XST_SUCCESS){
		return XST_FAILURE;
	}

	XGpio_SetDataDirection(&GpioInput , 1, 0xFFFFFFFF); /* ALL INPUT  */
	XGpio_SetDataDirection(&GpioOutput, 1, 0x00000000); /* ALL OUTPUT */

	while(1){


		if (receiveFrame(frame))
		{
			data   = frame + HEADLENGTH;
			device = frame + HEADLENGTH - 1;

			/* Write leds */
			if (*device)
			{
				switch (GET_ID(*data)){
					case D_LED0B: mask = LED0B; break;
					case D_LED0G: mask = LED0G; break;
					case D_LED0R: mask = LED0R; break;
					case D_LED1B: mask = LED1B; break;
					case D_LED1G: mask = LED1G; break;
					case D_LED1R: mask = LED1R; break;
					case D_LED2B: mask = LED2B; break;
					case D_LED2G: mask = LED2G; break;
					case D_LED2R: mask = LED2R; break;
					case D_LED3B: mask = LED3B; break;
					case D_LED3G: mask = LED3G; break;
					case D_LED3R: mask = LED3R; break;
				}

				/* ON */
				if(GET_STATE(*data))
					XGpio_DiscreteWrite(&GpioOutput,1, (u32)  mask | XGpio_DiscreteRead(&GpioOutput, 1));
				/* OFF */
				else
					XGpio_DiscreteWrite(&GpioOutput,1, (u32) ~mask & XGpio_DiscreteRead(&GpioOutput, 1));
			}

			/* Read sw */
			else
			{
				switch (GET_ID(*data)){
					case D_SW0: mask = SW0 ; break;
					case D_SW1: mask = SW1 ; break;
					case D_SW2: mask = SW2 ; break;
					case D_SW3: mask = SW3 ; break;
				}

				aux = (unsigned char)((XGpio_DiscreteRead(&GpioInput, 1) & mask) >> GET_ID(*data));
				sendFrame(&aux, (uint16_t) 1 , (uint8_t) 0);
			}
		}
	}

	cleanup_platform();
	return 0;
}

unsigned char receiveFrame(unsigned char *frame)
{
	/* Reads a byte, check if a frame init and if it is, read all head */
	if (read(stdin,frame, 1 ) && (GET_INIT(*frame) == FRAMEINIT) && \
			(read(stdin, (frame + 1), HEADLENGTH - 1) == HEADLENGTH -1))
	{
		/* Short frame */
		if (!GET_TYPE(*frame))
		{
			/* Read data and tail fields and then, check tail */
			if ((read(stdin, (frame + HEADLENGTH), GET_SSIZE(*frame) + 1) == GET_SSIZE(*frame) + 1) && \
					(GET_TAIL(*(frame + HEADLENGTH + GET_SSIZE(*frame))) == FRAMETAIL))
			{
				return 1;
			}
		}
		/* Long frame */
		/* Long frame is not suported */
	}

	return 0;
}

unsigned char sendFrame(unsigned char *data, uint16_t bytes, uint8_t device){
	uint8_t  frame  [FRAMELENGTH];
	uint16_t idx;

	/* Short frame */
	if (bytes <= 16)
	{
		*frame       = SET_INIT(FRAMEINIT) | SET_TYPE(0) | SET_SSIZE(bytes);
		*(frame + 1) = 0;
		*(frame + 2) = 0;
	}
	/* Long frame */
	else
	{
		*frame  = SET_INIT(FRAMEINIT) | SET_TYPE(1) | SET_SSIZE(0);
		*(frame + LSIZEH_OFFSET) = (uint8_t)((bytes >> 8) & 0xFF);
		*(frame + LSIZEL_OFFSET) = (uint8_t) (bytes & 0xFF);
	}

	/* Device field*/
	*(frame + DEVICE_OFFSET) = device & 0xFF;;

	/* Set data field */
	for (idx = 0; idx < bytes ; idx++)
		*(frame + DATA_OFFSET + idx) = *(data + idx);

	/* Add tail */
	if (bytes <= 16)
		*(frame + HEADLENGTH + bytes) = SET_TAIL(FRAMETAIL) | SET_TYPE(0) | SET_SSIZE(bytes);
	else
		*(frame + HEADLENGTH + bytes) = SET_TAIL(FRAMETAIL) | SET_TYPE(1) | SET_SSIZE(0);

	while(XUartLite_IsSending(&uart_module)){}

	return XUartLite_Send(&uart_module, frame , bytes + FRAMEOVERHEAD);
}
