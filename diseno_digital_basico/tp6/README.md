# Trabajo Práctico Nº 6: Plataforma de verificación.

El presente trabajo tiene como finalidad utilizar el MicroBlaze de Xilinx para controlar LEDs y leer el estado de los switch de la placa. Un diagrama del sistema se ilustra a continuacion.

![system](img/diagrama.png)

## Instanciacion del MicroBlaze

![micro](img/microblaze.png)

## Instanciacion VIO

![vio](img/vio.png)

## Funcionamiento

Luego de bajar el `bitstream` a la placa y de grabar el `.hex` al microcontrolador, es posible modificar el estado de los leds y leer el estado de los pulsadores desde Python. Conociendo el puerto al que está conectada la FPGA ejecutamos:

```bash
$ ./tp6/tp6 /dev/ttyUSB5
```
El resultado:

![script](img/script.png)

La FPGA posee 4 leds RGB que a través de este script pueden controlarse independientemente. Por ejemplo, para poner el led 0 en color rojo:

```bash
<< 0R1
```

Al hacer esto el script nos responde con la trama enviada al microcontrolador a través de la UART:

![script](img/trama.png)

El cambio de estado de los leds se visualiza en el VIO:

![vio_leds](img/led_red.png)

Para leer el estado de los switch simplemente enviar el ID del switch:

```bash
<< 2
```
El microcontrolador lee el estado del switch indicado, construye una trama y la envia a través de la UART. El script imprime la trama recibida y el estado del switch solicitado:

![switch](img/switch.png)

## Archivos fuente

En el directorio `src` se encuentra el archivo `main.c` desarrollado para el MicroBlaze. Este recibe por la UART una trama, la decodifica y en función de si el campo `device` es `0` o `1`, lee los switchs o modifica el estado de los leds, respectivamente.