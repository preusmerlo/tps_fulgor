#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: ej1.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 12 jul. 2021
# Description 	: tp5 of digital design 
######################################################################

######################################################################
#                               MODULES
######################################################################

import os
import numpy as np
import matplotlib.pyplot as plt


import tool._fixedInt as fxd
import tool.DSPtools as dsp
    
######################################################################
#                               MAIN
######################################################################

def main():
    ''' Main function '''

    ### ------------------------ Configurables -----------------------
    # Generales
    BR = 25e6             # Symbol Rate               [Bd]
    N  = 4                # Factor de sobremuestreo
    L  = 1e3              # Cantidad de simbolos a simular (numero par)
    
    # FFT
    NFFT = 256            # Cantidad de puntos de fft
    
    # Pulse shaping
    rolloff = 0.5         # Roll-Off
    NT = 6                # Cantidad de cruces por cero (simbolos) de h(t)
    
    # Mapper
    M  = 2                # Cantidad de niveles del mapper (M = 2^n)

    # RX
    T0 = 1                # Fase de muestreo rx

    # Archivos de salida
    svg   = 1             # Guarda plots en svg    : (0) NO ; (1) SI
    show  = 0             # Ver plots              : (0) NO ; (1) SI
    clear = 0             # Limpiar (rm -rf img/*) : (0) NO ; (1) SI
                          # Usar clear = 1 puede romper el README.md
    ### -----------------------------------------------------------------

    ### --------------- Constantes y configuraciones --------------------
    
    T  = 1 / BR          # Tiempo entre simbolos     [seg]
    fs = N * BR          # Frecuencia de muestreo    [Hz]
    Ts = 1/fs            # Perdiodo de muestreo      [seg]
    
    # Plots
    H_plot = 6            # Alto  minimo de los plots
    W_plot = 6            # Ancho minimo de los plots
    L_size = 20           # Tamano de letra
    img_path = 'img/'     # Directorio de imagenes

    # Chequea si existe directorio de imagenes y si no existe lo crea
    os.system(f'[ ! -d "{img_path}" ] && mkdir -p "{img_path}"')

    # Limpia directorio de plots exportados
    if clear:
        os.system('rm -rf img/*')  
    ### -----------------------------------------------------------------

    ### --------------- Mini sistema de comunicaciones ------------------
    # Genera secuencias de simbolos PAM
    ak_I = dsp.pamm(M,L)
    ak_Q = dsp.pamm(M,L)

    # Sobremuestreo para modelar continua
    ak_up_I = dsp.upsample(ak_I,N)
    ak_up_Q = dsp.upsample(ak_Q,N)

    # Filtro interpolador coseno realzado
    (t, h) = dsp.rcosine(rolloff, T, N, NT, Norm = False)
    filter_delay  = int(len(h)/2)

    # Convolucion con filtro en punto flotante
    tx_I = np.convolve(h,ak_up_I, mode = 'full')
    tx_Q = np.convolve(h,ak_up_Q, mode = 'full')

    # Muestreo
    rx_I = dsp.downsample(tx_I, N, T0)
    rx_Q = dsp.downsample(tx_Q, N, T0)
    
    # Slicer
    rx_I[rx_I >= 0] =  1
    rx_I[rx_I <  0] = -1
    rx_Q[rx_Q >= 0] =  1
    rx_Q[rx_Q <  0] = -1

    # Contador de BER
    errados_I = 0
    errados_Q = 0
    for i in range(int(L)):
        if ak_I[i] != rx_I [i + int(filter_delay/N)]:
            errados_I += 1
        if ak_Q[i] != rx_Q [i + int(filter_delay/N)]:
            errados_Q += 1
    
    SER_I = errados_I / L
    SER_Q = errados_Q / L
    BER_I_aproxx = 1/np.log2(M) * SER_I
    BER_Q_aproxx = 1/np.log2(M) * SER_Q
    print(f'BER I (aprox) = {BER_I_aproxx}')
    print(f'BER Q (aprox) = {BER_Q_aproxx}')

    ### -----------------------------------------------------------------

    
    ### ----- Respuesta al impulso  y en frecuiencia --------------------
    # --- Tiempo ---
    f = plt.figure(figsize = [2*W_plot,H_plot])
    plt.subplot(1,2,1)
    plt.plot(t*1e6,h,'r-o',linewidth = 2, label = r'$\beta = {}$'.format(rolloff))
    
    plt.grid  (linestyle = '--', linewidth = 0.5)
    plt.legend(loc='lower center', shadow = True, ncol = 1,fontsize = L_size)
    
    plt.xlim  (t[0]*1e6,t[-1]*1e6)
    plt.ylim  (-0.3 , 1.1)
    plt.title ('Filtro coseno realzado',fontsize = L_size)
    plt.xlabel('Tiempo [us]',fontsize = L_size)
    plt.ylabel('Amplitud',fontsize = L_size)
    
    # --- Frecuencia ---
    [H,A,F] = dsp.resp_freq(h, Ts, NFFT)
    # Normaliza a 0dB en DC
    H = H / H[0]

    plt.subplot(1,2,2)
    plt.axhline(y = 20*np.log10(0.5),color = 'gray',linewidth = 1, linestyle='-.', label = '- 6dB')
    plt.axvline(x = (1./T)/2.       ,color = 'gray',linewidth = 1, linestyle='--', label = 'BR/2')
    
    plt.semilogx(F, 20*np.log10(H),'r-', linewidth = 2)
    
    plt.legend(loc='lower left', shadow=True, ncol=1,fontsize = L_size)
    plt.grid  (linestyle = '--', linewidth = 0.5)
            
    plt.xlim(F[1],F[-1])
    plt.ylim(-25,5)
    
    plt.title ('Respuesta en frecuencia',fontsize = L_size)
    plt.xlabel('Frequencia [Hz]'        ,fontsize = L_size)
    plt.ylabel('Magnitud [dB]'          ,fontsize = L_size)
    
    if svg:
        f.savefig(f"{img_path}/filtro.svg", bbox_inches='tight')

    ### --------------- Diagramas de OJO -------------------------
    period = 5

    f = plt.figure(figsize = [2*W_plot,H_plot])

    plt.subplot(1,2,1)
    plt.title ('$In - Phase $',fontsize = L_size)
    dsp.eyediagram(tx_I[filter_delay:-filter_delay],N,period,NT, color='r')
    plt.grid(linestyle='--', linewidth=0.5)
    plt.xlabel('Tiempo'  ,fontsize = L_size)
    plt.ylabel('Amplitud',fontsize = L_size)
    
    plt.subplot(1,2,2)
    plt.title ('$Quadrature$',fontsize = L_size)
    dsp.eyediagram(tx_Q[filter_delay:-filter_delay],N,period,NT, color='b')
    plt.grid(linestyle='--', linewidth=0.5)
    plt.xlabel('Tiempo'  ,fontsize = L_size)
    plt.ylabel('Amplitud',fontsize = L_size)

    if svg:
        f.savefig(f"{img_path}/ojos.svg", bbox_inches='tight')


    ### --------------- Constelacion -------------------------
    f = plt.figure(figsize = [2*W_plot,2*H_plot])
    for i in range(N):
        plt.subplot(int(N/2),int(N/2),i + 1)
        x = dsp.downsample(tx_I, N, i) 
        y = dsp.downsample(tx_Q, N, i) 
        plt.scatter(x[filter_delay:-filter_delay], y[filter_delay:-filter_delay], color = 'red', label = f'Fase {i}')
        plt.grid(linestyle='--', linewidth=0.5)
        plt.xlim(-M,M)
        plt.ylim(-M,M)
        plt.legend(loc='lower center', shadow=True, ncol=1,fontsize = L_size)
        # plt.title (f'Fase {i}' ,fontsize = 1.5*L_size)
        plt.xlabel('In - Phase',fontsize = L_size)
        plt.ylabel('Quadrature',fontsize = L_size)

    if svg:
        f.savefig(f"{img_path}/rx_constelaciones.svg", bbox_inches='tight')

    ### --------------- Salida del TX -----------------------
    x_start = 1000
    x_end   = 1100

    f = plt.figure(figsize = [2*W_plot,2*H_plot])

    plt.subplot(2,1,1)
    plt.plot(tx_I,'r-',linewidth = 3)

    for idx in range(x_start - filter_delay ,x_end - filter_delay):
        if(ak_up_I[idx] != 0):
            plt.scatter(idx + filter_delay,ak_up_I[idx], color = 'gray',s = 100)

    plt.xlim(x_start,x_end)
    plt.ylim(-M,M)
    plt.grid(linestyle='--', linewidth=1)
    plt.title ('$In - Phase$',fontsize = 1.5*L_size)
    plt.ylabel('Magnitud',fontsize = L_size)

    plt.subplot(2,1,2)
    plt.plot(tx_Q,'b-',linewidth = 3)

    for idx in range(x_start - filter_delay ,x_end - filter_delay):
        if(ak_up_Q[idx] != 0):
            plt.scatter(idx + filter_delay,ak_up_Q[idx], color = 'gray',s = 100)

    plt.xlim(x_start,x_end)
    plt.ylim(-M,M)
    plt.grid(linestyle='--', linewidth=1)
    plt.title ('$Quadrature$',fontsize = 1.5*L_size)
    plt.xlabel('Muestras',fontsize = L_size)
    plt.ylabel('Magnitud',fontsize = L_size)

    if svg:
        f.savefig(f"{img_path}/tx_banda_base.svg", bbox_inches='tight')
    
    ### -------------------- End ----------------------------

    # Muestra todos los plots
    if show:
        plt.show()

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        # Cierra plots con ctrl + c
        plt.close()