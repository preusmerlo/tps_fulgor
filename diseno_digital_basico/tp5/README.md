# Trabajo Práctico Nº 5: Mini sistema de comunicaciones QPSK

El presente práctico tiene como objetivo modelar el comportamiento de un sistema de comunicaciones digitales básico. El enunciado del práctico se encuentra en `tp5.pdf`.

**Requerimientos**:

- Modulación: QPSK.
- Frecuencia de clock: 100MHz.
- Factor de sobremuestreo: 4.
- Filtro conformador: Coseno Realzado (6 baudios).
- Roll-off: 0.5

![system](img/system.png)

---

## Simulador en punto flotante

Como primera aproximación al sistema a desarrollar, un simulador en punto flotante es de gran utilidad, ya que es el punto de partida a partir de la cual se construye el simulador de punto fijo que modela de cerca al hardware a describir en Verilog. El diagrama en bloques de este simulador es el siguiente:

```mermaid
    graph LR;
    
    PRBS --> MAPPER --> UP --> FIR --> DW --> SLICER --> DEMAPPER --> CHECKER;
    PRBS --> CHECKER;
```

Los plots exportados desde el simulador se muestran a continuación. Estos sirven como ***referencia*** al momento de evaluar la degradación del sistema debida a  la cuantización.

![float_filter](ej1/img/filtro.svg)
![float_eyes  ](ej1/img/ojos.svg)
![float_const ](ej1/img/rx_constelaciones.svg)
![float_tx    ](ej1/img/tx_banda_base.svg)

---

## Simulador en punto fijo

Para convertir el simulador de punto flotante a un simulador de punto fijo es necesario cuantizar la respuesta al impulso del filtro conformador. El formato seleccionado es **S(6,4)**:

![fixed_filter_f](ej2/img/filtro.svg)

En este caso, debido a que el simulador debe representar de cerca al circuito final, el diagrama en bloques es algo diferente:

```mermaid
    graph LR;
    
    PRBS9 --> BUFFER;
    
    subgraph "Filtro + Mapper + Upsampler"
        BUFFER ----> MAPPER_1 --> X1((X));
        H --> MUX1 --> X1((X));

        BUFFER ----> MAPPER_2 --> X2((X));
        H --> MUX2 --> X2((X));
        
        BUFFER ----> MAPPER_N --> XN((X));
        H --> MUX_N --> XN((X));

        X1 --> S((+));
        X2 --> S((+));
        XN --> S((+));
    end

    S --> DWSAMPLER;
    DWSAMPLER --> SLICER;
    SLICER    --> DEMAPPER --> CHECKER;
    PRBS9     --> CHECKER;
    
```

### *PRBS*

Al momento de describir el sistema QPSK en Verilog, será necesario contar con un generador de símbolos *"aleatorios"* que permita evaluar el comportamiento del circuito. Para ello, se utiliza una ***pseudorandom binary sequence*** ([PRBS](https://en.wikipedia.org/wiki/Pseudorandom_binary_sequence)). Una PRBS puede construirse a partir de un registro de desplazamiento realimentado de una manera específica, donde esta *específica* realimentación es la que define el *orden* de la PRBS.  

Este bloque de hardware, al igual que cualquier otro, requiere ser testeado. Una manera de hacerlo es modelando el comportamiento de la PRBS en Python y luego comparar los datos de salida del modelo con los del circuito descrito en HDL.

El siguiente script describe el comportamiento de la PRBS haciendo uso de una clase llamada `ff` que modela flip-flops tipo D:

```python
...
prbs9_I    = flp.ff(9)                                  # PRBS In - phase

prbs9_I.i  = '0x1AA'                                    # Semilla   
prbs9_I.run_clock()                                     # Inicializacion

prbs9_I_out = []                                        # Array de salida

for clock in range(int(L)):                             # L = largo de la simulacion
    prbs9_I_out.append(prbs9_I[8].o)

    prbs9_I[0].i =  prbs9_I[4].o ^ prbs9_I[8].o         # Realimentacion para PRBS9

    for idx in range(1,PRBS_ord):
        prbs9_I[idx].i = prbs9_I[idx-1].o

    # Nuevo clock
    prbs9_I.run_clock()
... 
```

### *Convolución*

El cálculo de la convolución en Python es sumamente simple. Sin embargo, modelar la operación del filtro flop a flop como se hizo con la PRBS permite entender como se resuelve la operación en el hardware. Además, el filtro a construir en Verilog no solo realiza la operación de convolución, sino que también implementa el mapeo de los bits en símbolos y sobremuestreo. El circuito a modelar se observa a continuación:

![diagram](img/diagram.png)

Este circuito es implementado mediante el siguiente script. Aquí `h` es un arreglo que contiene los coeficientes del filtro, `N` es el factor de sobremuestreo (relación entre *slow clock* y *fast clock*) y `symb_A` (+1) y `symb_B` (-1) son los símbolos a los que se mapean los bits de entrada.

```python
...
# Longitud del filtro
conv_len = int((size(h)-1)/N)

# Shift register de convolucion
shift_conv_I = flp.ff(conv_len)

# Arreglo de salida
tx_I = np.array([])
    
for clock in range(int(L)):

    # Shift register
    shift_conv_I[0].i = prbs9_I_out[clock]
    for idx in range(1,conv_len):
        shift_conv_I[idx].i = shift_conv_I[idx-1].o

    # MUX mapper
    mux_out_I = np.array([])
    for i in [shift_conv_I[0].i] + shift_conv_I.o:
        if i > 0:
            mux_out_I = np.append(mux_out_I,symb_A) # Symb_A =+1
        else:
            mux_out_I = np.append(mux_out_I,symb_B) # Symb_B =-1
    
    # Productos y sumas
    for i in range(N):
        aux_I = 0
                    
        for j in range(conv_len):
            aux_I = aux_I + (mux_out_I[j] * h[i + j * N]).fValue
            
        tx_I = np.append(tx_I,aux_I)
        
    # Nuevo clock
    shift_conv_I.run_clock() 
...
```

A continuación se ilustran los plots obtenidos a partir del simulador en punto fijo construido utilizando los scripts mostrados anteriormente.

![fixed_eyes    ](ej2/img/ojos.svg)

![fixed_const   ](ej2/img/rx_constelaciones.svg)

![fixed_tx      ](ej2/img/tx_banda_base.svg)

---

## Descripción en Verilog

El mini sistema de comunicaciones QPSK descrito en Verilog tiene el siguiente diagrama en bloques:

![hdl_all](ej2/img/all.png)

### *Bloque de control*

Parte del sistema funciona a la frecuencia del `clock` de la FPGA (`100MHz`) y otra parte funciona `N` veces más lento. En este caso, `N=4` por lo que la parte lenta del sistema funciona a `25MHz`. Una forma de implementar esto es utilizando la entrada de ***chip select*** que poseen los flip-flops de la FPGA, si los flops se habilitan cada 4 periodos de reloj, los cambios en estos se efectúan cada 4 clocks del sistema y el efecto resultante simula un reloj cuatro veces más lento.

El circuito es el siguiente:

![control](ej2/img/control.png)

La salida de este bloque es una señal de *enable* o *valid* que copia al reloj del sistema cada 4 periodos:

![control_verilog](img/control_verilog.png )

### *PRBS9*

Se describió una PRBS de orden parametrizable. Los órdenes soportados son (según Wikipedia) los más utilizados: 7, 9, 11, 15, 20, 23 y 31. Para esta aplicación, una *PRBS9*:

![prbs](ej2/img/prbs.png)

El módulo PRBS soporta además un modo *impulso*  que genera a la salida un impulso. Este modo  fue sumamente útil a la hora de verificar el diseño del filtro.

#### *Modo impulso*

![prbs_impulso_verilog](img/prbs_impulso_verilog.png)

#### *Modo secuencia*

![secuencia_verilog](img/prbs_secuencia_verilog.png)

### *Filtro + Mapper + Upsampler*

Este bloque implementa gran parte del *"sistema"* de comunicaciones. Es filtro, mapper y upsampler simultáneamente, a continuación se evaluará por partes:

#### *Registro de desplazamiento y contador*

En primer lugar, el registro de desplazamiento de entrada. El filtro implementado cuenta con un registro de desplazamiento de 6 flops, que podrían ser 5, pero a fin de simplificar el indexado de la descripción en Verilog, se agregó esta latencia extra:

```verilog
...
/* Input buffer */
always @(posedge i_clock or posedge i_reset) begin
    if (i_reset)
        buffer   <= {FILTERLENGTH{1'b0}};
    else begin
        if (i_enable)
            buffer <= {buffer[FILTERLENGTH -2:0], i_in};
        else
            buffer <= buffer;
    end
end
...
```

El resultado:

![fir_2         ](ej2/img/fir_2.png)

Junto al registro de entrada se observa un contador de módulo 4 (2 bits) que controla los multiplexores que seleccionan los coeficientes del filtro. La particularidad de este contador es que vuelve a `00` cada vez que la entrada `i_enable` se pone en uno. De este modo, siempre que entra un nuevo dato se selecciona el primer coeficiente de cada multiplexor.

```verilog
...
/* Taps counter */
always @(posedge i_clock or posedge i_reset) begin
    if (i_reset || i_enable)
        counter <= {COUNTERLENGTH-1{1'b0}};
    else 
        counter <= counter + {{COUNTERLENGTH-2{1'b0}}, 1'b1};
end
...
```

![fir_1_verilog ](img/fir_1_verilog.png)

#### *Mapper*

A la salida de la PRBS hay  bits mientras que a la entrada del filtro deben existir símbolos por lo que es necesario *mapear* la salida de la PRBS. De esto se encargan las compuertas `NOT`:

![fir_3         ](ej2/img/fir_3.png)

Los ceros se mapean en `11` mientras que los unos  en `01`, el bit MSB a la salida del mapper es la entrada negada mientras que el  bit LSB siempre permanece en `1`. En Verilog:

```verilog
...
/* Mapper MUX */
always @(*) begin
    for (i = 0; i < FILTERLENGTH; i = i + 1)
        if (MAPPER)
            symbols[i] = $signed({~buffer[i], 1'b1}); /* Map 0 to 11 and 1 to 01 */
        else
            symbols[i] = $signed({1'b0, buffer[i] }); /* Map 0 to 00 and 1 to 01 */
end
...
```

En conjunto con el modo *impulso* de la PRBS, el parámetro `MAPPER` del filtro permite agregar, o no, el efecto del mapper. Para chequear que el filtro funciona correctamente, el mapper se desactiva y la PRBS se pone en modo impulso, el resultado a la salida del filtro deben ser los coeficientes del mismo perfectamente ordenados.

En esta simulación puede verse como el primer pulso que recibe el filtro atraviesa el buffer de entrada y es mapeado sucesivamente.

![fir_2_verilog ](img/fir_2_verilog.png)

#### *Productos y sumas*

Los coeficientes del filtro son exportados desde Python y se almacenan en forma de memoria en un `.v` que luego es importado por el módulo que implementa el filtro. A partir de esta memoria se multiplexan los coeficientes y se multiplican por los símbolos obtenidos a la salida del mapper:

```verilog
...
/* Taps MUX and product */
always @(*) begin
    for (i = 0; i < FILTERLENGTH; i = i + 1) begin
        products[i] = $signed(symbols[i]) * $signed(taps[counter][i]);
    end
end
...
```

![fir_4         ](ej2/img/fir_4.png)

Como se resalta en azul en la captura anterior, todos los productos se suman en un arbol de suma que calcula el resultado final de la convolución. Este se implementa como sigue:

```verilog
...
/* Sums */
always @(*) begin
    sums = 0;
    for (i = 0; i < FILTERLENGTH; i = i + 1) begin
        sums = sums + products[i];
    end
end  
...

```

El resultado:

![fir_3_verilog ](img/fir_3_verilog.png)

### *Slicer + Demapper*

A la salida del filtro se obtiene una palabra en formato **S(13,4)** de la cual solo se utiliza el MSB para definir si se trata de un símbolo `+1` o `-1`. Cuando el MSB es uno, el símbolo del que proviene es `-1` y este, a su vez, fue mappeado a partir de un cero. Es decir, el bit correspondiente al símbolo de entrada del *slicer + demapper* se obtiene simplemente invirtiendo el bit de signo.  

![slicer        ](ej2/img/slicer.png)

Esta operación, teóricamente, se realiza después del downsample. Sin embargo,  en este caso es menos costoso hacerla antes y el resultado no se ve afectado.

### *Downsampler*

Ahora el downsampler simplemente come bits y debe seleccionar el bit correspondiente a la fase de muestreo correcta para recuperar la salida de la PRBS. Para ello, se construye un registro de desplazamiento donde la salida de cada flop entra a un multiplexor que permite seleccionar una de las posibles cuatro fases de muestreo. La salida del mux va a un registro comandado por `i_valid`, este se actualiza a una frecuencia 4 veces inferior a la frecuencia de la entrada de datos `i_in`, lográndose así el downsample.

![dwsample      ](ej2/img/dwsample.png)

![downsampler_verilog](img/downsampler_verilog.png)

### *BER Checker*

La última evaluación del sistema la implementa el módulo `prbs_checker`. Este recibe la salida de la PRBS y la salida del downsampler y verifica que, después de un tiempo de alineación, las secuencias se correspondan. Si la fase de muestreo del downsampler es correcta, el módulo machea las secuencias y activa una salida.

![checker       ](img/checker_verilog.png)
