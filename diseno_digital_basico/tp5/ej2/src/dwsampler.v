/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: dwsampler.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 03 Jul. 2021
* Description 	: tp5 of digital design - ej2 
************************************************************************************************
*/

/*
************************************************************************************************
*									      dwsampler()
*
* Description 	: implements a downsampler block
* File		    : dwsampler.v
* Notes			: ...
*
************************************************************************************************
*/


module dwsampler #(
        parameter OVRSAMPLE = 4
    )
    (
        /* ----- Inputs ----- */
        input                           i_in    ,
        input [$clog2(OVRSAMPLE)-1: 0 ] i_ctrl  ,
        input                           i_valid ,
        input                           i_clock ,
        input                           i_reset ,
        
        /* ----- Outputs ---- */
        output o_out
    );
    
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/
    
    localparam NB_SHIFT =  (OVRSAMPLE -1) ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    reg [NB_SHIFT-1:0] shiftreg    ;
    reg                out         ;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/
    /* Shift register */
    always @(posedge i_clock or posedge i_reset) begin
        if (i_reset)begin
            shiftreg <= {NB_SHIFT{1'b0}} ;
            out      <= 1'b0                ; 
        end
        else begin
            
            shiftreg <= {shiftreg[(NB_SHIFT -1)-1 : 0] , i_in};

            if (i_valid == 1)
                case (i_ctrl)
                    2'b00: out <= i_in         ;
                    2'b01: out <= shiftreg [0] ;
                    2'b10: out <= shiftreg [1] ;
                    2'b11: out <= shiftreg [2] ;
                endcase
        end
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/
    assign o_out = out ;
    
endmodule