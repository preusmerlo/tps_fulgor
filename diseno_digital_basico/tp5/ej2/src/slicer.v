/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: slicer .v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 02 Aug. 2021
* Description 	: tp5 of digital design - ej2 
************************************************************************************************
*/

/*
************************************************************************************************
*									      slicer()
*
* Description 	: implements a slicer PAM2
* File		    : slicer.v
* Notes			: ...
*
************************************************************************************************
*/

// TODO

module slicer #(

    )
    (
        /* ----- Inputs ----- */
        input   i_in,

        /* ----- Outputs ---- */
        output  o_out

    );
       
/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign o_out = ~i_in;
    
endmodule