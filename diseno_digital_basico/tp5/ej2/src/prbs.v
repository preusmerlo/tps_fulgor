/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: prbs.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 21 Jul. 2021
* Description 	: tp5 of digital design - ej2 
************************************************************************************************
*/

/*
************************************************************************************************
*									      prbs()
*
* Description 	: creates a pseudorandom binary sequence 
* File		    : prbs.v
* Notes			: Only support PRBSX with X = 7,9,11,15,20,23 and 31. If ingress order is not
*                 supported, it module select PRBS31. Default PRBS is PRBS9 with SEED = 1.
************************************************************************************************
*/

module prbs #(
        /* --- Parameters --- */
        parameter PRBSX   = 9 ,
        parameter SEED    = 1 ,
        parameter IMPULSE = 0
    )
    (
        /* ----- Inputs ----- */
        input     i_reset    ,
        input     i_clock    ,
        input     i_enable   ,

        /* ----- Outputs ---- */
        output    o_sequence
    );
        
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Order Checker */
    localparam ORDER         =  ( PRBSX == 7  || PRBSX == 9  || PRBSX == 11 )? PRBSX :
                                ( PRBSX == 15 || PRBSX == 20 || PRBSX == 23 )? PRBSX : 31 ;

    /* Feedback index */
    localparam FEEDBACK_IDX  =  ( ORDER == 7  )? 5  :       /* PRBS7    = x**7  + x**7  + 1 */
                                ( ORDER == 9  )? 4  :       /* PRBS9    = x**9  + x**5  + 1 */
                                ( ORDER == 11 )? 8  :       /* PRBS11   = x**11 + x**9  + 1 */
                                ( ORDER == 15 )? 13 :       /* PRBS15   = x**15 + x**14 + 1 */
                                ( ORDER == 20 )? 2  :       /* PRBS20   = x**20 + x**3  + 1 */
                                ( ORDER == 23 )? 17 : 27 ;  /* PRBS23   = x**23 + x**18 + 1 */
                                                            /* PRBS31   = x**31 + x**28 + 1 */

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    reg [ORDER - 1 : 0] shiftreg ;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/
    always @(posedge i_clock or posedge i_reset) begin
        if(i_reset)
            if (IMPULSE)
                shiftreg <= {{ORDER-1{1'b0}}, 1'b1};
            else
                shiftreg <= SEED;
        
        else if (i_enable)
            if (IMPULSE)
                shiftreg <= {shiftreg[ORDER-2: 0], 1'b0} ;               
            else
                shiftreg <= {shiftreg[ORDER-2: 0], shiftreg[ORDER-1] ^ shiftreg[FEEDBACK_IDX]} ;               
        
        else
            shiftreg <= shiftreg ;
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign o_sequence = shiftreg[ORDER -1];

endmodule