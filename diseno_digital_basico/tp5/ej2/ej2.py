#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: ej2.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 12 jul. 2021
# Description 	: tp5 of digital design 
######################################################################

######################################################################
#                               MODULES
######################################################################

import os
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import shape, size

import tool.ff_model  as flp 
import tool._fixedInt as fxd
import tool.DSPtools  as dsp
    
######################################################################
#                               MAIN
######################################################################

def main():
    ''' Main function '''

    ### ------------------------ Configurables -----------------------
    # Generales
    BR = 25e6             # Symbol Rate               [Bd]
    N  = 4                # Factor de sobremuestreo
    L  = 1e3              # Cantidad de simbolos a simular (numero par)
    
    # FFT
    NFFT = 256            # Cantidad de puntos de fft
    
    # Pulse shaping
    rolloff = 0.5         # Roll-Off
    NT = 6                # Cantidad de cruces por cero (simbolos) de h(t)
    
    # RX
    T0 = 1                # Fase de muestreo rx

    # Punto Fijo
    sign       = 'S'      # (S) signado : (U) no signado
    totalWidth = 8        # Cantidad de bits totales
    fractWidth = 7        # Cantidad de bits fraccionales

    # Archivos de salida
    svg   = 0             # Guarda plots en svg    : (0) NO ; (1) SI
    show  = 1             # Ver plots              : (0) NO ; (1) SI
    clear = 0             # Limpiar (rm -rf img/*) : (0) NO ; (1) SI
                          # Usar clear = 1 puede romper el README.md
    ### -----------------------------------------------------------------

    ### --------------- Constantes y configuraciones --------------------
    PRBS_ord   = 9        # Orden de la de PRBS
    M  = 2                # Cantidad de niveles del mapper (M = 2^n)
    T  = 1 / BR           # Tiempo entre simbolos     [seg]
    fs = N * BR           # Frecuencia de muestreo    [Hz]
    Ts = 1/fs             # Perdiodo de muestreo      [seg]
    
    # Plots
    H_plot = 6            # Alto  minimo de los plots
    W_plot = 6            # Ancho minimo de los plots
    img_path = 'img/'     # Directorio de imagenes

    symb_A  = fxd.DeFixedInt(  totalWidth   = 2          ,    # Constante +1
                               fractWidth   = 0          ,
                               signedMode   = sign       ,
                               roundMode    = 'round'    ,
                               saturateMode = 'saturate' )
    symb_A.value =  1.0

    symb_B  = fxd.DeFixedInt(  totalWidth   = 2          ,    # Constante -1
                               fractWidth   = 0          ,
                               signedMode   = sign       ,
                               roundMode    = 'round'    ,
                               saturateMode = 'saturate' )
    symb_B.value = -1.0

    # Chequea si existe directorio de imagenes y si no existe lo crea
    os.system(f'[ ! -d "{img_path}" ] && mkdir -p "{img_path}"')

    # Limpia directorio de plots exportados
    if clear:
        os.system('rm -rf img/*')  
    ### -----------------------------------------------------------------

    ### --------------- Mini sistema de comunicaciones ------------------

    #  -------------- PRBS con FF -------------- 
    prbs9_I    = flp.ff(PRBS_ord)           # PRBS In - phase
    prbs9_Q    = flp.ff(PRBS_ord)           # PRBS Quadrature
    prbs9_I.i  = '0x1AA'                    # Semilla   
    prbs9_Q.i  = '0x1FE'                    # Semilla
    prbs9_I.run_clock()                                     
    prbs9_Q.run_clock()

    prbs9_I_out = []
    prbs9_Q_out = []

    for clock in range(int(L)):
        prbs9_I[0].i =  prbs9_I[4].o ^ prbs9_I[8].o
        prbs9_Q[0].i =  prbs9_Q[4].o ^ prbs9_Q[8].o

        for idx in range(1,PRBS_ord):
            prbs9_I[idx].i = prbs9_I[idx-1].o
            prbs9_Q[idx].i = prbs9_Q[idx-1].o

        prbs9_I.run_clock()
        prbs9_Q.run_clock()

        prbs9_I_out.append(prbs9_I[8].o)    # Arreglos de salida de la PRBS
        prbs9_Q_out.append(prbs9_Q[8].o)    # Arreglos de salida de la PRBS
    # ----------------------------------------- 
    
    # -------------- CONV con NP --------------
    ak_I  = np.array(prbs9_I_out[:])
    ak_Q  = np.array(prbs9_Q_out[:])

    ak_I[ak_I >  0] =   1
    ak_I[ak_I <= 0] =  -1
    ak_Q[ak_Q >  0] =   1
    ak_Q[ak_Q <= 0] =  -1

    ak_up_I = dsp.upsample(ak_I,N)
    ak_up_Q = dsp.upsample(ak_Q,N)

    (t, h_np) = dsp.rcosine(rolloff, T, N, NT, Norm = False)

    h_np = fxd.arrayFixedInt(   totalWidth   = totalWidth   ,   # Cuantizo coeficientes
                                fractWidth   = fractWidth   ,
                                N            = h_np         ,
                                signedMode   = sign         ,
                                roundMode    = 'round'      ,
                                saturateMode = 'saturate'   )
    
    h_np = [h_np[i].fValue for i in range(len(h_np))]           # Vuelvo a float

    tx_I_np = np.convolve(h_np,ak_up_I, mode = 'full')
    tx_Q_np = np.convolve(h_np,ak_up_Q, mode = 'full')


    # -------------- CONV con FF --------------
    
    (t, h_float) = dsp.rcosine(rolloff, T, N, NT, Norm = False) # Calculo  coeficientes
    
    filter_delay  = int(len(h_float)/2)
    
    h = fxd.arrayFixedInt(  totalWidth   = totalWidth ,         # Cuantizo coeficientes
                            fractWidth   = fractWidth ,
                            N            = h_float    ,
                            signedMode   = sign       ,
                            roundMode    = 'round'    ,
                            saturateMode = 'saturate' )

    # Shift register de convolucion
    conv_len = int((size(h)-1)/N)
    
    shift_conv_I = flp.ff(conv_len)
    shift_conv_Q = flp.ff(conv_len)

    # Arreglos de salida
    tx_I = np.array([])
    tx_Q = np.array([])
        
    for clock in range(int(L)):
       
        # Shift register
        shift_conv_I[0].i = prbs9_I_out[clock]
        shift_conv_Q[0].i = prbs9_Q_out[clock]
        for idx in range(1,conv_len):
            shift_conv_I[idx].i = shift_conv_I[idx-1].o
            shift_conv_Q[idx].i = shift_conv_Q[idx-1].o 

        # MUX mapper
        mux_out_I = np.array([])
        for i in [shift_conv_I[0].i] + shift_conv_I.o:
            if i > 0:
                mux_out_I = np.append(mux_out_I,symb_A)
            else:
                mux_out_I = np.append(mux_out_I,symb_B)

        mux_out_Q = np.array([])
        for i in [shift_conv_Q[0].i] + shift_conv_Q.o:
            if i > 0:
                mux_out_Q = np.append(mux_out_Q,symb_A)
            else:
                mux_out_Q = np.append(mux_out_Q,symb_B)
        
        # Productos y sumas
        for i in range(N):
            aux_I = 0
            aux_Q = 0
                        
            for j in range(conv_len):
                aux_I = aux_I + (mux_out_I[j] * h[i+j*N]).fValue
                aux_Q = aux_Q + (mux_out_Q[j] * h[i+j*N]).fValue
                
            tx_I = np.append(tx_I,aux_I)
            tx_Q = np.append(tx_Q,aux_Q)
            
        # Nuevo clock
        shift_conv_I.run_clock()
        shift_conv_Q.run_clock()
    
    # Vuelvo a fixed (para vector matching)
    tx_I_fxd = fxd.arrayFixedInt( totalWidth   = totalWidth ,   # Cuantiza salida I
                                  fractWidth   = fractWidth ,
                                  N            = tx_I       ,
                                  signedMode   = sign       ,
                                  roundMode    = 'round'    ,
                                  saturateMode = 'saturate' )

    tx_Q_fxd = fxd.arrayFixedInt( totalWidth   = totalWidth ,   # Cuantiza salida Q
                                  fractWidth   = fractWidth ,
                                  N            = tx_Q       ,
                                  signedMode   = sign       ,
                                  roundMode    = 'round'    ,
                                  saturateMode = 'saturate' )
    # -----------------------------------------

    # --------------- DWSAMPLE ----------------
    rx_I = dsp.downsample(tx_I, N, T0)
    rx_Q = dsp.downsample(tx_Q, N, T0)
    # -----------------------------------------

    # ---------------- SLICER -----------------
    rx_I[rx_I >  0] =   1
    rx_I[rx_I <= 0] =  -1
    rx_Q[rx_Q >  0] =   1
    rx_Q[rx_Q <= 0] =  -1
    # -----------------------------------------

    '''
    # ------------------ BER ------------------ # CHEQUEAR QUE ESTAN MAL LOS IFFFF
    errados_I = 0
    errados_Q = 0
    
    for i in range(int(L-filter_delay/N)):
        if prbs9_I_out[i] != rx_I [i + int(filter_delay/N)]:
            errados_I += 1
        
        if prbs9_Q_out[i] != rx_Q [i + int(filter_delay/N)]:
            errados_Q += 1

    SER_I = errados_I / L
    SER_Q = errados_Q / L
    BER_I_aproxx = 1/np.log2(M) * SER_I
    BER_Q_aproxx = 1/np.log2(M) * SER_Q
    print(f'BER I (aprox) = {BER_I_aproxx}')
    print(f'BER Q (aprox) = {BER_Q_aproxx}')

    # -----------------------------------------
    '''
    # ----------------- PLOTS -----------------
    # Paso todo de fixed a float
    h = [h[i].fValue for i in range(len(h_float))]
    

    ### --------------- Respuesta al impulso -------------------------
    f = plt.figure(figsize = [W_plot,H_plot])
    plt.plot(t*1e6,h_float,'r-o',linewidth = 0.5, label = '$Punto Flotante$')
    plt.plot(t*1e6,h,'b-o',linewidth = 1  , label = '$Punto Fijo$')
    plt.grid(linestyle='--', linewidth=0.5)
    plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
    plt.xlim(t[0]*1e6,t[-1]*1e6)
    plt.ylim(-0.3,1.1)
    plt.title ('$Filtro coseno realzado$')
    plt.xlabel('Tiempo [us]')
    plt.ylabel('Amplitud')

    if svg:
        f.savefig(f"{img_path}/cos_realzado_tiempo.svg", bbox_inches='tight')
    
    ### --------------- Respuesta en frecuencia -------------------------
    [H    ,A    ,F] = dsp.resp_freq(h_float , Ts, NFFT)
    [H_fxd,A_fxd,F] = dsp.resp_freq(h       , Ts, NFFT)

    # Normaliza a 0dB en DC
    H = H / H[0]
    H_fxd = H_fxd / H_fxd[0]

    f = plt.figure(figsize = [2*W_plot,H_plot])

    # Modulo
    plt.subplot(1,2,1)
    plt.axhline(y = 20*np.log10(0.5),color = 'tab:orange',linewidth = 1, linestyle='-.', label = '- 6dB')
    plt.axvline(x = (1./T)/2.       ,color = 'tab:orange',linewidth = 1, linestyle='--', label = 'BR/2')
    
    plt.semilogx(F, 20*np.log10(H)    ,'b--', linewidth = 1  )
    plt.semilogx(F, 20*np.log10(H_fxd),'r-' , linewidth = 1.5)
    plt.legend(loc='upper right', shadow=True, ncol=1,fontsize=15)
    plt.grid(linestyle='--', linewidth=0.5)
    plt.xlim(F[1],F[-1])
    plt.ylim(-25,5)
    plt.title ('$Módulo$',fontsize=15)
    plt.xlabel('Frequencia [Hz]',fontsize=15)
    plt.ylabel('Magnitud [dB]'  ,fontsize=15)
    # Fase
    plt.subplot(1,2,2)
    plt.semilogx(F, A,'r-', linewidth = 1.5)
    plt.grid(linestyle='--', linewidth=0.5)
    plt.xlim(F[1],F[-1])
    plt.ylim(-np.pi,np.pi)
    plt.title ('$Fase$',fontsize=15)
    plt.xlabel('Frequencia [Hz]',fontsize=15)
    plt.ylabel('Fase [deg]'  ,fontsize=15)
    
    if svg:
        f.savefig(f"{img_path}/cos_realzado_frec.svg", bbox_inches='tight')

    ### --------------- Diagramas de OJO -------------------------
    period = 4

    f = plt.figure(figsize = [2*W_plot,H_plot])

    plt.subplot(1,2,1)
    plt.title ('$In - Phase $',fontsize=15)
    dsp.eyediagram(tx_I[filter_delay:-filter_delay],N,period,NT, color='r')
    plt.grid(linestyle='--', linewidth=0.5)
    
    plt.subplot(1,2,2)
    plt.title ('$Quadrature$',fontsize=15)
    dsp.eyediagram(tx_Q[filter_delay:-filter_delay],N,period,NT, color='b')
    plt.grid(linestyle='--', linewidth=0.5)

    if svg:
        f.savefig(f"{img_path}/ojos.svg", bbox_inches='tight')


    ### --------------- Constelacion -------------------------
    f = plt.figure(figsize = [W_plot,H_plot])
    plt.scatter(rx_I[filter_delay:-filter_delay], rx_Q[filter_delay:-filter_delay], color='red')
    plt.grid(linestyle='--', linewidth=0.2)
    plt.xlim(-M,M)
    plt.ylim(-M,M)
    plt.title ('$Constelacion$',fontsize=15)
    plt.xlabel('In - Phase'    ,fontsize=15)
    plt.ylabel('Quadrature'    ,fontsize=15)

    if svg:
        f.savefig(f"{img_path}/rx_constelaciones.svg", bbox_inches='tight')

    ### --------------- Salida del TX -----------------------
    x_start = 1000
    x_end   = 1200

    f = plt.figure(figsize = [3*W_plot,2*H_plot])

    plt.subplot(2,1,1)
    plt.plot(tx_I,'r-',linewidth = 1.5)
    plt.plot(tx_I_np,'m--',linewidth = 1.5)
    plt.xlim(x_start,x_end)
    plt.ylim(-M,M)
    plt.grid(linestyle='--', linewidth=0.5)
    plt.title ('$In - Phase$',fontsize=15)
    # plt.xlabel('Muestras',fontsize=15)
    plt.ylabel('Magnitud',fontsize=15)

    plt.subplot(2,1,2)
    plt.plot(tx_Q,'b-',linewidth = 1.5)
    plt.plot(tx_Q_np,'m--',linewidth = 1.5)
    plt.xlim(x_start,x_end)
    plt.ylim(-M,M)
    plt.grid(linestyle='--', linewidth=0.5)
    plt.title ('$Quadrature$',fontsize=15)
    plt.xlabel('Muestras',fontsize=15)
    plt.ylabel('Magnitud',fontsize=15)

    if svg:
        f.savefig(f"{img_path}/tx_banda_base.svg", bbox_inches='tight')
    
    ### -------------------- End ----------------------------

    # Muestra todos los plots
    if show:
        plt.show()

    ### -----------------------------------------------------------------


######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        # Cierra plots con ctrl + c
        plt.close()