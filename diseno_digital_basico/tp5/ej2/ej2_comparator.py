#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: ej2_vector_comparator.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 21 Jul. 2021
# Description 	: tp5 of digital design - ej2
######################################################################

######################################################################
#                               MODULES
######################################################################

import os
import numpy as np
import matplotlib.pyplot as plt

import tool._fixedInt as fxd
import tool.DSPtools as dsp

######################################################################
#                               MAIN
######################################################################

def main():
    ''' Main function '''

    # Contadores de errores (guardan los indices)
    prbs9_I_neq_idx = []
    tx_I_neq_idx = []

    # Selecciona directorio de archivos
    file_path = 'vectors/'
 
    # Elimina .log viejos
    os.system('rm ' + file_path +'*.log')   

    # Arreglos con salidas de Python y el Tb
    prbs9_I_out = [0,0]
    tx_I_out = [0,0]
    
    # Levanta datos de archivos de Python
    PY = 0
    TB = 1

    # Levanta datos de archivos del Testbench y alinea (de forma muy rudimentaria)
    prbs9_I_out[PY] = np.genfromtxt( file_path + 'prbs_I_py.out', usecols = 0, dtype = str )
    tx_I_out[PY]    = np.genfromtxt( file_path + 'tx_I_py.out'  , usecols = 0, dtype = str )[24:-1]
    prbs9_I_out[TB] = np.genfromtxt( file_path + 'prbs_I_tb.out', usecols = 0, dtype = str )
    tx_I_out[TB]    = np.genfromtxt( file_path + 'tx_I_tb.out'  , usecols = 0, dtype = str )[24:-1]

    # Compara arreglos
    for i in range(len(prbs9_I_out[PY])):
        
        if(prbs9_I_out[PY][i] != prbs9_I_out[TB][i]):
            prbs9_I_neq_idx.append(i)

        if(tx_I_out[PY][i] != tx_I_out[TB][i]):
            tx_I_neq_idx.append(i)

    
    # Exporta errores a un log para evaluar la cuestion
    if len(prbs9_I_neq_idx):
        file_err = open(file_path + 'prbs_I_error.log','w')
        for i in prbs9_I_neq_idx:
            file_err.write(f'{prbs9_I_out[PY][i]}\t{prbs9_I_out[TB][i]}\n')
        file_err.close()

    if len(tx_I_neq_idx):
        file_err = open(file_path + 'tx_I_error.log','w')
        for i in tx_I_neq_idx:
            file_err.write(f'{prbs9_I_out[PY][i]}\t{tx_I_out[PY][i]}\t{tx_I_out[TB][i]}\n')
        file_err.close()

    # Resultados
    print('Resultados:')
    print(f'(PRBS9 I) Errores : {len(prbs9_I_neq_idx)}')
    print(f'(TX    I) Errores : {len(tx_I_neq_idx)   }')

    if len(prbs9_I_neq_idx):
        print(f'Archivos exportados al directorio "{file_path}"\nSalu2')

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()