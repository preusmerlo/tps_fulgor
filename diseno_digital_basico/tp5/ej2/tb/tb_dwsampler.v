/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: tb_dwsampler.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 02 Aug. 2021
* Description 	: downsampler testbench module 
************************************************************************************************
*/

/* Timescale */
`timescale 1ns/100ps

module tb_dwsampler ();

    /* Local parameters   */
    localparam OVRSAMPLE = 4 ;

    /* DUT Inputs and Outputs */
    reg                           i_in    ;
    reg [$clog2(OVRSAMPLE)-1: 0 ] i_ctrl  ;
    reg                           i_valid ;
    reg                           i_clock ;
    reg                           i_reset ;

    /* ----- Outputs ---- */
    wire o_out ;

    /* Variables */

    /* Generates stimuli */
    initial begin
        i_clock = 1'b0       ;
        i_reset = 1'b0       ;
        i_valid = 1'b0       ;
        i_in    = 1'b0       ;
        i_ctrl  = 2'b00      ;

        #1 i_reset = 1'b1    ;
        #1 i_reset = 1'b0    ;

        #300 i_ctrl  = 2'b01 ;
        #300 i_ctrl  = 2'b10 ;
        #300 i_ctrl  = 2'b11 ;

        #300 $finish         ;
    end

    /* Input */
    always  begin
        #5  i_in = ~i_in;
        #10 i_in = ~i_in;
    end

    /* Fast Clock generation */
    always begin
        #5  i_clock = ~i_clock ;
    end

    /* Low Clock generation */
    always begin
        #30 i_valid = ~i_valid ;
        #10 i_valid = ~i_valid ;
    end

    /* Downsampler */
    dwsampler #(
        .OVRSAMPLE ( OVRSAMPLE )
    )
    DUT
    (
        .i_in      (  i_in     ) ,
        .i_ctrl    ( i_ctrl    ) ,
        .i_valid   ( i_valid   ) ,
        .i_clock   ( i_clock   ) ,
        .i_reset   ( i_reset   ) ,
        .o_out     ( o_out     )
    );
    
endmodule /* tb_dwsampler */