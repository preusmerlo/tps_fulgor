/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: tb_prbs.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 26 Jul. 2021
* Description 	: prbs testbench module 
************************************************************************************************
*/

/* Timescale */
`timescale 1ns/100ps

module tb_prbs ();

    /* Local parameters   */
    localparam ORDER = 9      ;
    localparam SEED  = 9'h1FE ;
    localparam IMPULSE = 0    ;

    localparam FILE_OUT = "prbs_I_tb.out" ;
    localparam OUT_PATH = "/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp5/ej2/vectors/" ; 
    localparam FILE     = {OUT_PATH,FILE_OUT} ; 
    
    /* DUT Inputs and Outputs */
    reg  i_reset    ;
    reg  i_clock    ;
    reg  i_enable   ;
    wire o_sequence ;

    /* Variables */
    integer fd;

    /* Probes */

    /* Generates stimuli */
    initial begin
        i_clock  = 1'b1 ;
        i_reset  = 1'b1 ;
        i_enable = 1'b1 ;
        
        #1 i_reset = 0 ;

        /* Creates output file */
        fd = $fopen(FILE,"w");
        
        /* Checks errors */
        if(fd == 0)
           $stop ; 
        
        #21000 $fclose(fd);
        $finish  ; 
    end

    /* Save PRBS output */
    always @(negedge i_clock) begin
        $fdisplay( fd,"%b", o_sequence );
    end

    /* Clock generation */
    always begin
        #5 i_clock = ~i_clock ;
    end

    /* PRBS I Channel */
    prbs #(
        .PRBSX      ( ORDER      ) ,
        .SEED       ( SEED       ) ,
        .IMPULSE    ( IMPULSE    ) 
    )
    DUT_PRBS_I
    (
        .i_enable   ( i_enable   ) ,
        .i_reset    ( i_reset    ) ,
        .i_clock    ( i_clock    ) ,
        .o_sequence ( o_sequence )
    );
    
endmodule /* tb_prbs */