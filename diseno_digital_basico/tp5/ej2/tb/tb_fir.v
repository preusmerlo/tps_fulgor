/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: tb_fir.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 26 Jul. 2021
* Description 	: fir testbench module 
************************************************************************************************
*/

/* Timescale */
`timescale 1ns/100ps

module tb_fir ();

    /* Local parameters   */
    localparam  OVRSAMPLE = 4  ;
    localparam  NTAPS     = 24 ;
    localparam  NB_TAPS   = 6  ;
    localparam  NBF_TAPS  = 4  ;
      
    /* Constants  */
    localparam FILTERLENGTH   = NTAPS / OVRSAMPLE ;
    localparam MULLENGTH      = (2+NB_TAPS)       ;
    localparam SUMLENGTH      =  MULLENGTH + FILTERLENGTH -1 ;
    localparam COUNTERLENGTH  = 2  ;

    localparam FILES_PATH = "/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp5/ej2/vectors/" ; 

    localparam FILE_IN_NAME   = "prbs_I_py.out";
    localparam FILE_IN        = {FILES_PATH , FILE_IN_NAME} ;

    localparam FILE_OUT_NAME  = "fir_tb.out" ;
    localparam FILE_OUT       = {FILES_PATH , FILE_OUT_NAME} ; 

    /* DUT Inputs and Outputs */
    reg  i_in      ;
    reg  i_valid   ;
    reg  i_clock   ;
    reg  i_reset   ;
    wire [((2 + NB_TAPS) + (NTAPS / OVRSAMPLE) -1)-1 : 0] o_out ;

    /* Variables */
    integer fd_in  ;
    integer fd_out ;
    integer error  ;
    integer save   ;

    /* Creates output file and check errors */
    initial begin
        /* Input file */
        fd_in = $fopen(FILE_IN  ,"r");
        if(fd_in == 0)
           $stop ;         

        /* Output file */
        fd_out = $fopen(FILE_OUT,"w");
        if(fd_out == 0)
           $stop ;         
    end

    /* Generates stimuli */
    initial begin
        save    = 0    ;
        i_clock = 1'b0 ;
        i_valid = 1'b0 ;
        i_reset = 1'b0 ;
        i_in    = 1'b0 ;
        #5 i_reset = 1 ;
        #5 i_reset = 0 ;
    end

    /* Get input data */
    always @(posedge i_valid) begin
        error <= $fscanf(fd_in,"%b", i_in);
        if( error != 1) begin
            $fclose(fd_in);
            $fclose(fd_out);
            $finish;
        end
        save = 1;
    end

    /* Save FIR output */
    always @(posedge i_clock) begin
        if (save)
            $fdisplay( fd_out,"%b\t%d", o_out,o_out);
    end

    /* Fast Clock generation */
    always begin
        #5  i_clock = ~i_clock ;
    end

    /* Low Clock generation */
    always begin
        #30 i_valid = ~i_valid ;
        #10 i_valid = ~i_valid ;
    end

    /* FIR I Channel */
    fir #(
        .OVRSAMPLE  ( OVRSAMPLE ) ,
        .NTAPS      ( NTAPS     ) ,
        .NB_TAPS    ( NB_TAPS   ) ,
        .NBF_TAPS   ( NBF_TAPS  ) 
    )
    DUT
    (
        .i_in       (  i_in     ) ,
        .i_enable   (  i_valid  ) ,
        .i_clock    (  i_clock  ) ,
        .i_reset    (  i_reset  ) ,
        .o_out      (  o_out    )
    );

endmodule /* tb_fir */