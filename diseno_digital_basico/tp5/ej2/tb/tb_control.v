/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: tb_control.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 01 Aug. 2021
* Description 	: control testbench module 
************************************************************************************************
*/

/* Timescale */
`timescale 1ns/100ps

module tb_control ();

    /* Local parameters   */
    localparam  NB_COUNTER = 2 ;

    /* DUT Inputs and Outputs */
    reg  i_reset ;
    reg  i_clock ;
    wire o_valid ;

    /* Creates output file and check errors */
    initial begin
        i_reset = 1'b0 ;
        i_clock = 1'b0 ;

        #1 i_reset = 1'b1;
        #0 i_reset = 1'b0;

        #1000 $finish;
    end

    /* Clock generation */
    always begin
        #5  i_clock = ~i_clock ;
    end

    /* System control */
    control #(
        .NB_COUNTER(NB_COUNTER)
    )
    DUT
    (
        .i_reset (i_reset) ,
        .i_clock (i_clock) ,
        .o_valid (o_valid)
    );

endmodule /* tb_control */