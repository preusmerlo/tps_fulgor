/*
************************************************************************************************
*	        						      PROCOM 2021
*			        			        Fundacion Fulgor 
*
* Filename		: tb_top.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 01 Aug. 2021
* Description 	: top testbench module 
************************************************************************************************
*/

/*
************************************************************************************************
*								      TIME SCALE
************************************************************************************************
*/

`timescale 1ns/100ps

/*
************************************************************************************************
*								      TESTBENCH
************************************************************************************************
*/

module tb_top ();

/*
************************************************************************************************
*								      TOP PARAMETERS
************************************************************************************************
*/
    /* --- System settings --- */
    localparam OVRSAMPLE     = 4       ;

    /* --- Control settings --- */
    localparam NB_COUNTER    = 2       ;

    /* --- PRBS settings --- */
    localparam PRBS_ORDER    = 9       ;
    localparam PRBS_IMPULSE  = 0       ;
    localparam PRBS_I_SEED   = 9'h1AA  ;
    localparam PRBS_Q_SEED   = 9'h1FE  ;

    /* --- FIR  settings --- */
    localparam NTAPS         = 24      ;
    localparam NB_TAPS       = 6       ;
    localparam NBF_TAPS      = 4       ;
    localparam MAPPER        = 1       ;
        
/*
************************************************************************************************
*								      CONSTANT PARAMETERS
************************************************************************************************
*/
    /* Output file */
    localparam FILES_PATH     = "/home/dell/Datos/BaseDeDatos/FULGOR/Materias/tps_fulgor/diseno_digital/tp5/ej2/vectors/" ; 
    
    localparam FILE_FIR_NAME  = "tx_I_tb.out"   ;
    localparam FILE_FIR       = { FILES_PATH , FILE_FIR_NAME  }; 

    localparam FILE_PRBS_NAME = "prbs_I_tb.out" ;
    localparam FILE_PRBS      = { FILES_PATH , FILE_PRBS_NAME }; 

    /* NB fir out */
    localparam NB_FIR_OUT = (2 + NB_TAPS) + (NTAPS / OVRSAMPLE) -1 ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    /* DUT Inputs and Outputs */
    reg        i_clock ;
    reg        i_reset ; 
    reg  [3:0] i_sw    ;
    wire [3:0] o_leds  ;


    /* File descriptors */
    integer fd_fir  ;
    integer fd_prbs ;

/*
************************************************************************************************
*								           PROBES
************************************************************************************************
*/

    /* Top probes */ 
    wire p_prbs_enable                              ;
    assign p_prbs_enable = tb_top.DUT.prbs_enable   ; 

    wire p_control_out                              ;
    assign p_control_out = tb_top.DUT.control_out   ; 
    
    wire p_prbs_I_out                               ;
    assign p_prbs_I_out  = tb_top.DUT.prbs_I_out    ;
    
    wire [NB_FIR_OUT-1 : 0] p_fir_I_out             ;
    assign p_fir_I_out   = tb_top.DUT.fir_I_out     ;

    /* Filter I probes */
    wire [(NTAPS / OVRSAMPLE) -1: 0] p_fir_buffer   ;
    assign p_fir_buffer  = tb_top.DUT.u_fir_I.buffer;


/*
************************************************************************************************
*								          OPEN FILES
************************************************************************************************
*/

    initial begin
        fd_fir  = $fopen( FILE_FIR  , "w");
        if(fd_fir == 0)
           $stop ;         
        
        fd_prbs = $fopen( FILE_PRBS , "w");
        if(fd_prbs == 0)
           $stop ;         
    end

/*
************************************************************************************************
*								          STIMULUS
************************************************************************************************
*/

    /* Generates stimulus */
    initial begin
        i_clock = 1'd0;
        i_reset = 1'd0;
        i_sw    = 4'hF;
        
        /* Reset system */
        #1 i_reset = 1 ;
        #1 i_reset = 0 ;

        #100000
        $fclose(fd_fir );
        $fclose(fd_prbs);
        $finish         ;
    end

    /* Clock generation */
    always begin
        #5  i_clock = ~i_clock ;
    end

/*
************************************************************************************************
*								        LOG DATA
************************************************************************************************
*/

    /* Save FIR I output */
    always @(posedge i_clock) begin
        $fdisplay( fd_fir,"%b\t%d", p_fir_I_out , p_fir_I_out);
    end

    /* Save PRBS I output */
    always @(posedge p_control_out) begin
        $fdisplay( fd_prbs,"%b", p_prbs_I_out);
    end

/*
************************************************************************************************
*								   MODULE UNDER TEST
************************************************************************************************
*/

    top #(
        .OVRSAMPLE   ( OVRSAMPLE    ),
        .NB_COUNTER  ( NB_COUNTER   ),
        .PRBS_ORDER  ( PRBS_ORDER   ),
        .PRBS_IMPULSE( PRBS_IMPULSE ),
        .PRBS_I_SEED ( PRBS_I_SEED  ),
        .PRBS_Q_SEED ( PRBS_Q_SEED  ),
        .NTAPS       ( NTAPS        ),
        .NB_TAPS     ( NB_TAPS      ),
        .NBF_TAPS    ( NBF_TAPS     ),
        .MAPPER      ( MAPPER       )
    )
    DUT
    (
        .i_clock     (   i_clock    ),
        .i_reset     (   i_reset    ),
        .i_sw        (   i_sw       ),
        .o_leds      (   o_leds     )
    );

endmodule /* tb_top */