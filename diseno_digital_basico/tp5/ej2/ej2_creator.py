#!/usr/bin/python3
######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
# Filename		: ej2_x.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 12 jul. 2021
# Description 	: tp5 of digital design 
######################################################################

######################################################################
#                               MODULES
######################################################################

import os
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import shape, size

import tool.ff_model  as flp 
import tool._fixedInt as fxd
import tool.DSPtools  as dsp
    
######################################################################
#                               MAIN
######################################################################

def main():
    ''' Main function '''

    ### ------------------------ Configurables -----------------------
    # Generales
    BR = 25e6               # Symbol Rate [Bd]
    N  = 4                  # Factor de sobremuestreo
    L  = 1e3                # Cantidad de simbolos a simular (numero par)
    
    # FFT
    NFFT = 256              # Cantidad de puntos de fft
    
    # Pulse shaping
    rolloff = 0.5           # Roll-Off
    NT = 6                  # Cantidad de cruces por cero (simbolos) de h(t)
    
    # RX
    T0 = 1                  # Fase de muestreo rx

    # Convolucion
    withNumpy = 1           # Convolucion con numpy (1) o a mano (0)

    # Punto Fijo
    sign       = 'S'        # (S) signado : (U) no signado
    totalWidth = 6          # Cantidad de bits totales
    fractWidth = 4          # Cantidad de bits fraccionales

    # Archivos de salida
    vectors = 0             # Exporta Vect. Martch.  : (0) NO ; (1) SI
    svg     = 1             # Guarda plots en svg    : (0) NO ; (1) SI
    show    = 0             # Ver plots              : (0) NO ; (1) SI
    clear   = 0             # Limpiar (rm -rf img/*) : (0) NO ; (1) SI
                            # Usar clear = 1 puede romper el README.md
    ### -----------------------------------------------------------------


    ### --------------- Constantes y configuraciones --------------------
    
    PRBS_ord   = 9          # Orden de la de PRBS
    M  = 2                  # Cantidad de niveles del mapper (M = 2^n)
    T  = 1 / BR             # Tiempo entre simbolos     [seg]
    fs = N * BR             # Frecuencia de muestreo    [Hz]
    Ts = 1/fs               # Perdiodo de muestreo      [seg]
    
    # Plots
    H_plot = 6              # Alto  minimo de los plots
    W_plot = 6              # Ancho minimo de los plots
    L_size = 20             # Tamano de letra
    img_path = 'img/'       # Directorio de imagenes

    # Vector matching
    file_path = 'vectors/'  # Directorio de vectores
    
    # Simbolos
    symb_A  = fxd.DeFixedInt(  totalWidth   = 2          ,    # Constante +1
                               fractWidth   = 0          ,
                               signedMode   = sign       ,
                               roundMode    = 'trunc'    ,
                               saturateMode = 'saturate' )
    symb_A.value =  1.0

    symb_B  = fxd.DeFixedInt(  totalWidth   = 2          ,    # Constante -1
                               fractWidth   = 0          ,
                               signedMode   = sign       ,
                               roundMode    = 'trunc'    ,
                               saturateMode = 'saturate' )
    symb_B.value = -1.0

    ### -----------------------------------------------------------------


    ### --------------- Mini sistema de comunicaciones ------------------


    #  -------------- PRBS con FF -------------- 
    prbs9_I    = flp.ff(PRBS_ord)           # PRBS In - phase
    prbs9_Q    = flp.ff(PRBS_ord)           # PRBS Quadrature
    prbs9_I.i  = '0x1AA'                    # Semilla   
    prbs9_Q.i  = '0x1FE'                    # Semilla
    prbs9_I.run_clock()                                     
    prbs9_Q.run_clock()

    prbs9_I_out = []
    prbs9_Q_out = []

    for clock in range(int(L)):
        prbs9_I_out.append(prbs9_I[8].o)
        prbs9_Q_out.append(prbs9_Q[8].o)

        prbs9_I[0].i =  prbs9_I[4].o ^ prbs9_I[8].o
        prbs9_Q[0].i =  prbs9_Q[4].o ^ prbs9_Q[8].o

        for idx in range(1,PRBS_ord):
            prbs9_I[idx].i = prbs9_I[idx-1].o
            prbs9_Q[idx].i = prbs9_Q[idx-1].o

        prbs9_I.run_clock()
        prbs9_Q.run_clock()
    # ----------------------------------------- 
    

    #  -------------- Filtro RC --------------- 
    (t, h_float) = dsp.rcosine(rolloff, T, N, NT, Norm = False) # Calculo  coeficientes
    
    filter_delay  = int(len(h_float)/2)
    
    h = fxd.arrayFixedInt(  totalWidth   = totalWidth ,         # Cuantizo coeficientes
                            fractWidth   = fractWidth ,
                            N            = h_float    ,
                            signedMode   = sign       ,
                            roundMode    = 'round'    ,
                            saturateMode = 'saturate' )
    # ----------------------------------------- 


    #  --------------- CONVOLUCION ---------------- 
    if withNumpy:
        # -------------- CONV con NP --------------
        
        # ---------------- MAPPER -----------------
        ak_I  = np.array(prbs9_I_out[:])
        ak_Q  = np.array(prbs9_Q_out[:])

        ak_I[ak_I >  0] =   1
        ak_I[ak_I <= 0] =  -1
        ak_Q[ak_Q >  0] =   1
        ak_Q[ak_Q <= 0] =  -1
        # ----------------------------------------- 

        # --------------- UPSAMPLE ----------------
        ak_up_I = dsp.upsample(ak_I,N)
        ak_up_Q = dsp.upsample(ak_Q,N)
        # -----------------------------------------

        # # ----------- PULSE SHAPEPING -------------
        h = [h[i].fValue for i in range(len(h))]

        tx_I = np.convolve(h,ak_up_I, mode = 'full')
        tx_Q = np.convolve(h,ak_up_Q, mode = 'full')    
        # -----------------------------------------

    else:
        # -------------- CONV con FF --------------
        
        conv_len = int((size(h)-1)/N)
        
        # Shift register de convolucion
        shift_conv_I = flp.ff(conv_len)
        shift_conv_Q = flp.ff(conv_len)

        # Arreglos de salida
        tx_I = np.array([])
        tx_Q = np.array([])
            
        for clock in range(int(L)):
        
            # Shift register
            shift_conv_I[0].i = prbs9_I_out[clock]
            shift_conv_Q[0].i = prbs9_Q_out[clock]
            for idx in range(1,conv_len):
                shift_conv_I[idx].i = shift_conv_I[idx-1].o
                shift_conv_Q[idx].i = shift_conv_Q[idx-1].o 

            # MUX mapper
            mux_out_I = np.array([])
            for i in [shift_conv_I[0].i] + shift_conv_I.o:
                if i > 0:
                    mux_out_I = np.append(mux_out_I,symb_A)
                else:
                    mux_out_I = np.append(mux_out_I,symb_B)

            mux_out_Q = np.array([])
            for i in [shift_conv_Q[0].i] + shift_conv_Q.o:
                if i > 0:
                    mux_out_Q = np.append(mux_out_Q,symb_A)
                else:
                    mux_out_Q = np.append(mux_out_Q,symb_B)
            
            # Productos y sumas
            for i in range(N):
                aux_I = 0
                aux_Q = 0
                            
                for j in range(conv_len):
                    aux_I = aux_I + (mux_out_I[j] * h[i + j * N]).fValue
                    aux_Q = aux_Q + (mux_out_Q[j] * h[i + j * N]).fValue
                    
                tx_I = np.append(tx_I,aux_I)
                tx_Q = np.append(tx_Q,aux_Q)
                
            # Nuevo clock
            shift_conv_I.run_clock()
            shift_conv_Q.run_clock()

        h = [h[i].fValue for i in range(len(h))]
        # -----------------------------------------

    # --------------- DWSAMPLE ----------------
    rx_I = dsp.downsample(tx_I, N, T0)
    rx_Q = dsp.downsample(tx_Q, N, T0)
    # -----------------------------------------


    # --------------- SLICER ------------------
    rx_I[rx_I >  0] =   1
    rx_I[rx_I <= 0] =  -1
    rx_Q[rx_Q >  0] =   1
    rx_Q[rx_Q <= 0] =  -1
    # -----------------------------------------   


    # --------------- DEMAPPER ----------------
    rx_bits_I = rx_I.copy() 
    rx_bits_Q = rx_Q.copy() 
    
    rx_bits_I[rx_bits_I >  0] =   1
    rx_bits_I[rx_bits_I <= 0] =   0
    rx_bits_Q[rx_bits_Q >  0] =   1
    rx_bits_Q[rx_bits_Q <= 0] =   0
    # -----------------------------------------   


    # ------------------ BER ------------------
    errados_I = 0
    errados_Q = 0
    
    for i in range ( int( L - filter_delay / N ) ):
        if prbs9_I_out[i] != rx_bits_I [ i + int( filter_delay / N) ]:
            errados_I += 1
        
        if prbs9_Q_out[i] != rx_bits_Q [ i + int( filter_delay / N) ]:
            errados_Q += 1

    BER_I = errados_I / L
    BER_Q = errados_Q / L

    print(f'BER I  = {BER_I}')
    print(f'BER Q  = {BER_Q}')
    # -----------------------------------------
    

    ### ------------------------ Vector Matching ------------------------
    if vectors:
        # Chequea si existe directorio de vectores y si no existe lo crea
        os.system(f'[ ! -d "{file_path}" ] && mkdir -p "{file_path}"')

        # Abre y si no existe crea archivos
        fd_prbs9_I  = open(file_path + 'prbs_I_py.out','w')
        fd_prbs9_Q  = open(file_path + 'prbs_Q_py.out','w')
        fd_tx_I     = open(file_path + 'tx_I_py.out'  ,'w')
        fd_tx_Q     = open(file_path + 'tx_Q_py.out'  ,'w')
        fd_taps     = open('src/taps.v','w')

        # Cuantiza salida tx
        tx_I_fxd = fxd.arrayFixedInt( totalWidth   = 2 + totalWidth + (NT -1),   # Cuantiza salida I
                                      fractWidth   = fractWidth ,
                                      N            = tx_I       ,
                                      signedMode   = sign       ,
                                      roundMode    = 'trunc'    ,
                                      saturateMode = 'saturate' )

        tx_Q_fxd = fxd.arrayFixedInt( totalWidth   = 2 + totalWidth + (NT - 1),   # Cuantiza salida Q
                                      fractWidth   = fractWidth ,
                                      N            = tx_Q       ,
                                      signedMode   = sign       ,
                                      roundMode    = 'trunc'    ,
                                      saturateMode = 'saturate' )

        # Exporta data de las PRBS
        for i in range(int(L)):
            fd_prbs9_I.write(fxd.formatForTestbenchFile(prbs9_I_out[i],1,separator='',end='\n'))
            fd_prbs9_Q.write(fxd.formatForTestbenchFile(prbs9_Q_out[i],1,separator='',end='\n'))
            
        # Exporta senales a la salida del tx
        for i in range(int(L*N)):
            fd_tx_I   .write(fxd.formatForTestbenchFile(tx_I_fxd[i].intvalue,tx_I_fxd[i].width,separator='',end='\t') + str(tx_I_fxd[i].intvalue) + '\n')            
            fd_tx_Q   .write(fxd.formatForTestbenchFile(tx_Q_fxd[i].intvalue,tx_Q_fxd[i].width,separator='',end='\t') + str(tx_Q_fxd[i].intvalue) + '\n')

        h_fxd = fxd.arrayFixedInt(  totalWidth   = totalWidth ,         # Cuantizo coeficientes
                                    fractWidth   = fractWidth ,
                                    N            = h_float    ,
                                    signedMode   = sign       ,
                                    roundMode    = 'round'    ,
                                    saturateMode = 'saturate' )

        # Exporta coeficientes ordenados en matriz
        row = 0
        col = 0
        for i in range(len(h)-1):
            tap = fxd.formatForTestbenchFile(h_fxd[i].intvalue,h_fxd[i].width,separator='',end='');
            fd_taps.write(f"assign taps[{row}][{col}] = {totalWidth}'b{tap} ; /* h{i} - {h_fxd[i].intvalue} */\n");
            row += 1
            if row == 4:
                row = 0
                col += 1

        # Cierra archivos
        fd_prbs9_I.close()
        fd_prbs9_Q.close()
        fd_tx_I   .close()
        fd_tx_Q   .close()
        fd_taps   .close()

    ### -----------------------------------------------------------------
        
        
    ### ----------------------------- Plots -----------------------------
    # Limpia directorio de plots exportados
    if clear:
        os.system('rm -rf img/*')

    if show or svg:
        # Chequea si existe directorio de imagenes y si no existe lo crea
        os.system(f'[ ! -d "{img_path}" ] && mkdir -p "{img_path}"')


        # ----- FIR - tiempo  y frecuiencia -------
        f = plt.figure(figsize = [2*W_plot,H_plot])
        plt.subplot(1,2,1)
        # -- Tiempo --
        plt.plot(t*1e6, h_float,'r-o',linewidth = 2, label = 'Float')
        plt.plot(t*1e6, h      ,'b-o',linewidth = 2, label = 'Fxd')
        
        plt.grid  (linestyle = '--', linewidth = 0.5)
        plt.legend(loc='lower center', shadow = True, ncol = 1,fontsize = L_size)
        
        plt.xlim  (t[0]*1e6,t[-1]*1e6)
        plt.ylim  (-0.3 , 1.1)
        plt.title ('Filtro coseno realzado',fontsize = L_size)
        plt.xlabel('Tiempo [us]',fontsize = L_size)
        plt.ylabel('Amplitud',fontsize = L_size)

        # -- Frecuencia --
        [H    ,A    ,F] = dsp.resp_freq(h_float , Ts, NFFT)
        [H_fxd,A_fxd,F] = dsp.resp_freq(h       , Ts, NFFT)
        # Normaliza a 0dB en DC
        H = H / H[0]
        H_fxd = H_fxd / H_fxd[0]

        plt.subplot(1,2,2)
        plt.axhline(y = 20*np.log10(0.5),color = 'gray',linewidth = 1, linestyle='-.', label = '- 6dB')
        plt.axvline(x = (1./T)/2.       ,color = 'gray',linewidth = 1, linestyle='--', label = 'BR/2')

        plt.semilogx(F, 20*np.log10(H)    ,'r--', linewidth = 1, label = 'Float')
        plt.semilogx(F, 20*np.log10(H_fxd),'b-' , linewidth = 1, label = 'Fxd'    )
        
        plt.legend(loc='lower left', shadow=True, ncol=1,fontsize = L_size)
        plt.grid  (linestyle = '--', linewidth = 0.5)
        
        plt.xlim(F[1],F[-1])
        plt.ylim(-25,5)
        
        plt.title ('Respuesta en frecuencia',fontsize = L_size)
        plt.xlabel('Frequencia [Hz]'        ,fontsize = L_size)
        plt.ylabel('Magnitud [dB]'          ,fontsize = L_size)
        
        if svg:
            f.savefig(f"{img_path}/filtro.svg", bbox_inches ='tight')
        # -----------------------------------------


        # ------------- Diagramas de OJO ----------
        period = 5

        f = plt.figure(figsize = [2*W_plot,H_plot])

        plt.subplot(1,2,1)
        plt.title ('$In - Phase $',fontsize = L_size)
        dsp.eyediagram(tx_I[filter_delay:-filter_delay],N,period,NT, color='r')
        plt.grid(linestyle='--', linewidth=0.5)
        plt.xlabel('Tiempo'  ,fontsize = L_size)
        plt.ylabel('Amplitud',fontsize = L_size)
        
        plt.subplot(1,2,2)
        plt.title ('$Quadrature$',fontsize = L_size)
        dsp.eyediagram(tx_Q[filter_delay:-filter_delay],N,period,NT, color='b')
        plt.grid(linestyle='--', linewidth=0.5)
        plt.xlabel('Tiempo'  ,fontsize = L_size)
        plt.ylabel('Amplitud',fontsize = L_size)

        if svg:
            f.savefig(f"{img_path}/ojos.svg", bbox_inches='tight')
        # -----------------------------------------


        # -------------- Constelacion -------------
        f = plt.figure(figsize = [2*W_plot,2*H_plot])
        for i in range(N):
            plt.subplot(int(N/2),int(N/2),i + 1)
            x = dsp.downsample(tx_I, N, i) 
            y = dsp.downsample(tx_Q, N, i) 
            plt.scatter(x[filter_delay:-filter_delay], y[filter_delay:-filter_delay], color = 'red', label = f'Fase {i}')
            plt.grid(linestyle='--', linewidth=0.5)
            plt.xlim(-M,M)
            plt.ylim(-M,M)
            plt.legend(loc='lower center', shadow=True, ncol=1,fontsize = L_size)
            # plt.title (f'Fase {i}' ,fontsize = 1.5*L_size)
            plt.xlabel('In - Phase',fontsize = L_size)
            plt.ylabel('Quadrature',fontsize = L_size)

        if svg:
            f.savefig(f"{img_path}/rx_constelaciones.svg", bbox_inches='tight')
        # -----------------------------------------


        # -------------- Salida del TX ------------
        # Genero simbolos para el plot
        ak_I  = np.array(prbs9_I_out[:])
        ak_Q  = np.array(prbs9_Q_out[:])
        
        # Los mapeo
        ak_I[ak_I >  0] =   1
        ak_I[ak_I <= 0] =  -1
        ak_Q[ak_Q >  0] =   1
        ak_Q[ak_Q <= 0] =  -1
        
        # Y upsampleo
        ak_up_I = dsp.upsample(ak_I,N)
        ak_up_Q = dsp.upsample(ak_Q,N)

        # Define franja a plotear
        x_start = 1000
        x_end   = 1100

        f = plt.figure(figsize = [2*W_plot,2*H_plot])

        plt.subplot(2,1,1)
        plt.plot(tx_I,'r-',linewidth = 3)

        for idx in range(x_start - filter_delay ,x_end - filter_delay):
            if(ak_up_I[idx] != 0):
                plt.scatter(idx + filter_delay,ak_up_I[idx], color = 'gray',s = 100)

        plt.xlim(x_start,x_end)
        plt.ylim(-M,M)
        plt.grid(linestyle='--', linewidth=1)
        plt.title ('$In - Phase$',fontsize = 1.5*L_size)
        # plt.xlabel('Muestras',fontsize = L_size)
        plt.ylabel('Magnitud',fontsize = L_size)

        plt.subplot(2,1,2)
        plt.plot(tx_Q,'b-',linewidth = 3)

        for idx in range(x_start - filter_delay ,x_end - filter_delay):
            if(ak_up_Q[idx] != 0):
                plt.scatter(idx + filter_delay,ak_up_Q[idx], color = 'gray',s = 100)

        plt.xlim(x_start,x_end)
        plt.ylim(-M,M)
        plt.grid(linestyle='--', linewidth=1)
        plt.title ('$Quadrature$',fontsize = 1.5*L_size)
        plt.xlabel('Muestras',fontsize = L_size)
        plt.ylabel('Magnitud',fontsize = L_size)

        if svg:
            f.savefig(f"{img_path}/tx_banda_base.svg", bbox_inches='tight')
        # -----------------------------------------

        if show:
            plt.show()

    ### -------------------- End ----------------------------


######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        # Cierra plots con ctrl + c
        plt.close()