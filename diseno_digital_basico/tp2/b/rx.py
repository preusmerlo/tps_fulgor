#!/bin/python3

import os
import time 
import serial
import matplotlib.pyplot as pl
from fulgor.framerMarcos import framerMarcos


ser = serial.Serial(
        port     = '/dev/ttyVPB',
        baudrate = 9600,
        parity   = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS
        )

framerRX = framerMarcos(ser)

while True:
    if(len(framerRX.receive()) != 0):
        opc = framerRX.decode('str')

        if   'calculadora' in opc:
            #os.system('../a/ej4.py') 
            
            frameRX = framerRX.getAttribute('dataFrame')
            print('> Ejecutar calculadora')
            print(f'> Bytes recibidos : {frameRX}')           
            

        elif 'graficar' in opc:
            array = framerRX.decode('list')[len('graficar'):]
            pl.figure()
            pl.stem(range(len(array)),array)
            pl.title('STEM - Cerrar para continuar ···')
            pl.grid()
            pl.show()
            
            frameRX = framerRX.getAttribute('dataFrame')
            print('> Graficar datos enviados')
            print(f'> Bytes recibidos : {frameRX}')
            print(f'> Datos recibidos: {array}')
            
        elif 'salir' in opc:
            os.system('clear')
            exit()
        else:
            pass
        
        
 