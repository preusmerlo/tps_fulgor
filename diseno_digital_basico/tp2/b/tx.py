#! /bin/python3
import os
import time
import serial
from numpy import random
from fulgor.framerMarcos import framerMarcos


ser = serial.Serial(
            port     = '/dev/ttyVPA',
            baudrate = 9600,
            parity   = serial.PARITY_NONE,
            stopbits = serial.STOPBITS_ONE,
            bytesize = serial.EIGHTBITS
        )

framerTX = framerMarcos(ser)

menu = '''
    MENU
> Calculadora
> Graficar
> Salir

> OPCION: '''

while True:
    os.system('clear')
    print(menu, end = '')
    opc = input().lower()
    if   opc == 'calculadora':
        framerTX.send(opc)
        frameTX = framerTX.getAttribute('dataFrame')
        print(f'> Bytes enviados : {frameTX}')
        input('\t\tEnter para continuar ···')

    elif opc == 'graficar':
        array = list(random.randint(0, 255, size = (1, 10), dtype = int)[0])
       
        framerTX.send(framerTX.encode(opc) + framerTX.encode(array))
        frameTX = framerTX.getAttribute('dataFrame')
        
        print(f'> Bytes enviados : {frameTX}')
        print(f'> Datos enviados: {array}')
        input('\t\t\tEnter para continuar ···')

    elif opc == 'salir':
        os.system('clear')
        framerTX.send(opc)
        exit()
    
    else:
        print('\t\tOpcion incorrecta ···')
        time.sleep(1)