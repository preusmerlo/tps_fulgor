######################################################################
#						Patricio Reus Merlo
#					patricio.reus.merlo@gmail.com
#
#	  (c) Copyright 2021, Patricio Reus Merlo, Cordoba, Argentina
# 						All Rights Reserved
#
# Filename		: filename.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 22 MAY. 2021
# Description 	: Clase codificador y decodificador de tramas.
######################################################################

######################################################################
#                               MODULES
######################################################################
from os import replace
import serial
from math import ceil

######################################################################
#                          CLASS framerMarcos
######################################################################

class framerMarcos():
    '''
        Clase que codifica y decodifica la trama Marcos. Esta trama 
        tiene la siguiente estructura:
            
            (head) -> Byte1 -> Init(3) Type(1) Ssize(4)
                    -> Byte2 -> SizeH (8)
                    -> Byte3 -> SizeL (8)
                    -> Byte4 -> Device(8)
            (data) -> Byte5 -> Data1 (8)
                    ...          ...  (n)
            (tail) -> ByteN -> End(3)  Type(1) Ssize(4)   
    
    '''
    ##################################################################
    #                               MACROS
    ##################################################################
    __isShort = (0 << 4)
    __isLong  = (1 << 4)
    __shortFrameMaxLen = 2**4  - 1
    __longFrameMaxLen  = 2**16 - 1
    __maxIntValue = 255
    __maxStrValue = chr(255)

    ##################################################################
    #                              ATRIBUTOS
    ##################################################################
    __dataFrame = ''

    ##################################################################
    #                            CONSTRUCTOR
    ##################################################################
    def __init__(self, framerPort = None, frameInit = '5') -> None:
        '''
                                Constructor
            Description : Metodo constructor de objetos framerMarcos.
            Arguments   : 
                          - framerPort : objeto de la clase serial a
                          traves del cual el objeto creado puede enviar
                          tramas codificadas.Default = None.

                          - frameInit  : toda "tramaMarcos" tiene 3
                          bits en su primer byte que definen el inicio
                          de trama y 3 bits en el ultimo byte que de-
                          finen el final de la misma. Este argumento 
                          permite personalizar estos bits. 
                          Default = (101)_2 = (5)_10.

            Returns     : None.
        '''

        if framerPort != None and isinstance(framerPort,serial.serialposix.Serial):
            self.__framerPort = framerPort
        else:
            self.__framerPort = None

        self.__frameInit  = (int(frameInit,16) & 7) << 5
        self.__frameEnd   = (~self.__frameInit) & 0xE0

    ##################################################################
    #                            CODIFICADOR
    ##################################################################
    def encode(self, data, device = 0, type = 'bytes') -> bytes:
        '''
                                Codificador
            Description : Recibe un conjunto de datos y los convierte a
                          bytes.
            Arguments   : 
                          - data:   conjunto de datos. Puede ser de tipo
                                    'lista','str','int' y 'byte'. Para las
                                    listas todos los elementos deben estar
                                    comprendidos en 0 <= x < 256. Para los
                                    string sucede lo mismo, ord(str)< 256.
                                    Para los enteros se debe cumplir que 
                                    0 <= int < 2**128. Estos se codifican
                                    como trama 'long' si superan el valor
                                    2**16 - 1. 

                          - device: contenido del campo device de la
                                    trama. Puede ser de tipo 'int' o 'str'
                                    tal que int < 256 y ord(str) < 256 y 
                                    mayor a cero en cambos casos.

                          - type:   define si el paquete de bytes devuelto
                                    incluye a 'head' y 'tail'. Default =
                                    'bytes'.

            Returns     : Si type = frame devuelve frame con 'head'y'tail'
                          Si no, si type == bytes devuelve un conjunto 
                          arreglo de bytes. Los arreglos de bytes pueden
                          concatenarse con +, por ejemplo:
                          
                          data = f.encode('hola') + f.encode([12,15,20]) 
        '''

        ### Evaluacion del tipo y valor de 'data'
        if   isinstance(data,list) and max(data) <= self.__maxIntValue and min(data) >= 0:
            dataBytes = bytes(data)
        
        elif isinstance(data,str ) and data <= self.__maxStrValue:
            dataBytes = data.encode()
        
        elif isinstance(data,int):
            aux = []
            nbytes = ceil(data.bit_length()/8)
            for i in range(nbytes):
                aux.append((data >> (i * 8)) & self.__maxIntValue)
            aux.reverse()
            dataBytes = bytes(aux)
        
        elif isinstance(data,bytes):
            dataBytes = data
            pass
        
        else:
            raise ValueError('Tipo de dato no soportado')
        

        ### Codifica solo a bytes o arma todo el frame
        if   type == 'bytes':
            return dataBytes
        elif type == 'frame':
            pass
        else:
            raise ValueError('Type no conocido')

        ### Calculo de longitud y tipo de trama
        ## data = int
        if isinstance(data,int):
        
            # int de 1 a 2**4 bytes
            if   nbytes <= self.__shortFrameMaxLen:
                self.__frameType  = self.__isShort
                self.__frameSsize = nbytes
                self.__frameSizeH = 0
                self.__frameSizeL = 0
        
            # int de 2**4 a 2**16 bytes
            elif nbytes <= self.__longFrameMaxLen:
                self.__frameType  = self.__isLong
                self.__frameSsize = 0 
                self.__frameSizeH = nbytes >> 8
                self.__frameSizeL = nbytes & self.__maxIntValue
            else:
                raise ValueError('Valor de int demasiado grande')
        
        ## data = list o str corto
        elif len(data) <= self.__shortFrameMaxLen:
            self.__frameType  = self.__isShort
            self.__frameSsize = len(data)
            self.__frameSizeH = 0
            self.__frameSizeL = 0
        
        ## data = list o str largo
        elif len(data) <= self.__longFrameMaxLen:
            self.__frameType  = self.__isLong
            self.__frameSsize = 0 
            self.__frameSizeH = len(data) >> 8
            self.__frameSizeL = len(data) & self.__maxIntValue
        else:
            raise ValueError('Cantidad de datos superior a la permitida')

        ### Evaluacion del tipo y valor de 'device'
        ## device = int
        if   isinstance(device,int) and device <= self.__maxIntValue:
            self.__frameDevice = device
        
        ## device = str
        elif isinstance(device,str) and len(device) == 1 and device <= self.__maxStrValue:
            self.__frameDevice = ord(device)
        else:
            raise ValueError('Valor de device no soportado')
        
        ### Construccion de head y tail
        head = []
        head.append(self.__frameInit | self.__frameType | self.__frameSsize)
        head.append(self.__frameSizeH )
        head.append(self.__frameSizeL )
        head.append(self.__frameDevice)
        head = bytes(head)
        
        tail = []
        tail.append(self.__frameEnd  | self.__frameType | self.__frameSsize)
        tail = bytes(tail)
        
        # Construccion de trama
        self.__dataFrame  = head + dataBytes + tail
        return self.__dataFrame

    ##################################################################
    #                            TRANSMISOR
    ##################################################################
    def send(self, data = None, device = 0):
        '''
                                    Transmisor
            Description : Envia por el puerto serie del objeto framerMarcos
                          los datos que recibe como argumento o el frame ya
                          construido enteriormente con encode().
                          
            Arguments   : 
                          - data:   conjunto de datos a enviar. Tolera los
                                    mismos tipos que encode(). Si este pa-
                                    rametro no se especifica el metodo en-
                                    via el dataFrame creado la ultima vez
                                    que se llamo a send() con datos o se 
                                    llamo a encode(). Default: data = None.

                          - device: contenido del campo device de la
                                    trama. Puede ser de tipo 'int' o 'str'
                                    tal que int < 256 y ord(str) < 256 y 
                                    mayor a cero en cambos casos.

            Returns     : Numero de bytes escritos en el puerto serie.
        '''
        
        ## Verificacion del puerto
        if   self.__framerPort == None:
            raise Exception('Objeto serial no inicializado')
        elif self.__framerPort.isOpen() == False:
            raise Exception('Puerto serie no disponible o cerrado')
        else:
            if data != None:
                self.encode(data, device=device , type = 'frame')
                return self.__framerPort.write(self.__dataFrame)
            elif  len(self.__dataFrame) != 0:
                return self.__framerPort.write(self.__dataFrame)
            else:
                raise ValueError('Especifique que enviar')


    ##################################################################
    #                            RECEPTOR
    ##################################################################
    def receive(self):
        '''
                                    Receptor
            Description : Recibe una trama Marcos y la convierte en un ob-
                          jeto de tipo 'list' eliminandi 'head' y 'tail'.
                          Puede utilizar el metodo decode() para interpre-
                          tar este arreglo. Para esto no es necesario al-
                          macenar el arreglo retornado, el dataFrame reci-
                          bido es conservado por el objeto framerMarcos. 
                          
            Arguments   : None.

            Returns     : Objeto de tipo lista de enteros menores a 255.
                          Elimina 'head' y 'tail', solo retorna 'data'.
                          Si no hay datos en el puerto retorna False.
        '''
        
        if   self.__framerPort == None:
            raise Exception('Objeto serial no inicializado')
        elif self.__framerPort.isOpen() == False:
            raise Exception('Puerto serie no disponible o cerrado')
        else:
            data = []
            decoderState = 'free'

            while self.__framerPort.inWaiting() > 0:
                data.append(ord(self.__framerPort.read(1)))
                ## Maquina de estados del decodificador
                if  decoderState == 'free' and ((data[0] & 0xE0) == self.__frameInit):
                    self.__frameType = (data[0] & 0x10)

                    if self.__frameType == self.__isShort:
                        decoderState = 'short'
                    else:
                        decoderState = 'long'

                elif decoderState == 'short':
                    if len(data)  == 3:
                        self.__frameSsize = data[0] & 0x0F
                        self.__frameSizeH = 0
                        self.__frameSizeL = 0
                        decoderState      = 'device'
                
                elif decoderState == 'long' :
                    if len(data)  == 3:
                        self.__frameSsize = 0
                        self.__frameSizeH = data[1]
                        self.__frameSizeL = data[2]
                        decoderState      = 'device'

                elif decoderState == 'device':
                    self.__frameDevice = data[3]
                    head = bytes(data[0:4])
                    data = data[3:-1]
                    decoderState = 'data'

                elif decoderState == 'data':
                    if len(data)  == (self.__frameSsize + ((self.__frameSizeH << 8) | self.__frameSizeL)):
                        decoderState = 'end'
                
                elif decoderState == 'end':
                    if data[-1] == (self.__frameEnd | self.__frameType | self.__frameSsize):
                        tail = bytes([data.pop(-1)])
                        self.__dataFrame = head + bytes(data) + tail
                    else:
                        data.clear()
                    decoderState = 'free'
                    break
            return data

    ##################################################################
    #                            DECODIFICADOR
    ##################################################################
    def decode (self, type = None):
        '''
                                    Decodificador
            Description : Decodifica la ultima trama Marcos generada 
                          conencode(), enviada con send() o recibida  
                          con receive(). 
                          
            Arguments   : 
                          - type: tipo del dato de salida. Son so-
                                  portados los tipos 'str', 'int' 
                                  y 'list'.

            Returns     : Objeto del tipo de dato especificado.
        '''

        if   type == None:
            return self.__dataFrame[4:-1]
        elif type == 'str':
            return self.__dataFrame[4:-1].decode(errors = 'replace')
        elif type == 'list':
            aux = []
            for x in self.__dataFrame[4:-1]:
                aux.append(x)
            return aux
        elif type == 'int':
            aux  = 0
            for i in range(4,len(self.__dataFrame[4:-1])):
                aux |= (self.__dataFrame[i] << (i * 8))
            return aux
        else:
            raise ValueError('Tipo de dato desconocido')


    ##################################################################
    #                            METODO GET
    ##################################################################
    def getAttribute(self, attribute):
        '''
                                    getAttribute
            Description : Permite obtener el dataFrame completo o los
                          componentes de este por separado. De este
                          modo se obtiene el contenido de 'Device'.
                          
            Arguments   : 
                          - attribute: componente de la trama deseado:
                                - dataFrame   (bytes array)
                                - frameInit   (int)
                                - frameType   (int)
                                - frameSsize  (int)
                                - frameSizeH  (int)
                                - frameSizeL  (int)
                                - frameDevice (int)
                                - frameEnd    (int)

            Returns     : Atributo solicitado.
        '''
        
        if   attribute == 'dataFrame'  :
            return self.__dataFrame
        elif attribute == 'frameInit'  :
            return self.__frameInit
        elif attribute == 'frameType'  :
            return self.__frameType
        elif attribute == 'frameSsize' :
            return self.__frameSsize
        elif attribute == 'frameSizeH' :
            return self.__frameSizeH
        elif attribute == 'frameSizeL' :
            return self.__frameSizeL
        elif attribute == 'frameDevice':
            return self.__frameDevice
        elif attribute == 'frameEnd'   :
            return self.__frameEnd
        else:
            raise ValueError('El objeto no posee con tal attributo')

######################################################################
#                        END CLASS framerMarcos
######################################################################