#!/usr/bin/python3

from framerMarcos import framerMarcos
import serial
import time

serA = serial.Serial(
        port     = '/dev/ttyVPA',
        baudrate = 9600,
        parity   = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS
        )

serB = serial.Serial(
        port     = '/dev/ttyVPB',
        baudrate = 9600,
        parity   = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS
        )


## TX
framerTX = framerMarcos(serA)

print('\nDATA TRANSMITIDA\n')

data2send = 10
framerTX.send(data2send)
dataframeTX = framerTX.getAttribute('dataFrame')
print(f'Enviado      : {dataframeTX}')

data2send = [1,2,3,4]
framerTX.send(data2send)
dataframeTX = framerTX.getAttribute('dataFrame')
print(f'Enviado      : {dataframeTX}')

data2send = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
framerTX.send(data2send)
dataframeTX = framerTX.getAttribute('dataFrame')
print(f'Enviado      : {dataframeTX}')

data2send = "Hola mundo"
framerTX.send(data2send)
dataframeTX = framerTX.getAttribute('dataFrame')
print(f'Enviado      : {dataframeTX}')

data2send = "HOLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
framerTX.send(data2send)
dataframeTX = framerTX.getAttribute('dataFrame')
print(f'Enviado      : {dataframeTX}')

data2send = framerTX.encode("HolaMundo") + framerTX.encode([1,2,3,4,120,6]) + framerTX.encode(15)
framerTX.send(data2send)
dataframeTX = framerTX.getAttribute('dataFrame')
print(f'Enviado      : {dataframeTX}')

time.sleep(1)

## RX
framerRX = framerMarcos(serB)

print('\nDATA RECIBIDA\n')

while True:
    data2receive = framerRX.receive() 
    if len(data2receive) != 0:
        dataframeRX = framerRX.getAttribute('dataFrame')
        print(f"Recibido     : {dataframeRX}")
    else:
        exit()