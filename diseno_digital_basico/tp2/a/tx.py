#!/bin/python3
################################################################################################
#                                       PATRICIO REUS MERLO
#                                   patricio.reus.merlo@gmail.com
#
#                           (c) Copyright 2021, Cordoba, Argentina
#                                       All Rights Reserved
#
#
# Filename      : tx.py
# Programmer(s) : Paatricio Reus Merlo
# Description   : ejecuta comandos recibidos por puerto serie
################################################################################################

import os
import serial

################################################################################################
#                                       MODULES
################################################################################################

################################################################################################
#                                     openPort()
#
# Description   : abre un puerto serie con configuracion default: 9600 y 8N1
# Arguments     : 
#                   - portX (string): puerto  Ej: '/dev/ttyACM0'
# Returns       : objeto serial si abre el puerto. De lo contrario, False.
################################################################################################

def openPort(portX):
    ser = serial.Serial(
        port     = portX,
        baudrate = 9600,
        parity   = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS
        )
    if ser.isOpen:
        return ser
    else:
        return False

################################################################################################
#                                       sendData()
#
# Description   : envia un string por puerto serie
# Arguments     : 
#                   - ser   : objeto serial
#                   - data  : string a enviar
# Returns       : numero de bytes enviados
################################################################################################

def sendData (ser, data):
    if ser.isOpen():
        written = ser.write(data.encode())
        return written
    else:
        return 0

################################################################################################
#                                     receiveData()
#
# Description   : extrae un string del buffer serial
# Arguments     : 
#                   - ser   : objeto serial
# Returns       : string
################################################################################################

def receiveData (ser):
    if ser.isOpen():
        data = ''
        while ser.inWaiting() > 0:
            read_data = ser.read(1)
            data += read_data.decode();
        return data
    else:
        return 0

################################################################################################
#                                          MAIN
#
# Description   : Bloque principal, envia comandos por puerto serie
################################################################################################

ser = openPort('/dev/ttyVPA')

while True:

    data_tx = input('({} - TX) << '.format(ser.portstr))
    sendData(ser,data_tx)

    # Espera respuesta desde el puerto serie
    while ser.inWaiting() == 0: pass
    
    # Lee datos recibidos por el puerto
    data_rx = receiveData(ser)

    if  len(data_rx) != 0:

        # Imprime respuesta en pantalla
        print('({} - RX) >> '.format(ser.portstr) + data_rx)

        # Decodifica comandos enviados por el receptor
        if data_rx == 'Salir':
            break

        elif data_rx == 'clear':
            os.system('clear')

os.system('clear')
ser.close()