#!/bin/python3
################################################################################################
#                                       PATRICIO REUS MERLO
#                                   patricio.reus.merlo@gmail.com
#
#                           (c) Copyright 2021, Cordoba, Argentina
#                                       All Rights Reserved
#
#
# Filename      : rx.py
# Programmer(s) : Patricio Reus Merlo
# Description   : ejecuta comandos recibidos por puerto serie
################################################################################################

################################################################################################
#                                       MODULES
################################################################################################

import os
import time
import serial
import numpy as np
import matplotlib.pyplot as pl

################################################################################################
#                                       FUNCTIONS
################################################################################################

################################################################################################
#                                     openPort()
#
# Description   : abre un puerto serie con configuracion default: 9600 y 8N1
# Arguments     : 
#                   - portX (string): puerto  Ej: '/dev/ttyACM0'
# Returns       : objeto serial si abre el puerto. De lo contrario, False.
################################################################################################

def openPort(portX):
    ser = serial.Serial(
        port     = portX,
        baudrate = 9600,
        parity   = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS
        )
    if ser.isOpen:
        return ser
    else:
        return False

################################################################################################
#                                       sendData()
#
# Description   : envia un string por puerto serie
# Arguments     : 
#                   - ser   : objeto serial
#                   - data  : string a enviar
# Returns       : numero de bytes enviados
################################################################################################

def sendData (ser, data):
    if ser.isOpen():
        written = ser.write(data.encode())
        return written
    else:
        return 0

################################################################################################
#                                     receiveData()
#
# Description   : extrae un string del buffer serial
# Arguments     : 
#                   - ser   : objeto serial
# Returns       : string
################################################################################################

def receiveData (ser):
    if ser.isOpen():
        data = ''
        while ser.inWaiting() > 0:
            read_data = ser.read(1)
            data += read_data.decode();
        return data
    else:
        return 0

################################################################################################
#                                     plot_loading()
#
# Description   : dibuja "text sec" con movimiento entre los datos de "s"ec". Es NO bloqueante.
#                 Genera efecto de "loading ···" con los puntos "moviendose".
# Arguments     : 
#                   - cnt  (int   ): contador de ciclo, modifica la tasa de actualizacion  
#                   - sec  (array ): dibujo movil      Ej: sec  = ["·  ","·· ","···"," ··","  ·"]
#                   - text (string): texto a imprimir  Ej: text = "loading"
# Returns       : nuevo valor de cnt
################################################################################################

def plot_loading (  cnt,
                    sec = ["·   ","··  ","··· ","····"," ···","  ··", "   ·"],
                    text = "loading"):
    
    if  cnt < len(sec):
        print ("\033[A \033[A")
        print(text + ' '+ sec[cnt])
        return cnt + 1 
    else:
        return 0


################################################################################################
#                                          MAIN
#
# Description   : Bloque principal, decodifica comandos enviados por puerto serie
################################################################################################

cnt  = 0;
ser  = openPort('/dev/ttyVPB')

while True:    

    if ser.inWaiting() > 0:
        data = receiveData(ser);

        if  len(data) != 0:
            # Borra ultimo Esperando···
            print ("\033[A \033[A")

            # Imprime lo que llega por el puerto
            print('({} - RX) >> '.format(ser.portstr) + data + "\n")  


            if   data == 'Calculadora':
                # Libera el puerto serie para que lo tome la calculadora como stdin y stdout
                port = ser.portstr
                ser.close()
                
                # Ejecuta el script que implementa la calculadora
                # El control de la calc. es a traves del puerto serie
                try:
                    os.system('./ej4.py --port ' + port)
                except:
                    os.system('clear')
                    sendData(ser,"Algo inesperado paso ejecutando calculadora")

                # Recupera el control del puerto serie
                ser = openPort(port)
            
            elif data == 'Graficar':
                
                # Ejecuta stem con un arreglo random de 20 elementos
                array = np.random.randint(0, 100, size = (1,20), dtype = int)
                pl.stem(np.arange(np.size(array, axis = 1)),array[0])
                pl.title('STEM - Cerrar para continuar ···')
                pl.ylabel('Amplitud')
                pl.xlabel('Muestras')
                pl.grid()
                pl.show()
                sendData(ser,'Grafica cerrada')
            
            elif data == 'Clear' or data == 'clear':

                # Limpia la del transmisor y luego la propia
                sendData(ser,'clear')
                os.system('clear')

            elif data == 'Salir' or data == 'salir':
            
                # Mata al transmisor y luego se suicida
                sendData(ser,'Salir')
                break
            
            else:
                sendData(ser,'Error, comando no valido')
    else:

        # Imprime "Esperando ····" con los puntos moviendose 
        cnt = plot_loading (cnt,text = '   Esperando   ')
        time.sleep(0.1)

os.system('clear')
ser.close()