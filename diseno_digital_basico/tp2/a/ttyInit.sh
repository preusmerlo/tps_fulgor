#!/bin/bash

sudo echo ""
echo "Instalando socat ···"  
sudo apt update &> /dev/null
sudo apt install socat &> /dev/null

# Creamos dos terminales virtuales ttyVP (tty Virtual Port)
echo "Creando puertos virtuales ···"
#sudo socat -d -d pty,raw,echo=0,link=/dev/$(portA) pty,raw,echo=0,link=/dev/$(portB)
sudo gnome-terminal --command="socat -d -d pty,raw,echo=0,link=/dev/ttyVPA pty,raw,echo=0,link=/dev/ttyVPB" --title="Puertos corriendo - NO CERRAR" &> /dev/null

# Agregamos permisos (a lo bruto)
echo "Agregando permisos ···"
sudo chmod 666 /dev/ttyVPA
sudo chmod 666 /dev/ttyVPB

# Data de puertos
echo " "
echo "Puertos creados:"
echo " + [virtual port A] /dev/ttyVPA"
echo " + [virtual port B] /dev/ttyVPB"
echo " "
echo "Listo el pollo XD ¡Salu2!"
