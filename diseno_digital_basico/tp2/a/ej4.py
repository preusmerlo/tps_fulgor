#!/bin/python3
################################################################################################
#                                       PATRICIO REUS MERLO
#                                   patricio.reus.merlo@gmail.com
#
#                           (c) Copyright 2021, Cordoba, Argentina
#                                       All Rights Reserved
#
#
# Filename      : ej4.py (incluye a ej3.py, ej2.py y ej1.py)
# Programmer(s) : Patricio Reus Merlo
# Description   : Implementación de una calculadora y algunas cosas más
################################################################################################

################################################################################################
#                                       MODULES
################################################################################################

import os
import sys
import numpy as np
import matplotlib.pyplot as pl
from numpy.core.fromnumeric import shape

################################################################################################
#                                       FUNCTIONS
################################################################################################

################################################################################################
#                                       creatematrix()
#
# Description   : Crea una matriz n X m y la completa con valores aleatorios
# Arguments     : 
#                   - rows  : filas
#                   - cols  : columnas
#                   - random: completado con valores aleatorios (opcional y por defecto)
# Returns       : numpy.array de rows X cols
################################################################################################

def creatematrix(rows,cols ,random = 'y'):
    if random == 'y':
        array = np.random.randint(0, 100, size = (rows, cols), dtype = int)
    else:
        array = np.zeros((rows,cols), dtype = float)
        for i in range (rows):
            for j in range (cols):
                array[i,j] = float(input('-- Data [{},{}]:'.format(i,j)))
    return array

################################################################################################
#                                       getdata()
#
# Description   : Solicita datos al usuario
# Arguments     : 
#                   - data_type: tipo de dato a solicitar (int, matrix, vector)
# Returns       : lista de 2 int, lista de dos numpy array o 1 array de una dimensión
################################################################################################

def getdata(data_type = 'int'):
    A = 0
    B = 0
    
    if   (data_type == 'int'):
        A = int(input("Ingresar A: "))
        B = int(input("Ingresar B: "))
    
    elif (data_type == 'matrix'):
        print('\nMATRIZ A')
        rows = int(input("Cantidad de Filas   : "))
        cols = int(input("Cantidad de Columnas: "))
        A = creatematrix(rows, cols, random = input('Completar con valores aleatorios? [y/n]: '))
        print(A)

        print('\nMATRIZ B')
        rows = int(input("Cantidad de Filas   : "))
        cols = int(input("Cantidad de Columnas: "))
        B = creatematrix(rows, cols, random = input('Completar con valores aleatorios? [y/n]: '))
        print(B)
    
    elif (data_type == 'vector'):
        print('\nVECTOR DE DATOS')
        cols = int(input("Cantidad de elementos: "))
        A = creatematrix(1, cols, random = input('Completar con valores aleatorios? [y/n]: '))
        print(A[0])
        return A

    return (A,B)

################################################################################################
#                                       calculate()
#
# Description   : Resuelve las opciones 1 a 6 del menu
# Arguments     : 
#                   - opc: opcion a resolver
# Returns       : Resultado (R,int) en opc 1 a 5 y (R,array) en opc 6
################################################################################################

def calculate(opc):
    R = 0

    if opc != '6':
        (A,B) = getdata('int')
    else:
        (A,B) = getdata('matrix')
    
    if   opc == '1':
        R = A + B
    elif opc == '2':
        R = A-B    
    elif opc == '3':
        R = A*B
    elif opc == '4':
        R = A/B
    elif opc == '5a':
        R = A*B
    elif opc == '5b':
        R = A*(-B)    
    elif opc == '5c':
        R = A**B
    elif opc == '6' :
        if( np.size(A, 0) == np.size(B, 0) and np.size(A, 1) == np.size(B, 1)):
            R = np.dot(A, B)
            
            # Guarda A , B y R en archivos
            np.savetxt('matA.txt', A, delimiter = ',')
            np.savetxt('matB.txt', B, delimiter = ',')
            np.savetxt('matR.txt', R, delimiter = ',')
        else:
            print('\tERROR: Matrices de distinto tamaño')
            R = 'NULL'
    return R

################################################################################################
#                                       plot()
#
# Description   : Resuelve las opciones 7a a 7d del menu
# Arguments     : 
#                   - opc: opcion a resolver
# Returns       : None
################################################################################################

def plot(opc):

    if opc != '7e':
        vect = getdata(data_type = 'vector')
    
    pl.figure()
    
    if   opc == '7a':
        pl.plot(np.arange(np.size(vect, axis = 1)),vect[0],'o-', linewidth=1.0)
        pl.title('PLOT - Cerrar para continuar ···')
    
    elif opc == '7b':
        pl.stem(np.arange(np.size(vect, axis = 1)),vect[0])
        pl.title('STEM - Cerrar para continuar ···')
    
    elif opc == '7c':
        pl.hist(vect[0])
        pl.title('HISTOGRAMA - Cerrar para continuar ···')
    
    elif opc == '7d':
        NFFT = 1024
        V    = np.fft.fft(vect[0], NFFT)
        N    = shape(V)
        w = np.arange(0.,np.pi ,np.pi/N[0])

        pl.title('FFT - Cerrar para continuar ···')

        pl.subplot(2,1,1)
        pl.plot(w,np.abs(V), 'r--',linewidth=1.0)
        pl.ylabel('Modulo [veces]')
        pl.grid()

        pl.subplot(2,1,2)
        pl.plot(w, np.angle(V),'b--',linewidth=1.0)
        pl.ylabel('Fase [rad]')
        pl.xlabel('Frecuencia Discreta [rad/seg]')

    elif opc == '7e':
        file = input('File: ')
        data = np.genfromtxt(file, dtype = int, delimiter = ",")
        pl.plot(np.arange(np.size(data)), data,'o-', linewidth=1.0)
        pl.title('ARCHIVO - Cerrar para continuar ···')

    
    if opc != '7d':
        pl.ylabel('Amplitud')
        pl.xlabel('Muestras')
    
    pl.grid()
    pl.show()

################################################################################################
#                                       input()
#
# Description   : redefine input() para que maneje el puerto si el script se ejecuta con el flag
#                 --port <port_path>. Si se ejecuta sin --port utiliza como stdin y stdout la 
#                 terminal.
# Arguments     : 
#                   - text (string) : texto a imprimir 
# Returns       : string recibido por puerto serie o ingresado por la terminal. 
################################################################################################


if '--port' in sys.argv:

    def input(text = ''):
        print(text)
        port = sys.argv[sys.argv.index('--port') + 1]
        with open(port, 'r') as ser:
            lines = ser.readlines()
            while len(lines) == 0:
                lines = ser.readlines()        
        ser.close()
        return lines[-1]

################################################################################################
#                                          MAIN
#
# Description   : Bloque principal, implementa una calculadora (entre otras cosas)
################################################################################################


# Redefine stdout como el puerto serie si esta especificado
if '--port' in sys.argv:
    idx = sys.argv.index('--port') + 1
    sys.stdout = open(sys.argv[idx], 'w')

menu =  '''
>------------->  MENU
>------------------------>
> 1 ) Sumar
> 2 ) Restar
> 3 ) Multiplicar
> 4 ) Dividir
> 5x) Iterativo
>     5a) Sumar
>     5b) Restar
>     5c) Mutiplicar
> 6 ) Producto punto
> 7x) Graficar
>     7a) Plot
>     7b) Stem
>     7c) Histograma
>     7d) FFT  Plot
>     7e) File Plot
>
> q) Salir
>------------------------>
>------------->  OPCION: '''

while True:
    opc = input(menu)

    if     opc == 'q' or opc == 'Q':
            print('Calculadora cerrada ')
            os.system('clear')
            break
    
    elif ('5' in opc or '7' in opc and len(opc) == 1):
        input('Indique item a ejecutar')

    elif   opc in menu and opc > '0' and opc < '8':
        
        if '7' in opc:
            plot(opc)
        
        else:
            R = calculate(opc)
            print('Resultado : {} '.format(R))
    else:
        print('Opcion Incorrecta')

    if '--port' in sys.argv:
        pass
    else:
        input('Presione ENTER para continuar ···')
        os.system('clear')