import os
import numpy as np

'''
ENUNCIADO:
    Empleando los conceptos aprendidos de manejo de variables, elaborar una calculadora la
    cual muestra el siguiente panel de opciones cuando se ejecuta el programa:
'''

menu =  '''>------------------------>
> 0) Cargar "A" y "B"
> 1) Sumar
> 2) Restar
> 3) Multiplicar
> 4) Dividir
> 5) Iterativo
>     a) Sumar
>     b) Restar
>     c) Mutiplicar
> 6) Producto punto (dot)
> Q) Salir
>------------------------>
>------------->  OPCIÓN: '''

def getmatrix(random = 'yes'):
    rows = int(input("\tNumero de Filas   : "))
    cols = int(input("\tNumero de Columnas: "))
    
    if random == 'y':
        array = np.random.randint(0, 10, size = (rows, cols))
    else:
        array = np.zeros((rows,cols))
        for i in range (rows):
            for j in range (cols):
                array[i,j] = int(input('-- Data [{},{}]:'.format(i,j)))
    return array

def getdata(data_type):
    A = 0
    B = 0
    
    if   (data_type == 'int'):
        A = int(input("Ingresar A: "))
        B = int(input("Ingresar B: "))
    
    elif (data_type == 'array'):
        A = getmatrix(random = input('MATRIZ A Value Random? [y/n]: '))
        print(A)
        B = getmatrix(random = input('MATRIZ B Value Random? [y/n]: '))
        print(B)

    return (A,B)

def calculate(opc):
    R = 0

    if opc != '6':
        (A,B) = getdata('int')
    else:
        (A,B) = getdata('array')
    
    if   opc == '1':
        R = A + B
    elif opc == '2':
        R = A-B    
    elif opc == '3':
        R = A*B
    elif opc == '4':
        R = A/B
    elif opc == '5a':
        R = A*B
    elif opc == '5b':
        R = A*(-B)    
    elif opc == '5c':
        R = A**B
    elif opc == '6' :
        if( np.size(A, 0) == np.size(B, 0) and np.size(A, 1) == np.size(B, 1)):
            R = np.dot(A, B)
        else:
            print('\tERROR: Matrices de distinto tamaño')
            R = 'NULL'
    else:
        input('Opción Incorrecta - Enter para continuar')
    
    return R


# ------------- MAIN -------------
while True:
    os.system('clear')
    opc = input(menu)

    if opc == 'q' or opc == 'Q':
        os.system('clear')
        break
    
    R = calculate(opc)
    print('>------------->  RESULTADO: {}'.format(list(R)))
    input('Enter para otra operacion')
