import os
import numpy as np
import matplotlib.pyplot as pl

def getmatrix(rows,cols ,random = 'y'):
    if random == 'y':
        array = np.random.randint(0, 100, size = (rows, cols))
    else:
        array = np.zeros((rows,cols))
        for i in range (rows):
            for j in range (cols):
                array[i,j] = int(input('-- Data [{},{}]:'.format(i,j)))
    return array

def getdata(data_type = 'int'):
    A = 0
    B = 0
    
    if   (data_type == 'int'):
        A = int(input("Ingresar A: "))
        B = int(input("Ingresar B: "))
    
    elif (data_type == 'matrix'):
        print('\nMATRIZ A')
        rows = int(input("\tCantidad de Filas   : "))
        cols = int(input("\tCantidad de Columnas: "))
        A = getmatrix(rows, cols, random = input('Completar con valores aleatorios? [y/n]: '))
        print(A)

        print('\nMATRIZ B')
        rows = int(input("\tCantidad de Filas   : "))
        cols = int(input("\tCantidad de Columnas: "))
        B = getmatrix(rows, cols, random = input('Completar con valores aleatorios? [y/n]: '))
        print(B)
    
    elif (data_type == 'vector'):
        print('\nVECTOR DE DATOS')
        cols = int(input("\tCantidad de elementos: "))
        A = getmatrix(1, cols, random = input('Completar con valores aleatorios? [y/n]: '))
        return A

    return (A,B)

def calculate(opc):
    R = 0

    if opc != '6':
        (A,B) = getdata('int')
    else:
        (A,B) = getdata('matrix')
    
    if   opc == '1':
        R = A + B
    elif opc == '2':
        R = A-B    
    elif opc == '3':
        R = A*B
    elif opc == '4':
        R = A/B
    elif opc == '5a':
        R = A*B
    elif opc == '5b':
        R = A*(-B)    
    elif opc == '5c':
        R = A**B
    elif opc == '6' :
        if( np.size(A, 0) == np.size(B, 0) and np.size(A, 1) == np.size(B, 1)):
            R = np.dot(A, B)
        else:
            print('\tERROR: Matrices de distinto tamaño')
            R = 'NULL'
    return R

def plot(opc):

    if opc != '7e':
        vect = getdata(data_type = 'vector')
    
    pl.figure()
    if   opc == '7a':
        pl.plot(np.arange(np.size(vect, axis = 1)),vect[0],'o-', linewidth=1.0)
        pl.title('PLOT - Cerrar para continuar ···')
    
    elif opc == '7b':
        pl.stem(np.arange(np.size(vect, axis = 1)),vect[0])
        pl.title('STEM - Cerrar para continuar ···')
    
    elif opc == '7c':
        pl.hist(vect[0])
        pl.title('HISTOGRAMA - Cerrar para continuar ···')
        pass
    
    elif opc == '7d':
        # NO ANDA NI A PALO
        NFFT = 8*1024
        V     = np.fft.fft(vect)
        w = np.arange(start = 0,stop = NFFT)/NFFT
        pl.plot(w,np.abs(V))
        pass
    
    elif opc == '7e':
        file = input('File: ')
        data = np.genfromtxt(file, dtype = int, delimiter = ",")
        pl.plot(np.arange(np.size(data)), data,'o-', linewidth=1.0)
        pl.title('ARCHIVO - Cerrar para continuar ···')
        pass

    pl.ylabel('Amplitud')
    pl.xlabel('Muestras')
    pl.grid()
    pl.show()


# ------------- MAIN -------------

menu =  '''>------------------------>
> 1 ) Sumar
> 2 ) Restar
> 3 ) Multiplicar
> 4 ) Dividir
> 5x) Iterativo
>     5a) Sumar
>     5b) Restar
>     5c) Mutiplicar
> 6 ) Producto punto
> 7x) Graficar
>     7a) Plot
>     7b) Stem
>     7c) Histograma
>     7d) FFT  Plot
>     7e) File Plot
>
> q) Salir
>------------------------>
>------------->  OPCIÓN: '''

while True:
    os.system('clear')
    opc = input(menu)

    if     opc == 'q' or opc == 'Q':
            os.system('clear')
            break

    elif ((opc[0] == '5' or opc[0] == '7') and len(opc) == 1):
        input('Indique item a ejecutar - Enter para continuar ···')

    elif   opc in menu and opc[0] > '0' and opc[0] < '8':
        
        if '7' in opc:
            plot(opc)
        
        else:
            R = calculate(opc)
            print('>------------->  RESULTADO: {}'.format(R))
            input('Enter para continuar ...')
    else:
        input('Opción Incorrecta - Enter para continuar ···')
    
    
    

    
