import os

'''
ENUNCIADO:
    Empleando los conceptos aprendidos de manejo de variables, elaborar una calculadora la
    cual muestra el siguiente panel de opciones cuando se ejecuta el programa:
'''

menu =  '''
        >------------------------>
        > 0) Cargar "A" y "B"
        > 1) Sumar
        > 2) Restar
        > 3) Multiplicar
        > 4) Dividir
        > 5) Iterativo
        >     a) Sumar
        >     b) Restar
        >     c) Mutiplicar
        > Q) Salir
        >------------------------>
        >------------->  OPCIÓN: '''

def calculate(opc):
    A = int(input("\tIngresar A: "))
    B = int(input("\tIngresar B: "))

    if   opc == '1':
        R = A + B
    elif opc == '2':
        R = A-B    
    elif opc == '3':
        R = A*B
    elif opc == '4':
        R = A/B
    elif opc == '5a':
        R = A*B
    elif opc == '5b':
        R = A*(-B)    
    elif opc == '5c':
        R = A**B
    else:
        input('Opción Incorrecta - Enter para continuar')
    
    return R


# ------------- MAIN -------------
while True:
    os.system('clear')
    opc = input(menu)

    if opc == 'q' or opc == 'Q':
        os.system('clear')
        break
    
    R = calculate(opc)
    input('\t<------------->  RESULTADO: {}'.format(R))
