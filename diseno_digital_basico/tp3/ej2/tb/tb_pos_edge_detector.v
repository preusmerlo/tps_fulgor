/* Timescale */
`timescale 1ns/100ps

/* Definiciones */
`define NBITS 4

/* Testbench */
module tb_pos_edge_detector ();
    
    /* Entradas y salidas */
    wire [`NBITS -1:0] o_edge   ;
    reg                i_clock  ;
    reg  [`NBITS -1:0] i_signal ;
    
    /* Generacion de estimulos */
    initial begin
        i_clock  = 0 ;
        i_signal = 0 ;

        #137 i_signal = `NBITS'hA ;
        #217 i_signal = `NBITS'h8 ;
        #137 i_signal = `NBITS'h4 ;
        #217 i_signal = `NBITS'hC ;
        #137 i_signal = `NBITS'hF ;
        #217 i_signal = `NBITS'h7 ;
        #137 i_signal = `NBITS'hE ;
        #217 i_signal = `NBITS'h1 ;
 
        #10 $finish ;
    end

    /* Generacion de i_clock */
    always begin
        #10 i_clock = ~i_clock;
    end

    /* Instanciacion del DUT */
    pos_edge_detector DUT(
            .i_signal (i_signal ) ,
            .i_clock  (i_clock  ) ,
            .o_edge   (o_edge   )  
        );

endmodule /* tb_pos_edge_detector */