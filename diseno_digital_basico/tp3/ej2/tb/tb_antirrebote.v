/* Timescale */
`timescale 1ns/100ps

/* Definiciones */
`define N_BUTTONS       4
`define NB_BTNCOUNTER   7 // Retardo de 1.28us (valor bajo para simular poco)

/* Testbench */
module tb_antirrebote ();

    /* Parametros */
    localparam  N_BUTTONS   = `N_BUTTONS     ; 
    localparam  NB_BTNCOUN  = `NB_BTNCOUNTER ;
    
    /* Entradas y salidas */
    wire [N_BUTTONS -1:0] o_btn ;
    reg  [N_BUTTONS -1:0] i_btn ;
    reg                   i_rst ;
    reg                   i_clk ;

    wire [NB_BTNCOUN -1:0] counter;

    /* Puntas de prueba */
    assign  counter = tb_antirrebote.DUT.btn_counter;

    /* Generacion de estimulos */
    initial begin
        i_btn = `N_BUTTONS'b0;
        i_rst = 1'b0;
        i_clk = 1'b0;

        #4 i_rst = 1'b1;
        #4 i_rst = 1'b0;
        
        #37 i_btn  = `N_BUTTONS'hF ;
        #37 i_btn  = `N_BUTTONS'h0 ;

        #47 i_btn  = `N_BUTTONS'hF ;
        #47 i_btn  = `N_BUTTONS'h0 ;
        
        #27 i_btn  = `N_BUTTONS'hF ;
        #27 i_btn  = `N_BUTTONS'h0 ;
        
        #67 i_btn  = `N_BUTTONS'hF ;
        #67 i_btn  = `N_BUTTONS'h0 ;
        
        #67 i_btn  = `N_BUTTONS'hF ;
        #67 i_btn  = `N_BUTTONS'h0 ;
        
        #97 i_btn  = `N_BUTTONS'hF ;
        #97 i_btn  = `N_BUTTONS'h0 ;

        #73 i_btn  = `N_BUTTONS'hF ;
        
        #5000 i_btn  = `N_BUTTONS'hF ;

        #37 i_btn  = `N_BUTTONS'hF ;
        #37 i_btn  = `N_BUTTONS'h0 ;

        #47 i_btn  = `N_BUTTONS'hF ;
        #47 i_btn  = `N_BUTTONS'h0 ;
        
        #27 i_btn  = `N_BUTTONS'hF ;
        #27 i_btn  = `N_BUTTONS'h0 ;
        
        #67 i_btn  = `N_BUTTONS'hF ;
        #67 i_btn  = `N_BUTTONS'h0 ;
        
        #67 i_btn  = `N_BUTTONS'hF ;
        #67 i_btn  = `N_BUTTONS'h0 ;
        
        #97 i_btn  = `N_BUTTONS'hF ;
        #97 i_btn  = `N_BUTTONS'h0 ;

        #5000 $finish ;
    end

    /* Generacion de i_clock */
    always begin
        #10 i_clk = ~i_clk;
    end

    /* Instanciacion del DUT */
    antirrebote #(
            .N_BUTTONS  (N_BUTTONS )  , 
            .NB_BTNCOUN (NB_BTNCOUN) 
        )
        DUT (
            .o_btn (o_btn) ,
            .i_btn (i_btn) ,
            .i_rst (i_rst) ,
            .i_clk (i_clk)
        );

endmodule /* tb_pos_edge_detector */