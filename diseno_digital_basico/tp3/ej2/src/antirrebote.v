/* Modulo */
module antirrebote #(
        /* Parametros */
        parameter M_N_BUTTONS   = 4   , 
        parameter M_NB_COUNTER  = 23    /* Delay de 50ms  */
    )
    (
        /* Salidas */
        output [M_N_BUTTONS -1:0] o_btn ,

        /* Entradas */
        input  [M_N_BUTTONS -1:0] i_btn ,
        input                   i_rst ,
        input                   i_clk
    );

    /* Variables */
    reg  [M_N_BUTTONS  -1:0]   btn_prev    ;
    reg  [M_N_BUTTONS  -1:0]   btn_out     ;   
    reg  [M_NB_COUNTER -1:0]   btn_counter ;

    /* Arquitectura */
    always @(posedge i_clk or posedge i_rst) begin
        if (i_rst) begin
            btn_prev  <= {M_N_BUTTONS{1'b0}} ;
            btn_out   <= {M_N_BUTTONS{1'b0}} ;
        end

        else begin
            if ((btn_prev ^ i_btn) != {M_N_BUTTONS{1'b0}}) begin
                btn_counter <= {{M_NB_COUNTER-1{1'b0}},{1'b1}};
                btn_prev    <= i_btn;
            end

            else if (btn_counter > {M_NB_COUNTER{1'b0}})
                btn_counter <= btn_counter + {{M_NB_COUNTER-1{1'b0}},{1'b1}};

            else
                btn_out <= btn_prev;
        end
    end

    /* Asignacion de puertos*/
    assign o_btn = btn_out;

endmodule /* Fin antirrebote */