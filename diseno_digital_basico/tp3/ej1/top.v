/* MODULO */
module top #(
        /* PARAMETROS */
        parameter NB_COUNTER  = 32              ,
        parameter NB_SELECTOR = 2               ,
        
        parameter N_LEDS      = 4               ,
        parameter N_SWITCHES  = 4               ,
        
        parameter N_BUTTONS   = 4               , 
        parameter NB_BTNCOUN  = 23              ,

        parameter R0          = 32'h0000_FFFF   ,
        parameter R1          = 32'h00FF_FFFF   ,
        parameter R2          = 32'h0FFF_FFFF   ,
        parameter R3          = 32'hFFFF_FFFF          
    ) 
    (
        /* SALIDAS */
        output [N_LEDS - 1:0] o_led_r   ,
        output [N_LEDS - 1:0] o_led_g   ,
        output [N_LEDS - 1:0] o_led_b   ,
        output [N_LEDS - 1:0] o_leds    ,  

        /* ENTRADAS */
        input  [N_SWITCHES -1:0] i_sw   ,
        input  [N_BUTTONS  -1:0] i_btn  ,
        input                    i_rst  ,
        input                    i_clk  
    );
    
    /* CONSTANTES */
    localparam SEL_R0 = 2'h0 ;
    localparam SEL_R1 = 2'h1 ;
    localparam SEL_R2 = 2'h2 ;
    localparam SEL_R3 = 2'h3 ;
  
    localparam SEL_LEDS_R = 3'h1 ;
    localparam SEL_LEDS_G = 3'h2 ;
    localparam SEL_LEDS_B = 3'h4 ;

    /* VARIABLES */  
    wire                     rst          ;
    wire [N_BUTTONS  -1:0]   btn          ;
 
    reg  [NB_COUNTER -1:0]   counter      ;
    wire [NB_COUNTER -1:0]   top          ;
    reg  [N_LEDS     -1:0]   shiftreg     ;
    reg  [N_LEDS     -1:0]   flashreg     ;
 
    wire [N_BUTTONS  -1:0]   btn_pos_edge ;
    wire [N_BUTTONS  -1:0]   btn_state    ;
    reg  [N_BUTTONS  -1:0]   btn_mode     ;
 
    wire [N_LEDS     -1:0]   leds         ;

    /* ARQUITECTURA */
    assign rst = ~ i_rst ;
    assign btn = ~ i_btn ;

    /* MUX selector de TOP */
    assign top = (i_sw[(N_SWITCHES-1)- 1 -: NB_SELECTOR] == SEL_R0 )? R0 :
                 (i_sw[(N_SWITCHES-1)- 1 -: NB_SELECTOR] == SEL_R1 )? R1 :
                 (i_sw[(N_SWITCHES-1)- 1 -: NB_SELECTOR] == SEL_R2 )? R2 : R3;

    
    /* Contador + Shift register + Flash register */
    always @(posedge i_clk or posedge rst ) begin
        if (rst) begin
            counter   <= {NB_COUNTER{1'b0}}            ;
            shiftreg  <= {{N_LEDS -1 {1'b0}} , {1'b1}} ;
            flashreg  <= {N_LEDS{1'b1}}                ;
        end
        else begin
            if (i_sw[0]) begin            
                if (counter >= top) begin
                    counter  <= {NB_COUNTER{1'b0}};
                    flashreg <= ~flashreg ;
                    if (i_sw[3])
                        shiftreg <= {shiftreg[N_LEDS - 2 :0], shiftreg[N_LEDS -1]};
                    else
                        shiftreg <= {shiftreg[0], shiftreg[N_LEDS - 1 :1]};
                end

                else
                    counter  <= counter + {{NB_COUNTER - 1{1'b0}}, {1'b1}};
            end
        end
    end

    /* Antirrebote */
    antirrebote #(
            .M_N_BUTTONS  (N_BUTTONS    ),
            .M_NB_COUNTER (NB_BTNCOUN   )
        )
        u_antirrebote(
            .o_btn( btn_state ) ,
            .i_btn(   btn     ) ,
            .i_rst(   rst     ) ,
            .i_clk( i_clk     )
        );

    /* Detector de flanco positivo */
    pos_edge_detector #(
            .M_N_LINES(N_BUTTONS)
        )
        u_pos_edge_detecto(
            .o_lines_pos_edge ( btn_pos_edge ) ,
            .i_lines          ( btn_state    ) ,
            .i_clk            ( i_clk        )
        );

    /* Control de modo */
    always @(posedge i_clk or posedge rst) begin
        if (rst)
            btn_mode <= {N_BUTTONS{1'b0}};
        else begin
            /* Selector de modo */
            if (btn_pos_edge[0])
                btn_mode[0] <= ~btn_mode;
            
            /* Selector de color */
            case (btn_pos_edge[N_BUTTONS-1:1])
                SEL_LEDS_R:
                    btn_mode[N_BUTTONS-1:1] <= SEL_LEDS_R;
                
                SEL_LEDS_G:
                    btn_mode[N_BUTTONS-1:1] <= SEL_LEDS_G;

                SEL_LEDS_B:
                    btn_mode[N_BUTTONS-1:1] <= SEL_LEDS_B;
                
                default:
                    btn_mode[N_BUTTONS-1:1] <= btn_mode[N_BUTTONS-1:1];
            endcase

        end
    end

    /* Multiplexor de modo */
    assign leds = (btn_mode[0])? flashreg : shiftreg;

    /* ASIGNACION DE PUERTOS */
    assign o_led_r = (btn_mode[N_BUTTONS -1 :1] == SEL_LEDS_R)? leds : {N_LEDS{1'b0}} ;
    assign o_led_g = (btn_mode[N_BUTTONS -1 :1] == SEL_LEDS_G)? leds : {N_LEDS{1'b0}} ;
    assign o_led_b = (btn_mode[N_BUTTONS -1 :1] == SEL_LEDS_B)? leds : {N_LEDS{1'b0}} ;
    assign o_leds  =  btn_state; 

endmodule