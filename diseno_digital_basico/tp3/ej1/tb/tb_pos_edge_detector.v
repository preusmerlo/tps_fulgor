/* Timescale */
`timescale 1ns/100ps

/* Definiciones */
`define NBITS 4

/* Testbench */
module tb_pos_edge_detector ();
    
    /* Entradas y salidas */
    wire [`NBITS -1:0] o_lines_pos_edge ;
    reg                i_clk            ;
    reg  [`NBITS -1:0] i_lines          ;
    
    /* Generacion de estimulos */
    initial begin
        i_clk  = 0 ;
        i_lines = 0 ;

        #137 i_lines = `NBITS'hA ;
        #217 i_lines = `NBITS'h8 ;
        #137 i_lines = `NBITS'h4 ;
        #217 i_lines = `NBITS'hC ;
        #137 i_lines = `NBITS'hF ;
        #217 i_lines = `NBITS'h7 ;
        #137 i_lines = `NBITS'hE ;
        #217 i_lines = `NBITS'h1 ;
 
        #10 $finish ;
    end

    /* Generacion de i_clk */
    always begin
        #10 i_clk = ~i_clk;
    end

    /* Instanciacion del DUT */
    pos_edge_detector DUT(
            .i_lines (i_lines ) ,
            .i_clk  (i_clk  ) ,
            .o_lines_pos_edge   (o_lines_pos_edge   )  
        );

endmodule /* tb_pos_edge_detector */