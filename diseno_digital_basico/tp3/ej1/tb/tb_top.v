/* TIME SCALE*/
`timescale 10ns/100ps

/* MACROS */
`define TB_NB_COUNTER      32  /* Cantidad de bits del contador */
`define TB_NB_SELECTOR     2   /* Cantidad de bits del selector de top */

`define TB_N_LEDS          4   /* Cantidad de leds por color */
`define TB_N_SWITCHES      4   /* Cantidad de switches       */

`define TB_N_BUTTONS       4   /* Cantidad de botones */
`define TB_NB_BTNCOUNTER   7   /* Bits para delay de rebote de 1.28us */

`define TB_R0              `TB_NB_COUNTER'h0000_00FF
`define TB_R1              `TB_NB_COUNTER'h0000_0FFF
`define TB_R2              `TB_NB_COUNTER'h0000_FFFF
`define TB_R3              `TB_NB_COUNTER'h000F_FFFF

/* MODULO */
module tb_top();
    
    /* SALIDAS */
    wire [`TB_N_LEDS - 1:0]    o_led_r    ;
    wire [`TB_N_LEDS - 1:0]    o_led_g    ;
    wire [`TB_N_LEDS - 1:0]    o_led_b    ;
    wire [`TB_N_LEDS - 1:0]    o_leds     ;  

    /* ENTRADAS */
    reg  [`TB_N_SWITCHES -1:0] i_sw  ;
    reg  [`TB_N_BUTTONS  -1:0] i_btn ;
    reg                     i_rst ;
    reg                     i_clk ; 
 
    /* PUNTAS DE PRUEBA */ 
    wire [`TB_NB_COUNTER -1:0] p_counter   ;
    wire [`TB_N_LEDS     -1:0] p_shiftreg  ;
    wire [`TB_N_LEDS     -1:0] p_flashreg  ;
    wire [`TB_N_BUTTONS  -1:0] p_btn_mode  ;

    assign p_counter   = tb_top.DUT.counter   ; 
    assign p_shiftreg  = tb_top.DUT.shiftreg  ;
    assign p_flashreg  = tb_top.DUT.flashreg  ;
    assign p_btn_mode  = tb_top.DUT.btn_mode  ;

    /* ESTIMULOS */
    initial begin
        /* Inicializacion */
        i_sw  = {`TB_N_SWITCHES{1'b0}} ;
        i_rst = 1'b1 ;
        i_clk = 1'b0 ;
        i_btn = {`TB_N_BUTTONS {1'b1}} ;

        /* Reset */
        #20 i_rst = 1'b0;
        #20 i_rst = 1'b1;

        /* Habilita contador, shiftreg y flashreg */
        #100 i_sw[0]  = 1'b1 ;

        /* Activa ROJO */
        #100 i_btn[1] = 1'b0 ;
        #600 i_btn[1] = 1'b1 ;

        #3000000 $stop;

        /* Habilita modo Flash */
        #100 i_btn[0] = 1'b0 ;
        #600 i_btn[0] = 1'b1 ;

        /* Activa AZUL */
        #100 i_btn[2] = 1'b0 ;
        #600 i_btn[2] = 1'b1 ;

        #3000000 $stop;
        
        /* Habilita modo Shift */
        #100 i_btn[0] = 1'b0 ;
        #600 i_btn[0] = 1'b1 ;

        /* Invierte sentido Shift */
        #100 i_sw[3]  = 1'b1 ;

        /* Activa VERDE */
        #100 i_btn[3] = 1'b0 ;
        #600 i_btn[3] = 1'b1 ;

        #3000000 $finish;
            
    end

    /* GENERACION DE CLOCK */
    always begin
        #1 i_clk = ~i_clk;
    end
    
    /* INSTANCIACION DEL DUT */
    top #(
            .TB_NB_COUNTER (`TB_NB_COUNTER    )  ,
            .TB_NB_SELECTOR(`TB_NB_SELECTOR   )  ,       
            .TB_N_LEDS     (`TB_N_LEDS        )  ,
            .TB_N_SWITCHES (`TB_N_SWITCHES    )  ,
            .TB_N_BUTTONS  (`TB_N_BUTTONS     )  , 
            .NB_BTNCOUN    (`TB_NB_BTNCOUNTER )  ,
            .TB_R0         (`TB_R0            )  ,
            .TB_R1         (`TB_R1            )  ,
            .TB_R2         (`TB_R2            )  ,
            .TB_R3         (`TB_R3            )          
        ) 
        DUT (
            .o_led_r ( o_led_r )  ,
            .o_led_g ( o_led_g )  ,
            .o_led_b ( o_led_b )  ,
            .o_leds  ( o_leds  )  ,  
            .i_sw    ( i_sw    )  ,
            .i_btn   ( i_btn   )  ,
            .i_rst   ( i_rst   )  ,
            .i_clk   ( i_clk   )     
        );

endmodule /* tb_top */