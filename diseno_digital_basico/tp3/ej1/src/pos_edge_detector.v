/* Definiciones */
`define M_N_LINES 4

/* Modulo */
module pos_edge_detector #(
        /* Parametros */
        parameter M_N_LINES = `M_N_LINES
    )
    (
        /* Salidas */
        output [M_N_LINES -1:0] o_lines_pos_edge ,

        /* Entradas */
        input  [M_N_LINES -1:0] i_lines ,
        input               i_clk
    );

    /* Variables */
    reg [M_N_LINES -1:0] lines_dly;
    reg [M_N_LINES -1:0] lines_pos_edge    ;

    /* Arquitectura */
    always @(posedge i_clk) begin
        lines_dly       <= i_lines ;
        lines_pos_edge  <= i_lines & ~lines_dly ;
    end

    /* Asignacion de puertos*/
    assign o_lines_pos_edge = lines_pos_edge;

endmodule /* Fin pos_edge_detector */