%---------------------------------------------------------------------%
%						    Beca Fulgor 2021
%					 patricio.reus.merlo@gmail.com
%
% Filename		: main.m
% Programmer(s)	: Patricio Reus Merlo
% Created on	: 10 Jul. 2021
% Description 	: digital communications simulator 
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                               IMPORTANT
%                               *********
%    To run the simulator it is necessary to go to the simulator/
%    directory in MATLAB and then, from this point, execute main.m
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                            PATH SETTINGS
%---------------------------------------------------------------------%

addpath(strcat(pwd,'\src\' )) ;  % Add src  path to MATLAB paths
addpath(strcat(pwd,'\test\')) ;  % Add test path to MATLAB paths

%---------------------------------------------------------------------%
%                           SIMULATOR SETTINGS 
%---------------------------------------------------------------------%

% ------------ Simulation settings --------------
config.simulationIterations         = 2       ; % Must be  >= 1          (not iterable)
config.sim.simulationLength         = 5e3     ; % Must be  >= 1          (  iterable  )

% --------------- Plots settings ----------------
config.sim.showPlots                = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportPlots              = 1       ; % (1) Yes  : (0) No      (  iterable  )

% -- TX plots --
config.sim.numberOfSymbolsToShow    = 15      ; % Must be  > 0           (  iterable  )
config.sim.transmittedSymbols       = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedSignals       = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedConstellation = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedEyeDiagram    = 0       ; % (1) Yes  : (0) No      (  iterable  )

% -- TX vs RX plots --
config.sim.TxRxSpectrum             = 0       ; % (1) Yes  : (0) No      (  iterable  )

% -- RX plots --
config.sim.receivedSignals          = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.receivedEyeDiagram       = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.receivedConstellation    = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.correlatorOutVsSlicerOut = 1       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.slicerInputHistogram     = 0       ; % (1) Yes  : (0) No      (  iterable  )

% --------------- Logs settings -----------------
config.sim.exportLogs               = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportSER                = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportBER                = 0       ; % (1) Yes  : (0) No      (  iterable  )

% ---- Digital communications system settings ---
config.sys.modulationType           = 1       ; % (1) QAM  : (0) PAM     (  iterable  )
config.sys.symbolRate               = 32e9    ; % Must be  >  0          (  iterable  )
config.sys.mapperLevels             = [2 4]   ; % Must be =   2,4 or 8   (  iterable  )
config.sys.useMatchedFilter         = 1       ; % (1) Yes  : (0) No      (not iterable)
config.sys.oversamplingFactor       = 4       ; % Must be  >  2          (  iterable  )

% ------------ Transmitter settings -------------
config.tx.filterType                = 1       ; % (1) RRC  : (0) RC      (  iterable  )
config.tx.filterLength              = 150     ; % Must be  >  1          (  iterable  )
config.tx.filterRolloff             = 0.1     ; % Must be 0 < x <= 1     (  iterable  )

% -------------- Channel settings ---------------
config.ch.addNoise                  = 1       ; % (1) Yes  : (0) No      (  iterable  )
config.ch.EbNo_dB                   = 7       ; % Must be real           (  iterable  )

% ------------- Receiver settings ---------------
config.rx.correlatorPhase           = 0       ; % [Hz]                   (  iterable  )
config.rx.dwConvPhaseError          = 0       ; % [deg]                  (  iterable  )
config.rx.dwConvfrequencyOffset     = 0       ; % [Hz]                   (  iterable  )

%----------------------------------------- % Config this block if didnt use MF
if config.sys.useMatchedFilter == 0
    config.rx.filterType            = 0       ; % (1) RRC  : (0) RC      (  iterable  )
    config.rx.filterLength          = 0       ; % Must be  >  1          (  iterable  )
    config.rx.filterRolloff         = 0       ; % Must be 0 < x <= 1     (  iterable  )
end
%----------------------------------------- % Config this block if didnt use MF

% --------------- Post processing settings ----------------- 

% Post processing options                                                               % Pending

%---------------------------------------------------------------------%
%                                RUN 
%---------------------------------------------------------------------%
tic
sim = simulator(config);
sim.run();
toc
%---------------------------------------------------------------------%
%                           SIMULATOR END 
%---------------------------------------------------------------------%