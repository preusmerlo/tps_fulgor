function [y_slicer] = slicer_qam_M(x, M)

if M==4
    y_slicer = slicer_pam2(real(x)) + 1j*slicer_pam2(imag(x));
else
    error("The selected modulation is not supported");
end

end

