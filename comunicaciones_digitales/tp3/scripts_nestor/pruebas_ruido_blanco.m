clear all
close all

NFFT = 8*1024;

sigma = 0.1;
xi = sigma.*randn(1e6,1);
xq = sigma.*randn(1e6,1);
x = xi + 1j*xq;

fprintf(" Sigma: %2.2e \n", sigma);
fprintf(" Sigma real/imag: %2.2e \n", std(xi));
fprintf(" Variance real/imag: %2.2e \n", var(xi));
fprintf(" Variance complex: %2.2e \n", var(x));

figure
[pxx, f] = pwelch(xi, hanning(NFFT/4), 0, NFFT, 10);
plot(f, pxx)
grid on
title('PSD of the real part')

figure
[pxx, f] = pwelch(x, hanning(NFFT/4), 0, NFFT, 10);
plot(f, pxx)
grid on
title('PSD of the complex noise')

