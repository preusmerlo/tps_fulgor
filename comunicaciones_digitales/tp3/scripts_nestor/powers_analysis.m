clear all
close all

%% Basic TX QPSK o QAM16
BR = 32e9; %Bd
L = .2e6; % Simulati on Length
N = 4; % Oversampling rate
fs = N*BR; % Sampling rate to emulate analog domain
T = 1/BR; % Time interval between two consecutive symbols
Ts = 1/fs; % Time between 2 conseutive samples at Tx output
rolloff = 0.1;
M=4;

% Two symbols generation (+1,-1) for QPSK
xi = 2*randi([0,1],L,1)-1; % Real
xq = 2*randi([0,1],L,1)-1; % Imag
x = xi + 1j*xq;

% Upsampling to change sampling rate
xup = sqrt(N)*upsample(x,N); % Normalizo para que la energia por segundo no cambie (truquito)

% Pulse shaping con RRC
htx = rcosine(BR, fs, 'sqrt', rolloff, 10); 
s_t = filter(htx,1,xup);
     
% Por un lado proceso la señal transmitida con el filtro de recepcion
mf = conj(htx(end:-1:1)); 
mf = mf/sum(mf); % Normalizo el filtro para que tenga ganancia 1 en f=0
r_t = filter(mf, 1, s_t);

% Finalmente downsampleo (elegir bien la fase de muestreo)
n0=1;
rx_symbs = r_t(n0:N:end);
figure
plot(real(rx_symbs(100e3:110e3)),'.') % To check phase

figure
NFFT = 4096;
fvec = 0:fs/NFFT:fs-fs/NFFT;
HTX = fft(htx, NFFT);
plot(fvec/1e9, abs(HTX));
grid on
xlabel('Frequency [GHz]')
ylabel('Magnitude')
title('Freq. Response of the TX Filter')

figure
NFFT = 4096;
[pxx, fvec] = pwelch(x, hanning(NFFT/4), 0, NFFT, BR);
plot(fvec/1e9, pxx);
grid on
xlabel('Frequency [GHz]')
ylabel('PSD')
title('PSD of the transmitted symbols')

figure
NFFT = 4096;
[pxx, fvec] = pwelch(xup, hanning(NFFT/4), 0, NFFT, fs);
plot(fvec/1e9, pxx);
hold all
[pxx, fvec] = pwelch(s_t, hanning(NFFT/4), 0, NFFT, fs);
plot(fvec/1e9, pxx);
[pxx, fvec] = pwelch(r_t, hanning(NFFT/4), 0, NFFT, fs);
plot(fvec/1e9, pxx);
legend('Filter Input', 'Filter Output', 'MF Output')
grid on
xlabel('Frequency [GHz]')
ylabel('PSD')
title('PSD of the transmitted signal')

fprintf(" \n")
fprintf(" Potencia de los simbolos transmitidos: %2.2e \n", var(x));
fprintf(" Potencia de los simbolos sobremuestreados: %2.2e \n", var(xup));
fprintf(" Potencia de la señal transmitida: %2.2e \n", var(s_t));
fprintf(" Potencia de la señal recibida: %2.2e \n", var(r_t));
fprintf(" Potencia de los simbolos recibidos: %2.2e \n", var(rx_symbs));

%% Agrego ruido
No = 0.1; % varianza del ruido complejo
noise_real = sqrt(No/2)*randn(size(s_t)); % Lo que acompaña al ruido es el sigma (desvio estandard)
noise_imag = sqrt(No/2)*randn(size(s_t)); % Asigno la mitad de la POTENCIA a cada componente
noise = noise_real + 1j*noise_imag;

noise_prima_t = filter(mf, 1, noise);
rx_noise = noise_prima_t(n0:N:end);

figure
NFFT = 4096;
[pxx, fvec] = pwelch(noise, hanning(NFFT/4), 0, NFFT, fs);
plot(fvec/1e9, pxx);
hold all
[pxx, fvec] = pwelch(noise_prima_t, hanning(NFFT/4), 0, NFFT, fs);
plot(fvec/1e9, pxx);
legend('White Noise', 'Noise after filter')
grid on
xlabel('Frequency [GHz]')
ylabel('PSD')
title('PSD of the noise signals')

fprintf(" \n")
fprintf(" Potencia del ruido en el canal: %2.2e \n", var(noise));
fprintf(" Potencia del ruido recibido: %2.2e \n", var(noise_prima_t));
fprintf(" Potencia del ruido desp del downsample: %2.2e \n", var(rx_noise));


fprintf(" \n")
fprintf(" SNR en el canal: %2.2e \n", var(s_t)/var(noise));
fprintf(" SNR en el slicer: %2.2e \n", var(rx_symbs)/var(rx_noise));
% La SNR en el slicer es N veces la SNR en el canal




