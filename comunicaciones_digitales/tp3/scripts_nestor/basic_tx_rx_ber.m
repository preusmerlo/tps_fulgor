clear all
close all

%% Basic TX QPSK o QAM16
BR = 32e9; %Bd
L = 5000000; % Simulati on Length
N = 4; % Oversampling rate
fs = N*BR; % Sampling rate to emulate analog domain
T = 1/BR; % Time interval between two consecutive symbols
Ts = 1/fs; % Time between 2 conseutive samples at Tx output
rolloff = 0.5;
M=4;
EbNo_dB = 9; % dB
SCALE_FACTOR=2;

% Calculos de la SNR
EbNo = 10^(EbNo_dB/10);
SNR_Slc = EbNo * log2(M);
SNR_canal = SNR_Slc / N;

% Two symbols generation (+1,-1) for QPSK
xi = 2*randi([0,1],L,1)-1; % Real
xq = 2*randi([0,1],L,1)-1; % Imag
x = xi + 1j*xq;

% Upsampling to change sampling rate
xup = upsample(x,N);

% Pulse shaping con RRC
htx = rcosine(BR, fs, 'sqrt', rolloff, 10); 
yup = SCALE_FACTOR*filter(htx,1,xup);

%% Agrego ruido
% SNR_canal [dB] = EbNo [dB] + 10*log10(k) - 10*log10(N); % Formula que por ahi se ve en internet
% k = log2(M)
Psignal = var(yup);
Pnoise = Psignal/SNR_canal;
No = Pnoise; % varianza por unidad de frecuencia
noise_real = sqrt(No/2)*randn(size(yup)); % Lo que acompaña al ruido es el sigma (desvio estandard)
noise_imag = sqrt(No/2)*randn(size(yup));
noise = noise_real + 1j*noise_imag;
% NO VALE noise= (1+1j)*randn(size(yup))
rxs = yup + noise;

% 
% figure
% [pxx, f] = pwelch(noise_real, hanning(512), 0, 1024, 1);
% plot(f, pxx);
% xlabel("Frequency [GHz]")
% ylabel("PSD [V**2/Hz]")
% grid on


%% Filtrar con el matched filter
mf = conj(htx(end:-1:1));

% figure
% plot(htx)
% hold all
% plot(mf)
% plot(conv(htx, mf))
% grid on
% legend('Tx Filter', 'MF', 'TOTAL')

y = filter(mf, 1, rxs)/SCALE_FACTOR;
Eh = sum(abs(htx).^2);
y_norm = y/Eh;

% Muestreo a tasa t=kT
% Es lo mismo que subsamplear por la tasa N
To = 1;
z = y_norm(To:N:end);

% Checkeo que la fase de muestreo este bien
% scatterplot(z)

%% Slicer
a_hat = slicer_qam_M(z, M);
figure
plot(real(z), '.r');
hold all
plot(real(a_hat), 'xb');
legend("Slicer Input","Slicer Output")

%% Calcular la BER
% 1. Alinear
d0 = finddelay(x, z);
guard = 10e3;
a_hat_align = a_hat (1+d0+guard:end-guard);
x_align = x (1+guard:end-guard-d0);

% 2. OPCION 1 (millenial, facil)
simbolos_totales = length(x_align);
simbolos_errados = sum(x_align ~= a_hat_align);
symbol_error_rate = simbolos_errados / simbolos_totales
aprox_ber = 1/log2(M) * symbol_error_rate
ber_teo = berawgn(EbNo_dB, 'qam', M)


