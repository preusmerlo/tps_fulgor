function [odata] = transmisor_MQAM(iparams)
    
    % Config por default
    config.M = 4; % Cantidad de niveles de la modulacion
    config.Nos = 2; % Tasa de sobremuestreo
    config.Lsymbs = 100e3; % Cantidad de simbolos
    config.rolloff = 0.1; % Rolloff del filtro conformador
    config.pulse_shaping_ntaps = 200;
    config.pulse_shaping_type = 0; % 0: RRC, 1: RC
    
    fn = fieldnames(iparams);
    for k = 1:numel(fn)
        if isfield(config,(fn{k}))==1
            config.(fn{k})= iparams.(fn{k});
        else
            error("%s: Parametro del simulador no valido", fn{k})
        end
    end
    
    M = config.M;
    Nos = config.Nos;
    Lsymbs = config.Lsymbs;
    rolloff = config.rolloff;
    pulse_shaping_ntaps = config.pulse_shaping_ntaps;
    pulse_shaping_type = config.pulse_shaping_type;
    
    % Two symbols generation (+1,-1) for QPSK
    dec_labels = randi([0 M-1], Lsymbs, 1);
    tx_symbs = qammod(dec_labels,M);

    % Upsampling to change sampling rate
    xup = upsample(tx_symbs,Nos);

    % Pulse shaping con RRC-RC
    if pulse_shaping_type==0
        type='sqrt';
    else
        type='normal';
    end
    span_simbolos = floor(pulse_shaping_ntaps/2/Nos);
    htx = rcosine(1, Nos, type ,rolloff, span_simbolos); 
    yup = filter(htx,1,xup);
    clear xup
    
    odata.oversampled_output = yup;
    odata.tx_symbs = tx_symbs;
        
end

