clear 
close all

%% FILTRO TARGET
h = fir1(6,0.7);        % Canal target
h = h/sum(h);           % Normaliza

% figure; stem(h)
% figure; freqz(h)

%% ENTRADA
% Puede ser blanca (alpha = 0) o coloreada (0 < alpha < 1)
xk = randn(1000e3,1);            % Datos blancos (gausiano)

alpha = 0.0;                    % Filtro IIR (tp1)
xk = filter(1, [1 -alpha], xk); % Coloreado de ak
xk = xk/std(xk);                % Normalizo para var = 1

fprintf("Varianza de la entrada: %2.2e\n", var(xk));

%% AUTOCORRELACION DE LA ENTRADA (SOLO ES UN ANALISIS)
L = 1 ;  % Cantidad de taps de memoria de la entrada
         % Si hay ISI es cuantos simbolos para adelante y atras afecta el
         % simbolo actual.

corrv = xcorr(xk,xk,L, 'biased'); % Calculo  de autocorrelacion
r = corrv(L+1:end);               % Parte causal de la autocorrelacion
CorrMtx = toeplitz(r, conj(r));   % Matriz de autocorrelacion de la entrada
                                  % calculada como esta en el libro (�?)

fprintf("Matriz de autocorrelacion de la entrada:\n")
fprintf([repmat('\t%f\t', 1, size(CorrMtx, 2)) '\n'], CorrMtx');

fprintf("Autovalores de la matrix de autocorrelacion:\n")
eigenValue = eig(CorrMtx);
fprintf([repmat('\t%f\t', 1, size(eigenValue, 2)) '\n'], eigenValue');

% IMPORTANTE:
% Para xk blanca    CorrMtx  = I. Sus autovalores son todos 1 (iguales).
% Para xk coloreada CorrMtx != I. Sus autov son != 1 y distintos entre si.

%% FILTRADO CON FILTRO TARGET Y AGREGADO DE RUIDO
zk = filter(h,1,xk);            % Filtro con target
nk = 0.1*randn(length(zk),1);   % Vector de ruido
yk = zk + nk;                   % Salida del filtro target + ruido

%% ALGORITMO LMS
NTAPS = 15 ;                % Cantidad de coeficientes del filtro adaptivo
beta = .25e-2/4;            % Paso

xbuffer  = zeros(NTAPS,1);           % Buffer de convolucion (rk)
ck       = zeros(NTAPS,1);           % Vector de coeficientes del filtro
ck_log   = zeros(length(yk), NTAPS); % Evolucion de ck en el tiempo
gradient = zeros(NTAPS,1);           % Gradiente
Ek       = zeros(length(yk),1);      % Error
yk_prima = zeros(length(yk),1);      % Salida del filtro adaptivo

INPUT_DELAY  = 0 ;
OUTPUT_DELAY = 0 ;

for k = INPUT_DELAY + OUTPUT_DELAY + 1 : length(xk)
    xbuffer(2:end) = xbuffer(1:end-1) ; % Shift buffer de entrada
    xbuffer(1)     = xk(k-INPUT_DELAY); % Mete muestra en el inicio del buffer
    yk_prima(k)    = (ck.') * xbuffer ; % Convolucion matricial
    
    Ek(k) = yk_prima(k) - yk(k-OUTPUT_DELAY); % Calculo de error
    
    gradient = Ek(k).* conj(xbuffer);   % Gradiente del error
    
    ck_log(k,:) = ck;                   % Guarda coeficientes
    ck = ck - beta*gradient;            % Actualiza coeficientes
end

%% RESULTADO FINAL
figure
Lh = length(h); Mh = (Lh-1)/2;
time_h = -Mh:Mh;
stem(time_h, h)
hold all

%ck_eff = [zeros(INPUT_DELAY,1); ck];
ck_eff  = ck;
gd=grpdelay(ck_eff); gd0 = gd(1);
time_c= -gd0:(length(ck_eff)-gd0-1);
stem(time_c,  ck_eff)
grid on

legend('Respuesta del canal', 'Respuesta del LMS')

%% ERROR INSTANTANEO
figure
plot(Ek)
title('Error instantaneo')
grid on
xlabel('Tiempo [samples]')

%% CALCULO DEL MSE
figure
mse = abs(Ek).^2;
mse = filter(ones(1e3,1)./1e3, 1, mse);
plot(10*log10(mse))
title('MSE')
grid on
xlabel('Tiempo [samples]')
ylabel('MSE [dB]')

%% EVOLUCION DE LOS COEFICIENTES EN EL TIEMPO
figure
plot(ck_log)
grid on
xlabel('Tiempo [samples]')
ylabel('Magnitud de los coeficientes')
