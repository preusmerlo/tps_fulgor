%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : main.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 10 Jul. 2021
% Description 	: digital communications simulator 
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                               IMPORTANT
%                               *********
%    To run the simulator it is necessary to go to the simulator/
%    directory in MATLAB and then, from this point, execute main.m
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                            PATH SETTINGS
%---------------------------------------------------------------------%

addpath(strcat(pwd,'\src\' )) ;  % Add src  path to MATLAB paths
addpath(strcat(pwd,'\test\')) ;  % Add test path to MATLAB paths

%---------------------------------------------------------------------%
%                           SIMULATOR SETTINGS 
%---------------------------------------------------------------------%

clear;

% ------------ Simulation settings --------------
config.simulationIterations         = 1       ; % Must be  >= 1          (not iterable)
config.sim.simulationLength         = 50e3    ; % Must be  >= 1          (  iterable  )

% --------------- Plots settings ----------------
config.sim.showPlots                = 1       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportPlots              = 0       ; % (1) Yes  : (0) No      (  iterable  )

% -- TX plots --
config.sim.numberOfSymbolsToShow    = 15      ; % Must be  > 0           (  iterable  )
config.sim.transmittedSymbols       = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedSignals       = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedConstellation = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.transmittedEyeDiagram    = 0       ; % (1) Yes  : (0) No      (  iterable  )

% -- TX vs RX plots --
% FIXME: Verificar que este plot es coherente (ta medio roto, creo que los labels no estan del todo bien)
config.sim.TxRxSpectrum             = 1       ; % (1) Yes  : (0) No      (  iterable  )

% -- RX plots --
config.sim.receivedSignals          = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.receivedEyeDiagram       = 0       ; % (1) Yes  : (0) No      (  iterable  )

% TODO: cambiar colores de este plot
config.sim.receivedConstellation    = 1       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.correlatorOutVsSlicerOut = 1       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.slicerInputHistogram     = 1       ; % (1) Yes  : (0) No      (  iterable  )

% TODO: Agregar plot del espectro plegado (Lo mostro nestor en una de las clases)
% TODO: Agregar plot de los filtros anti alias y el filtro del canal (superpuesto a las psd del ruido y señal)
% TODO: Agregar guarda a los plots para no ver la convergencia del FSE + CMA
% TODO: Agregar plot de los coeficientes de FSE vs tiempo (o iteracion del algoritmo)

% --------------- Logs settings -----------------
% TODO: Guardar BER y SER con una guarda para no contar en la convergencia del FSE
config.sim.exportLogs               = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportSER                = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.sim.exportBER                = 0       ; % (1) Yes  : (0) No      (  iterable  )

% ---- Digital communications system settings ---
config.sys.modulationType           = 1       ; % (1) QAM  : (0) PAM     (  iterable  )
config.sys.symbolRate               = 32e9    ; % Must be  >  0          (  iterable  )
config.sys.mapperLevels             = 4       ; % Must be =   2,4 or 8   (  iterable  )
config.sys.oversamplingFactor       = 4       ; % TX and CH factor       (  iterable  )
config.sys.dspOversamplingFactor    = 2       ; % RX (FSE factor         (  iterable  )

% ------------ Transmitter settings -------------
config.tx.filterType                = 1       ; % (1) RRC  : (0) RC      (  iterable  )
config.tx.filterLength              = 150     ; % Must be  >  1          (  iterable  )
config.tx.filterRolloff             = 0.1     ; % Must be 0 < x <= 1     (  iterable  )

% -------------- Channel settings ---------------
% -- Noise --
config.ch.addNoise                  = 1       ; % (1) Yes  : (0) No      (  iterable  )
config.ch.EbNo_dB                   = 12       ; % Must be real           (  iterable  )

% FIXME: Me parece que  filterCutFrequency en Hz es mas comodo  
% -- ISI --
config.ch.bandwidthType             = 0       ; % (1) LPF  : (0) Ideal   (  iterable  )
config.ch.filterLength              = 10       ; % Must be  >  1          (  iterable  )
config.ch.filterCutFrequency        = 0.5     ; % 0 < fc < 1 (BR = 0.25) (  iterable  )

% ------------- Receiver settings ---------------

% -- Down converter --
config.rx.dwConvPhaseError          = 0       ; % [deg]                  (  iterable  )
config.rx.dwConvfrequencyOffset     = 0       ; % [Hz]                   (  iterable  )

% -- Anti-alias filter --
% FIXME: Me parece que  antialiasCutFrequency en Hz es mas comodo 
config.rx.antialiasLength           = 15      ; % Must be  >  1          (  iterable  )
config.rx.antialiasCutFrequency     = 0.9     ; % 0 < fc < 1             (  iterable  )

% -- ADC --
config.rx.adcInitialPhase           = 0       ; %                        (  iterable  )

% -- Automatic gain control --
% FIXME: para QAM64 hay que tunear bien esto sino da cualquier cosa
config.rx.agcOutputLevel            = sqrt(2) ; % [rms]                  (  iterable  )

% -- FSE --
config.rx.fseLength                 = 31      ; % Must be  >  1           (  iterable  )

% -- LMS --
config.rx.lmsStepCMA                = 2^(-9)  ; % LMS step with CMA       (  iterable  )
config.rx.lmsStepDecisionDriven     = 2^(-11) ; % LMS step with DD        (  iterable  )
config.rx.lmsTapsLeakage            = 1e-4    ; % Taps forgetting factor  (  iterable  )

% -- CMA --
config.rx.forceCMA                  = 0       ; % (1) Yes  : (0) No      (  iterable  )
config.rx.symbolsWithCMAEnabled     = 3e3     ; % Symbols quantity       (  iterable  )

%---------------------------------------------------------------------%
%                                RUN 
%---------------------------------------------------------------------%
tic
sim = simulator(config);
sim.run();
toc
%---------------------------------------------------------------------%
%                           SIMULATOR END 
%---------------------------------------------------------------------%