%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : transmitter.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 7 Jul. 2021
% Description 	: digital communications transmitter class
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                           TRANSMITTER CLASS
%---------------------------------------------------------------------%

classdef transmitter

%---------------------------------------------------------------------%
%                           PRIVATE PROPIETIES
%---------------------------------------------------------------------%
    
    properties (GetAccess = public, SetAccess = private)
        modulationType     {mustBeInteger                 } = 1    ; % Tipo de transmisor (0:PAM o 1:QAM)
        symbolRate         {mustBePositive, mustBeReal    } = 32e9 ; % Baud Rate BR
        mapperLevels       {mustBePositive, mustBeInteger } = 2    ; % Cantidad de niveles del mapper M
        
        filterType         {mustBeInteger                 } = 1    ; % Tipo de filtro conformador (0:RC o 1:RRC)
        filterLength       {mustBePositive, mustBeInteger } = 201  ; % Coeficientes del filtro
        filterRolloff      {mustBePositive, mustBeReal    } = 0.1  ; % Exceso de ancho de banda del filtro
        
        oversamplingFactor {mustBePositive, mustBeInteger } = 8    ; % Factor de sobremuestreo
    end

%---------------------------------------------------------------------%
%                         DEPENDENT PROPIETIES
%---------------------------------------------------------------------%
    
    properties (Dependent)
        filterTaps                                                  % Cantidad de coeficientes del filtro
        filterSamplingFreq                                          % Frecuencia  de sampling  del filtro
        configStruct                                                % Estructura con la configuracion del tx
    end
    
%---------------------------------------------------------------------%
%                           PUBLIC METHODS
%---------------------------------------------------------------------%
    methods

        %-------------------------------------------------------------%
        %                       CONSTRUCTOR METHOD
        %-------------------------------------------------------------%
        function obj = transmitter(configStruc)
            % transmitter - Return an transmitter object
            %
            % Syntax: tx = transmitter()

            if nargin == 0
                return
            end
            
            fields = fieldnames(configStruc);
            % Define campos contenidos en 'fields'
            for i = 1:length(fields)
                switch fields{i}
                    case 'modulationType'
                        obj.modulationType = configStruc.modulationType;
                    case 'symbolRate'
                        obj.symbolRate = configStruc.symbolRate ;
                    case 'mapperLevels'
                        obj.mapperLevels = configStruc.mapperLevels ;
                    case 'filterType'
                        obj.filterType = configStruc.filterType ;
                    case 'filterLength'
                        obj.filterLength = configStruc.filterLength ;
                        obj.filterLength = obj.filterLength - mod(obj.filterLength,2) + 1; % Force odd 
                    case 'filterRolloff'
                        obj.filterRolloff = configStruc.filterRolloff ;
                    case 'oversamplingFactor'
                        obj.oversamplingFactor = configStruc.oversamplingFactor ;
                    otherwise
                        error(strcat('Invalid configuration field -> ',fields{i}));
                end
            end
        end

        %-------------------------------------------------------------%
        %                         GETTER METHODS
        %-------------------------------------------------------------%

        function value = get.filterSamplingFreq(obj)
            value = obj.oversamplingFactor * obj.symbolRate ;
        end
        
        function value = get.filterTaps(obj)
            if obj.filterType == 1
                value = raiz_cos_realzado(obj.symbolRate, obj.filterSamplingFreq, obj.filterRolloff, obj.filterLength);
            else
                value = cos_realzado(obj.symbolRate, obj.filterSamplingFreq, obj.filterRolloff, obj.filterLength);
            end
        end

        function value = get.configStruct(obj)
            value.modulationType        = obj.modulationType     ;
            value.symbolRate            = obj.symbolRate         ;
            value.mapperLevels          = obj.mapperLevels       ;
            value.filterType            = obj.filterType         ;
            value.filterLength          = obj.filterLength       ;
            value.filterRolloff         = obj.filterRolloff      ;
            value.oversamplingFactor    = obj.oversamplingFactor ;
        end
        
        function value = get.modulationType(obj)
            value = obj.modulationType;
        end
                
        function value = get.mapperLevels(obj)
            value = obj.mapperLevels;
        end

        %-------------------------------------------------------------%
        %                         SETTER METHODS
        %-------------------------------------------------------------%

        
        %-------------------------------------------------------------%
        %                         MAIN METHOD
        %-------------------------------------------------------------%

        function [tx_out, tx_ak] = transmit (obj, simulationLength)
            % transmit - Create random symbols, shape it  and return output continuous signal
            %
            % Syntax: [tx_outup, ak] = transmit()
             
            % QAM transmitter
            if obj.modulationType == 1
                ak_I  = 2 * randi([0,obj.mapperLevels-1], simulationLength, 1) - (obj.mapperLevels-1);
                ak_Q  = 2 * randi([0,obj.mapperLevels-1], simulationLength, 1) - (obj.mapperLevels-1);
                tx_ak = ak_I + 1j * ak_Q;
            
            % PAM transmitter
            else
                tx_ak = 2 * randi([0,obj.mapperLevels-1], simulationLength, 1) - (obj.mapperLevels-1);
            end
            
            % Pulse shaping (With aux variable its most fast)
            tx_ak_up = upsample(tx_ak,obj.oversamplingFactor); % Con la variable intermedia tarda menos
            tx_out = filter(obj.filterTaps, 1, tx_ak_up);
        end
    end

%---------------------------------------------------------------------%
%                           PRIVATE METHODS
%---------------------------------------------------------------------%


%---------------------------------------------------------------------%
%                       TRANSMITTER CLASS END
%---------------------------------------------------------------------%
end

