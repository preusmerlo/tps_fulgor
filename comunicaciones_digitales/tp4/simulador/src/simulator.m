%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : simulator.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 7 Jul. 2021
% Description 	: digital communications simulator class
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                           SIMULATOR CLASS
%---------------------------------------------------------------------%

classdef simulator < handle
    
%---------------------------------------------------------------------%
%                           PRIVATE PROPIETIES
%---------------------------------------------------------------------%

    properties (Access = private)
        % ------------ Simulation settings --------------
        simulationIterations      {mustBePositive, ...
                                   mustBeInteger } % Number of simulations 
        
        simulationLength          {mustBePositive, ...
                                   mustBeInteger } % Number of points per simulation 
        outputBasePath                             % Output directory path 

        % --------------- Plots settings ----------------
        plotsPath                                  % Path a donde exportar plots
        showPlots                 {mustBeInteger } % (1) Yes : (0) No
        exportPlots               {mustBeInteger } % (1) Yes : (0) No
        
        % -- TX plots --
        numberOfSymbolsToShow     {mustBeInteger , ...
                                   mustBePositive} % Must be > 0 and < simulatorLength
        transmittedSymbols        {mustBeInteger } % (1) Yes : (0) No
        transmittedSignals        {mustBeInteger } % (1) Yes : (0) No
        transmittedConstellation  {mustBeInteger } % (1) Yes : (0) No
        transmittedEyeDiagram     {mustBeInteger } % (1) Yes : (0) No
        
        % -- TX vs RX plots --
        TxRxSpectrum              {mustBeInteger } % (1) Yes : (0) No

        % -- RX plots --
        receivedSignals           {mustBeInteger } % (1) Yes : (0) No
        receivedEyeDiagram        {mustBeInteger } % (1) Yes : (0) No
        receivedConstellation     {mustBeInteger } % (1) Yes : (0) No
        correlatorOutVsSlicerOut  {mustBeInteger } % (1) Yes : (0) No
        slicerInputHistogram      {mustBeInteger } % (1) Yes : (0) No

        % ---------------- Logs settings ----------------
        logsPath                                   % Logs path
        exportLogs                                 % (1) Yes : (0) No
        exportSER                                  % (1) Yes : (0) No
        exportBER                                  % (1) Yes : (0) No

        % -------- System constructive blocks -----------
        tx                                         % Transmisor (obj de transmitter)
        rx                                         % Receptor   (obj de receiver   )
        ch                                         % Canal      (obj de channel    )
    
        % ----------- Configuration arrays --------------
        simDynamicConfig                           % (1) Yes : (0) No
        simConfigArrays                            % Simulator   configuration arrays structure to iterate
        
                                                   % System is always dynamic if tx and/or rx are dynamic 
        sysConfigArrays                            % Com. system configuration arrays structure to iterate

        txDynamicConfig                            % (1) Yes : (0) No
        txConfigArrays                             % Transmitter configuration arrays structure to iterate
    
        chDynamicConfig                            % (1) Yes : (0) No
        chConfigArrays                             % Channel     configuration arrays structure to iterate
    
        rxDynamicConfig                            % (1) Yes : (0) No
        rxConfigArrays                             % Receiver    configuration arrays structure to iterate
        
    end
    properties (GetAccess = public, SetAccess = private)
        
        % --------------- System signals ----------------
        tx_ak                                      % Transmitted symbols
        tx_out                                     % Transmitted signal
        ch_out                                     % Channel output signal
        rx_ak                                      % Decided symbols
        rx_filter_out                              % Match filter output signal
        rx_correlator_out                          % Slicer input samples
    
    end
    
%---------------------------------------------------------------------%
%                           PUBLIC METHODS
%---------------------------------------------------------------------%
    methods

        %-------------------------------------------------------------%
        %                       CONSTRUCTOR METHOD
        %-------------------------------------------------------------%
        function obj = simulator (configStruct)

            if nargin == 0
                error('Specify the simulator config structure');
            end
            
            % --- Set simulator settings ---
            obj.simulationIterations        = configStruct.simulationIterations ;

            % --- Configure TX, RX and CH for defined communications system ---
            configStruct.tx.modulationType        = configStruct.sys.modulationType        ;
            configStruct.ch.modulationType        = configStruct.sys.modulationType        ;
            configStruct.rx.modulationType        = configStruct.sys.modulationType        ;
            
            configStruct.tx.symbolRate            = configStruct.sys.symbolRate            ;
            configStruct.rx.symbolRate            = configStruct.sys.symbolRate            ;
            
            configStruct.tx.mapperLevels          = configStruct.sys.mapperLevels          ;
            configStruct.ch.mapperLevels          = configStruct.sys.mapperLevels          ;
            configStruct.rx.mapperLevels          = configStruct.sys.mapperLevels          ;
            
            configStruct.tx.oversamplingFactor    = configStruct.sys.oversamplingFactor    ;
            configStruct.ch.oversamplingFactor    = configStruct.sys.oversamplingFactor    ;
            configStruct.rx.oversamplingFactor    = configStruct.sys.oversamplingFactor    ;

            configStruct.rx.dspOversamplingFactor = configStruct.sys.dspOversamplingFactor ;

            % --- Create simulation vectors ---
            [obj.simConfigArrays , obj.simDynamicConfig] = obj.simulationVectorCreator( configStruct.sim ) ;
            [obj.txConfigArrays  , obj.txDynamicConfig ] = obj.simulationVectorCreator( configStruct.tx  ) ;
            [obj.chConfigArrays  , obj.chDynamicConfig ] = obj.simulationVectorCreator( configStruct.ch  ) ;
            [obj.rxConfigArrays  , obj.rxDynamicConfig ] = obj.simulationVectorCreator( configStruct.rx  ) ;

            % --- Create the out directory if it doesnt exist ---
            obj.outputBasePath = strcat(pwd,'\out\');
            if ~exist(obj.outputBasePath, 'dir')
                mkdir(obj.outputBasePath);
            end            
        end

        %-------------------------------------------------------------%
        %                         SETTER METHODS
        %-------------------------------------------------------------%

        function setNewConfiguration(obj,configStruct)
            f = fieldnames(configStruct);
            for i = 1:length(f)
                obj.(f{i})  =  configStruct.(f{i});
            end
        end

        %-------------------------------------------------------------%
        %                         MAIN METHOD
        %-------------------------------------------------------------%
        function run(obj)

            for i = 1:obj.simulationIterations
                fprintf('Running iteration %d ... \n',i);
                
                % Set iteration settings
                obj.configNewIteration(i);
                
                % Create simulator directories structure 
                obj.createIterationDirectory();

                % Run new iteration
                obj.runIteration();
            end
        end
    end

%---------------------------------------------------------------------%
%                           PRIVATE METHODS
%---------------------------------------------------------------------%
    methods (Access = private)

        %-------------------------------------------------------------%
        %                     CONFIG NEW ITERATION
        %-------------------------------------------------------------%

        function configNewIteration(obj, idx)
            % Update simulator
            if ( obj.simDynamicConfig || idx == 1)
                obj.setNewConfiguration( obj.getIterationSettings('sim', idx));
            end

            % Creates new transmitter
            if ( obj.txDynamicConfig  || idx == 1)
                obj.tx = transmitter( obj.getIterationSettings('transmitter', idx));
            end

            % Creates new channel
            if ( obj.chDynamicConfig  || idx == 1 )                
                obj.ch = channel( obj.getIterationSettings('channel', idx));
            end

            % Creates new receiver
            if ( obj.rxDynamicConfig  || idx == 1 )
                obj.rx = receiver( obj.getIterationSettings('receiver', idx));
            end
        end

        %-------------------------------------------------------------%
        %                     GET ITERATION SETTINGS
        %-------------------------------------------------------------%

        function configStruct = getIterationSettings(obj, block, idx)
            switch (block)
                case 'sim'
                    fields = fieldnames(obj.simConfigArrays);         
                    matrix = cell2mat(struct2cell(obj.simConfigArrays));

                case 'transmitter'
                    fields = fieldnames(obj.txConfigArrays);         
                    matrix = cell2mat(struct2cell(obj.txConfigArrays));
                    
                case 'receiver'
                    fields = fieldnames(obj.rxConfigArrays);         
                    matrix = cell2mat(struct2cell(obj.rxConfigArrays));

                case 'channel'
                    fields = fieldnames(obj.chConfigArrays);         
                    matrix = cell2mat(struct2cell(obj.chConfigArrays));

                otherwise
                    error('Unknown building block.');
            end
            iterationValues = num2cell(matrix(:,idx));
            configStruct    = cell2struct(iterationValues(:), fields(:));
        end

        %-------------------------------------------------------------%
        %                         RUN ITERATION
        %-------------------------------------------------------------%
        
        function runIteration(obj)
            % Generate simbols and shaped signal
            [obj.tx_out, obj.tx_ak] = obj.tx.transmit(obj.simulationLength);
            
            % Add channel noise and ISI        
            obj.ch_out = obj.ch.run(obj.tx_out);
            
            % Receive signal
            % TODO: HACER ESTO MENOS HARCODEADO
            CMAReference = sqrt(mean(abs(obj.tx_ak(1:1000)).^4)/mean(abs(obj.tx_ak(1:1000)).^2));
            [obj.rx_ak, obj.rx_correlator_out , obj.rx_filter_out] = obj.rx.receive(obj.ch_out, CMAReference);
            
            % Plots
            if obj.exportPlots || obj.showPlots
                obj.systemPlots();
            end

            % Logs
            if obj.exportLogs && all(obj.exportBER | obj.exportSER)
                obj.systemErrors();
            end
        end

        %-------------------------------------------------------------%
        %                       SYSTEM ERRORS
        %-------------------------------------------------------------%

        function systemErrors(obj)                        
            d0            = finddelay(obj.tx_ak, obj.rx_correlator_out);
            guard         = (obj.tx.filterLength - 1)/2;
            rx_ak_align   = obj.rx_ak (1 + d0 + guard : end-guard);
            tx_ak_align   = obj.tx_ak (1 + guard : end-guard - d0);

            totalSymbols  = length(tx_ak_align);
            wrongSymbols  = sum(tx_ak_align ~= rx_ak_align);
            SER           = wrongSymbols / totalSymbols;

            if obj.tx.modulationType
                aproximateBER = SER/log2(obj.tx.mapperLevels^2);
            else
                aproximateBER = SER/log2(obj.tx.mapperLevels  );
            end

            if obj.tx.modulationType
                if obj.tx.mapperLevels >= 4
                    theoreticalBER = berawgn(obj.ch.EbNo_dB, 'qam', obj.tx.mapperLevels^2);
                else
                    theoreticalBER = berawgn(obj.ch.EbNo_dB, 'psk', obj.tx.mapperLevels^2,'nondiff');
                end
            else
                theoreticalBER = berawgn(obj.ch.EbNo_dB, 'pam', obj.tx.mapperLevels);
            end
            
            % Create BER file 
            if obj.exportBER
                fd = fopen(strcat(obj.logsPath,'ber.txt'),'w');
                fprintf(fd,'%d\t%f\t%e\tModulationOrder / EbNo[dB] / theoreticalBER\n',...
                                            obj.tx.mapperLevels^2,obj.ch.EbNo_dB, theoreticalBER);
                fprintf(fd,'%d\t%f\t%e\tModulationOrder / EbNo[dB] / aproximateBER\n',...
                                            obj.tx.mapperLevels^2,obj.ch.EbNo_dB, aproximateBER );
                fclose(fd);
            end

            % Create SER file 
            if obj.exportSER
                fd = fopen(strcat(obj.logsPath,'ser.txt'),'w');
                fprintf(fd,'%d\t%f\t%e\tModulationOrder / EbNo[dB] / SER\n',obj.tx.mapperLevels^2,obj.ch.EbNo_dB,SER);
                fclose(fd);
            end
        end

        %-------------------------------------------------------------%
        %                       VECTOR CREATOR
        %-------------------------------------------------------------%

        function [outputStruct, isDynamic] = simulationVectorCreator(obj, inputStruct)
            outputStruct = inputStruct;
            fn = fieldnames(inputStruct);

            % --- Verifies if exist vectors in inputStruct ---
            if length(fn) == length(struct2array(inputStruct))
                % Input struct don't have vector, is not dynamic.
                isDynamic = 0 ;
            else
                % The inputStruct has at least one vector, it is dynamic.
                isDynamic = 1 ;
                
                % Expand input vectors
                aux = zeros(1,obj.simulationIterations);
                for fieldName = 1:numel(fn)
                    fieldLen = length(inputStruct.(fn{fieldName}));
                    if  fieldLen < obj.simulationIterations
                        aux(1 : fieldLen)      = inputStruct.(fn{fieldName})(1:end);
                        aux(fieldLen + 1: end) =  repelem(aux(fieldLen) , ... 
                                                        obj.simulationIterations - fieldLen);
                        outputStruct.(fn{fieldName}) = aux;
                    else
                        outputStruct.(fn{fieldName}) = inputStruct.(fn{fieldName})(1:obj.simulationIterations);
                    end
                end
            end
        end
        
        %-------------------------------------------------------------%
        %                     DIRECTORY CREATOR
        %-------------------------------------------------------------%
        
        function outputPath = createIterationDirectory(obj)
            if obj.tx.modulationType == 1
                mtype = 'QAM';
            else
                mtype = 'PAM';
            end

            if obj.tx.filterType == 1
                ftype = 'RRC';
            else
                ftype = 'RC';
            end

            if obj.ch.addNoise
                ebno =  sprintf('%.2f',obj.ch.EbNo_dB);
            else
                ebno = 'inf';
            end
            outputPath = sprintf( '%s%s_%d_EbNodB_%s_%s_rolloff_%.2f' , ...
                                    obj.outputBasePath                  , ...
                                    mtype                               , ...
                                    obj.tx.mapperLevels^2               , ...
                                    ebno                                , ...
                                    ftype                               , ...
                                    obj.tx.filterRolloff                );
            
            outputPath    = strcat(outputPath,'\'    );
            obj.plotsPath = strcat(outputPath,'\img\');
            obj.logsPath  = strcat(outputPath,'\log\');
            
            % Create the img directory if it doesnt exist
            if obj.exportPlots
                if ~exist(obj.plotsPath, 'dir')
                    mkdir(obj.plotsPath);
                end
            end

            % Create the log directory if it doesnt exist
            if obj.exportLogs
                if ~exist(obj.logsPath , 'dir')
                    mkdir(obj.logsPath);
                end
            end
            
        end
    
        %-------------------------------------------------------------%
        %                       PLOTS MAIN METHOD
        %-------------------------------------------------------------%
        function  systemPlots(obj)

            % Transmitted symbols
            if obj.transmittedSymbols
                obj.plotTransmittedSymbols();
            end
            
            % Transmitted Signals
            if obj.transmittedSignals
                obj.plotTransmittedSignals();
            end

            % Transmitted Constellation
            if obj.transmittedConstellation
                obj.plotTransmittedConstellation();
            end

            % Transmitted Eye Diagram
            if obj.transmittedEyeDiagram
                obj.plotTransmittedEyeDiagram();
            end
            
            % TX spectrum vs RX spectrum
            if obj.TxRxSpectrum
                obj.plotTxRxSpectrum();
            end

            % Received signals
            if obj.receivedSignals
                obj.plotReceivedSignals();
            end

            % Received Eye Diagram
            if obj.receivedEyeDiagram
                obj.plotReceivedEyeDiagram();
            end

            % Received Constellation
            if obj.receivedConstellation
                obj.plotReceivedConstellation();
            end

            % Correlator Out Vs Slicer Out
            if obj.correlatorOutVsSlicerOut
                obj.plotCorrelatorOutVsSlicerOut();
            end

            % Slicer input Histogram
            if obj.slicerInputHistogram
                obj.plotSlicerInputHistogram();
            end

        end

        %-------------------------------------------------------------%
        %                           PLOTS
        %-------------------------------------------------------------%

        function plotTransmittedSymbols(obj)
            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end

            for i = [1,2]
                subplot(2,1,i)
                if i == 1
                    stem(real(obj.tx_ak(1:obj.numberOfSymbolsToShow)),'--r','filled');                
                    % legend({'$a_k^I[n]$'},'Interpreter','latex','Location','east')
                    title('Transmitted I Symbols ','Interpreter','latex','FontSize', 18)
                else
                    stem(imag(obj.tx_ak(1:obj.numberOfSymbolsToShow)),'--r','filled');
                    % legend({'$a_k^Q[n]$'},'Interpreter','latex','Location','east','FontSize', 16)
                    title('Transmitted Q Symbols ','Interpreter','latex','FontSize', 18)
                end
                %grid on
                xlabel('Discreet time','Interpreter','latex','FontSize', 16)
                ylabel('Amplitude','Interpreter','latex','FontSize', 16)

                ylim([-(obj.tx.mapperLevels-1)*(1+0.3),(obj.tx.mapperLevels-1)*(1+0.3)])
                xlim([0,obj.numberOfSymbolsToShow+1])
            end
            set(gcf, 'Position', [100 100 400 400],'Color', 'w');

            % Export plot as .svg
            if obj.exportPlots == 1
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_tx_ak.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_tx_ak.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end
        end

        % ESTA FUNCION NO ALINEA BIEN LOS SIMBOLOS (¡¡¡CORREGIR URGENTEMENTE!!!)
        function plotTransmittedSignals(obj)
            LPLOT = round(obj.numberOfSymbolsToShow * obj.tx.oversamplingFactor + (obj.tx.filterLength-1)/2);
            t_symbols = (0:obj.numberOfSymbolsToShow -1  ).* (1/obj.tx.symbolRate            );
            t_samples = (0:LPLOT    -1  ).* (1/obj.tx.filterSamplingFreq    );
            delay_t   = (obj.tx.filterLength)/(2*obj.tx.filterSamplingFreq);

            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end
            
            for i = 1:2
                subplot(2,1,i)
                if i == 1
                    stem((t_symbols + delay_t), real(obj.tx_ak((1:obj.numberOfSymbolsToShow))),'--r','filled');
                    hold all
                    plot(t_samples, real(obj.tx_out(1:LPLOT)),'b-', 'Linewidth',1)
                    title('Tx I Output','Interpreter','latex','FontSize', 18)
                else
                    stem((t_symbols + delay_t), imag(obj.tx_ak((1:obj.numberOfSymbolsToShow))),'--r','filled');
                    hold all
                    plot(t_samples, imag(obj.tx_out(1:LPLOT)),'b-', 'Linewidth',1)
                    title('Tx Q Output','Interpreter','latex','FontSize', 18)
                end                
                ylabel('Amplitude','Interpreter','latex','FontSize', 16)
                xlabel('Time [sec]','Interpreter','latex','FontSize', 16)
                grid on
                xlim([0, t_samples(LPLOT)])
                set(gcf, 'Position', [100 100 800 400],'Color', 'w');
            end
            
            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_tx_out.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_tx_out.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end
        end

        function plotTransmittedConstellation(obj)
            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end

            plot(0,0,'w')
            hold on
            scatter(real(obj.tx_ak),imag(obj.tx_ak),'r','filled')
            grid on
            title('Transmitted Constellation','interpreter','latex','FontSize', 18)
            xlabel('In-Phase','interpreter','latex','FontSize', 16)
            ylabel('Quadrature','interpreter','latex','FontSize', 16)
            xlim([-obj.tx.mapperLevels,obj.tx.mapperLevels])
            ylim([-obj.tx.mapperLevels,obj.tx.mapperLevels])
            
            set(gcf, 'Position', [100 100 400 400],'Color', 'w') ;

            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_tx_constellation.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_tx_constellation.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end            
        end

        function plotTransmittedEyeDiagram(obj)            
            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end
            guard = (obj.tx.filterLength-1)/2;
            eyediagramPlus(obj.tx_out(guard:end-guard),obj.tx.oversamplingFactor*3,1,0,'b-');
            set(gcf, 'Position', [100 100 950 400],'Color', 'w');

            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_tx_eye.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_tx_eye.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end
        end

        function plotTxRxSpectrum(obj)

            NFFT = 1024;
            fs   = obj.tx.filterSamplingFreq;
            
            % PSD tx_out
            Ptx      = pwelch(obj.tx_out, hanning(NFFT/2), 0, NFFT, fs);
            Ptx      = Ptx ./ Ptx(1);

            % PSD rx_in (ch_out)
            Pch      = pwelch(obj.ch_out, hanning(NFFT/2), 0, NFFT, fs);
            Pch      = Pch ./ Pch(1);

            % PSD rx_mf (Match filter output)
            [Prx, f] = pwelch(obj.rx_filter_out, hanning(NFFT/2), 0, NFFT, fs);
            Prx      = Prx ./ Prx(1);
            
            if ~isreal(obj.tx_out)              
                Ptx = fftshift(Ptx);
                Pch = fftshift(Pch);
                Prx = fftshift(Prx);
                f   = (f - fs/2);
            end

            f = f./1e9;

            Ptx_dB   = 10*log10(Ptx);        
            Pch_dB   = 10*log10(Pch);       
            Prx_dB   = 10*log10(Prx);        
            

            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end

            subplot(1,2,1)
            plot(f, Pch_dB,'-r', 'Linewidth',1)
            hold on            
            plot(f, Ptx_dB,'-b', 'Linewidth',1)
            grid on
            title('PSD Tx out Vs Channel out','Interpreter','latex','FontSize', 18)
            ylabel('PSD Magnitude [dB]','Interpreter','latex','FontSize', 16)
            xlabel('Frequency [GHz]','Interpreter','latex','FontSize', 16)
            ylim([min(Prx_dB),10])
            xlim([f(1), f(end)])
            legend({'Channel out', 'Tx out'},'Interpreter','latex','Location','south','FontSize', 16)
            
            subplot(1,2,2)
            plot(f, Pch_dB,'-r', 'Linewidth',1)
            hold on
            plot(f, Prx_dB,'-b', 'Linewidth',1)
            grid on
            title('PSD Rx filter out Vs Channel out','Interpreter','latex','FontSize', 18)
            ylabel('PSD Magnitude [dB]','Interpreter','latex','FontSize', 16)
            xlabel('Frequency [GHz]','Interpreter','latex','FontSize', 16)
            ylim([min(Prx_dB),10])
            xlim([f(1), f(end)])
            legend({'Channel out','Rx filter out'},'Interpreter','latex','Location','south','FontSize', 16)
            
            set(gcf, 'Position', [100 100 950 400],'Color', 'w');
            
            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_PSD_TxRxCh.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_PSD_TxRxCh.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end
        end

        function plotReceivedSignals(obj)
            SPLOT_OVER = obj.simulationLength/2 * obj.tx.oversamplingFactor;
            LPLOT = round(obj.numberOfSymbolsToShow * obj.tx.oversamplingFactor + (obj.tx.filterLength-1)/2);
            t_samples = SPLOT_OVER .* (0 : LPLOT -1  ).* (1/obj.tx.filterSamplingFreq );
            signal_span = obj.ch_out(SPLOT_OVER: SPLOT_OVER - 1 + LPLOT);

            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end
            
            for i = 1:2
                subplot(2,1,i)
                if i == 1
                    plot(t_samples, real(signal_span),'b-', 'Linewidth',1);
                    title('RX input - Channel I','Interpreter','latex','FontSize', 18)
                else
                    plot(t_samples, imag(signal_span),'b-', 'Linewidth',1)
                    title('RX input - Channel Q','Interpreter','latex','FontSize', 18)
                end                
                ylabel('Amplitude','Interpreter','latex','FontSize', 16)
                xlabel('Time [sec]','Interpreter','latex','FontSize', 16)
                grid on
                xlim([0, t_samples(LPLOT)])
                
            end  
            set(gcf, 'Position', [100 100 800 400],'Color', 'w');

            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_rx_input.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_rx_input.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end
        end

        function plotReceivedEyeDiagram(obj)
            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end

            guard = (obj.tx.filterLength-1)/2;
            eyediagramPlus(obj.ch_out(guard:end-guard),obj.tx.oversamplingFactor*3,1,0,'b-');
            set(gcf, 'Position', [100 100 950 400],'Color', 'w');

            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_rx_eye.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_rx_eye.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end            
        end

        function plotReceivedConstellation(obj)
            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end

            plot(0,0,'w')
            hold on
            guard = (obj.tx.filterLength-1)/2;

            if ((obj.ch.addNoise || obj.ch.bandwidthType) && obj.tx.modulationType)
                scatplot(real(obj.rx_correlator_out(guard:end-guard)),...
                        imag(obj.rx_correlator_out(guard:end-guard)));

            else
                scatter(real(obj.rx_correlator_out(guard:end-guard)), ...
                imag(obj.rx_correlator_out(guard:end-guard)),'r','filled')
            end


            grid on
            title('Received Constellation','interpreter','latex','FontSize', 18)
            xlabel('In-Phase','interpreter','latex','FontSize', 16)
            ylabel('Quadrature','interpreter','latex','FontSize', 16)
            xlim([-obj.tx.mapperLevels,obj.tx.mapperLevels])
            ylim([-obj.tx.mapperLevels,obj.tx.mapperLevels])
    
            if ((obj.ch.addNoise || obj.ch.bandwidthType) && obj.tx.modulationType)
                set(gcf, 'Position', [100 100 480 400],'Color', 'w') ;
            else
                set(gcf, 'Position', [100 100 400 400],'Color', 'w') ;
            end

            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_rx_constellation.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_rx_constellation.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end
           
        end

        function plotCorrelatorOutVsSlicerOut(obj)
            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end

            guard = (obj.tx.filterLength-1)/2;

            for i = [1,2]
                subplot(1,2,i)
                if i == 1
                    plot(real(obj.rx_correlator_out(guard:end)), '.r');
                    hold all
                    plot(real(obj.rx_ak((guard:end))), 'xb');
                    title('Slicer - Channel I','interpreter','latex','FontSize', 18)
                else
                    plot(imag(obj.rx_correlator_out(guard:end)), '.r');
                    hold all
                    plot(imag(obj.rx_ak((guard:end))), 'xb');
                    title('Slicer - Channel Q','interpreter','latex','FontSize', 18)
                end
                xlim([0,obj.simulationLength-guard])
                ylim([-obj.rx.mapperLevels,obj.rx.mapperLevels])
                xlabel('Samples','interpreter','latex','FontSize', 16)
                ylabel('Amplitude','interpreter','latex','FontSize', 16)
                legend({"Slicer Input","Slicer Output"},'interpreter','latex','Location','east','FontSize', 16)
            end
            set(gcf, 'Position', [100 100 950 400],'Color', 'w') ;

            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_rx_slicer.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_rx_slicer.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end
        end

        function plotSlicerInputHistogram(obj)   
            if obj.showPlots == 1
                figure
            else
                figure('visible','off');
            end
            
            guard = (obj.tx.filterLength-1)/2;
            
            for i = [1,2]
                subplot(1,2,i)
                if i == 1
                    histogram(real(obj.rx_correlator_out(guard:end)),80);
                    title('Channel I','interpreter','latex','FontSize', 18)
                else
                    histogram(imag(obj.rx_correlator_out(guard:end)),80);
                    title('Channel Q','interpreter','latex','FontSize', 18)
                end
                ylabel('Repetitions','interpreter' ,'latex','FontSize', 16)
                xlabel('Slicer input','interpreter','latex','FontSize', 16)
            end
                set(gcf, 'Position', [100 100 950 400],'Color', 'w') ;

            if obj.exportPlots == 1
                % Export plot as .svg
                if obj.tx.modulationType
                    saveas(gcf, sprintf('%sQAM%d_rx_slicer_input_histogram.svg',obj.plotsPath,obj.tx.mapperLevels^2), 'svg')
                else
                    saveas(gcf, sprintf('%sPAM%d_rx_slicer_input_histogram.svg',obj.plotsPath,obj.tx.mapperLevels  ), 'svg')
                end
            end    
        end

    end

%---------------------------------------------------------------------%
%                       TRANSMITTER CLASS END
%---------------------------------------------------------------------%
end
