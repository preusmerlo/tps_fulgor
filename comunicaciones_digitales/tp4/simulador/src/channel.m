%---------------------------------------------------------------------%
%						    Simulador QAM-M
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Filename      : channel.m
% Programmers   : Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
% Created on	: 7 Jul. 2021
% Description 	: digital communications channel class
%---------------------------------------------------------------------%

%---------------------------------------------------------------------%
%                           CHANNEL CLASS 
%---------------------------------------------------------------------%

classdef channel < handle

%---------------------------------------------------------------------%
%                           PRIVATE PROPIETIES
%---------------------------------------------------------------------%

    properties (GetAccess = public, SetAccess = private)
        % -- System settings --
        modulationType      {mustBeInteger  } = 1    ; % (0):PAM o (1)QAM 
        mapperLevels        {mustBePositive ,...
                             mustBeInteger  } = 2    ; 
        oversamplingFactor  {mustBePositive ,...
                             mustBeInteger  } = 8    ; 

        % -- Noise settings --
        addNoise            {mustBeInteger   } = 0    ; % (1) Yes : (0) No                   
        EbNo_dB             { mustBeReal     } = 1000 ;

        % -- ISI settings --
        bandwidthType       { mustBeInteger  } = 0    ;
        filterLength        { mustBeReal     } = 127  ;
        filterCutFrequency  { mustBePositive ,...
                              mustBeReal     ,...
        mustBeLessThan(filterCutFrequency,1) } = 0.5  ;
        
        % -- Filter taps --
        channelTaps 
    end

%---------------------------------------------------------------------%
%                           PUBLIC METHODS
%---------------------------------------------------------------------%

    methods

        %-------------------------------------------------------------%
        %                         CONSTRUCTOR
        %-------------------------------------------------------------%
        function obj = channel(configStruct)

            if nargin == 0
                return
            end

            fields = fieldnames(configStruct);
            for i = 1:length(fields)
                switch fields{i}  
                    % --- System parameters ---
                    case 'modulationType'
                        obj.modulationType = configStruct.modulationType;
                    case 'mapperLevels'
                        obj.mapperLevels = configStruct.mapperLevels;
                    case 'oversamplingFactor'
                        obj.oversamplingFactor = configStruct.oversamplingFactor;
                    
                    % --- Noise parameters ---
                    case 'EbNo_dB'
                        obj.EbNo_dB    = configStruct.EbNo_dB;
                    case 'addNoise'
                        obj.addNoise   = configStruct.addNoise;
                    
                    % --- ISI parameters ---
                    case 'bandwidthType'
                        obj.bandwidthType = configStruct.bandwidthType;
                    case 'filterLength'
                        obj.filterLength = configStruct.filterLength;
                    case 'filterCutFrequency'
                        obj.filterCutFrequency = configStruct.filterCutFrequency;
                    
                    otherwise
                        error(strcat('Invalid configuration field -> ',fields{i}));
                end
            end

            % Creates channel filter
            if obj.bandwidthType
                obj.channelTaps = fir1(obj.filterLength , obj.filterCutFrequency);
            end
        end

        %-------------------------------------------------------------%
        %                         MAIN FUNCTION
        %-------------------------------------------------------------%

        function ch_out = run ( obj, tx_out)
            
            % -- Add ISI --
            if obj.bandwidthType
                tx_out = filter(obj.channelTaps, 1, tx_out);
            end            
            
            % -- Add noise --
            if obj.addNoise
                if obj.modulationType
                    SNR_slicer  = 10^(obj.EbNo_dB/10) * log2(obj.mapperLevels^2);
                else
                    SNR_slicer  = 10^(obj.EbNo_dB/10) * log2(obj.mapperLevels  );
                end

                SNRChannel = SNR_slicer / obj.oversamplingFactor;
                noisePower = var(tx_out) / SNRChannel;
                
                if isreal(tx_out)
                    channelDesviation = sqrt(noisePower/2);
                    noise = channelDesviation * randn(size(tx_out));
                else
                    channelDesviation = sqrt(noisePower/2);
                    realNoise = channelDesviation * randn(size(tx_out)) ;
                    imagNoise = channelDesviation * randn(size(tx_out)) ;
                    noise = realNoise + 1j * imagNoise ;    
                end

                ch_out = tx_out + noise;
            else
                ch_out = tx_out ;
            end
            
        end
    end

%---------------------------------------------------------------------%
%                           PRIVATE METHODS
%---------------------------------------------------------------------%


%---------------------------------------------------------------------%
%                       RECEIVER CLASS END
%---------------------------------------------------------------------%
end