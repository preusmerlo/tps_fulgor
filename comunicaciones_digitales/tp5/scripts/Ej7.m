%---------------------------------------------------------------------%
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Programmers: Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
%---------------------------------------------------------------------%

% Ejercicio 7 - PLL en recuperacion de portadora

clear; clc; close all;

% Parámetros de modulacion QAM
BR      = 32e9  ;    % Baud Rate [Bd]
L       = 1e3   ;    % Longitud de simulacion
fs      = BR    ;    % Frecuencia de muestreo
T       = 1/BR  ;    % Tiempo entre simbolos
Ts      = 1/fs  ;    % Tiempo entre muestras
M       = 4     ;     % Orden de modulacion
EbNo_dB = 20    ;    % EbNO en dB

% Parámetros de error en la portadora del LO en Rx
delta_freq  = 000e6     ; % Offset del LO   [Hz]
LW          = 000e3     ; % Ancho de linea  [Hz]
theta0      = 45/180*pi  ; % Fase inicial    [deg]

%% Calculo de SNR
EbNo = 10^(EbNo_dB/10)  ;
SNR_Slc = EbNo * log2(M);
SNR_canal = SNR_Slc     ;

%% Generacion de simbolos con ruido

% Generacion de simbolos QAM-M
xsymb = qammod(randi([0 M-1], L,1), M);

% Agregado de ruido
% SNR_canal [dB] = EbNo [dB] + 10*log10(k) - 10*log10(N) --%%-- k = log2(M)
Psignal = var(xsymb);
Pnoise = Psignal/SNR_canal;
No = Pnoise;                % Varianza por unidad de frecuencia
noise_real = sqrt(No/2)*randn(size(xsymb)); % sqrt implica desv estandar
noise_imag = sqrt(No/2)*randn(size(xsymb));
noise = noise_real + 1j*noise_imag;

rx_noisy = xsymb + noise;   % Signal con ruido
clear noise noise_real noise_imag

%% Agregado de defectos de portadora

Ldata= length(rx_noisy);
time = (0:Ldata-1).'.*Ts;
lo_offset = exp(1j*2*pi*delta_freq*time);   % LO offset de frecuencia
phase_offset = exp(1j*theta0);              % LO offset de fase
% Para el ruido de fase:
freq_noise = sqrt(2*pi*LW/fs).*randn(Ldata,1);
phase_noise = cumsum(freq_noise);           % Proceso de Wiener (integral)
osc_pn = exp(1j.*phase_noise);              % LO ruido de fase
    
rxs = rx_noisy.*lo_offset.*phase_offset.*osc_pn;    %Signal con defectos

%% PLL como carrier recovery
Ldata           = length(rxs)   ;
Kp              = 20e-2         ;
Ki              = Kp/500        ;
phase_error     = zeros(Ldata,1);
nco_output      = zeros(Ldata,1);
integral_branch = zeros(Ldata,1);
pll_output      = zeros(Ldata,1);   %Salida del carrier recovery

for m=2:Ldata
    xpll = rxs(m);
    derot_x = xpll*exp(-1j*nco_output(m-1));
    pll_output(m) = derot_x;
    
    a_hat = slicer(derot_x, M);
    phase_error(m) = angle(derot_x .* conj(a_hat));
    prop_error = phase_error(m) * Kp;
    integral_branch(m) = integral_branch(m-1) + Ki*phase_error(m);
    nco_output(m) = nco_output(m-1) + prop_error +integral_branch(m);
end

%% Plots

% Constelacion a la entrada del carrier recovery (CR)
figure
scatter(real(rxs),imag(rxs),'b','filled');
grid on
title('Constelacion entrada FCR','interpreter','latex')
xlabel('a-I','interpreter','latex')
ylabel('a-Q','interpreter','latex')
hold on
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej7_constIN.svg', 'svg')

% Constelacion a la salida del CR
figure
scatter(real(pll_output(Ldata/2:end)),imag(pll_output(Ldata/2:end)),'r','filled');
grid on
title('Constelacion salida FCR','interpreter','latex')
xlabel('a-I','interpreter','latex')
ylabel('a-Q','interpreter','latex')
hold on
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej7_constOUT.svg', 'svg')