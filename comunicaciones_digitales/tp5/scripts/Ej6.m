%---------------------------------------------------------------------%
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Programmers: Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
%---------------------------------------------------------------------%

% Ejercicio 6 - PLL simulacion custom

clear; clc; close all;

% Parametros
fs = 16e9;               % Frecuencia de muestreo
Ts = 1/fs;               % Periodo de muestreo

Kp = 0.01;               % Ganancia proporcional
Ki = Kp/100;            % Ganancia integral

L = 10;                  % Latencia en la rama de realimentación
Length = 5e4;            % Longitud de simulacion

%% Generacion de la entrada

input = 'Ramp';
switch(input)
    case 'Step'
        tita0 = 20; %Grados
        tita_in = tita0/180*pi*ones(Length,1);
    case 'Ramp'
        f0 = 200e6; %Hz
        w0 = 2*pi*f0;
        t = (0:Length-1).*Ts;
        tita_in = w0.*t;
    case 'Sine'
        fn = 90e6;
        wn = 2*pi*fn;
        tita0 = 15; %Grados
        t = (0:Length-1).*Ts;
        tita_in = 15/180*pi * sin(wn.*t);
end


%% Modelado

error       = zeros(Length,1);   % Salida del detector de fase 
error_prop  = zeros(Length,1);   % Salida del controlador proporcional
error_int   = zeros(Length,1);   % Salida del controlador integral
nco_in      = zeros(Length,1);   % Entrada del NCO
tita_out    = zeros(Length,1);   % Fase de salida
tita_delay  = zeros(Length,1);   % Fase de salida retardada con latencia

DELAY_LINE  = zeros(L+1,1)   ;   % FIFO para modelar la latencia

for n=2:Length
    % Detector de fase
    error(n) = tita_in(n) - tita_delay(n-1);
    
    % Filtro de lazo (Controlador PI)
    error_prop(n) = Kp*error(n);
    error_int(n) = error_int(n-1)+Ki*error(n);
    nco_in(n) = error_prop(n)+error_int(n);
    
    % NCO (integrador)
    tita_out(n) = tita_out(n-1)+nco_in(n);
    
    % Modelado del retardo
    % Asumo que los datos entran por el final y salen por el frente
    % Primero roto, luego cargo el nuevo valor al final
    DELAY_LINE = [DELAY_LINE(2:end); tita_out(n)]; 
    % Finalmente, logueo la senial retardada
    tita_delay(n) = DELAY_LINE(1); 
end

%% Plots

t = (0:Length-1).*Ts;

% Plot respuesta a la entrada
figure
plot(t/1e-6, tita_out, '-r', 'Linewidth',1.5);
hold all
plot(t/1e-6, tita_in, '--b','Linewidth',1.5)
xlabel('Time [us]','interpreter','latex')
ylabel('Amplitudes [rad]','interpreter','latex')
grid on
legend({'Output', 'Input'},'interpreter','latex')
title('Salida vs entrada','interpreter','latex')
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
switch(input)
    case 'Step'
        xlim([0,0.2])
    case 'Ramp'
        xlim([0,0.1])
    case 'Sine'
        xlim([0,0.1])
end
saveas(gcf,'img/Ej6_SalidaVsEntrada.svg', 'svg')

% Plot error estacionario (salida del PD)
figure
plot(t/1e-6, error, '-b', 'Linewidth',1.5)
hold all
xlabel('Time [us]','interpreter','latex')
ylabel('Amplitude [rad]','interpreter','latex')
grid on
title('Evolucion del error','interpreter','latex')
legend({'Error de fase'},'interpreter','latex')
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
xlim([0,0.1])
if strcmp(input,'Sine')
    xlim([0,0.1])
end
%saveas(gcf,'img/Ej6_Error.svg', 'svg')

% Plot salida del filtro 
figure
plot(t/1e-6, error_prop, '-b', 'Linewidth',1.5)
hold all
plot(t/1e-6, error_int, '-r', 'Linewidth',1.5)
plot(t/1e-6, nco_in, '--k', 'Linewidth',1.5)
xlabel('Time [us]','interpreter','latex')
ylabel('Amplitude [rad]','interpreter','latex')
grid on
legend({'Error Prop.', 'Error Int.', 'NCO In'},'interpreter','latex')
title('Salida del filtro de lazo','interpreter','latex')
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
xlim([0,0.1])
if strcmp(input,'Sine')
    xlim([0,0.1])
end
%saveas(gcf,'img/Ej6_SalidaFiltro.svg', 'svg')

% Plot error Integral escalado 
figure
plot(t/1e-6, error_int*fs/2/pi/1e6, '-r', 'Linewidth',1.5)
xlabel('Time [us]','interpreter','latex')
ylabel('Frequency [MHz]','interpreter','latex')
grid on
title('Offset de portadora en rama integral','interpreter','latex')
legend({'Carrier offset'},'interpreter','latex')
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
xlim([0,0.1])
if strcmp(input,'Sine')
    xlim([0,0.1])
end
%saveas(gcf,'img/Ej6_ErrorIntegralEscalado.svg', 'svg')