%---------------------------------------------------------------------%
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Programmers: Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
%---------------------------------------------------------------------%

% Ejercicio 4 - PLL con Ki > 0

clear; clc; close all;

% Parametros
fs = 16e9;                      % Frecuencia de muestreo
Ts = 1/fs;                      % Periodo de muestreo

Kp = 0.01;                      % Ganancia proporcional
Ki = [Kp/100,Kp/200,Kp/500];    % Ganancia integral

%% Modelado
z = tf('z',Ts);

%Primer Kp
G_z_1 = (Kp+Ki(1)*(z/(z-1)))*(z/(z-1));
H_z_1 = z^(-1);
F_z_1 = feedback(G_z_1,H_z_1);

[F_z_mag_1,~,wout_1] = bode(F_z_1);             % Rta en frecuencia
[y_e_1,t_e_1] = step(F_z_1);                    % Rta al escalon
[y_r_1,t_r_1] = step(F_z_1*(z/(z-1)));          % Rta a la rampa

%Segundo Kp
G_z_2 = (Kp+Ki(2)*(z/(z-1)))*(z/(z-1));
H_z_2 = z^(-1);
F_z_2 = feedback(G_z_2,H_z_2);

[F_z_mag_2,~,wout_2] = bode(F_z_2);             % Rta en frecuencia
[y_e_2,t_e_2] = step(F_z_2);                    % Rta al escalon
[y_r_2,t_r_2] = step(F_z_2*(z/(z-1)));          % Rta a la rampa

%Tercer Kp
G_z_3 = (Kp+Ki(3)*(z/(z-1)))*(z/(z-1));
H_z_3 = z^(-1);
F_z_3 = feedback(G_z_3,H_z_3);

[F_z_mag_3,~,wout_3] = bode(F_z_3);             % Rta en frecuencia
[y_e_3,t_e_3] = step(F_z_3);                    % Rta al escalon
[y_r_3,t_r_3] = step(F_z_3*(z/(z-1)));          % Rta a la rampa

[y_rampa,t_rampa] = step(z/(z-1));              % Rampa para grafica

%% Plots

% Respuesta al escalon
figure
plot(t_e_1/1e-9,y_e_1,'-r','Linewidth',1.5);
hold on
plot(t_e_2/1e-9,y_e_2,'-b','Linewidth',1.5);
plot(t_e_3/1e-9,y_e_3,'-m','Linewidth',1.5);
plot(t_e_3/1e-9,ones(size(t_e_3)),'--k','Linewidth',1.5);
grid on
ylabel('Amplitud','interpreter','latex')
xlabel('Time [$nseg$]','interpreter','latex')
legend({strcat('Ki=',string(Ki(1))),strcat('Ki=',string(Ki(2))),...
    strcat('Ki=',string(Ki(3))),'Escalon'},'interpreter','latex')
title('Respuesta al escalon del PLL T1','interpreter','latex')
ylim([0,1.8])
xlim([0,100])
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej4_RtaEscalon.svg', 'svg')

% Respuesta a la rampa
figure
plot(t_r_1/1e-9,y_r_1,'-r','Linewidth',1.5);
hold on
plot(t_r_2/1e-9,y_r_2,'-b','Linewidth',1.5);
plot(t_r_3/1e-9,y_r_3,'-m','Linewidth',1.5);
plot(t_rampa/1e-9,y_rampa,'--k','Linewidth',1.5);
grid on
ylabel('Amplitud','interpreter','latex')
xlabel('Time [$nseg$]','interpreter','latex')
legend({strcat('Ki=',string(Ki(1))),strcat('Ki=',string(Ki(2))),...
    strcat('Ki=',string(Ki(3))),'Rampa'},'interpreter','latex',...
    'location','northwest')
title('Respuesta a la rampa del PLL T1','interpreter','latex')
xlim([0,100])
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej4_RtaRampa.svg', 'svg')

% Respuesta en frecuencia
figure
semilogx(wout_1(:)./(2*pi),20*log10(F_z_mag_1(:)),'r','Linewidth',1.5);
hold on
semilogx(wout_2(:)./(2*pi),20*log10(F_z_mag_2(:)),'b','Linewidth',1.5);
semilogx(wout_3(:)./(2*pi),20*log10(F_z_mag_3(:)),'m','Linewidth',1.5);
grid on
xlabel('Frecuencia [Hz]','interpreter','latex')
ylabel('Magnitud [dB]','interpreter','latex')
plot([1 1]*fs/2,ylim,'--k','LineWidth', 1.5)
legend({strcat('Ki=',string(Ki(1))),strcat('Ki=',string(Ki(2))),...
    strcat('Ki=',string(Ki(3))),'fs/2=8GHz'},'interpreter','latex',...
    'location','southwest')
title('Respuesta en frecuencia del PLL T1','interpreter','latex')
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej4_RtaFrec.svg', 'svg')

% Diagrama de polos y ceros
figure
ax = axes();
pzmap(ax, F_z_1,'r');
hold on
%pzmap(F_z_2,'b');
pzmap(F_z_3,'b');
grid on
xlabel('Eje real','interpreter','latex')
ylabel('Eje imaginario','interpreter','latex')
title('Diagrama de polos y ceros','interpreter','latex')
legend({strcat('Ki=',string(Ki(1))),...
    strcat('Ki=',string(Ki(3)))},'interpreter','latex',...
    'location','southwest')
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
xlim([0.985,1])
ylim([-0.025,0.025])
%saveas(gcf,'img/Ej4_DiagramaPolosCeros.svg', 'svg')