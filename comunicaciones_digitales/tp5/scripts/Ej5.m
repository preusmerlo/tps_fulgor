%---------------------------------------------------------------------%
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Programmers: Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
%---------------------------------------------------------------------%

% Ejercicio 5 - PLL con latencia L

clear; clc; %close all;

% Parametros
fs = 16e9;               % Frecuencia de muestreo
Ts = 1/fs;               % Periodo de muestreo

Kp = 0.1;                % Ganancia proporcional
Ki = Kp/100;             % Ganancia integral

L = 10;                  % Latencia en la rama de realimentación

%% Modelado
z = tf('z',Ts);

G_z = (Kp+Ki*(z/(z-1)))*(z/(z-1));
H_z = z^(-L);
F_z = feedback(G_z,H_z);

[F_z_mag,~,wout] = bode(F_z);             % Rta en frecuencia

%% Plots

% Respuesta en frecuencia
figure
semilogx(wout(:)./(2*pi),20*log10(F_z_mag(:)),'Linewidth',1.5);
hold on
grid on
xlabel('Frecuencia [Hz]','interpreter','latex')
ylabel('Magnitud [dB]','interpreter','latex')
% xlim([1e7,1e10])
% ylim([0,100])
plot([1 1]*fs/2,ylim,'--k','LineWidth', 1.5)
legend({'Ki = 0','fs/2=8GHz'},'interpreter','latex','location','southwest')
title('Respuesta en frecuencia del PLL','interpreter','latex')
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej5_RtaFrec.svg', 'svg')

% Diagrama de polos y ceros
figure
ax = axes();
pzmap(F_z,'r');
grid on
xlabel('Eje real','interpreter','latex')
ylabel('Eje imaginario','interpreter','latex')
title('Diagrama de polos y ceros','interpreter','latex')
l_zero = findall(ax, 'tag', 'PZ_Zero');
l_pole = findall(ax, 'tag', 'PZ_Pole');
l_zero.LineWidth = 1.5;
l_pole.LineWidth = 1.5;
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej5_DiagramaPolosCeros.svg', 'svg')