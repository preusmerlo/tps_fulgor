%---------------------------------------------------------------------%
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Programmers: Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
%---------------------------------------------------------------------%

% Ejercicio 2 - PLL modelado matematicamente vs PLL Matlab ( fx bode)

clear; clc; close all;

% Parametros
fs = 16e9;              % Frecuencia de muestreo
Ts = 1/fs;              % Periodo de muestreo
paso = 1e6;             % Inversa de la resolucion de la Rta en frecuencia

Kp = 0.010;             % Ganancia proporcional
Ki = Kp/1000;           % Ganancia integral

f = 0:paso:fs/2;        % Vector frecuencia [Hz] 
w = 2*pi*f;             % Vector w          [rad/s]
w_d = w./fs;            % Frecuencia angular digital (De 0 a 2pi)

%% Rta en frecuencia PLL calculada
e = exp(1);
F_w = (Kp+Ki-Kp*e.^(-1j.*w_d))./...
    (1+(Kp+Ki-2)*e.^(-1j.*w_d)+(1-Kp)*e.^(-1j*2.*w_d));

F_mod = 20*log10(abs(F_w));
F_ph = (180/pi)*angle(F_w);

%% Rta en frecuencia utilizando Matlab
z = tf('z',Ts);

G_z = (Kp+Ki*(z/(z-1)))*(z/(z-1));
H_z = z^(-1);
F_z = feedback(G_z,H_z);

[F_z_mag,F_z_phase,wout] = bode(F_z);
F_z_mag = 20*log10(abs(F_z_mag));

%% Plots
figure
subplot(2,1,1)
semilogx(wout(:)./(2*pi),F_z_mag(:),'Linewidth',1.5);
hold on
semilogx(w./(2*pi),F_mod,'--r','Linewidth',1.5);
grid on
xlabel('Frecuencia [Hz]','interpreter','latex')
ylabel('Magnitud [dB]','interpreter','latex')
plot([1 1]*fs/2,ylim,'--k','LineWidth', 1.5)
legend({'Matlab','Calculada','fs/2=8GHz'},'interpreter','latex','location','southwest')
title('Respuesta en frecuencia del PLL','interpreter','latex')

subplot(2,1,2)
F_z_phase(1,1,end) = F_z_phase(1,1,end-1);
semilogx(wout(:)./(2*pi),F_z_phase(:),'Linewidth',1.5);
hold on
semilogx(w./(2*pi),F_ph,'--r','Linewidth',1.5);
grid on
xlabel('Frecuencia [Hz]','interpreter','latex')
ylabel('Fase [deg]','interpreter','latex')
plot([1 1]*fs/2,ylim,'--k','LineWidth', 1.5)
legend({'Matlab','Calculada','fs/2=8GHz'},'interpreter','latex','location','southwest')

set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej2_BodeCompare.svg', 'svg')