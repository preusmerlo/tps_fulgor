function a_hat = slicer(a,M)

    z_real = real(a);
    z_imag = imag(a);
    
    ak_I = -1 * ones(length(a));
    ak_Q = -1 * ones(length(a));

    switch M
        case 4
            ak_I(z_real > 0) = 1 ;
            ak_Q(z_imag > 0) = 1 ;
                        
        case 16
            ak_I(z_real >= 2 ) =  3;
            ak_I(z_real < -2 ) = -3;
            ak_I(z_real > 0  & z_real < 2) = 1;
                        
            ak_Q(z_imag >= 2 ) =  3;
            ak_Q(z_imag < -2 ) = -3;
            ak_Q(z_imag >  0 & z_imag < 2) = 1;                        
    
        case 64
            ak_I(z_real >= 4 ) =  5;
            ak_I(z_real < -4 ) = -5;
            ak_I(z_real >= 2 & z_real <  4) =  3;
            ak_I(z_real < -2 & z_real > -4) = -3;
            ak_I(z_real > 0  & z_real <  2) =  1;

            ak_Q(z_imag >= 4 ) =  5;
            ak_Q(z_imag < -4 ) = -5;
            ak_Q(z_imag >= 2 & z_real <  4) =  3;
            ak_Q(z_imag < -2 & z_real > -4) = -3;
            ak_Q(z_imag > 0  & z_real <  2) =  1;                    
                    
        otherwise
            error("The selected modulationType is not supported");
    end
    
    a_hat = ak_I + 1j*ak_Q;

end