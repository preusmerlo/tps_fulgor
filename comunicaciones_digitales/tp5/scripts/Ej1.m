%---------------------------------------------------------------------%
%					patricio.reus.merlo@gmail.com
%                       franrainero1@gmail.com
%                      stgoleguizamon@gmail.com
%
% Programmers: Patricio Reus Merlo; Francisco Rainero; Santiago Leguizamon
%---------------------------------------------------------------------%

% Ejercicio 1 - PLL modelado 

clear; clc; close all;

% Parametros
fs = 16e9;              % Frecuencia de muestreo
Ts = 1/fs;              % Periodo de muestreo
step = 1e5;             % Inversa de la resolución de la Rta en frecuencia

Kp = 0.005;             % Ganancia proporcional
Ki = Kp/100;            % Ganancia integral

f = 0:step:fs/2;        % Vector frecuencia [Hz] 
w = 2*pi*f;             % Vector w          [rad/s]
w_d = w./fs;            % Frecuencia angular digital (De 0 a 2pi)

% Rta en frecuencia PLL calculada
e = exp(1);
F_w = (Kp+Ki-Kp*e.^(-1j.*w_d))./...
    (1+(Kp+Ki-2)*e.^(-1j.*w_d)+(1-Kp)*e.^(-1j*2.*w_d));

F_mod = 20*log10(abs(F_w));
F_ph = (180/pi)*angle(F_w);

figure
subplot(2,1,1)
semilogx(w/(2*pi),F_mod,'r','Linewidth',1.5);
grid on
xlabel('Freq [Hz]','interpreter','latex')
ylabel('Magnitude [dB]','interpreter','latex')
xlim([1e6,1e10])
hold on
plot([1 1]*fs/2,ylim,'--k','LineWidth', 1.5)
legend({'Magnitud','fs/2=8GHz'},'interpreter','latex','location','southwest')
title('Respuesta en frecuencia del PLL','interpreter','latex');

subplot(2,1,2)
semilogx(w/(2*pi),F_ph,'-r','Linewidth',1.5);
grid on
xlabel('Freq [Hz]','interpreter','latex')
ylabel('Phase [deg]','interpreter','latex')
xlim([1e6,1e10])
hold on
plot([1 1]*fs/2,ylim,'--k','LineWidth', 1.5)
legend({'Fase','fs/2=8GHz'},'interpreter','latex','location','southwest')
set(gcf, 'Position', [100 100 600 600],'Color', 'w') ;
%saveas(gcf,'img/Ej1_Bode.svg', 'svg')