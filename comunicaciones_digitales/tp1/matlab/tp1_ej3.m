%% Filtro FIR - Promediador movil  

%   �IMPORTANTE!
%   Para ejecutar los bloques "guardar plot" y exportar las graficas desde
%   el script es necesario instalar el paquete export_fig como se indica:
%   
%   1) Descargar como .zip el repo 
%   2) Crear una carpeta llamada "Tools" en C:\Program Files\MATLAB 
%   3) Descomprimir el .zip en "Tools"
%   4) Ejecutar el comando "pathtool" en la terminal de MATLAB
%   5) Agregar la carpeta C:\Program Files\MATLABTools/<carpeta_descomprimida>/src
%   6) Abrir en MATLAB la carpeta donde esta ubicado este script
%   7) Crear una carpeta llamada "tex" en el directorio del script
%   
%   Si instala el paquete, no ejecute los bloques "guardado de plot".

%% Respuesta al impulso (a)
clear all
close all

M2 = 4;

% N    = Cantidad de coeficientes
N  = M2 + 1;            

% h[n] = Respuesta al impulso
h  = 1/N .* ones(1,N);  
n  = 0: 1: M2;

% Plot de h[n]
figure
stem(n,h,'--r','filled');
grid on
title('Respuesta al impulso','Interpreter','latex')
xlabel('Tiempo Discreto','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
legend({'$h[n]$'},'Interpreter','latex')
ylim([0,1/N*(1+0.3)])
xlim([-2,M2 + 2])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\h_n_FIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\h_n_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Respuesta en frecuencia - A partir del calculo analitico (b-1)
close all

NFFT = 1*1024;
delta_omega = 2*pi /NFFT;
w = 0: delta_omega: 2*pi - delta_omega; 

% Respuesta en frecuencia calculada analiticamente
H = (sin(w.*N/2)./(sin(w./2).*N)).* exp(-1j.*w.*M2./2);

H = fftshift(H);

% Grafico de modulo
figure;
plot(w-pi, abs(H),'-.r','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend({'$|H (e^{jw})|$'},'Interpreter','latex');
ylim([0,1]);
xlim([-pi,pi]);

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\mod_H_FIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Respuesta en frecuencia - A partir del calculo analitico (b-2)
close all

%Grafico de fase
figure;
plot(w-pi, angle(H),'.b','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend({'$/H (e^{jw})$'},'Interpreter','latex');
ylim([-pi,pi]);
xlim([-pi,pi]);

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\phase_H_FIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\phase_H_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Respuesta en frecuencia - |H(e^{jw})| [dB] (c-1)
close all

NFFT = 1*1024;
delta_omega = 2*pi /NFFT;
w = 0: delta_omega: 2*pi - delta_omega;

H_fft = fftshift(fft(h,NFFT));

figure;
plot(w - pi,20*log10(abs(H_fft)),'-.r','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex');
legend({'$|H (e^{jw})|_{fft}$'},'Interpreter','latex');
ylim([-60,0]);
xlim([-pi,pi]);

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\mod_H_dB_FIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_dB_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Respuesta en frecuencia - /H(e^{jw}) (c-2)
close all

figure;
plot(w - pi,angle(H_fft),'-.b','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia Discreta','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend({'$\angle H (e^{jw})_{fft}$'},'Interpreter','latex');
xlim([-pi,pi]);

%% Respuesta en frecuencia - |H(e^{jw})| [dB] (d-1)
close all

figure
plot(w - pi,20*log10(abs(H)),'r','LineWidth', 2);
hold on
plot(w - pi,20*log10(abs(H_fft)),'--b','LineWidth', 2);
grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex');
legend('$|H (e^{jw})|$','$|H (e^{jw})|_{fft}$','Interpreter','latex');
ylim([-60,0]);
xlim([-pi,pi]);

%% Guardado de plot

% Exporto plot en .png
%set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
%export_fig img\mod_H_dB_comparativa_FIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_dB_comparativa_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Respuesta en frecuencia - /H(e^{jw}) (d-2)
close all

figure;
hold on
plot(w - pi,angle(H),'r','LineWidth', 2);
plot(w - pi,angle(H_fft),'--b','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend('/H (e^{jw})','/H (e^{jw})_{fft}');
xlim([-pi,pi]);

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\phase_H_comparativa_FIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\phase_H_comparativa_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Barrido de M - Modulo en dB - (f)
close all

N_list = [4,8,16,32];
line_3dB  = ones(NFFT,1)*-3;

labels = {};
for idx = 1 : length(N_list)
    N = N_list(idx);
    h = 1/N.*ones(N,1);
    [H,w] = freqz(h,1,NFFT);
    plot(w,20*log10(abs(H)), '--', 'LineWidth', 1);
    labels{end +1} = sprintf('N = %d',N);
    hold all
    
end

plot(w,line_3dB, '-.k', 'LineWidth', 0.8);
labels{end +1} = sprintf('-3dB');
grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex');
legend(labels)
xlim([0,pi])
ylim([-60,0])


%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\mod_H_dB_barrido_FIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_dB_barrido_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Barrido de M - Modulo en veces - (f)
close all

N_list = [4,8,16,32];
labels = {};
for idx = 1 : length(N_list)
    N = N_list(idx);
    h = 1/N.*ones(N,1);
    [H,w] = freqz(h,1,NFFT);
    plot(w,abs(H), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('N = %d',N);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend(labels)
xlim([0,pi])
ylim([0,1])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\mod_H_barrido_FIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_barrido_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Barrido de M - Fase - (f)
close all

N_list = [4,8,16,32];
labels = {};
for idx = 1 : length(N_list)
    N = N_list(idx);
    h = 1/N.*ones(N,1);
    [H,w] = freqz(h,1,NFFT);
    plot(w,angle(H), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('N = %d',N);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend(labels)
xlim([0,pi])
ylim([-pi,pi/2])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\phase_H_barrido_FIR.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\phase_H_barrido_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Prueba del filtro FIR (complemento Item f)
close all
x = randn(500,1) + 1;
plot(x, '--k')
hold all

% Respuesta con M = 16
M = 16;
b = 1/(M).*ones(1,M);
a = 1;
y = filter(b, a, x);
plot(y, '-r', 'LineWidth', 2);

% Respuesta con M = 128
M = 128;
b = 1/(M).*ones(1,M);
a = 1;
y = filter(b, a, x);
plot(y, '-b', 'LineWidth', 2);


grid on
xlabel("Tiempo Discreto");
ylabel("Amplitud");
legend(["x[n]", "y[n]_{N=16}",'y[n]_{N=128}'])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\prueba_FIR.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\prueba_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% FIN
close all