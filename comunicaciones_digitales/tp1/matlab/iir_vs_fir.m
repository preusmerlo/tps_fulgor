
%% FIR equivalente a IIR con alpha = 0,95
clc
close all
clear all

% Filtro IIR
alpha = 0.95;
NFFT = 512;
delta_omega = 2*pi/NFFT;
omega = 0:delta_omega:2*pi-delta_omega;
H_IIR = (1-alpha)./(1-alpha*exp(-1j*omega));
H_IIR = fftshift(H_IIR);

% Filtro FIR
M = 54;
b = 1/(M).*ones(1,M);           % Rta al impulso del MAF
H_FIR = fft(b,NFFT);
H_FIR = fftshift(H_FIR);

%% Modulo
close all

line_3dB  = ones(NFFT,1)*-3;

figure
plot(omega-pi, 20*log10(abs(H_FIR)),'-b', 'LineWidth', 1)
hold on
plot(omega-pi, 20*log10(abs(H_IIR)), '--r', 'LineWidth', 1.3)
plot(omega-pi, line_3dB, '-.k', 'LineWidth', 0.8)
grid on
legend('FIR', 'IIR', '-3dB','Location','south')
ylabel('Magnitud [dB]','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex')
xlim([-pi,pi])
ylim([-35,0])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
%export_fig img\IIR_vs_FIR_mod_H_dB.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\IIR_vs_FIR_mod_H_dB.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Fase
close all

figure
plot(omega-pi, angle(H_FIR),'-b', 'LineWidth', 1)

hold on
plot(omega-pi, angle(H_IIR), '--r', 'LineWidth', 2)
grid on
legend('FIR', 'IIR', '-3dB','Location','northeast')
ylabel('Fase [rad]','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex')
xlim([-pi,pi])
ylim([-pi,pi])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\IIR_vs_FIR_phase_H.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\IIR_vs_FIR_phase_H.tikz','height', '6cm', 'width', '6cm','showInfo', false);