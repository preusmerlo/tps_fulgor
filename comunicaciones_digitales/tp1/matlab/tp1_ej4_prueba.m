%% Prueba del filtro IIR (complemento Item f)
close all

x = randn(500,1) + 1;            % Entrada random con valor medio 0.7.
alpha = 0.9;
b = (1 -alpha);
a = [1,-alpha];
y = filter(b, a, x);

figure
plot(x, '--k','LineWidth',0.5)
hold all
plot(y, '-r', 'LineWidth', 2);
alpha = 0.99;
b = (1 -alpha);
a = [1,-alpha];
y = filter(b, a, x);
plot(y, '-b', 'LineWidth', 2);

grid on
xlabel("Tiempo Discreto");
ylabel("Amplitud");
legend(["x[n]", "y[n]_{\alpha = 0.9}", "y[n]_{\alpha = 0.99}"])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\prueba_IIR.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\prueba_IIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);