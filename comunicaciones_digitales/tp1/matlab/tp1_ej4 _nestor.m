%% Filtro IIR - Promediador movil

%% Respuesta en frecuencia - Modulo (a)
close all
clear all

alpha = 0.8;

NFFT = 1*1024;
delta_w = 2*pi / NFFT;

w = 0: delta_w : 2*pi - delta_w;
H = (1-alpha)./(1 - alpha .* exp(-1j .* w ));

H = fftshift(H);

figure
plot(w-pi,20*log10(abs(H)),'-.r','LineWidth',2);
grid on
title('Respuesta en frecuencia','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex')
legend({'$|H(e^{j\omega})|_{\alpha = 0.8}$'},'Interpreter','latex')
xlim([-pi,pi])

%% %% Respuesta en frecuencia - Fase (a)
close all

figure
plot(w-pi, angle(H),'-.b','LineWidth',2);
grid on
title('Respuesta en frecuencia','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex')
legend({'$\angle H(e^{j\omega})|_{\alpha = 0.8}$'},'Interpreter','latex')
xlim([-pi,pi])

%% Respuesta al impulso (b)
close all

n = 0:20;
h = (1 - alpha) .* alpha .^n;

figure
stem(n,h,'--r','filled');
title('Respuesta al impulso','Interpreter','latex')
xlabel('Tiempo Discreto','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
legend({'$h[n]_{\alpha = 0.8}$'},'Interpreter','latex')
grid on
ylim([0,(1-alpha)*(1+0.2)])
xlim([-2,length(n)+ 2])

%% Respuesta en frecuencia fft() - (d)
close all

n = 0:500;
h = (1 - alpha) .* alpha .^n;

H_fft = fftshift(fft(h,NFFT));

figure
plot(w-pi,20*log10(abs(H)),'r','LineWidth',2);
hold on
plot(w-pi,20*log10(abs(H_fft)),'--b','LineWidth',2);
grid on
title('Respuesta en frecuencia','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex')
legend('|H(e^{j\omega})|_{\alpha = 0.8}','|H(e^{j\omega})|_{\alpha = 0.8 y fft}')
xlim([-pi,pi])

%% Barrido de alpha - Modulo en dB - (f)
close all

alpha_list = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99];

labels = {};
for idx = 1 : length(alpha_list)
    alpha = alpha_list(idx);
    h = (1 - alpha) .* alpha .^n;
    H = fftshift(fft(h,NFFT));
    plot(w-pi,20*log10(abs(H)), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('alpha = %.2f',alpha);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex');
leg = legend(labels ,'Location','northeastoutside');
xlim([-pi,pi])
ylim([-20,0])

%% Barrido de alpha - Modulo en veces - (f)
close all

alpha_list = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99];

labels = {};
for idx = 1 : length(alpha_list)
    alpha = alpha_list(idx);
    h = (1 - alpha) .* alpha .^n;
    H = fftshift(fft(h,NFFT));
    plot(w-pi,abs(H), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('alpha = %.2f',alpha);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend(labels,'Location','northeastoutside')
xlim([-pi,pi])
ylim([0,1])

%% Barrido de alpha - Fase - (f)
close all

alpha_list = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99];

labels = {};
for idx = 1 : length(alpha_list)
    alpha = alpha_list(idx);
    h = (1 - alpha) .* alpha .^n;
    H = fftshift(fft(h,NFFT));
    plot(w-pi,angle(H), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('alpha = %.2f',alpha);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend(labels,'Location','northeastoutside')
xlim([-pi,pi])
ylim([-pi/2,pi/2])

%% Prueba del filtro IIR (complemento Item f)
close all

x = randn(500,1) + 1;            % Entrada random con valor medio 0.7.
alpha = 0.9;
b = (1 -alpha);
a = [1,-alpha];
y = filter(b, a, x);

figure
plot(x, '--k','LineWidth',0.5)
hold all
plot(y, '-r', 'LineWidth', 2);
alpha = 0.99;
b = (1 -alpha);
a = [1,-alpha];
y = filter(b, a, x);
plot(y, '-b', 'LineWidth', 2);

grid on
xlabel("Tiempo Discreto");
ylabel("Amplitud");
legend(["x[n]", "y[n]_{\alpha = 0.9}", "y[n]_{\alpha = 0.99}"])

%% FIN
close all
clear all 