%% Prueba del filtro FIR (complemento Item f)
close all
x = randn(500,1) + 1;
plot(x, '--k')
hold all

% Respuesta con M = 16
M = 16;
b = 1/(M).*ones(1,M);
a = 1;
y = filter(b, a, x);
plot(y, '-r', 'LineWidth', 2);

% Respuesta con M = 128
M = 128;
b = 1/(M).*ones(1,M);
a = 1;
y = filter(b, a, x);
plot(y, '-b', 'LineWidth', 2);


grid on
xlabel("Tiempo Discreto");
ylabel("Amplitud");
legend(["x[n]", "y[n]_{N=16}",'y[n]_{N=128}'])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\prueba_FIR.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\prueba_FIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);