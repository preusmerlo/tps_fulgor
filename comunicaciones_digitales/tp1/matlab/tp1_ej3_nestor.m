%% Filtro FIR - Promediador movil  

%% Respuesta al impulso (a)
clear all
close all

M2 = 4;

% N    = Cantidad de coeficientes
N  = M2 + 1;            

% h[n] = Respuesta al impulso
h  = 1/N .* ones(1,N);  
n  = 0: 1: M2;

% Plot de h[n]
figure
stem(n,h,'--r','filled');
grid on
title('Respuesta al impulso','Interpreter','latex')
xlabel('Tiempo Discreto','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
legend({'$h[n]$'},'Interpreter','latex')
ylim([0,1/N*(1+0.3)])
xlim([-2,M2 + 2])

%% Respuesta en frecuencia - A partir del calculo analitico (b-1)
close all

NFFT = 1*1024;
delta_omega = 2*pi /NFFT;
w = 0: delta_omega: 2*pi - delta_omega; 

% Respuesta en frecuencia calculada analiticamente
H = (sin(w.*N/2)./(sin(w./2).*N)).* exp(-1j.*w.*M2./2);

H = fftshift(H);

% Grafico de modulo
figure;
plot(w-pi, abs(H),'-.r','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend({'$|H (e^{jw})|$'},'Interpreter','latex');
ylim([0,1]);
xlim([-pi,pi]);

%% Respuesta en frecuencia - A partir del calculo analitico (b-2)
close all

%Grafico de fase
figure;
plot(w-pi, angle(H),'.b','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend({'$/H (e^{jw})$'},'Interpreter','latex');
ylim([-pi,pi]);
xlim([-pi,pi]);

%% Respuesta en frecuencia - |H(e^{jw})| [dB] (c-1)
close all

NFFT = 1*1024;
delta_omega = 2*pi /NFFT;
w = 0: delta_omega: 2*pi - delta_omega;

H_fft = fftshift(fft(h,NFFT));

figure;
plot(w - pi,20*log10(abs(H_fft)),'-.r','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex');
legend({'$|H (e^{jw})|_{fft}$'},'Interpreter','latex');
ylim([-60,0]);
xlim([-pi,pi]);

%% Respuesta en frecuencia - /H(e^{jw}) (c-2)
close all

figure;
plot(w - pi,angle(H_fft),'-.b','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia Discreta','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend({'$\angle H (e^{jw})_{fft}$'},'Interpreter','latex');
xlim([-pi,pi]);

%% Respuesta en frecuencia - |H(e^{jw})| [dB] (d-1)
close all

figure
plot(w - pi,20*log10(abs(H)),'r','LineWidth', 2);
hold on
plot(w - pi,20*log10(abs(H_fft)),'--b','LineWidth', 2);
grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex');
legend('$|H (e^{jw})|$','$|H (e^{jw})|_{fft}$','Interpreter','latex');
ylim([-60,0]);
xlim([-pi,pi]);

%% Respuesta en frecuencia - /H(e^{jw}) (d-2)
close all

figure;
hold on
plot(w - pi,angle(H),'r','LineWidth', 2);
plot(w - pi,angle(H_fft),'--b','LineWidth', 2);
grid on;
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend('/H (e^{jw})','/H (e^{jw})_{fft}');
xlim([-pi,pi]);

%% Barrido de M - Modulo en dB - (f)
close all

N_list = [4,8,16,32];
line_3dB  = ones(NFFT,1)*-3;

labels = {};
for idx = 1 : length(N_list)
    N = N_list(idx);
    h = 1/N.*ones(N,1);
    [H,w] = freqz(h,1,NFFT);
    plot(w,20*log10(abs(H)), '--', 'LineWidth', 1);
    labels{end +1} = sprintf('N = %d',N);
    hold all
    
end

plot(w,line_3dB, '-.k', 'LineWidth', 0.8);
labels{end +1} = sprintf('-3dB');
grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex');
legend(labels)
xlim([0,pi])
ylim([-60,0])

%% Barrido de M - Modulo en veces - (f)
close all

N_list = [4,8,16,32];
labels = {};
for idx = 1 : length(N_list)
    N = N_list(idx);
    h = 1/N.*ones(N,1);
    [H,w] = freqz(h,1,NFFT);
    plot(w,abs(H), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('N = %d',N);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend(labels)
xlim([0,pi])
ylim([0,1])

%% Barrido de M - Fase - (f)
close all

N_list = [4,8,16,32];
labels = {};
for idx = 1 : length(N_list)
    N = N_list(idx);
    h = 1/N.*ones(N,1);
    [H,w] = freqz(h,1,NFFT);
    plot(w,angle(H), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('N = %d',N);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend(labels)
xlim([0,pi])
ylim([-pi,pi/2])

%% Prueba del filtro FIR (complemento Item f)
close all
x = randn(500,1) + 1;
plot(x, '--k')
hold all

% Respuesta con M = 16
M = 16;
b = 1/(M).*ones(1,M);
a = 1;
y = filter(b, a, x);
plot(y, '-r', 'LineWidth', 2);

% Respuesta con M = 128
M = 128;
b = 1/(M).*ones(1,M);
a = 1;
y = filter(b, a, x);
plot(y, '-b', 'LineWidth', 2);


grid on
xlabel("Tiempo Discreto");
ylabel("Amplitud");
legend(["x[n]", "y[n]_{N=16}",'y[n]_{N=128}'])

%% FIN
close all
clear all