%% Filtro IIR - Promediador movil

%   �IMPORTANTE!
%   Para ejecutar los bloques "guardar plot" y exportar las graficas desde
%   el script es necesario instalar el paquete export_fig como se indica:
%   
%   1) Descargar como .zip el repo https://github.com/altmany/export_fig
%   2) Crear una carpeta llamada "Tools" en C:\Program Files\MATLAB 
%   3) Descomprimir el .zip en "Tools"
%   4) Ejecutar el comando "pathtool" en la terminal de MATLAB
%   5) Agregar la carpeta C:\Program Files\MATLABTools/export_fig-master
%   6) Abrir en MATLAB la carpeta donde esta ubicado este script
%   7) Crear una carpeta llamada "img" en el directorio del script
%   
%   Si instala el paquete, no ejecute los bloques "guardado de plot".

%% Respuesta en frecuencia - Modulo (a)
close all
clear all

alpha = 0.8;

NFFT = 1*1024;
delta_w = 2*pi / NFFT;

w = 0: delta_w : 2*pi - delta_w;
H = (1-alpha)./(1 - alpha .* exp(-1j .* w ));

H = fftshift(H);

figure
plot(w-pi,20*log10(abs(H)),'-.r','LineWidth',2);
grid on
title('Respuesta en frecuencia','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex')
legend({'$|H(e^{j\omega})|_{\alpha = 0.8}$'},'Interpreter','latex')
xlim([-pi,pi])


%% Guardado de plot

% Exporto plot a .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\mod_H_IIR_dB.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_IIR_dB.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% %% Respuesta en frecuencia - Fase (a)
close all

figure
plot(w-pi, angle(H),'-.b','LineWidth',2);
grid on
title('Respuesta en frecuencia','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex')
legend({'$\angle H(e^{j\omega})|_{\alpha = 0.8}$'},'Interpreter','latex')
xlim([-pi,pi])


%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\phase_H_IIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\phase_H_IIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Respuesta al impulso (b)
close all

n = 0:20;
h = (1 - alpha) .* alpha .^n;

figure
stem(n,h,'--r','filled');
title('Respuesta al impulso','Interpreter','latex')
xlabel('Tiempo Discreto','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
legend({'$h[n]_{\alpha = 0.8}$'},'Interpreter','latex')
grid on
ylim([0,(1-alpha)*(1+0.2)])
xlim([-2,length(n)+ 2])


%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\h_n_IIR.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\h_n_IIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Respuesta en frecuencia fft() - (d)
close all

n = 0:500;
h = (1 - alpha) .* alpha .^n;

H_fft = fftshift(fft(h,NFFT));

figure
plot(w-pi,20*log10(abs(H)),'r','LineWidth',2);
hold on
plot(w-pi,20*log10(abs(H_fft)),'--b','LineWidth',2);
grid on
title('Respuesta en frecuencia','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex')
legend('|H(e^{j\omega})|_{\alpha = 0.8}','|H(e^{j\omega})|_{\alpha = 0.8 y fft}')
xlim([-pi,pi])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\mod_H_IIR_dB_comparativa.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_IIR_dB_comparativa.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Barrido de alpha - Modulo en dB - (f)
close all

alpha_list = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99];

labels = {};
for idx = 1 : length(alpha_list)
    alpha = alpha_list(idx);
    h = (1 - alpha) .* alpha .^n;
    H = fftshift(fft(h,NFFT));
    plot(w-pi,20*log10(abs(H)), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('alpha = %.2f',alpha);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud [dB]','Interpreter','latex');
leg = legend(labels ,'Location','northeastoutside');
xlim([-pi,pi])
ylim([-20,0])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\mod_H_IRR_dB_alpha_var.png  -opengl -m20

% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_IRR_dB_alpha_var.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Barrido de alpha - Modulo en veces - (f)
close all

alpha_list = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99];

labels = {};
for idx = 1 : length(alpha_list)
    alpha = alpha_list(idx);
    h = (1 - alpha) .* alpha .^n;
    H = fftshift(fft(h,NFFT));
    plot(w-pi,abs(H), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('alpha = %.2f',alpha);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend(labels,'Location','northeastoutside')
xlim([-pi,pi])
ylim([0,1])


%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\mod_H_IRR_alpha_var.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\mod_H_IRR_alpha_var.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Barrido de alpha - Fase - (f)
close all

alpha_list = [0.5, 0.6, 0.7, 0.8, 0.9, 0.99];

labels = {};
for idx = 1 : length(alpha_list)
    alpha = alpha_list(idx);
    h = (1 - alpha) .* alpha .^n;
    H = fftshift(fft(h,NFFT));
    plot(w-pi,angle(H), '-.', 'LineWidth', 1);
    labels{end +1} = sprintf('alpha = %.2f',alpha);
    hold all
end

grid on
title('Respuesta en frecuencia','Interpreter','latex')
xlabel('Frecuencia Discreta [rad/seg]','Interpreter','latex');
ylabel('Fase [rad]','Interpreter','latex');
legend(labels,'Location','northeastoutside')
xlim([-pi,pi])
ylim([-pi/2,pi/2])


%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\phase_H_IRR_alpha_var.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\phase_H_IRR_alpha_var.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% Prueba del filtro IIR (complemento Item f)
close all

x = randn(500,1) + 1;            % Entrada random con valor medio 0.7.
alpha = 0.9;
b = (1 -alpha);
a = [1,-alpha];
y = filter(b, a, x);

figure
plot(x, '--k','LineWidth',0.5)
hold all
plot(y, '-r', 'LineWidth', 2);
alpha = 0.99;
b = (1 -alpha);
a = [1,-alpha];
y = filter(b, a, x);
plot(y, '-b', 'LineWidth', 2);

grid on
xlabel("Tiempo Discreto");
ylabel("Amplitud");
legend(["x[n]", "y[n]_{\alpha = 0.9}", "y[n]_{\alpha = 0.99}"])

%% Guardado de plot

% Exporto plot en .png
set(gcf, 'Position', [400 400 400 400],'Color', 'w','Renderer', 'opengl');
export_fig img\prueba_IIR.png  -opengl -m20


% Exporto plot para graficarlo en LATEX
matlab2tikz('tex\prueba_IIR.tikz','height', '6cm', 'width', '6cm','showInfo', false);

%% FIN
close all