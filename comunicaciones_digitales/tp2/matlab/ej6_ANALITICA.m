%% GENERACION DE SE�AL ANALITICA Y POSTERIOR RECUPERACION

%% TRANSMISOR
clear all
close all

% ------ Configurables
L  = 10e3          ;  % Puntos de simulacion
N  = 8             ;  % Tasa de sobremuestreo
BR = 16e9          ;  % Symbol rate [Bd]
rolloff = 0.1      ;  % Exceso de ancho de banda 
TAPS = 500         ;  % Cantidad de taps del conformador
M  = 4             ;  % Niveles de la modulacion PAM

NFFT    = 1024 * 1 ;  % Puntos de la fft / psd
% --------------------

fs = N * BR   ;  % Frecuencia de muestreo
T  = 1/BR     ;  % Tiempo entre simbolos
Ts = 1/fs     ;  % Tiempo entre muestras de s(t)

% Genera simbolos aleatorios (+3,+1,-1,-3)
ak   = 2 * randi([0,M-1], L, 1) - (M-1);

% Modela ak como continua
ak_up = upsample(ak,N);

% Conforma pulsos con filtro RC
g_t = cos_realzado(BR,fs,rolloff,TAPS);
m_t = filter(g_t,1,ak_up);

% PSD de m_t
[psd_m, f] = pwelch(m_t, hanning(NFFT/2), 0, NFFT, fs);
psd_m_dB = 10*log10(psd_m);       % Ojo, es potencia

%% PLOTS m(t) y PSD{m(t)} 
close all

% Tiempo
% Linea de tiempo de simbolos y muestras
t_symbols = (0:L-1).*T;
t_samples = (0:length(m_t)-1).*Ts;

% El filtro retrasa los simbolos en esta cantidad de muestras
delay_s = (length(g_t)-1)/2 ;    % Delay en muestras
delay_t = delay_s * Ts      ;    % Delay en tiempo

figure

subplot(2,2,1)
plot(t_samples, m_t,'b-', 'Linewidth',1)
hold all
stem((t_symbols + delay_t), ak,'--r','filled');
title('Salida de modulador PAM4','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo continuo [seg]','Interpreter','latex')
grid on
xlim([1e-9, 0.005*t_samples(end)])
ylim([min(m_t),max(m_t)])
legend({'$m(t)$','$ak[n]$'},'Interpreter','latex')

% PSD m(t)
subplot(2,2,2)
plot(f./1e9, psd_m_dB,'b-', 'Linewidth',1)
title('PSD de la senal real m(t)','Interpreter','latex')
legend({'$PSD-m(t)$'},'Interpreter','latex')
ylabel('Amplitud [dB]','Interpreter','latex')
xlabel('Frecuencia [GHz]','Interpreter','latex')
xlim([0, (fs/2)/1e9])
grid on

%% MIXER

% *** IMPORTANTE ***
% 2 * fmax{M(f)} < fc < fs/2
% fc = scale * 2 * fmax{M(f)} donde scale > 1
% ------ Configurables
scale = 1.1;
% --------------------

fmax_m = (1+rolloff)*BR/2;  
fc = 2 * fmax_m * scale;  
t  = (0:length(m_t)-1).' .* Ts ;

% Portadora compleja 
p_t = exp(1j*2*pi*fc*t);

% Se�al analitica centrada en fc (analitica)
s_t = p_t .* m_t;

% PSD de s_t
[psd_s, f_s] = pwelch(s_t, hanning(NFFT/2), 0, NFFT, fs);
psd_s = fftshift(psd_s);          % Por ser s(t) compleja
psd_s_dB = 10*log10(psd_s);       % Ojo, es potencia

% Se�al transmitida (real)
x_t = real(s_t);

% PSD de x_t
[psd_x, f_x] = pwelch(x_t, hanning(NFFT/2), 0, NFFT, fs);
psd_x_dB = 10*log10(psd_x);       % Ojo, es potencia

%% PLOT de PSD

% PSD s(t)
subplot(2,2,3)
plot(f_s./1e9 - (fs/2) /1e9, psd_s_dB,'b-', 'Linewidth',1)
title('PSD de la senal compleja s(t)','Interpreter','latex')
legend({'$PSD-s(t)$'},'Interpreter','latex')
ylabel('Amplitud [dB]','Interpreter','latex')
xlabel('Frecuencia [GHz]','Interpreter','latex')
xlim([(-fs/2)/1e9, (fs/2)/1e9])
grid on

% PSD x(t)
subplot(2,2,4)
plot(f_x./1e9, psd_x_dB,'b-', 'Linewidth',1)
title('PSD de la senal real x(t)','Interpreter','latex')
legend({'$PSD-x(t)$'},'Interpreter','latex')
ylabel('Amplitud [dB]','Interpreter','latex')
xlabel('Frecuencia [GHz]','Interpreter','latex')
xlim([0, (fs/2)/1e9])
grid on

set(gcf, 'Position', [100 100 600 600],'Color', 'w','Renderer', 'opengl');

%% CANAL

r_t = x_t ;

%% RECEPTOR

% Transformador de Hilbert
i_t = imag(hilbert(r_t)) ;

delay = finddelay(i_t, r_t);

f_t = r_t + 1j*i_t ;

% Vuelve espectro a baja frecuencia
g_t = conj(p_t) .* f_t;

% Se�al de salida
g_t = real(g_t);

%% PLOTS m(t) vs g(t)
figure

% m(t)
subplot(2,1,1)
plot(t_samples, m_t,'b-', 'Linewidth',1)
hold all
stem((t_symbols + delay_t), ak,'--r','filled');
title('Salida de modulador PAM4','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo continuo [seg]','Interpreter','latex')
grid on
xlim([1e-9, 0.01*t_samples(end)])
ylim([min(m_t),max(m_t)])
legend({'$m(t)$','$a_k[n]$'},'Interpreter','latex')

% g(t)
subplot(2,1,2)
plot(t_samples, g_t,'k-', 'Linewidth',1)
hold all
stem((t_symbols + delay_t), ak,'--r','filled');
title('Salida de receptor','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo continuo [seg]','Interpreter','latex')
grid on
xlim([1e-9, 0.01*t_samples(end)])
ylim([min(g_t),max(g_t)])
legend({'$g(t)$','$a_k[n]$'},'Interpreter','latex')

set(gcf, 'Position', [100 100 600 600],'Color', 'w','Renderer', 'opengl');