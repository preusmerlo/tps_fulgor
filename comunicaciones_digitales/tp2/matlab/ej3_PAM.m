%% TRANSMISOR PAM-M

%% GENERADOR DE SIMBOLOS ALEATORIOS
clear all
close all

% ------ Configurables
L  = 1e3       ;  % Puntos de simulacion
N  = 16       ;  % Tasa de sobremuestreo
BR = 32e9     ;  % Symbol rate [Bd]
fs = N * BR   ;  % Frecuencia de muestreo
T  = 1/BR     ;  % Tiempo entre simbolos
Ts = 1/fs     ;  % Tiempo entre muestras de s(t)
M  = 4        ;  % Niveles de la modulacion PAM
% --------------------

% Genera simbolos aleatorios (+3,+1,-1,-3)
ak = 2 * randi([0,M-1], L, 1) - (M-1);
%%
% Plot de ak[n]
figure
stem(ak(1:18),'--r','filled');
grid on
xlabel('Tiempo Discreto','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
legend({'$a_k[n]$'},'Interpreter','latex')
ylim([-(M-1)*(1+0.3),(M-1)*(1+0.3)])
xlim([0,18+2])

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\randi_ak.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\randi_ak.tikz','height', '7cm', 'width', '7cm','showInfo', false);

%% SOBREMUESTREO
% Sobremuestrea para modelar filtro analogico
ak_up = upsample(ak,N);

% Plot de ak_up[n]
figure
plot(ak_up,'r-','LineWidth', 1.5);
grid on
xlabel('Tiempo Discreto','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
legend({'$a_{k(up)}[n]$'},'Interpreter','latex')
ylim([-(M-1)*(1+0.3),(M-1)*(1+0.3)])
xlim([0,10*N + 30])

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% FILTRO INTERPOLADOR RC y RRC
rolloff = 0.2  ;  % Exceso de ancho de banda
TAPS    = 300  ;  % Cantidad de TAPS del filtro interpolador

% Filtro interpolador
h_RC  = cos_realzado(BR, fs, rolloff, TAPS);
h_RRC = raiz_cos_realzado(BR, fs, rolloff, TAPS);

% Salida del "DAC"
yup_RC  = filter(h_RC,1,ak_up);
yup_RRC = filter(h_RRC,1,ak_up);

%% SALIDA ANALOGICA s(t) VS SIMBOLOS ENVIADOS
% Cantidad de simbolos a imprimir
LPLOT = 25*N;

% Linea de tiempo de simbolos y muestras
t_symbols = (0:L-1).*T;
t_samples = (0:length(yup_RC)-1).*Ts;

% El filtro retrasa los simbolos en esta cantidad de muestras
delay_s = (length(h_RC)-1)/2 ;    % Delay en muestras
delay_t = delay_s * Ts    ;    % Delay en tiempo

figure
stem((t_symbols + delay_t), ak,'--r','filled');
hold all
plot(t_samples, yup_RC,'b-', 'Linewidth',1)
% title('Filtro interpolador coseno realzado - RC','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo continuo [seg]','Interpreter','latex')
grid on
xlim([0, t_samples(LPLOT)])
ylim([-4,4])
set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\rc_salida.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\rc_salida.tikz','height', '7cm', 'width', '7cm','showInfo', false);

%%
figure

stem((t_symbols + delay_t), ak,'--r','filled');
hold all
plot(t_samples, yup_RRC,'k-', 'Linewidth',1)
% title('Filtro interpolador raiz coseno realzado - RRC','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo continuo [seg]','Interpreter','latex')
grid on
xlim([0, t_samples(LPLOT)])
ylim([-4,4])

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\rrc_salida.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\rrc_salida.tikz','height', '7cm', 'width', '7cm','showInfo', false);

%% DIAGRAMAS DE OJO - RC
eyediagram(yup_RC(1e3:15e3),N*3,'k')
title(' ','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo','Interpreter','latex')
set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\rc_ojo.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\rc_ojo.tikz','height', '7cm', 'width', '7cm','showInfo', false);

%% DIAGRAMAS DE OJO - RRC
eyediagram(yup_RRC(1e3:15e3),N*3)
title(' ','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo','Interpreter','latex')
set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\rrc_ojo.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\rrc_ojo.tikz','height', '7cm', 'width', '7cm','showInfo', false);