%% Coseno Realzado - RC
function [h] = cos_realzado(BR, fs, rolloff, TAPS)
% Evita puntos donde RC no existe
rolloff = rolloff +0.01; 

% Calcula periodo de muestreo Ts y periodo de simbolo T
Ts = 1/fs ;
T  = 1/BR ;

% Fuerza a N a impar
if rem(TAPS,2)==1
    TAPS = TAPS + 1; 
end

% Define arreglo temporal normalizado
tn = (-TAPS/2:1:TAPS/2).*Ts./T;

h = sinc(tn).*( cos(pi.*rolloff.*tn) ) ./ (1- (2*rolloff.*tn).^2 ); 
end