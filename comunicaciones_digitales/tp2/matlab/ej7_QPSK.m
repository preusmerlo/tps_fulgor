%% TRANSCEPTOR QPSK

%% TRANSMISOR
clear all
close all

% ------ Configurables
L  = 10e3          ;  % Puntos de simulacion
N  = 4             ;  % Tasa de sobremuestreo
BR = 16e9          ;  % Symbol rate [Bd]
rolloff = 0.1      ;  % Exceso de ancho de banda 
TAPS = 500         ;  % Cantidad de taps del conformador
M  = 2             ;  % Niveles de la modulaciones PAM

NFFT    = 1024 * 1 ;  % Puntos de la fft / psd
% --------------------

fs = N * BR   ;  % Frecuencia de muestreo
T  = 1/BR     ;  % Tiempo entre simbolos
Ts = 1/fs     ;  % Tiempo entre muestras de s(t)

% Genera simbolos aleatorios (I,Q) y sobremuestrea
ak_i_up   = randi_symbols(M,L,N);
ak_q_up   = randi_symbols(M,L,N);

% Conforma pulsos con filtro RC
g_t = cos_realzado(BR,fs,rolloff,TAPS);
m_real_t = filter(g_t,1,ak_i_up);
m_imag_t = filter(g_t,1,ak_q_up);

% Banda base (se�al compleja)
m_t = m_real_t + 1j * m_imag_t;

%% PSD

% PSD de m_t - Espectro no necesariamente par 
[psd_m, f_m] = pwelch(m_t, hanning(NFFT/2), 0, NFFT, fs);
psd_m = fftshift(psd_m);              % Por ser m(t) compleja
psd_m_dB = 10*log10(psd_m);           % Ojo, es potencia

% PSD de Real[m(t)] - Parte simetrica de PSD{m(t)}
[psd_m_r, f_m_r] = pwelch(m_real_t, hanning(NFFT/2), 0, NFFT, fs);
psd_m_r_dB = 10*log10(psd_m_r);       % Ojo, es potencia


% PSD de Real[m(t)] - Parte asimetrica de PSD{m(t)}
[psd_m_i, f_m_i] = pwelch(m_imag_t, hanning(NFFT/2), 0, NFFT, fs);
psd_m_i_dB = 10*log10(psd_m_i);       % Ojo, es potencia

%% PLOTS PSD de m(t)
close all
figure

% PSD de m(t)
subplot(3,1,1)
plot(f_m./1e9 - (fs/2)/1e9, psd_m_dB,'b-', 'Linewidth',1)
title('PSD de m(t)','Interpreter','latex')
legend({'PSD $m(t)$'},'Interpreter','latex')
ylabel('Amplitud [dB]','Interpreter','latex')
xlabel('Frecuencia [GHz]','Interpreter','latex')
xlim([-(fs/2)/1e9, (fs/2)/1e9])
grid on

% PSD de m_real(t)
subplot(3,1,2)
plot(f_m_r./1e9, psd_m_r_dB,'r-', 'Linewidth',1)
title('PSD de Real[ m(t) ]','Interpreter','latex')
legend({'PSD $m_{real}(t)$'},'Interpreter','latex')
ylabel('Amplitud [dB]','Interpreter','latex')
xlabel('Frecuencia [GHz]','Interpreter','latex')
xlim([0, (fs/2)/1e9])
grid on

% PSD de m_imag(t)
subplot(3,1,3)
plot(f_m_i./1e9, psd_m_i_dB,'k-', 'Linewidth',1)
title('PSD de Imag[ m(t) ]','Interpreter','latex')
legend({'PSD $m_{imag}(t)$'},'Interpreter','latex')
ylabel('Amplitud [dB]','Interpreter','latex')
xlabel('Frecuencia [GHz]','Interpreter','latex')
xlim([0, (fs/2)/1e9])
grid on

set(gcf, 'Position', [100 100 600 600],'Color', 'w','Renderer', 'opengl');
%% MIXER

% *** IMPORTANTE ***
% 2 * fmax{M(f)} < fc < fs/2
% fc = scale * 2 * fmax{M(f)} donde scale > 1
% ------ Configurables
scale = 1.1;
% --------------------

fmax_m = (1+rolloff)*BR/2;  
fc = 2 * fmax_m * scale;  
t  = (0:length(m_t)-1).' .* Ts ;

% Portadora compleja 
p_t = exp(1j*2*pi*fc*t);

% Se�al analitica centrada en fc (analitica)
s_t = p_t .* m_t;

% Se�al transmitida (real)
x_t = real(s_t);

%% CANAL

r_t = x_t ;

%% RECEPTOR

% Transformador de Hilbert
i_t = imag(hilbert(r_t)) ;

delay = finddelay(i_t, r_t);

f_t = r_t + 1j*i_t ;

% Vuelve espectro a baja frecuencia
g_t = conj(p_t) .* f_t;

% Se�al de salida
g_real_t = real(g_t);
g_imag_t = imag(g_t);

%% PLOTS m(t) vs g(t)
figure

% Tiempo
% Linea de tiempo de simbolos y muestras
t_symbols = (0:L-1).*T;
t_samples = (0:length(m_t)-1).*Ts;

% El filtro retrasa los simbolos en esta cantidad de muestras
delay_s = (length(g_t)-1)/2 ;    % Delay en muestras
delay_t = delay_s * Ts      ;    % Delay en tiempo

% Real de m(t) vs Real de g(t)
subplot(2,1,1)
plot(t_samples, m_real_t,'b-', 'Linewidth',1)
hold all
plot(t_samples, g_real_t,'r--', 'Linewidth',1)
title('Parte Real','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo continuo [seg]','Interpreter','latex')
grid on
xlim([3e-9, 0.01*t_samples(end)])
ylim([min(m_real_t),max(m_real_t)])
legend({'$m_r(t)$','$g_r(t)$'},'Interpreter','latex')

% Imag de m(t) vs Imag g(t)
subplot(2,1,2)
plot(t_samples, m_imag_t,'b-', 'Linewidth',1)
hold all
plot(t_samples, g_imag_t,'r--', 'Linewidth',1)
title('Parte Imaginaria','Interpreter','latex')
ylabel('Amplitud','Interpreter','latex')
xlabel('Tiempo continuo [seg]','Interpreter','latex')
grid on
xlim([3e-9, 0.01*t_samples(end)])
ylim([min(m_imag_t),max(m_imag_t)])
legend({'$m_i(t)$','$g_i(t)$'},'Interpreter','latex')

set(gcf, 'Position', [100 100 600 600],'Color', 'w','Renderer', 'opengl');