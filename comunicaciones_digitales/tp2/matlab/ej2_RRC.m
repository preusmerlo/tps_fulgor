%% EJ 2 - (a) RAIZ COSENO REALZADO
% Genere una funcion de MatLab que reciba como par�metros: el symbol-rate
% (que es equivalente a 1/T), la frecuencia de muestreo del filtro, el
% rolloff o exceso de ancho de banda y la cantidad de taps.
%
% Resolucion en archivo fuente "raiz_cos_realzado.m"

%% EJ 2 - (b) RAIZ COSENO REALZADO VS COSENO REALZADO EN TIEMPO
% En una misma figura, superponga la respuesta al impulso de un RC y un RRC
% que compartan los mismos par�metros de entrada.
% - �D�nde se producen los cruces por cero en uno y otro filtro?
% - �Qu� relaci�n tiene ese cruce por cero con los par�metros de entrada?
%%
clear all
close all

% Define parametros
N    = 8      ;  % Tasa de sobremuestreo  [Veces]
BR   = 32e6   ;  % Symbol Rate            [Bd]
fs   = N * BR ;  % Frecuencia de muestreo [Hz]
TAPS = 1e2    ;  % Cant. de coeficientes 
rolloff = 0.5 ;  % 0 <= rolloff  <= 1

figure
% Coseno Realzado - RC
h = cos_realzado(BR, fs, rolloff, TAPS);
h = h ./ h(TAPS/2+1);
plot(h, 'r-', 'LineWidth', 1.5);
hold on

% Raiz Coseno Realzado - RRC
h = raiz_cos_realzado(BR, fs, rolloff, TAPS);
h = h ./ h(TAPS/2+1);
plot(h, 'b-', 'LineWidth', 1.5);

grid on
% title('Coseno Realzado RC Vs Raiz Coseno Realzado RRC','Interpreter','latex')
xlabel('Muestras','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend({'$h_{rc}(t)$','$h_{rrc}(t)$'},'Interpreter','latex')
xlim([0,TAPS])
xticks(0:25:TAPS)
ylim([min(h),max(h)])
yticks(-0.2:0.2:1)

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .png
saveas(gcf, 'img\RC_vs_RRC.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\RC_vs_RRC.tikz','height', '7cm', 'width', '7cm','showInfo', false);

%% EJ 2 - (c) RAIZ COSENO REALZADO VS COSENO REALZADO EN FRECUENCIA

NFFT = 1024 * 1;
delta_omega = 2*pi /NFFT;
w = 0: delta_omega: 2*pi - delta_omega; 

figure
labels = {};

% Coseno Realzado - RC
h = cos_realzado(BR, fs, rolloff, TAPS);
H = fft(h,NFFT);
H = H ./ H(1);
H = fftshift(H);
% plot(w - pi, 20*log10(abs(H)), 'r-.', 'LineWidth', 1.5);
plot(w - pi, abs(H), 'r-', 'LineWidth', 1.5);
labels{end + 1} = sprintf('$H_{rc}(w)$');
hold on

% Raiz Coseno Realzado - RC
h = raiz_cos_realzado(BR, fs, rolloff, TAPS);
H = fft(h,NFFT);
H = H ./ H(1);
H = fftshift(H);
% plot(w - pi, 20*log10(abs(H)), 'b-.', 'LineWidth', 1.5);
plot(w - pi, abs(H), 'b-', 'LineWidth', 1.5);
labels{end + 1} = sprintf('$H_{rrc}(w)$');
hold on

% Plot
% plot(w-pi, -3*ones(NFFT,1), '--k','LineWidth', 1);
plot(w-pi, 0.5*ones(NFFT,1), '--k','LineWidth', 1);
% labels{end + 1} = sprintf('-3 dB');
grid on
% title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia discreta [rad/seg]','Interpreter','latex');
% ylabel('Magnitud [dB]','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend(labels,'Position',[0.47, 0.15, .1, .1],'Interpreter','latex')
xlim([-1,1])
ylim([0,1.2])

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\RC_vs_RRC_frec.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\RC_vs_RRC_frec.tikz','height', '7cm', 'width', '7cm','showInfo', false);

%% EJ 2 - (d) CONVOLUCION DE FILTROS RAIZ COSENO REALZADO
clear all

% Define parametros
N    = 8      ;  % Tasa de sobremuestreo  [Veces]
BR   = 32e6   ;  % Symbol Rate            [Bd]
fs   = N * BR ;  % Frecuencia de muestreo [Hz]
TAPS = 1e2    ;  % Cant. de coeficientes 
rolloff = 0.3 ;  % 0 <= rolloff  <= 1

figure
% Coseno Realzado - RC
h = cos_realzado(BR, fs, rolloff, 2*TAPS);
plot(h, 'r-', 'LineWidth', 1.5);
hold on

% Raiz Coseno Realzado - RRC
h = raiz_cos_realzado(BR, fs, rolloff,TAPS);
h = conv(h,h);
h = h ./ h(2*TAPS/2+1);
plot(h, 'b--', 'LineWidth', 1.5);

grid on
% title('Coseno Realzado RC Vs Raiz Coseno Realzado RCC convolucionado','Interpreter','latex')
xlabel('Muestras','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend({'$h_{rc}(t)$','$h_{rrc}(t) * h_{rrc}(t)$'},'Interpreter','latex')
xlim([TAPS-50,TAPS+50])
xticks(TAPS-50:25:TAPS+50)
ylim([min(h),max(h)])
yticks(-0.2:0.2:1)

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\RC_vs_RRC_conv.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\RC_vs_RRC_conv.tikz','height', '7cm', 'width', '7cm','showInfo', false);