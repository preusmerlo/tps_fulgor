%% TRANSCEPTOR QPSK CON DECIMADOR 
% Idem al ej7 agregando decimador a la salida del receptor y graficando las
% constelaciones enviada y recibida.

%% TRANSMISOR
clear all
close all

% ------ Configurables
L  = 1e2          ;  % Puntos de simulacion
N  = 4             ;  % Tasa de sobremuestreo
BR = 16e9          ;  % Symbol rate [Bd]
rolloff = 0.1      ;  % Exceso de ancho de banda 
TAPS = 500         ;  % Cantidad de taps del conformador
M  = 2             ;  % Niveles de la modulaciones PAM
d_phi = 0*pi/8         ;  % Error de fase entre osciladores
d_w   = 2*pi*20e6;   % Error de frecuencia entre osciladores
NFFT    = 1024 * 1 ;  % Puntos de la fft / psd
% --------------------

fs = N * BR   ;  % Frecuencia de muestreo
T  = 1/BR     ;  % Tiempo entre simbolos
Ts = 1/fs     ;  % Tiempo entre muestras de s(t)

% Genera simbolos aleatorios (I,Q) y sobremuestrea
ak_i_up   = randi_symbols(M,L,N);
ak_q_up   = randi_symbols(M,L,N);

% Conforma pulsos con filtro RC
g_t = cos_realzado(BR,fs,rolloff,TAPS);
m_real_t = filter(g_t,1,ak_i_up);
m_imag_t = filter(g_t,1,ak_q_up);

% Banda base (se�al compleja)
m_t = m_real_t + 1j * m_imag_t;

% Decimado de m(t)
T0 = 2;
m_dw_t = m_t(T0+1:N:end); 

%% MIXER

% *** IMPORTANTE ***
% 2 * fmax{M(f)} < fc < fs/2
% fc = scale * 2 * fmax{M(f)} donde scale > 1
% ------ Configurables
scale = 1.1;
% --------------------

fmax_m = (1+rolloff)*BR/2;  
fc = 2 * fmax_m * scale;  
t  = (0:length(m_t)-1).' .* Ts ;

% Portadora compleja 
p_t = exp(1j*2*pi*fc*t);

% Se�al analitica centrada en fc (analitica)
s_t = p_t .* m_t;

% Se�al transmitida (real)
x_t = real(s_t);

%% CANAL

r_t = x_t ;

%% RECEPTOR

% Transformador de Hilbert
i_t = imag(hilbert(r_t)) ;

delay = finddelay(i_t, r_t);

f_t = r_t + 1j*i_t ;

% Vuelve espectro a baja frecuencia
g_t = conj(p_t) .* exp(1j*d_phi) .* exp(-1j*d_w*t) .* f_t;

%% PLOT DE CONSTELACIONES
% ------ Configurables
T0_m = 2;
T0_g = 2;
% --------------------
legends = {};

h = scatterplot(g_t,N,T0_m,'b*');
legends{end+1} = sprintf('$Recibido$');

hold on

scatterplot(m_t,N,T0_g,'r*',h);
legends{end+1} = sprintf('$Enviado$');

grid on
title('Constelacion QPSK','Interpreter','latex')
legend(legends,'location','east','Interpreter','latex');
ylabel('Simbolos Q','Interpreter','latex')
xlabel('Simbolos I','Interpreter','latex')

set(gcf, 'Position', [100 100 600 600],'Color', 'w','Renderer', 'opengl');