%% EJ 1 - (a) COSENO REALZADO
% Genere una funcion de MatLab que reciba como par�metros: el symbol-rate
% (que es equivalente a 1/T), la frecuencia de muestreo del filtro, el
% rolloff o exceso de ancho de banda y la cantidad de taps.
%
% Resolucion en archivo fuente "cos_realzado.m"

%% EJ 1 - (b) BARRIDO DE ROLLOFF EN TIEMPO
% Genere una figura donde se superponga el plot de varias respuestas
% al impulso para symbol-rate = 32GBd, tasa de sobre-muestreo = 8 y rolloff
% variable.
% - �Qu� tienen en comun todas las respuestas?
% - �En qu� se diferencian unas de otras?
%%
clear all
close all

% Define parametros
N    = 8      ;  % Tasa de sobremuestreo  [Veces]
BR   = 32e6   ;  % Symbol Rate            [Bd]
fs   = N * BR ;  % Frecuencia de muestreo [Hz]
TAPS = 400    ;  % Cant. de coeficientes 

rolloff_min = 0.1 ; % Min rolloff >= 0
rolloff_max = 0.9 ; % Max rolloff <= 1
rolloff_stp = 0.2 ; % Stp rolloff 0 < stp < 1

% Barrido de rolloff
rolloff_list = rolloff_min:rolloff_stp:rolloff_max - rolloff_stp;
rolloff_list = fliplr(rolloff_list);

% Labels
labels = {};
for idx = 1 : length(rolloff_list)
    rolloff = rolloff_list(idx); 
    h = cos_realzado(BR, fs, rolloff, TAPS);
    plot(h, '-', 'LineWidth', 1.5);
    labels{end + 1} = sprintf('$beta = %.1f$',rolloff);
    hold all
end

% title('Coseno Realzado','Interpreter','latex')
xlabel('Muestras','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend(labels,'Interpreter','latex')
xlim([150,250])
xticks(150:25:250)
ylim([min(h),max(h)])
grid on

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\cos_realzado.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\cos_realzado.tikz','height', '7cm', 'width', '7cm','showInfo', false);

%% EJ 1 - (c) BARRIDO DE ROLLOFF EN FRECUENCIA

NFFT = 1024 * 1;
delta_omega = 2*pi /NFFT;
w = 0: delta_omega: 2*pi - delta_omega; 

figure
% Labels
labels = {};
for idx = 1 : length(rolloff_list)
    rolloff = rolloff_list(idx);
    h = cos_realzado(BR, fs, rolloff, TAPS);
    H = fft(h,NFFT);
    % Normaliza a 0dB (�-3dB pasa a 6dB?)
    H = H ./ H(1);
    H = fftshift(H);
%     plot(w - pi, 20*log10(abs(H)), '-', 'LineWidth', 1.5);
    plot(w - pi, abs(H), '-', 'LineWidth', 1.5);
    labels{end + 1} = sprintf('$beta = %.1f$',rolloff);
    hold all
end

% plot(w-pi, -6*ones(NFFT,1), '--k','LineWidth', 1);
plot(w-pi, 0.5*ones(NFFT,1), '--k','LineWidth', 1);
% labels{end + 1} = sprintf('-6 dB');
grid on
% title('Respuesta en frecuencia','Interpreter','latex');
xlabel('Frecuencia discreta [rad/seg]','Interpreter','latex');
% ylabel('Magnitud [dB]','Interpreter','latex');
ylabel('Magnitud','Interpreter','latex');
legend(labels,'Position',[0.47, 0.2, .1, .1],'Interpreter','latex')
xlim([-1,1])
% ylim([-20,2])

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\cos_realzado_frec.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\cos_realzado_frec.tikz','height', '7cm', 'width', '7cm','showInfo', false);