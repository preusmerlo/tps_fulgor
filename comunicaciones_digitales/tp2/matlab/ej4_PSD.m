%% PROCESAMIENTO ESTOCASTICO

%% GENERADOR DE SIMBOLOS ALEATORIOS
clear all
close all

% ------ Configurables
L  = 10e3     ;  % Puntos de simulacion
N  = 16       ;  % Tasa de sobremuestreo
BR = 32e9     ;  % Symbol rate [Bd]
fs = N * BR   ;  % Frecuencia de muestreo
T  = 1/BR     ;  % Tiempo entre simbolos
Ts = 1/fs     ;  % Tiempo entre muestras de s(t)
M  = 4        ;  % Niveles de la modulacion PAM
% --------------------

% Genera simbolos aleatorios (+3,+1,-1,-3)
ak   = 2 * randi([0,M-1], L, 1) - (M-1);

% Modela ak como la continua a(t)
ak_up = upsample(ak,N);

%% FILTRO CONFORMADOR Y SALIDA DEL TX

% ------ Configurables
rolloff = 0.1;
TAPS    = 500;
NFFT    = 1024 * 4;
% --------------------

h = cos_realzado(BR, fs, rolloff, TAPS);
H = fft(h, NFFT);

y_up = filter(h,1,ak_up);

%% COMPARATIVA PSD ak(k) VS PSD s(t) VS H(+w)

% ------ Configurables
OVERLAP = 0 * NFFT;
% --------------------

legends = {};

% PSD de la ENTRADA del filtro
[Pxx, f] = pwelch(ak_up, hanning(NFFT/2) , OVERLAP, NFFT, fs);
Px_dB= 10*log10(Pxx);       % Ojo, es potencia
Px_dB = Px_dB - Px_dB(1);   % Normaliza a 0dB
plot(f, Px_dB,'-b', 'Linewidth',1)
legends{end + 1} = sprintf('$PSD_{ak}$');

hold all

% PSD de la SALIDA del filtro
[Pxx, f] = pwelch(y_up, hanning(NFFT/2), OVERLAP, NFFT, fs);
Px_dB= 10*log10(Pxx);       % Ojo, es potencia
Px_dB = Px_dB - Px_dB(1);   % Normaliza a 0dB
plot(f, Px_dB,'-r', 'Linewidth',1)
legends{end + 1} = sprintf('$PSD_{s}$');

% Respuesta en frecuencia del filtro conformador
H_pos = abs(H(1:length(f)));% Ajusta FFT para coincidir con el largo de PSD y elimina frec < 0
H_pos = H_pos/H_pos(1);     % Normaliza a 0dB
H_dB = 20.*log10(H_pos);    % Ojo, NO es potencia
plot(f, H_dB, '--k', 'Linewidth',1)
legends{end + 1} = sprintf('$H(w)$');

grid on
% title('$PSD_{ak(t)}$ Vs $PSD_{s(t)}$ Vs H($\omega$)','Interpreter','latex')
legend(legends,'Position',[0.7, 0.75, .08, .08],'Interpreter','latex')
xlabel('Frecuencia [Hz]','Interpreter','latex')
ylabel('Amplitud [dB]','Interpreter','latex')

xlim([0,1e11])
ylim([-120,10])

set(gcf, 'Position', [100 100 400 400],'Color', 'w');

%% Guardado de plot (requiere toolbox externo)

% Exporta plot en .svg
saveas(gcf, 'img\coloreado_psd.svg', 'svg')

% Exporta plot para graficarlo en LATEX
matlab2tikz('tex\coloreado_psd.tikz','height', '7cm', 'width', '7cm','showInfo', false);

