%% CONDICION DE NO ISI Y CRITERIO DE NYQUIST
% CRITERIO EN TIEMPO
clear all
close all

% --- Configurables ---
BR      = 1e3     ; % Symbol Rate
T       = 1/BR    ; % Tiempo entre simbolos
N       = 16       ; % Sobremuestreo
fs      = N * BR  ; % Frecuencia de muestreo
Ts      = 1/fs    ; % Periodo de muestreo
TAPS    = 500      ; % Cantidad de coeficientes del filtro
rolloff = 0.1     ; % Exceso de ancho de banda del filtro
filtro  = ' RRC' ;   % Tipo de filtro (RC o RRC)
% Importante: dejar un espacio despues de la primer ' (comilla) en FILTRO.
% ---------------------

% Filtro de Pulse Shaping (tiempo continuo)
if strcmp(filtro,' RC')
    g_t  = cos_realzado(BR,fs,rolloff,TAPS);
else
    g_t  = raiz_cos_realzado(BR,fs,rolloff,TAPS);
end

figure

subplot(2,1,1)
plot(g_t,'r-','LineWidth',1)
grid on
legend('g(t)')
title(strcat('Criterio de Nyquist en tiempo para filtro ',filtro));
xlabel('Muestras')
ylabel('Amplitud')
xlim([1,length(g_t)])
ylim([min(g_t)-0.1,max(g_t)+0.1])

% El criterio de Nyquist en timepo indica que la secuencia resultante de
% muestrear la respuesta al impulso del filtro conformador cada T segundos
% es un impulso. Es decir g(kT) = delta(n). Muestrear cada T segundos
% implica que fs = BR = 1/T.

% Muestreo cada kT (fs = BR)
g_kT_up = downsample(g_t(TAPS/2+1:end),N);
g_kT_dw = flip(downsample(flip(g_t(1:TAPS/2+1)),N));

g_kT = [g_kT_dw(1:end-1) g_kT_up];

subplot(2,1,2)
stem(g_kT,'--r','filled');
grid on
legend('g[n] = g(kT)')
xlabel('Muestras')
ylabel('Amplitud')
xlim([1,length(g_kT)])
ylim([min(g_kT)-0.1,max(g_kT)+0.1])

set(gcf, 'Position', [100 100 600 600],'Color', 'w');

% CRITERIO EN FRECUENCIA

% --- Configurables ---
NFFT = 1024 * 8      ; 
% ---------------------

% Arreglo de frecuencia
dw = fs /NFFT      ;
w = -fs/2: dw: fs/2 - dw ;

% Respuesta en frecuencia del filtro 
G_w    = fftshift( fft(g_t,NFFT) );

figure
plot(w,abs(G_w),'-k', 'Linewidth',1);      % Espectro original
hold all
plot(w+1*BR,abs(G_w),'-r', 'Linewidth',1)  % Replica en BR = 1/T
plot(w+2*BR,abs(G_w),'-g', 'Linewidth',1)  % Replica en BR = 2/T
plot(w+3*BR,abs(G_w),'-b', 'Linewidth',1)  % Replica en BR = 3/T

plot(w-1*BR,abs(G_w),'-.m', 'Linewidth',1)  % Replica en BR = 4/T
plot(w+4*BR,abs(G_w),'-.m', 'Linewidth',1)  % Replica en BR = 4/T

grid on
title(strcat('Criterio de Nyquist en frecuencia para filtro ',filtro));
xlabel('Frecuencia [Hz]')
ylabel('Amplitud')
legend('G(\omega)','G(\omega - BR)','G(\omega - 2*BR)','G(\omega - 3*BR)')
xlim([-1e3,4e3])

set(gcf, 'Position', [100 100 600 600],'Color', 'w');

% El criterio de Nyquist en frecuencia indica que la suma del espectro
% original del filtro G(w) con el mismo pero replicado cada BR = 1/T debe 
% resultar en una constante. Si el ancho de banda del filtro conformador es
% demasiado chico, va a existir ISI. Para un BR dado, el minimo ancho de
% banda posible es BW = B/2 = 1/2T, es decir, para transmitir B = 100 MBd
% son necesarios al menos BW = B/2 = 50MHz de ancho de banda en el canal y
% en el filtro conformador.

% El rolloff afecta el ancho de banda requerido para no tener ISI. El ancho
% de banda necesario para un rolloff dado es necesario BW > (1+rolloff)/2T.
