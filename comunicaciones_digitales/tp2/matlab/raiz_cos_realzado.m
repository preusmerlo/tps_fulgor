%% Raiz Coseno Realzado - RRC
function [h] = raiz_cos_realzado(fc, fs, rolloff, TAPS)
% Evita puntos donde RC no existe
rolloff = rolloff +0.01; 

% Calcula periodo de muestreo Ts y periodo de simbolo T
Ts = 1/fs ;
T  = 1/fc ;

% Fuerza a N a impar
if mod(TAPS,2)==1
    TAPS = TAPS + 1; 
end

% Define arreglo temporal normalizado
tn = (-TAPS/2:1:TAPS/2).*Ts./T;

h=(sin(pi.*(1-rolloff).*tn)+4*rolloff.*tn.*cos(pi*(1+rolloff).*tn))./(pi.*tn.*(1-(4*rolloff.*tn).^2));
h(TAPS/2+1) = (1 + rolloff .* (4./pi - 1));
end