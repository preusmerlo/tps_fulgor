%% Generador PAM-M
function [ak_up] = randi_symbols(M,L,N)
    % Genera L simbolos mapeados en M niveles de distribucion uniforme.
    % Devolviendo una se�al "continua" sobremuestreada a N.
    % Si no se explicita N retorna simbolos sin sobremuestreo.

    if nargin < 3
        N = 1;
    end

    ak = 2 * randi([0,M-1], L, 1) - (M-1);
    ak_up = upsample(ak,N);
end