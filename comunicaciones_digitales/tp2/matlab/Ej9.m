%% Ejercicio 9

clc
clear all
close all

BR	= 16e9 ;	% Bd
L	= 10000 ;	% Simulation Length
N	= 4 ;   	% Oversampling rate
fs	= N*BR ;	% Sampling rate to emulate analog domain
T	= 1/BR ;	% Time interval between two consecutive symbols
Ts	= 1/fs ;	% Time between 2 conseutive samples at Tx output

% Two symbols generation (+1,-1)
x_i = 2*randi([0,1],L,1)-1 ;
x_q = 2*randi([0,1],L,1)-1 ; 
x_total = x_i + 1j*x_q ;
% Upsampling to change sampling rate
xup = upsample(x_total, N) ;

% Filter to interpolate the signal
rolloff = 0.2 ;
h = raiz_cos_realzado(BR, fs, rolloff, 500) ;	% h cumple la func de conformador 
                                    % espectral
                                    
m = filter(h,1,xup) ;      % Salida del transmisor PAM2


% -- Construccion del diagrama en bloques Figura 6 TP2 --
fc = (1+rolloff)*BR/2*1.5 ;          % Carrier frequency
t = [0:length(m)-1].' .* Ts ;
p = sqrt(2) .* exp(1j*2*pi*fc.*t) ;
s = m .* p ;                        

x = real(s) ;

r = x ;

i = imag(hilbert(r)) ;
delay = finddelay(i, r);
f = r + 1j*i ;

g1 = conj(p) .* f ./ 2;

T0 = 2;
gD1 = g1(T0+1:N:end) ;

% -- Construccion del diagrama en bloques Figura 7 TP2 --
phase_error = 0 ;
T0 = 2;

ep = exp(phase_error(1)) ;
g2 = m .* ep ;

gD2 = g2(T0+1:N:end) ;


% -------------------------------- Item a ---------------------------------
figure
hold on

scatter(real(gD1(100:end)),imag(gD1(100:end)), 'ob')
scatter(real(gD2(100:end)),imag(gD2(100:end)), '*r')
xlabel('In-Phase')
ylabel('Quadrature')
title('Ejercicio 9')
h = legend('g(t) a partir de esquema Figura 6', 'g(t) a partir de esquema Figura 7') ;
set(h, 'Position', [0.47, 0.8, .10, .10]) ;
grid on

set(gcf, 'Position', [100 100 600 600]) ;
saveas(gcf, 'Plots/9.svg', 'svg') ;

% CONCLUSIONES: Se demuestra que es lo mismo.
% -------------------------------------------------------------------------