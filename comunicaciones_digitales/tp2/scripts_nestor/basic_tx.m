clear all
close all


%Basic TX pam 2
BR = 32e9; %Bd
L = 10000; % Simulati on Length
N = 16; % Oversampling rate
fs = N*BR; % Sampling rate to emulate analog domain
T = 1/BR; % Time interval between two consecutive symbols
Ts = 1/fs; % Time between 2 conseutive samples at Tx output

% Two symbols generation (+1,-1)
x = 2*randi([0,1],L,1)-1;

% Upsampling to change sampling rate
xup = upsample(x,N);

% Filter to interpolate the signal
h = cos_realzado(BR, fs, .1, 500);

yup = filter(h,1,xup);

%%
% Plot of Tx output and original symbols
t_symbols = [0:L-1].*T; %Timeline for symbols before upsampling 
t_samples = [0:length(yup)-1].*Ts;
delay = (length(h)-1)/2; % The filter delays the datapath by this number of samples
LPLOT = 30*N; % Total symbols to plot
figure
plot(t_samples, yup,'-x', 'Linewidth',2)
hold all
extra_time = delay*Ts; % Delay is expressed in samples!
stem(t_symbols+extra_time, x, 'Linewidth',2)
ylabel('Amplitude')
xlabel('Time in seconds')
grid on
xlim([0, t_samples(LPLOT)])

%% PSD AT TX OUTPUT
NFFT = 1024*8;
WELCH_OVERLAP = 0.*NFFT;

figure
[Pxx, f] = pwelch(xup, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs);
Px_dB= 10*log10(Pxx);
Px_dB = Px_dB - Px_dB(1); %Normalize Px_dB to get 0dB at f=0, only for plot purposes
plot(f, Px_dB,'-b', 'Linewidth',1)
hold on

[Pxx, f] = pwelch(yup, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs);
Px_dB= 10*log10(Pxx);
Px_dB = Px_dB - Px_dB(1); %Normalize Px_dB to get 0dB at f=0, only for plot purposes
plot(f, Px_dB,'-r', 'Linewidth',1)
grid on
xlabel('Frequency [Hz]')
ylabel('PSD Magnitude [dB(V^2)/Hz]')

hold on
H = abs(fft(h,NFFT));
H_pos = H(1:length(f)); %Match length of FFT to PSD and selecting only positive frequencies
H_pos = H_pos/H_pos(1); %Normalization to get Gain=1 at f=0
H_dB = 20.*log10(H_pos); % It is a filter, is 20log10, PSD is power and it is 10log10
plot(f, H_dB, '--k', 'Linewidth',2)

legend('PSD at Tx Input','PSD at Tx Output', 'Squared Filter Frequency Response')

%% Diagrama de ojo
eyediagram(yup(50e3:150e3),N*2)