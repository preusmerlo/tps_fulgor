clear all
%close all

%% Basic TX QPSK o QAM16
BR = 32e9; %Bd
L = 10000; % Simulati on Length
N = 4; % Oversampling rate
fs = N*BR; % Sampling rate to emulate analog domain
T = 1/BR; % Time interval between two consecutive symbols
Ts = 1/fs; % Time between 2 conseutive samples at Tx output

% Two symbols generation (+1,-1) for QPSK
xi = 2*randi([0,1],L,1)-1; % Real
xq = 2*randi([0,1],L,1)-1; % Imag
x = xi + 1j*xq;

% Upsampling to change sampling rate
xup = upsample(x,N);

% Filter to interpolate the signal
rolloff = 0.5;
h = coseno_realzado(BR, fs, rolloff, 100);
yup = filter(h,1,xup);

%% Let's transform yup to analytic signal
% yup is a real signal with BW=(1+rolloff)*BR/2
% yup must be rotated in frequency domain until the whole spectrum is
% placed at positive frequencies
t = [0:length(yup)-1].'.*Ts; %Note the operator .' to transpose
% WARNING operator ' transpose+conjugate!, but .' only transpose
f0 = (1+rolloff)*BR/2* (1.2); %20% extra shift just in case
yup_analytic = yup.*exp(j*2*pi*f0.*t);

figure
NFFT = 1024;
WELCH_OVERLAP = 0;

[Pxx, f] = pwelch(yup, [], WELCH_OVERLAP, NFFT, fs);
Px_dB= 10*log10(Pxx);
Px_dB = Px_dB - Px_dB(1); %Normalize Px_dB to get 0dB at f=0, only for plot purposes
plot(f, Px_dB,'-b', 'Linewidth',1)
grid on
xlabel('Frequency [Hz]')
ylabel('PSD Magnitude [dB(V^2)/Hz]')

figure
[Pxx, f] = pwelch(yup_analytic, hanning(NFFT/2), WELCH_OVERLAP, NFFT, fs);
Px_dB= 10*log10(Pxx);
Px_dB = Px_dB - Px_dB(1); %Normalize Px_dB to get 0dB at f=0, only for plot purposes
plot(f, Px_dB,'-b', 'Linewidth',1)
grid on
xlabel('Frequency [Hz]')
ylabel('PSD Magnitude [dB(V^2)/Hz]')

%% Split analytic signal in real and imaginary part
y_real = real(yup_analytic);
y_imag = imag(yup_analytic);

%% Model hilber transform
% [INSERTE AQUI SU TRANSFORMADOR DE HILBERT]
%%
% Filtrar la parte real con el TH y ver si coincide con la parte
% imaginaria!
%%


