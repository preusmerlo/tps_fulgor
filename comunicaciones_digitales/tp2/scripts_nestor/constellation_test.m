clear all
%close all

%% Basic TX QPSK o QAM16
BR = 32e9; %Bd
L = 10000; % Simulati on Length
N = 8; % Oversampling rate
fs = N*BR; % Sampling rate to emulate analog domain
T = 1/BR; % Time interval between two consecutive symbols
Ts = 1/fs; % Time between 2 conseutive samples at Tx output

% Two symbols generation (+1,-1) for QPSK
xi = 2*randi([0,1],L,1)-1; % Real
xq = 2*randi([0,1],L,1)-1; % Imag
x = xi + 1j*xq;

% Upsampling to change sampling rate
xup = upsample(x,N);

% Filter to interpolate the signal
rolloff = 0.25;
h = coseno_realzado(BR, fs, rolloff, 111);
yup = filter(h,1,xup);

%% Hasta aca, transmiti la envolvente compleja
% El receptor optimo para este caso es una llave que muestrea a kT segundos
% Esto equivale a decimar xN (pero hay que encontrar la mejor fase de
% muestreo)
for To=0:N-1
    y_rx = yup(To+1:N:50*N);
    scatterplot(y_rx)
    title(sprintf("%d", To));
end

%%
To = 0;
y_rx = yup(To+1:N:end);
scatterplot(y_rx)