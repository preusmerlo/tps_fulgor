/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: tb_top.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 13 Sep. 2021
* Description 	: laboratory number 1
************************************************************************************************
*/

`timescale 1ns/100ps

module tb_top ();

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam N_LEDS   = 4         ;
    localparam N_SWITCH = 4         ;
    
    localparam NB_COUNTER = 32      ;        
    
    localparam R0 = 32'h00_00_00_04 ;
    localparam R1 = 32'h00_00_00_08 ;
    localparam R2 = 32'h00_00_0F_0C ;
    localparam R3 = 32'h00_00_FF_0F ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Module inputs  */
    reg [N_SWITCH-1:0] i_sw     ;
    reg                i_reset  ;
    reg                i_clock  ; 

    /* Module outputs */
    wire [N_LEDS  -1:0] o_led   ;
    wire [N_LEDS  -1:0] o_led_b ;
    wire [N_LEDS  -1:0] o_led_g ;

/*
************************************************************************************************
*								       STIMULUS
************************************************************************************************
*/

    initial begin
        i_sw    = {N_SWITCH{1'b0}};
        i_reset = 1'h1;
        i_clock = 1'h0;

        /* Reset system */
        #4 i_reset = 1'b0;
        #4 i_reset = 1'b1;
        
        /* Enable counter */
            i_sw[0] = 1'b1;

        /* R1 with blue leds  */
        #50 i_sw[2:1] = 2'b1;
            i_sw[3]   = 1'b0;

        /* R0 with green leds */
        #50 i_sw[2:1] = 2'b0;
            i_sw[3]   = 1'b1;

        /* R1 with green leds */
        #50 i_sw[2:1] = 2'b1;
            i_sw[3]   = 1'b1;

        #80 $finish();
    end

    always  begin
        #1 i_clock = ~i_clock;
    end

/*
************************************************************************************************
*								       MODULE INSTANCE
************************************************************************************************
*/

    top #(
        .N_LEDS     ( N_LEDS     ),
        .N_SWITCH   ( N_SWITCH   ),
        .NB_COUNTER ( NB_COUNTER ),
        .R0         ( R0         ),
        .R1         ( R1         ),
        .R2         ( R2         ),
        .R3         ( R3         )
    )
    DUT (
        .i_sw       ( i_sw    ),
        .i_reset    ( i_reset ),
        .i_clock    ( i_clock ), 
        .o_led      ( o_led   ),
        .o_led_b    ( o_led_b ),
        .o_led_g    ( o_led_g ) 
    );

endmodule /* tb_top */
