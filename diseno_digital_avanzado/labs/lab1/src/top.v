/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: top.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 13 Sep. 2021
* Description 	: laboratory number 1
************************************************************************************************
*/

module top #(
        /* Parameters */
        parameter N_LEDS   = 4         ,
        parameter N_SWITCH = 4         ,

        parameter NB_COUNTER = 32      ,
        
        parameter R0 = 32'h0F_FF_FF_FF ,
        parameter R1 = 32'h00_FF_AF_FF ,
        parameter R2 = 32'h00_0F_FF_EF ,
        parameter R3 = 32'h00_00_FF_FF
    )
    (
        /* Inputs */    
        input  wire [N_SWITCH-1:0] i_sw     ,
        input  wire                i_reset  ,
        input  wire                i_clock  , 
        
        /* Outputs */                    
        output wire [N_LEDS  -1:0] o_led    ,
        output wire [N_LEDS  -1:0] o_led_b  ,
        output wire [N_LEDS  -1:0] o_led_g  
    );

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/
    

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    wire                  reset                 ;
    wire                  counter_enable        ;
    wire [2         -1:0] counter_top_selector  ;
    wire                  led_colour_selector   ;


    reg  [NB_COUNTER-1:0] counter     ;
    reg  [NB_COUNTER-1:0] counter_top ;

    reg  [N_LEDS    -1:0] shift       ;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/
    /* Rename inputs */
    assign reset                = ~ i_reset ;
    assign counter_enable       = i_sw[0]   ;
    assign counter_top_selector = i_sw[2:1] ;
    assign led_colour_selector  = i_sw[3]   ;

    /* Top counter mux (4x1) */
    always @(*) begin
        case(counter_top_selector)
            2'b00   : counter_top = R0;
            2'b01   : counter_top = R1;
            2'b10   : counter_top = R2;
            2'b11   : counter_top = R3;
            default : counter_top = {NB_COUNTER{1'b0}};               
        endcase
    end

    /* Counter and shift register */
    always @(posedge i_clock or  posedge reset) begin
        if (reset) begin
            shift   <= {{N_LEDS-1 {1'b0}}, 1'b1};
            counter <= {NB_COUNTER{1'b0}};
        end
        else begin
            if (counter_enable) begin
                if (counter >= counter_top) begin
                    counter <= {NB_COUNTER{1'b0}};
                    shift   <= {shift[N_LEDS-2 : 0], shift[N_LEDS-1]};
                end
                else begin
                    counter <= counter +  1'b1;
                end                
            end
        end
    end
    
/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign o_led = shift;
    assign o_led_b = (!led_colour_selector)? shift : {N_LEDS{1'b0}};
    assign o_led_g = ( led_colour_selector)? shift : {N_LEDS{1'b0}};

endmodule /* top */