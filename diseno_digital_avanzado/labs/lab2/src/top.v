/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: top.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 20 Sep. 2021
* Description 	: ...
************************************************************************************************
*/

/*
************************************************************************************************
*                                       DOCUMENTATION
************************************************************************************************
*/

//! @title   Signal generator and FIR filter
//! @file    top.v
//! @author  Patricio Reus Merlo
//! @date    20 Sep. 2021

/*
************************************************************************************************
*								            MODULE
************************************************************************************************
*/

module top
    #(
        /* Module parameters */
        parameter FIR_NB_IN     = 8         ,               //! Filter: Total number of bits
        parameter FIR_NBF_IN    = 7         ,               //! Filter: Number of fractional bits
        parameter FIR_NB_OUT    = 20        ,               //! Filter: Total number of bits
        parameter FIR_NBF_OUT   = 14        ,               //! Filter: Number of fractional bits
        parameter FIR_NB_TAPS   = 8         ,               //! Filter: Total number of bits 
        parameter FIR_NBF_TAPS  = 7         ,               //! Filter: Number of fractional bits
        parameter FIR_N_TAPS    = 15        ,               //! Filter: Quantity of filter coefficients
        parameter FIR_BW        = 0         ,               //! Filter: (0) 0.5kHz (1) 8kHz (2) 17kHz

        parameter GEN_NB_DATA       = 8         ,           //! Total number of output bits 
        parameter GEN_NB_COUNT      = 10        ,           //! Quantity of bits of memory address
        parameter GEN_MEM_INIT_FILE = "/home/dell/Datos/BaseDeDatos/cursos/fulgor/tps_fulgor/diseno_digital_avanzado/labs/lab2/src/mem_init.bin"        //! Memory data

    )
    (
        /* Inputs */    
        input  wire                   i_clock   ,           //! System clock
        input  wire                   i_reset   ,           //! System reset
        input  wire                   i_enable  ,           //! System enable

        /* Outputs */
        output wire [FIR_NB_OUT-1 :0] o_fir_output          //! Filter output
    );
    
/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    wire [FIR_NB_IN  -1 : 0] gen_output;
    wire [FIR_NB_OUT -1 : 0] fir_output;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    signal_generator #(
        .NB_DATA        ( GEN_NB_DATA       ) ,
        .NB_COUNT       ( GEN_NB_COUNT      ) ,
        .MEM_INIT_FILE  ( GEN_MEM_INIT_FILE )
    )
    u_signal_generator (
        .i_clock        ( i_clock     ) ,
        .i_reset        ( i_reset     ) ,
        .o_signal       ( gen_output  )
    );

    filter #(
        .NB_IN          ( FIR_NB_IN   ) ,
        .NBF_IN         ( FIR_NBF_IN  ) ,
        .NB_OUT         ( FIR_NB_OUT  ) ,
        .NBF_OUT        ( FIR_NBF_OUT ) ,
        .NB_TAPS        ( FIR_NB_TAPS ) ,
        .NBF_TAPS       ( FIR_NBF_TAPS) ,
        .N_TAPS         ( FIR_N_TAPS  ) ,    
        .BW             ( FIR_BW      )
    )
    u_filter (
        .i_fir_input    ( gen_output ),
        .i_clock        ( i_clock    ),
        .i_reset        ( i_reset    ),
        .i_enable       ( i_enable   ),
        .o_fir_output   ( fir_output )
    );

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/
    assign o_fir_output = fir_output;

endmodule /* top */
