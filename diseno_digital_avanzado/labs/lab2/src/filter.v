/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: filter.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 14 Sep. 2021
* Description 	: laboratory number 2
************************************************************************************************
*/

/*
************************************************************************************************
*                                       DOCUMENTATION
************************************************************************************************
*/

//! @title   Laboratory Nº 2
//! @file    filter.v
//! @author  Patricio Reus Merlo
//! @date    14 Sep. 2021

//! - FIR  with 15 coefficients
//! - **i_reset** is the system reset.
//! - **i_enable** controls the enable (1) of the FIR.

/*
************************************************************************************************
*								            MODULE
************************************************************************************************
*/

module filter 
    #(
        parameter NB_IN     = 8     ,               //! Total number of bits
        parameter NBF_IN    = 7     ,               //! Number of fractional bits

        parameter NB_OUT    = 14    ,               //! Total number of bits
        parameter NBF_OUT   =  8    ,               //! Number of fractional bits

        parameter NB_TAPS   = 8     ,               //! Total number of bits 
        parameter NBF_TAPS  = 7     ,               //! Number of fractional bits

        parameter N_TAPS    = 15    ,               //! Quantity of filter coefficients

        parameter BW        = 0     ,               //! (0) 0.5kHz (1) 8kHz (2) 17kHz
    
        parameter TRIM_MODE = 2                     //! Bit trimmer mode
    )
    (
        /* Inputs */    
        input  wire signed [NB_IN -1 :0] i_fir_input,  //! Filter input
        input  wire                      i_clock    ,  //! Clock
        input  wire                      i_reset    ,  //! Asynchronous reset
        input  wire                      i_enable   ,  //! Enable

        /* Outputs */
        output wire signed [NB_OUT-1 :0] o_fir_output  //! Filter output
    );

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam NBI_IN   = NB_IN - NBF_IN    ;   //! Integer bits number of input  signal
    localparam NBI_OUT  = NB_OUT - NBF_OUT  ;   //! Integer bits number of output signal
    localparam NBI_TAPS = NB_TAPS-NBF_TAPS  ;   //! Integer bits number of filter coefficients

    localparam NB_PROD  = NB_TAPS  + NB_IN  ;   //! Total number of bits at multiplier output
    localparam NBF_PROD = NBF_IN  + NBF_TAPS;   //! Fractional bits at multiplier output

    localparam NB_CARRY = $clog2(N_TAPS)    ;   //! Number of sums tree levels

    localparam NB_TRIM  = NB_OUT - NB_CARRY ; //! Total number of bits at trimmer output 
    localparam NBF_TRIM = NBF_OUT           ; //! Fractional bits at trimmer output

    localparam NB_ADD   = NB_OUT            ;   //! Total bits number of adder
    localparam NBF_ADD  = NBF_OUT           ;   //! Fractional bits number of adder 
    localparam NBI_ADD  = NBI_OUT           ;   //! Integer bits number of adder 

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    reg  signed [NB_IN   -1:0] shift_register [N_TAPS -1: 1] ;  //! Input shift register
    wire signed [NB_TAPS -1:0] coefficients   [N_TAPS -1: 0] ;  //! Filter coefficients
    reg  signed [NB_PROD -1:0] products       [N_TAPS -1: 0] ;  //! Multipliers output
    wire signed [NB_TRIM -1:0] trimmers       [N_TAPS -1: 0] ;  //! Trimmers output
    reg  signed [NB_TRIM -1:0] trimmers_reg   [N_TAPS -1: 0] ;  //! Trimmers output register
    reg  signed [NB_ADD  -1:0] sums                          ;  //! Adders output
    reg  signed [NB_OUT  -1:0] output_register               ;  //! Output register

    integer idx;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    /* Set coefficients */
    generate
        case (BW)
            32'd0   : begin : bandwidth_05
                assign coefficients[0]  = 8'b00000001 ; /* h0  - 1  */
                assign coefficients[1]  = 8'b00000010 ; /* h1  - 2  */
                assign coefficients[2]  = 8'b00000100 ; /* h2  - 4  */
                assign coefficients[3]  = 8'b00000111 ; /* h3  - 7  */
                assign coefficients[4]  = 8'b00001010 ; /* h4  - 10 */
                assign coefficients[5]  = 8'b00001101 ; /* h5  - 13 */
                assign coefficients[6]  = 8'b00010000 ; /* h6  - 16 */
                assign coefficients[7]  = 8'b00010000 ; /* h7  - 16 */
                assign coefficients[8]  = 8'b00010000 ; /* h8  - 16 */
                assign coefficients[9]  = 8'b00001101 ; /* h9  - 13 */
                assign coefficients[10] = 8'b00001010 ; /* h10 - 10 */
                assign coefficients[11] = 8'b00000111 ; /* h11 - 7  */
                assign coefficients[12] = 8'b00000100 ; /* h12 - 4  */
                assign coefficients[13] = 8'b00000010 ; /* h13 - 2  */
                assign coefficients[14] = 8'b00000001 ; /* h14 - 1  */
            end
            
            32'd1   : begin : bandwidth_8
                assign coefficients[0]  = 8'b00000000 ; /* h0  - 0   */
                assign coefficients[1]  = 8'b11111111 ; /* h1  - 255 */
                assign coefficients[2]  = 8'b11111110 ; /* h2  - 254 */
                assign coefficients[3]  = 8'b11111100 ; /* h3  - 252 */
                assign coefficients[4]  = 8'b00000000 ; /* h4  - 0   */
                assign coefficients[5]  = 8'b00001110 ; /* h5  - 14  */
                assign coefficients[6]  = 8'b00100001 ; /* h6  - 33  */
                assign coefficients[7]  = 8'b00101010 ; /* h7  - 42  */
                assign coefficients[8]  = 8'b00100001 ; /* h8  - 33  */
                assign coefficients[9]  = 8'b00001110 ; /* h9  - 14  */
                assign coefficients[10] = 8'b00000000 ; /* h10 - 0   */
                assign coefficients[11] = 8'b11111100 ; /* h11 - 252 */
                assign coefficients[12] = 8'b11111110 ; /* h12 - 254 */
                assign coefficients[13] = 8'b11111111 ; /* h13 - 255 */
                assign coefficients[14] = 8'b00000000 ; /* h14 - 0   */
            end

            default : begin : bandwidth_17
                assign coefficients[0]  = 8'b11111111 ; /* h0  - 255 */
                assign coefficients[1]  = 8'b00000000 ; /* h1  - 0   */
                assign coefficients[2]  = 8'b11111110 ; /* h2  - 254 */
                assign coefficients[3]  = 8'b00000000 ; /* h3  - 0   */
                assign coefficients[4]  = 8'b00000110 ; /* h4  - 6   */
                assign coefficients[5]  = 8'b11101111 ; /* h5  - 239 */
                assign coefficients[6]  = 8'b00011011 ; /* h6  - 27  */
                assign coefficients[7]  = 8'b01100000 ; /* h7  - 96  */
                assign coefficients[8]  = 8'b00011011 ; /* h8  - 27  */
                assign coefficients[9]  = 8'b11101111 ; /* h9  - 239 */
                assign coefficients[10] = 8'b00000110 ; /* h10 - 6   */
                assign coefficients[11] = 8'b00000000 ; /* h11 - 0   */
                assign coefficients[12] = 8'b11111110 ; /* h12 - 254 */
                assign coefficients[13] = 8'b00000000 ; /* h13 - 0   */
                assign coefficients[14] = 8'b11111111 ; /* h14 - 255 */
            end
        endcase
    endgenerate

    /* Input register */
    always @(posedge i_clock or posedge i_reset) begin: input_register
        if(i_reset) begin
            for (idx = 1; idx < N_TAPS; idx = idx + 1) begin
                shift_register[idx] <= {NB_IN {1'b0} };
            end
        end
        else begin
            if(i_enable) begin
                for (idx = 1; idx < N_TAPS ; idx = idx + 1) begin
                    if (idx == 1)
                        shift_register[idx] <= i_fir_input;
                    else
                        shift_register[idx] <= shift_register[idx-1]; 
                end                
            end
        end
    end

    /* Products */
    always @(*) begin : output_products
        for(idx = 0; idx < N_TAPS ; idx = idx + 1) begin
            if (idx == 0)
                products[idx] = coefficients[idx] * i_fir_input;
            else
                products[idx] = coefficients[idx] * shift_register[idx];
        end
    end

    /* Bit trimmers */
    generate
        genvar gen_idx;
        for (gen_idx = 0; gen_idx < N_TAPS; gen_idx = gen_idx + 1) begin

            bit_trimmer #(
                .NB_IN      ( NB_PROD   ) ,
                .NBF_IN     ( NBF_PROD  ) ,
                .NB_OUT     ( NB_TRIM   ) ,
                .NBF_OUT    ( NBF_TRIM  ) ,
                .MODE       ( TRIM_MODE )
            )
            u_bit_trimmer (
                .i_full     (products[gen_idx]) ,
                .o_trim     (trimmers[gen_idx])  
            );
            
        end
    endgenerate

     /* Trimmers output registers */
    always @(posedge i_clock) begin : output_trimmers
        for(idx = 0; idx < N_TAPS ; idx = idx + 1) begin
            trimmers_reg[idx] <= trimmers[idx];
        end
    end

    /* Sums */
    always @(*) begin: output_sums
        sums = {NB_ADD{1'b0}};
        for(idx = 0; idx < N_TAPS ; idx = idx + 1) begin
            sums = sums + trimmers_reg[idx];
        end
    end
    
    /* Output register */
    always @(posedge i_clock or posedge i_reset) begin
        if (i_reset) 
            output_register <= {NB_OUT{1'b0}};
        else
            output_register <= sums;
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/
    assign o_fir_output = sums;

endmodule /* filter */
