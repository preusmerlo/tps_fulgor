/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: bit_trimmer.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 20 Sep. 2021
* Description 	: bit trimmer module
************************************************************************************************
*/

/*
************************************************************************************************
*                                       DOCUMENTATION
************************************************************************************************
*/

//! @title   Bit trimmer
//! @file    bit_trimmer.v
//! @author  Patricio Reus Merlo
//! @date    20 Sep. 2021

//! - Configurable trimmer module. MODE code:
//! --- (0) - FULL RESOLUTION (default)
//! --- (1) - OVERFLOW WITH TRUNCATE 
//! --- (2) - SATURATION WITH TRUNCATE
//! --- (3) - SATURATION WITH ROUNDING

/*
************************************************************************************************
*								            MODULE
************************************************************************************************
*/

module bit_trimmer #(
        /* Parametres */
        parameter NB_IN    = 16         ,   //! Total number of bits at input
        parameter NBF_IN   = 14         ,   //! Number of fractional bits at input        
        
        parameter NB_OUT   = 9          ,   //! Total number of bits desired at output 
        parameter NBF_OUT  = 8          ,   //! Number of fractional bits desired at output 
        
        parameter MODE     = 3              //! Type of trimmer operation
    )
    (
        /* Inputs  */
        input  [NB_IN  - 1:0] i_full     ,  //! Trimmer input

        /* Outputs */        
        output                o_sat_flag ,  //! Saturation flag
        output [NB_OUT - 1:0] o_trim        //! Trimmer output
    );
    
/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Trimmer mode code */
    localparam OUT_FULL_RES = 0 ;
    localparam OUT_TRN_OVF  = 1 ;
    localparam OUT_TRN_SAT  = 2 ;
    localparam OUT_RND_SAT  = 3 ;

    localparam NBI_IN  = NB_IN - NBF_IN ;
    localparam NBI_OUT  = ( NB_OUT  > NBF_OUT  )? NB_OUT  - NBF_OUT  : 0;

    localparam NB_RND   = (1 + NBI_IN + NBF_OUT + 1) ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    reg                           sat_flag      ;
    reg  signed [NB_OUT - 1 : 0]  trimm_output  ;
    reg  signed [NB_RND - 1 : 0]  full_rnd      ;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    generate
        case (MODE)
            /* --- Truncado y overflow --- */            
            32'd1 : begin : overflow_truncate 
                        always @(*) begin
                            trimm_output = i_full [(NB_IN -1) - (NBI_IN - NBI_OUT) -: NB_OUT];
                            sat_flag     = 1'b0;       
                        end    
                    end

            /* --- Truncado y saturacion --- */           
            32'd2 : begin : saturation_truncate
                        always @(*) begin
                            if ( &i_full[(NB_IN -1) -: (NBI_IN - NBI_OUT) + 1] || ~|i_full[(NB_IN -1) -: (NBI_IN - NBI_OUT) + 1] ) begin
                                trimm_output = i_full[ (NB_IN -1) - (NBI_IN - NBI_OUT) -: NB_OUT ];
                                sat_flag = 1'b0;
                            end
                            else if ( i_full[(NB_IN -1)] ) begin
                                trimm_output = { 1'b1 , { NB_OUT-1 {1'b0} } } ;
                                sat_flag = 1'b1;
                            end
                            else begin
                                trimm_output = { 1'b0 , { NB_OUT-1 {1'b1} } } ;
                                sat_flag = 1'b1;
                            end
                        end            
                    end

            /* --- Redondeo y saturacion ---  */
            32'd3 : begin : saturation_rounding
                        always @(*) begin
                            /* Redondeo */
                            full_rnd = $signed(i_full[(NB_IN -1) -: (NB_RND -1)]) + $signed(2'b01);

                            /* Saturacion */
                            if ( &full_rnd[(NB_RND -1) -: (NBI_IN - NBI_OUT) + 2] || ~|full_rnd[(NB_RND -1) -: (NBI_IN - NBI_OUT) + 2] ) begin
                                trimm_output = full_rnd[ (NB_RND -1) - (NBI_IN - NBI_OUT) -1 -: NB_OUT ];
                                sat_flag = 1'b0;
                            end
                                
                            else if ( full_rnd[(NB_RND -1)] ) begin
                                trimm_output = { 1'b1 , { NB_OUT-1 {1'b0} } } ;
                                sat_flag = 1'b1;
                            end
                            else begin
                                trimm_output = { 1'b0 , { NB_OUT-1 {1'b1} } } ;
                                sat_flag = 1'b1;
                            end
                        end                    
                    end    

            default : begin : full_resolution
                        always @(*) begin
                            trimm_output = i_full;
                            sat_flag = 1'b0;
                        end
                    end
        endcase
    endgenerate

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/
    /* Salida recortada */
    assign o_trim     = trimm_output;

    /* Indicador de saturacion */
    assign o_sat_flag = sat_flag;

endmodule