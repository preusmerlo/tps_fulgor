/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: signal_generator.v
* Programmer(s): Patricio Reus Merlo
* Created on	: 20 Sep. 2021
* Description 	: ...
************************************************************************************************
*/

/*
************************************************************************************************
*                                       DOCUMENTATION
************************************************************************************************
*/

//! @title   Signal generator
//! @file    signal_generator.v
//! @author  Patricio Reus Merlo
//! @date    20 Sep. 2021

//! Description:
//! Generator of  resultant signal from sum two sine waves with different frequencies.

/*
************************************************************************************************
*								            MODULE
************************************************************************************************
*/

module signal_generator 
    #(
        /* Parameters */
        parameter NB_DATA       = 8         ,       //! Total number of output bits 
        parameter NB_COUNT      = 10        ,       //! Quantity of bits of memory address
        parameter MEM_INIT_FILE = "mem_init.bin"    //! Memory data
    )
    (
        /* Inputs */    
        input                    i_clock    ,       //! System clock
        input                    i_reset    ,       //! System reset
        
        /* Outputs */
        output [NB_DATA - 1 : 0] o_signal           //! Generator output signal
    );

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    reg [NB_COUNT  - 1 : 0] counter                       ; //! Address counter
    reg [NB_DATA   - 1 : 0] data   [(2 ** NB_COUNT) -1 :0]; //! Memory
    reg [NB_DATA   - 1 : 0] sample                        ; //! Signal sample
    
/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    initial begin
        if (MEM_INIT_FILE != "") begin
            $readmemb(MEM_INIT_FILE, data);
        end
    end

    always@(posedge i_clock or posedge i_reset) begin: generate_signal
        if(i_reset) begin
            counter  <= {NB_COUNT{1'b0}};
            sample   <= {NB_DATA {1'b0}};
    end
        else begin
            counter  <= counter + {{NB_COUNT-1{1'b0}},{1'b1}};
            sample   <= data[counter];
        end
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign o_signal = sample;

endmodule /* signal_generator */