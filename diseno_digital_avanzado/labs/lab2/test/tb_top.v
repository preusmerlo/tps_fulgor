/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: tb_top.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: dd mmm. 2021
* Description 	: ...
************************************************************************************************
*/

`timescale 1ns/100ps

module tb_top ();

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

        localparam FIR_NB_IN         = 8  ;
        localparam FIR_NBF_IN        = 7  ;
        
        localparam FIR_NB_OUT        = 14 ;
        localparam FIR_NBF_OUT       = 8  ;
        
        localparam FIR_NB_TAPS       = 8  ;
        localparam FIR_NBF_TAPS      = 7  ;
        
        localparam FIR_N_TAPS        = 15 ;
        localparam FIR_BW            = 2  ;
        localparam FIR_TRIM_MODE     = 2  ;
        
        localparam GEN_NB_DATA       = 0  ;
        localparam GEN_NB_COUNT      = 10 ;
        localparam GEN_MEM_INIT_FILE = "/home/dell/Datos/BaseDeDatos/cursos/fulgor/tps_fulgor/diseno_digital_avanzado/labs/lab2/src/mem_init.bin";

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Module inputs  */
    reg                    i_clock      ;
    reg                    i_reset      ;
    reg                    i_enable     ;

    /* Module outputs */
    wire [FIR_NB_OUT-1 :0] o_fir_output ;


/*
************************************************************************************************
*								            PROBES
************************************************************************************************
*/

    wire [GEN_NB_DATA -1 : 0] gen_output;
    assign gen_output = u_top.gen_output;

/*
************************************************************************************************
*								           STIMULUS
************************************************************************************************
*/

    initial begin
        i_clock  = 1'd0 ;
        i_reset  = 1'd0 ;
        i_enable = 1'd1 ;

        #4 i_reset  = 1'd1 ; 
        #4 i_reset  = 1'd0 ; 

        #1000 $finish();
    end

    always  begin
        #1 i_clock = ~i_clock;
    end

/*
************************************************************************************************
*								       MODULE INSTANCE
************************************************************************************************
*/

    top #(
        .FIR_NB_IN          ( FIR_NB_IN         ),
        .FIR_NBF_IN         ( FIR_NBF_IN        ),
        .FIR_NB_OUT         ( FIR_NB_OUT        ),
        .FIR_NBF_OUT        ( FIR_NBF_OUT       ),
        .FIR_NB_TAPS        ( FIR_NB_TAPS       ),
        .FIR_NBF_TAPS       ( FIR_NBF_TAPS      ),
        .FIR_N_TAPS         ( FIR_N_TAPS        ),
        .FIR_BW             ( FIR_BW            ),
        .GEN_NB_DATA        ( GEN_NB_DATA       ),
        .GEN_NB_COUNT       ( GEN_NB_COUNT      ),
        .GEN_MEM_INIT_FILE  ( GEN_MEM_INIT_FILE ),
        .FIR_TRIM_MODE      ( FIR_TRIM_MODE     )

    )
    u_top (
        .i_clock            ( i_clock      )  ,
        .i_reset            ( i_reset      )  ,
        .i_enable           ( i_enable     )  ,
        .o_fir_output       ( o_fir_output )
    );

endmodule /* tb_top */