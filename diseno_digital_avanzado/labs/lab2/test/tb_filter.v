/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: tb_filter.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 14 Sep. 2021
* Description 	: laboratory number 2
************************************************************************************************
*/

`timescale 1ns/100ps

module tb_filter ();

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam NB_IN    = 8  ;                  //! Total number of bits
    localparam NBF_IN   = 7  ;                  //! Number of fractional bits
    
    localparam NB_OUT   = 14 ;                  //! Total number of bits
    localparam NBF_OUT  =  8 ;                  //! Number of fractional bits
    
    localparam NB_TAPS  = 8  ;                  //! Total number of bits 
    localparam NBF_TAPS = 7  ;                  //! Number of fractional bits
    
    localparam N_TAPS   = 15 ;                  //! Quantity of filter coefficients

    localparam BW       = 1  ;                  //! (0) 0.5kHz (1) 8kHz (2) 17kHz

    localparam TRIM_MODE = 3 ;                  //! Bit trimmer mode

    localparam FILES_PATH = "/home/dell/Datos/BaseDeDatos/cursos/fulgor/tps_fulgor/diseno_digital_avanzado/labs/lab2/py/" ; 
    
    localparam FILE_NAME  = "mem_init.bin"   ;
    localparam FILE_INPUT = { FILES_PATH , FILE_NAME  }; 

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Module inputs  */
    reg  signed [NB_IN -1 :0] i_fir_input ;     //! Filter input
    reg                       i_clock     ;     //! Clock
    reg                       i_reset     ;     //! Asynchronous reset
    reg                       i_enable    ;     //! Enable

    /* Module outputs */
    wire signed [NB_OUT-1 :0] o_fir_output;     //! Filter output

    /* File descriptor */
    integer fd_signal;
    integer error    ;

/*
************************************************************************************************
*								          OPEN FILES
************************************************************************************************
*/

    initial begin
        fd_signal  = $fopen( FILE_INPUT , "r");
        if(fd_signal == 0)
           $stop ;         
    end

/*
************************************************************************************************
*								       STIMULUS
************************************************************************************************
*/

    initial begin
        i_clock  = 1'd0 ;
        i_reset  = 1'd0 ;
        i_enable = 1'b1 ;

        #5 i_reset = 1 ;
        #5 i_reset = 0 ;

        #100000
        $fclose(fd_signal);
        $finish();
    end

    always @(posedge i_clock) begin
        error <= $fscanf(fd_signal,"%b", i_fir_input);
        
        if( error != 1) begin
            $fclose(fd_signal);
            $finish;
        end  
    end

    always  begin
        #1 i_clock = ~i_clock;
    end

/*
************************************************************************************************
*								       MODULE INSTANCE
************************************************************************************************
*/

    filter #(
        .NB_IN          (   NB_IN      ) ,
        .NBF_IN         (   NBF_IN     ) ,
        .NB_OUT         (   NB_OUT     ) ,
        .NBF_OUT        (   NBF_OUT    ) ,
        .NB_TAPS        (   NB_TAPS    ) ,
        .NBF_TAPS       (   NBF_TAPS   ) ,
        .N_TAPS         (   N_TAPS     ) ,    
        .BW             (   BW         ) ,
        .TRIM_MODE      (   TRIM_MODE  )
    )
    DUT (
        .i_fir_input    ( i_fir_input  ),
        .i_clock        ( i_clock      ),
        .i_reset        ( i_reset      ),
        .i_enable       ( i_enable     ),
        .o_fir_output   ( o_fir_output )
    );

endmodule /* tb_filter */