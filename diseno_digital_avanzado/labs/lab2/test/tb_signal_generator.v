/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: tb_signal_generator.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: dd mmm. 2021
* Description 	: ...
************************************************************************************************
*/

`timescale 1ns/100ps

module tb_signal_generator ();

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Module parameters */
    localparam NB_DATA       = 8                ;
    localparam NB_COUNT      = 10               ;
    localparam MEM_INIT_FILE = "/home/dell/Datos/BaseDeDatos/cursos/fulgor/tps_fulgor/diseno_digital_avanzado/labs/lab2/src/mem_init.bin"   ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Module inputs  */
    reg                    i_clock    ;
    reg                    i_reset    ;

    /* Module outputs */
    wire [NB_DATA - 1 : 0] o_signal   ; 

/*
************************************************************************************************
*								           STIMULUS
************************************************************************************************
*/

    initial begin
        i_clock  = 1'd0 ;
        i_reset  = 1'd0 ;

        /* Stimulus */ 
        #4 i_reset  = 1'd1 ;
        #4 i_reset  = 1'd0 ;

        #2048$finish();
    end

    always  begin
        #1 i_clock = ~i_clock;
    end

/*
************************************************************************************************
*								       MODULE INSTANCE
************************************************************************************************
*/

    signal_generator #(
        .NB_DATA        ( NB_DATA       ),
        .NB_COUNT       ( NB_COUNT      ),
        .MEM_INIT_FILE  ( MEM_INIT_FILE )
    )
    DUT (
        .i_clock        ( i_clock       ),
        .i_reset        ( i_reset       ),
        .o_signal       ( o_signal      )    
    );

endmodule /* tb_signal_generator */