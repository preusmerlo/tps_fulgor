assign coefficients[0] = 8'b00000000 ; /* h0 - 0 */
assign coefficients[1] = 8'b11111111 ; /* h1 - 255 */
assign coefficients[2] = 8'b11111110 ; /* h2 - 254 */
assign coefficients[3] = 8'b11111100 ; /* h3 - 252 */
assign coefficients[4] = 8'b00000000 ; /* h4 - 0 */
assign coefficients[5] = 8'b00001110 ; /* h5 - 14 */
assign coefficients[6] = 8'b00100001 ; /* h6 - 33 */
assign coefficients[7] = 8'b00101010 ; /* h7 - 42 */
assign coefficients[8] = 8'b00100001 ; /* h8 - 33 */
assign coefficients[9] = 8'b00001110 ; /* h9 - 14 */
assign coefficients[10] = 8'b00000000 ; /* h10 - 0 */
assign coefficients[11] = 8'b11111100 ; /* h11 - 252 */
assign coefficients[12] = 8'b11111110 ; /* h12 - 254 */
assign coefficients[13] = 8'b11111111 ; /* h13 - 255 */
assign coefficients[14] = 8'b00000000 ; /* h14 - 0 */
