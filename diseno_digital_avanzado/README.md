# Guías Prácticas

- [ ] GP1:
  - [x] **Ej 1** : sumador de numeros enteros.
  - [x] Ej 2     : mini ALU.
  - [ ] Ej 3     : evaluacion de código Verilog.
  - [ ] **Ej 4** : filtro IIR.

- [ ] GP2:
  - [ ] **Ej 1** : multiplicador en punto flotante.
  - [ ] Ej 2     : multiplicacion signada y no signada.
  - [ ] Ej 3     : multiplicador en punto fijo. 
  - [ ] **Ej 4** : filtro FIR con truncado y redondeo.

**Negrita**: ejercicios sugeridos.

# Laboratorios

- [x] Lab 1: shift leds.
- [ ] Lab 2: filtro FIR.