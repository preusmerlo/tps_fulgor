import numpy as np

class ff_single:
    def __init__(self, init_val=0):
        self.__i = 0
        self.__o = init_val
   
    @property
    def o(self):
        return self.__o
    
    @property
    def i(self):
        return self.__i
    
    @i.setter
    def i(self,data):
        self.__i = data
    
    def run_clock(self):
        self.__o = self.__i
        
    def reset(self):
        self.__o = 0
        
class ff:
    def __init__(self, size=1, init_val=0):
        self.__size = size
        self.__ffs = [ff_single(init_val) for k in range(size)]
        
    def __getitem__(self, k):
        return self.__ffs[k]
    
    def __len__(self):
        return len(self.__ffs)

    @property
    def o(self):
        if(self.__size>1):
            return [self.__ffs[k].o for k in range(self.__size)]
        else:
             return self.__ffs[0].o
    
    @property
    def i(self):
        if(self.__size>1):
            return [self.__ffs[k].i for k in range(self.__size)]
        else:
             return self.__ffs[0].i
    
    @i.setter
    def i(self,data):
        if isinstance(data,str):
            if '0x' in data:
                data = list(bin(int(data,16))[2:].zfill(self.__size))[::-1]
            elif '0b' in data:
                data = list(bin(int(data,2))[2:].zfill(self.__size))[::-1]
            else:
                raise('The representation is not supported')

            data = [int(data[i]) for i in range(self.__size)]

        for k in range(self.__size):
            if isinstance(data, (list, tuple, np.ndarray)):
                self.__ffs[k].i = data[k]
            else:
                self.__ffs[k].i = data
    
    def run_clock(self):
        for k in range(self.__size):
            self.__ffs[k].run_clock()
        
    def reset(self):
        for k in range(self.__size):
            self.__ffs[k].reset()