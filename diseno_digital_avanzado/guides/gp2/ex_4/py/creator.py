######################################################################
#                       DISEÑO DIGITAL AVANZADO 
#                           Fundacion Fulgor 
#
# Filename		: creator.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 21 Sep. 2021
# Description 	: guide 2 - exercise 4
######################################################################

######################################################################
#                               MODULES
######################################################################

import numpy             as np
import tool._fixedInt    as fxd
import scipy.signal      as sig
import scipy.fftpack     as fft
import matplotlib.pyplot as plt

######################################################################
#                              CONSTANTS
######################################################################

sample_rate   = 48000.   # [Hz]
n_samples     = 1024     # [-]

signal_freq   = 0.5e3    # [Hz]
signal_amp    = 1        # [V]

noise_freq    = 20e3     # [Hz]
noise_amp     = 0.5      # [V]

filter_fc     = 0.3e3    # [Hz]
filter_n_taps = 4        # [-]

fixed_nb_total= 16
fixed_nb_frac = 15    

EXPORT_TAPS   = 1
EXPORT_SIGNAL = 1
SHOW_PLOTS    = 1

######################################################################
#                               MAIN
######################################################################

def main():

    # Transmited signal
    time   = np.arange(n_samples) / sample_rate
    noise  = noise_amp  * np.sin(2*np.pi*noise_freq *time)
    signal = signal_amp * np.sin(2*np.pi*signal_freq*time)
    tx_out = signal + noise

    # Low pass filter taps
    float_coeff = sig.firwin(filter_n_taps, 2 * filter_fc / sample_rate)
    # float_coeff = float_coeff / np.max(float_coeff) 
    
    fixed_coeff = fxd.arrayFixedInt(fixed_nb_total, fixed_nb_frac, float_coeff)
    fxd_obj     = fixed_coeff.copy()
    fixed_coeff = [a.fValue for a in fixed_coeff]

    # Filtering
    rx_float = sig.lfilter(float_coeff, 1.0, tx_out)
    rx_fixed = sig.lfilter(fixed_coeff, 1.0, tx_out)
    
    # --- Spectrum plot ---
    if SHOW_PLOTS:
        # Noisy signal spectrum
        w , tx_fft = get_fft(tx_out,1024)
        tx_fft_n = tx_fft / max(np.abs(tx_fft))

        # Filter Response
        w, float_fir_fft = get_fft(float_coeff,1024)

        w, fixed_fir_fft = get_fft(fixed_coeff,1024)

        # Filtered signal spectrum
        w , float_rx_fft = get_fft(rx_float,1024)
        w , fixed_rx_fft = get_fft(rx_fixed,1024)

        plt.figure(1, figsize=(8, 6))

        plt.subplot(2,2,1)
        plt.plot(w, np.abs(tx_fft_n     ),'-m', label='Noisy signal fft')
        plt.plot(w, np.abs(float_fir_fft),'--r', label='Float filter fft')
        plt.plot(w, np.abs(fixed_fir_fft),'--b', label='Fixed filter fft')
        
        plt.title('Normalized Spectrum')
        plt.xlabel('Frequency [rad/seg]')
        plt.ylabel('Amplitude')
        plt.xlim(-3, 3)
        plt.legend()
        plt.grid()

        plt.subplot(2,2,2)
        plt.plot(w, np.abs(tx_fft      ) ,'-m', label='Noisy signal fft')
        plt.plot(w, np.abs(float_rx_fft) ,'--r', label='Filtered signal with float filter')
        plt.plot(w, np.abs(fixed_rx_fft) ,'--b', label='Filtered signal with fixed filter')
        
        plt.title('Filtered signals')
        plt.xlabel('Frequency [rad/seg]')
        plt.ylabel('Amplitude')
        plt.xlim(-3, 3)
        plt.legend()
        plt.grid()

        plt.subplot(2,2,3)
        plt.plot(float_coeff ,'-or', label='Float coefficients')
        plt.plot(fixed_coeff ,'-ob', label='Fixed coefficients')
        
        plt.title('Impulse response')
        plt.xlabel('Time [samples]')
        plt.ylabel('Amplitude')
        plt.legend()
        plt.grid()

        samples = 300
        plt.subplot(2,2,4)
        plt.plot(tx_out  [0:samples],'--m', label='Filter input'       )
        plt.plot(rx_float[0:samples],'--r', label='Float fitler output')
        plt.plot(rx_fixed[0:samples],'--b', label='Fixed fitler output')
        
        plt.title('Filter output')
        plt.xlabel('Time [samples]')
        plt.ylabel('Amplitude')
        plt.xlim(0, samples)
        plt.legend()
        plt.grid()

        plt.show()

    # Export taps
    if EXPORT_TAPS:
        fd_taps     = open('taps.v','w')
        for i in range(len(fxd_obj)):
            tap = fxd.formatForTestbenchFile(fxd_obj[i].intvalue,fxd_obj[i].width,separator='',end='');
            fd_taps.write(f"assign coefficients[{i}] = {fixed_nb_total}'b{tap} ; /* h{i} - {fxd_obj[i].intvalue} */\n");
        fd_taps.close()

    # Export "noisy" signal
    if EXPORT_SIGNAL:
        fixed_tx_out= fxd.arrayFixedInt(fixed_nb_total, fixed_nb_frac, tx_out)
        fd_signal =  open('mem_init.bin','w')

        for i in range(len(fixed_tx_out)):
            fd_signal.write(fxd.formatForTestbenchFile(fixed_tx_out[i].intvalue,fixed_tx_out[i].width,separator='',end='\n'));

        fd_signal.close()

######################################################################
#                               FFT
######################################################################

def get_fft(signal, nfft):
    w = np.linspace(-np.pi, np.pi, nfft)
    return w , fft.fftshift(fft.fft(signal,nfft))

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()