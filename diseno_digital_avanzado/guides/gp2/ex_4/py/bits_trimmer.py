######################################################################
#                    DISEÑO DIGITAL AVANZADO 
#                        Fundacion Fulgor 
#
# Filename		: ex_4.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 13 Sep. 2021
# Description 	: guide 2 - exercise 4
######################################################################

######################################################################
#                               MODULES
###################################################################### 

from tool import _fixedInt as fxd
import numpy as np

######################################################################
#                           CONSTANTS & MACROS
######################################################################

INPUT_LENGTH = 1000

# Module parameters
NB_IN    = 32
NBF_IN   = 30

NB_OUT   = 12
NBF_OUT  = 11

MODE     = 2

######################################################################
#                               MAIN
######################################################################

def main():
    # Create random input between +-1
    # input_array = 2 * np.random.rand(INPUT_LENGTH) - 1
    time = np.linspace(0,6 * np.pi , INPUT_LENGTH)
    input_array = np.sin(2* np.pi * 1 * time)

    # Cuantize random input
    i_full = fxd.arrayFixedInt(
                    totalWidth      = NB_IN       ,     
                    fractWidth      = NBF_IN      ,
                    N               = input_array ,
                    signedMode      = 'S'         ,
                    roundMode       = 'trunc'     ,
                    saturateMode    = 'saturate'  )
    
    i_full_f = [fixed.fValue for fixed in i_full]

    # Trim bits
    if MODE == 0 :
        o_trim = fxd.arrayFixedInt(
                    totalWidth      = NB_OUT      ,     
                    fractWidth      = NBF_OUT     ,
                    N               = i_full_f    ,
                    signedMode      = 'S'         ,
                    roundMode       = 'trunc'     ,
                    saturateMode    = 'saturate'  )
    elif MODE == 1:
        o_trim = fxd.arrayFixedInt(
                    totalWidth      = NB_OUT       ,     
                    fractWidth      = NBF_OUT      ,
                    N               = i_full_f    ,
                    signedMode      = 'S'         ,
                    roundMode       = 'trunc'     ,
                    saturateMode    = 'wrap'      )
    elif MODE == 2:
        o_trim = fxd.arrayFixedInt(
                    totalWidth      = NB_OUT      ,     
                    fractWidth      = NBF_OUT     ,
                    N               = i_full_f    ,
                    signedMode      = 'S'         ,
                    roundMode       = 'trunc'     ,
                    saturateMode    = 'saturate'  )
    else:
        o_trim = fxd.arrayFixedInt(
                    totalWidth      = NB_OUT      ,     
                    fractWidth      = NBF_OUT     ,
                    N               = i_full_f    ,
                    signedMode      = 'S'         ,
                    roundMode       = 'round'     ,
                    saturateMode    = 'saturate'  )
    
    # Create output vector file
    fd_in   = open(f'../log/py_rand_mode{MODE}_in.log','w' )
    fd_out  = open(f'../log/py_trim_mode{MODE}_out.log','w')
    
    for i in range(INPUT_LENGTH):
        fd_in .write(fxd.formatForTestbenchFile(i_full[i].intvalue,i_full[i].width,separator='',end='\n'))
        fd_out.write(fxd.formatForTestbenchFile(o_trim[i].intvalue,o_trim[i].width,separator='',end='\n'))
        
    fd_in.close()
    fd_out.close()

######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()