/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: tb_bit_trimmer.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 20 Sep. 2021
* Description 	: bit_trimmer module testbench
************************************************************************************************
*/

`timescale 1ns/100ps

module tb_bit_trimmer ();

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Module parameters */
    localparam NB_IN    = 32 ;   //! Total number of bits at input
    localparam NBF_IN   = 30 ;   //! Number of fractional bits at input            
    localparam NB_OUT   = 12 ;   //! Total number of bits desired at output 
    localparam NBF_OUT  = 11 ;   //! Number of fractional bits desired at output 
    
    localparam MODE_2   = 2  ;   //! Type of trimmer operation: SATURATION WITH TRUNCATE
    localparam MODE_3   = 3  ;   //! Type of trimmer operation: SATURATION WITH ROUNDING

    /* Files */
    localparam FILES_PATH   = "/home/dell/Datos/BaseDeDatos/cursos/fulgor/tps_fulgor/diseno_digital_avanzado/guides/gp2/ex_4/log/";     

    localparam FILE_IN     = { FILES_PATH , "py_rand_mode2_in.log" }; 
    localparam FILE_M2_OUT = { FILES_PATH , "tb_trim_mode2_out.log"}; 
    localparam FILE_M3_OUT = { FILES_PATH , "tb_trim_mode3_out.log"}; 

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    /* Testbench */
    reg i_clock;

    /* Module inputs  */
    reg  [NB_IN  - 1:0] i_full        ;    //! Trimmer input

    /* Module outputs */
    wire                o_sat_flag_m2 ;    //! Saturation flag
    wire                o_sat_flag_m3 ;    //! Saturation flag
    wire [NB_OUT - 1:0] o_trim_m2     ;    //! Trimmer output
    wire [NB_OUT - 1:0] o_trim_m3     ;    //! Trimmer output

    /* File descriptor */
    integer fd_in     ;
    integer fd_m2_out ;
    integer fd_m3_out ;
    integer error     ;

/*
************************************************************************************************
*								          OPEN FILES
************************************************************************************************
*/

    initial begin
        fd_in     = $fopen( FILE_IN     , "r");   
        fd_m2_out = $fopen( FILE_M2_OUT , "w");
        fd_m3_out = $fopen( FILE_M3_OUT , "w");   

        if(fd_in == 0 || fd_m2_out == 0 || fd_m3_out == 0)
           $stop;          
    end

/*
************************************************************************************************
*								           STIMULUS
************************************************************************************************
*/

    initial begin
        i_clock = 1'd0 ;
        i_full  = {NB_IN{1'b0}};  

        #100000
            $fclose(fd_in    );
            $fclose(fd_m2_out);
            $fclose(fd_m3_out);
            $finish();
    end

    always  begin
        #1 i_clock = ~i_clock;
    end

/*
************************************************************************************************
*                                   READ DATA FROM FILE
************************************************************************************************
*/
    always @(posedge i_clock) begin
        error <= $fscanf(fd_in,"%b", i_full);

        if( error != 1 ) begin
            $fclose(fd_in    );
            $fclose(fd_m2_out);
            $fclose(fd_m3_out);
            $finish();
        end
    end

/*
************************************************************************************************
*								    SAVE DATA TO FILE  
************************************************************************************************
*/

    always @(negedge i_clock) begin
        $fdisplay( fd_m2_out,"%b", o_trim_m2);
        $fdisplay( fd_m3_out,"%b", o_trim_m3);
    end

/*
************************************************************************************************
*								       MODULE INSTANCE
************************************************************************************************
*/

    /* Trimmer mode 2 */
    bit_trimmer #(
        .NB_IN    (NB_IN  ),
        .NBF_IN   (NBF_IN ),
        .NB_OUT   (NB_OUT ),
        .NBF_OUT  (NBF_OUT),
        .MODE     (MODE_2 )
    )
    u_bit_trimmer_2(
        .i_full     ( i_full        ) ,
        .o_sat_flag ( o_sat_flag_m2 ) ,
        .o_trim     ( o_trim_m2     )  
    );

    /* Trimmer mode 3 */
    bit_trimmer #(
        .NB_IN    (NB_IN  ),
        .NBF_IN   (NBF_IN ),
        .NB_OUT   (NB_OUT ),
        .NBF_OUT  (NBF_OUT),
        .MODE     (MODE_3 )
    )
    u_bit_trimmer_3(
        .i_full     ( i_full        ) ,
        .o_sat_flag ( o_sat_flag_m3 ) ,
        .o_trim     ( o_trim_m3     )  
    );

endmodule /* tb_bit_trimmer */