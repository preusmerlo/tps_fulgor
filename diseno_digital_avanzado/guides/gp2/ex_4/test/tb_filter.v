/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: tb_filter.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 21 Sep. 2021
* Description 	: guide 2 - exercise 4
************************************************************************************************
*/

`timescale 1ns/100ps

module tb_filter ();

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    /* Module parameters */
    localparam NB_IN     = 16 ;   //! Total number of bits
    localparam NBF_IN    = 15 ;   //! Number of fractional bits
    localparam NB_TAPS   = 16 ;   //! Total number of bits 
    localparam NBF_TAPS  = 15 ;   //! Number of fractional bits
    localparam NB_OUT    = 18 ;   //! Total number of bits
    localparam NBF_OUT   = 17 ;   //! Number of fractional bits
    localparam N_TAPS    = 4  ;   //! Quantity of filter coefficients
    localparam TRIM_MODE = 3  ;   //! Bit trimmer moide

    /* Files */
    localparam FILES_PATH= "/home/dell/Datos/BaseDeDatos/cursos/fulgor/tps_fulgor/diseno_digital_avanzado/guides/gp2/ex_4/py/"; 
    localparam FILE_IN   = { FILES_PATH , "mem_init.bin" }; 

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    /* Module inputs  */
    reg  [NB_IN -1 :0] i_fir_input   ;  //! Filter input
    reg                i_enable      ;  //! Enable
    reg                i_reset       ;  //! Asynchronous reset
    reg                i_clock       ;  //! Clock

    /* Module outputs */
    wire  [NB_OUT-1 :0] o_fir_output ;  //! Filter output

    /* File descriptor */
    integer fd_in;
    integer error;
/*
************************************************************************************************
*								          OPEN FILES
************************************************************************************************
*/

    initial begin
        fd_in  = $fopen( FILE_IN , "r");
        if(fd_in == 0)
           $stop;          
    end

/*
************************************************************************************************
*								           STIMULUS
************************************************************************************************
*/

    initial begin
        i_clock     = 1'd0 ;
        i_reset     = 1'd0 ;
        i_enable    = 1'b1 ;
        i_fir_input = {NB_IN{1'b0}}; 

        /* Stimulus */ 
        # 10 i_reset     = 1'd1 ;
        # 10 i_reset     = 1'd0 ;

        #100000
        $fclose(fd_in);
        $finish();
    end

    always  begin
        #1 i_clock = ~i_clock;
    end

/*
************************************************************************************************
*                                   READ DATA FROM FILE
************************************************************************************************
*/
    always @(posedge i_clock) begin
        error <= $fscanf(fd_in,"%b", i_fir_input);

        if( error != 1 ) begin
            $fclose(fd_in);
            $finish();
        end
    end

/*
************************************************************************************************
*								       MODULE INSTANCE
************************************************************************************************
*/

    filter #(
        .NB_IN          ( NB_IN       ) ,
        .NBF_IN         ( NBF_IN      ) ,
        .NB_TAPS        ( NB_TAPS     ) ,
        .NBF_TAPS       ( NBF_TAPS    ) ,
        .NB_OUT         ( NB_OUT      ) ,
        .NBF_OUT        ( NBF_OUT     ) ,
        .N_TAPS         ( N_TAPS      ) ,
        .TRIM_MODE      ( TRIM_MODE   ) 
    )
    u_filter (
        .i_fir_input    (i_fir_input  ) ,
        .i_enable       (i_enable     ) ,
        .i_reset        (i_reset      ) ,
        .i_clock        (i_clock      ) ,
        .o_fir_output   (o_fir_output ) 
    );

endmodule /* tb_filter */