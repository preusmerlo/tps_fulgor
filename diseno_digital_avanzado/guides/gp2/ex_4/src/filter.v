/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: filter.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 20 Sep. 2021
* Description 	: guide 2 - exercise 4
************************************************************************************************
*/

/*
************************************************************************************************
*                                       DOCUMENTATION
************************************************************************************************
*/

//! @title   FIR filter
//! @file    filter.v
//! @author  Patricio Reus Merlo
//! @date    20 Sep. 2021

/*
************************************************************************************************
*								            MODULE
************************************************************************************************
*/

module filter
    #(
        /* Module parameters */
        parameter NB_IN    = 16                 ,   //! Total number of bits
        parameter NBF_IN   = 15                 ,   //! Number of fractional bits

        parameter NB_TAPS  = 16                 ,   //! Total number of bits 
        parameter NBF_TAPS = 15                 ,   //! Number of fractional bits

        parameter NB_OUT   = 18                 ,   //! Total number of bits
        parameter NBF_OUT  = 17                 ,   //! Number of fractional bits

        parameter N_TAPS   = 4                  ,   //! Quantity of filter coefficients
    
        parameter TRIM_MODE = 3                     //! Bit trimmer moide
    )
    (
        /* Inputs */    
        input signed [NB_IN -1 :0] i_fir_input  ,   //! Filter input
        input                      i_enable     ,   //! Enable
        input                      i_reset      ,   //! Asynchronous reset
        input                      i_clock      ,   //! Clock

        /* Outputs */
        output signed [NB_OUT-1 :0] o_fir_output    //! Filter output

    );

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam NBI_IN   = NB_IN   - NBF_IN  ;   //! Integer bits number of input  signal
    localparam NBI_OUT  = NB_OUT  - NBF_OUT ;   //! Integer bits number of output signal
    localparam NBI_TAPS = NB_TAPS - NBF_TAPS;   //! Integer bits number of filter coefficients

    localparam NB_PROD  = NB_TAPS + NB_IN   ;   //! Total number of bits at multiplier output
    localparam NBF_PROD = NBF_IN  + NBF_TAPS;   //! Fractional bits at multiplier output

    localparam NB_CARRY = $clog2(N_TAPS)    ;   //! Number of sums tree levels
    
    localparam NB_TRIM  = NB_PROD  - NB_OUT - NB_CARRY; //! Total number of bits at trimmer output 
    localparam NBF_TRIM = NBF_PROD - NBF_OUT- NB_CARRY; //! Fractional bits at trimmer output

    localparam NB_ADD   = NB_OUT            ;   //! Total bits number of adder
    localparam NBF_ADD  = NBF_OUT           ;   //! Fractional bits number of adder 
    localparam NBI_ADD  = NBI_OUT           ;   //! Integer bits number of adder 

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    reg  signed [NB_IN   -1:0] shift_register [N_TAPS -1: 1] ;  //! Input shift register
    wire signed [NB_TAPS -1:0] coefficients   [N_TAPS -1: 0] ;  //! Filter coefficients
    reg  signed [NB_PROD -1:0] products       [N_TAPS -1: 0] ;  //! Multipliers output
    wire signed [NB_TRIM -1:0] trimmers       [N_TAPS -1: 0] ;  //! Trimmers output
    reg  signed [NB_ADD  -1:0] sums                          ;  //! Adders output
    reg  signed [NB_OUT  -1:0] output_register               ;  //! Output register

    integer idx;
/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/
    /* Define coefficients fc = 0.1kHz fs = 48kHz */
    assign coefficients[0] = 16'b0000011000000101 ; /* h0 - 1541  */
    assign coefficients[1] = 16'b0011100111111010 ; /* h1 - 14842 */
    assign coefficients[2] = 16'b0011100111111010 ; /* h2 - 14842 */
    assign coefficients[3] = 16'b0000011000000101 ; /* h3 - 1541  */

    /* Input register */
    always @(posedge i_clock or posedge i_reset) begin: input_shift_register
        if(i_reset) begin
            for (idx = 1; idx < N_TAPS; idx = idx +1) begin
                shift_register[idx] <= {NB_IN {1'b0} };
            end
        end
        else begin
            if(i_enable) begin
                for (idx = 1; idx < N_TAPS; idx = idx + 1) begin
                    if (idx == 1)
                        shift_register[idx] <= i_fir_input;
                    else
                        shift_register[idx] <= shift_register[idx-1]; 
                end                
            end
        end
    end

    /* Products */
    always @(*) begin : output_products
        for(idx = 0; idx < N_TAPS ; idx = idx + 1) begin
            if (idx == 0)
                products[idx] = coefficients[idx] * i_fir_input;
            else
                products[idx] = coefficients[idx] * shift_register[idx];
        end
    end

    /* Bit trimmers */
    generate
        genvar gen_idx;
        for (gen_idx = 0; gen_idx < N_TAPS; gen_idx = gen_idx + 1) begin

            bit_trimmer #(
                .NB_IN      ( NB_PROD   ) ,
                .NBF_IN     ( NBF_PROD  ) ,
                .NB_OUT     ( NB_TRIM   ) ,
                .NBF_OUT    ( NBF_TRIM  ) ,
                .MODE       ( TRIM_MODE )
            )
            u_bit_trimmer (
                .i_full     (products[gen_idx]) ,
                .o_trim     (trimmers[gen_idx])  
            );
            
        end
    endgenerate

    /* Sums */
    always @(*) begin: output_sums
        sums = {NB_ADD{1'b0}};
        for(idx = 0; idx < N_TAPS ; idx = idx + 1) begin
            sums = sums + trimmers[idx];
        end
    end
    
    /* Output register */
    always @(posedge i_clock or posedge i_reset) begin: output_reg
        if (i_reset) 
            output_register <= {NB_OUT{1'b0}};
        else
            output_register <= sums;
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign o_fir_output = output_register;

endmodule /* filter */
