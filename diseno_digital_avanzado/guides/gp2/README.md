# Guía Práctica 1

---

## Ejercicio 1 (sugerido):

### Enunciado

---

### Desarrollo

## Ejercicio 4 (sugerido):

### Enunciado
Escribir el código RTL Verilog para implementar el filtro FIR de cuatro coeficientes que se muestra en la figura:

<img src="ex_4/img/diagrama.png" alt="testbench" width="700"/>


1. Asuma que todos los coeficientes (h0 . . . h3) y los datos de entrada x[n] se encuentran en formato S(16,15).

2. Realizar el truncado en el resultado de las multiplicaciones para implementar sumadores de 18 bits y defina el formato para y[n].

3.  Proponga una optimización del diseño asumiendo que los coeficientes del filtro son simétricos (**PENDIENTE**).

4. Re-escriba el código RTL Verilog pero reemplazando el truncado del apartado b) por redondeo.

---

### Desarrollo

#### Modelado en Python

<img src="ex_4/img/modelado.svg" alt="diagram" width="700"/>

#### Modulo `bit_trimmer.v`

<img src="ex_4/img/bit_trimmer.svg" alt="diagram" width="700"/>

##### Descripcion

Configurable trimmer module. MODE code:
- `MODE = 0` : FULL RESOLUTION (default)
- `MODE = 1` : OVERFLOW WITH TRUNCATE 
- `MODE = 2` : SATURATION WITH TRUNCATE
- `MODE = 3` : SATURATION WITH ROUNDING## Generics and ports

##### Parametros

| Generic name | Type | Value | Description                                   |
| ------------ | ---- | ----- | --------------------------------------------- |
| NB_IN        |      | 16    |  Total number of bits at input                |
| NBF_IN       |      | 14    |  Number of fractional bits at input           |
| NB_OUT       |      | 9     |  Total number of bits desired at output       |
| NBF_OUT      |      | 8     |  Number of fractional bits desired at output  |
| MODE         |      | 3     |  Type of trimmer operation                    |

##### Puertos

| Port name  | Direction | Type           | Description     |
| ---------- | --------- | -------------- | --------------- |
| i_full     | input     | [NB_IN  - 1:0] | Trimmer input   |
| o_sat_flag | output    |                | Saturation flag |
| o_trim     | output    | [NB_OUT - 1:0] | Trimmer output  |

##### Señales

| Name         | Type                         | Description |
| ------------ | ---------------------------- | ----------- |
| sat_flag     | reg                          |             |
| trimm_output | reg  signed [NB_OUT - 1 : 0] |             |
| full_rnd     | reg  signed [NB_RND - 1 : 0] |             |

##### Truncado y saturacion

<img src="ex_4/img/truncado_saturacion.png" alt="testbench" width="700"/>

##### Redondeo y saturacion

<img src="ex_4/img/redondeo_saturacion.png" alt="testbench" width="700"/>

#### Modulo `filter.v`

<img src="ex_4/img/filter.svg" alt="diagram" width="700"/>

##### Parametros
| Generic name | Type | Value | Description                      |
| ------------ | ---- | ----- | -------------------------------- |
| NB_IN        |      | 16    |  Total number of bits            |
| NBF_IN       |      | 15    |  Number of fractional bits       |
| NB_TAPS      |      | 16    |  Total number of bits            |
| NBF_TAPS     |      | 15    |  Number of fractional bits       |
| NB_OUT       |      | 18    |  Total number of bits            |
| NBF_OUT      |      | 17    |  Number of fractional bits       |
| N_TAPS       |      | 4     |  Quantity of filter coefficients |
| TRIM_MODE    |      | 3     |  Bit trimmer moide               |

##### Puertos

| Port name    | Direction | Type                 | Description        |
| ------------ | --------- | -------------------- | ------------------ |
| i_fir_input  | input     | signed [NB_IN -1 :0] | Filter input       |
| i_enable     | input     |                      | Enable             |
| i_reset      | input     |                      | Asynchronous reset |
| i_clock      | input     |                      | Clock              |
| o_fir_output | output    | signed [NB_OUT-1 :0] | Filter output      |

##### Puertos

| Name            | Type                       | Description          |
| --------------- | -------------------------- | -------------------- |
| shift_register  | reg  signed [NB_IN   -1:0] | Input shift register |
| coefficients    | wire [NB_TAPS -1:0]        | Filter coefficients  |
| products        | reg  signed [NB_PROD -1:0] | Multipliers output   |
| trimmers        | wire [NB_TRIM -1:0]        | Trimmers output      |
| sums            | reg  signed [NB_ADD  -1:0] | Adders output        |
| output_register | reg  signed [NB_OUT  -1:0] | Output register      |
| idx             | integer                    |                      |

##### Constantes

| Name     | Type | Value                        | Description                                |
| -------- | ---- | ---------------------------- | ------------------------------------------ |
| NBI_IN   |      | NB_IN   - NBF_IN             | Integer bits number of input  signal       |
| NBI_OUT  |      | NB_OUT  - NBF_OUT            | Integer bits number of output signal       |
| NBI_TAPS |      | NB_TAPS - NBF_TAPS           | Integer bits number of filter coefficients |
| NB_PROD  |      | NB_TAPS + NB_IN              | Total number of bits at multiplier output  |
| NBF_PROD |      | NBF_IN  + NBF_TAPS           | Fractional bits at multiplier output       |
| NB_CARRY |      | $clog2(N_TAPS)               | Number of sums tree levels                 |
| NB_TRIM  |      | NB_PROD  - NB_OUT - NB_CARRY | Total number of bits at trimmer output     |
| NBF_TRIM |      | NBF_PROD - NBF_OUT- NB_CARRY | Fractional bits at trimmer output          |
| NB_ADD   |      | NB_OUT                       | Total bits number of adder                 |
| NBF_ADD  |      | NBF_OUT                      | Fractional bits number of adder            |
| NBI_ADD  |      | NBI_OUT                      | Integer bits number of adder               |

#### Filtro completo

<img src="ex_4/img/filter.png" alt="testbench" width="700"/>


##### Truncado y saturacion

<img src="ex_4/img/fir_truncado_saturacion.png" alt="testbench" width="700"/>

##### Redondeo y saturacion

<img src="ex_4/img/fir_redondeo_saturacion.png" alt="testbench" width="700"/>
