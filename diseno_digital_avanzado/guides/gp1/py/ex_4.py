######################################################################
#                    DISEÑO DIGITAL AVANZADO 
#                        Fundacion Fulgor 
#
# Filename		: ex_4.py
# Programmer(s)	: Patricio Reus Merlo
# Created on	: 13 Sep. 2021
# Description 	: guide 1 - exercise 4
######################################################################

######################################################################
#                               MODULES
###################################################################### 

# from tool import _fixedInt as fxd
import numpy as np
import matplotlib.pyplot as plt

######################################################################
#                           CONSTANTS & MACROS
######################################################################
    
######################################################################
#                               MAIN
######################################################################

def main():
    forward_coefs  = [1, -1, 1, 1]
    feedback_coefs = [1, 0.25, 0.5]

    # Filter input (impulse)
    length  = 50
    one_pos = 5

    time = list(range(-one_pos,length-one_pos))

    filter_in = np.zeros(length);
    filter_in[5] = 1;

    # Filter output
    filter_out = np.zeros(length);

    # ------- Floating point -------
    for idx in range(max(len(forward_coefs), len(feedback_coefs)),len(filter_in)):
        
        # Forward block
        for i in range(len(forward_coefs)):
            filter_out[idx] += filter_in[idx - i]  * forward_coefs[i]
        
        # Feedback block
        for i in range(len(feedback_coefs)):
            filter_out[idx] += filter_out[idx - i] * feedback_coefs[i]    
    # ------------------------------

    # ------------- Plot -----------

    plt.stem(time, filter_out, linefmt='r--', markerfmt='r.', basefmt='r', label='Filter Output')
    plt.stem(time, filter_in , linefmt='b--', markerfmt='b.', basefmt='b', label='Filter Input' )
    plt.grid()
    plt.legend()
    plt.ylabel('Amplitude')
    plt.xlabel('Samples')
    plt.show()
    # ------------------------------



######################################################################
#                        CALL MAIN FUNCTION
######################################################################
if __name__ == "__main__":
    main()
