# Guía Práctica 1

---

## Ejercicio 1 (sugerido):

### Enunciado

1. Escribir el código Verilog para implementar el diseño de la figura considerando reset asincrono.

<img src="img/ex_1_diagram.png" alt="diagram" width="400"/>

1. Genere la señal de reset apropiada para el registro de realimentación utilizado en el diseño.

2.  Escribir un testbench que permita verificar el correcto funcionamiento del circuito propuesto. Los estímulos pueden ser generados con python o modelados en el testbench.

3.  Cuantos ciclos de reloj son necesarios para que el registro o data produzca overflow cuando i sel, i data1 e i data2 son iguales a 1?.

### Desarrollo

La descripción Verilog del esquema propuesto se encuentra en `src/ex_1.v`. El resultado es el siguiente: 

<img src="img/ex_1_rtl.png" alt="rtl" width="700"/>

El testbench del sistema se encuentra en `test/tb_ex_1.v`. Las entradas del bloque se modelaron en el mismo testbench resultando:

<img src="img/ex_1_tb.png" alt="testbench" width="700"/>

En esta simulacion las entradas son `i_data_1 = i_data_2 = 1`. Inicialmente `i_sel = 1` por lo que la suma acumulada se incrementa de 2 en 2. A la mitad de la simulación cambia el control del multiplexor a `i_sel = 2` y como `i_data_1 = 1`, la salida se incrementa de uno en uno.

El registro de cuenta de 7 bits se desborda luego de 64 periodos de clock cuando todas las entradas son unitarias.

---

## Ejercicio 4 (sugerido):

### Enunciado

1. Dibujar la arquitectura de la siguiente ecuacion en diferencias:

```math
    y[n] = x[n] - x[n-1] + x[n-2] + x[n-3] + 0,5 \; y[n-1] + 0,25 \; y[n-2]
```

2. Escribir el codigo Verilog.
3. Escribir el testbench para verificar el comportamiento.

### Desarrollo

La ecuacion en diferencias dada puede implementarse con la siguiente arquitectura:

<img src="img/ex_4.png" alt="testbench" width="700"/>

