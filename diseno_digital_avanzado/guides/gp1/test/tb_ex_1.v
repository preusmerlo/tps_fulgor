/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: tb_ex_1.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 13 Sep. 2021
* Description 	: guide 1 - exercise 1
************************************************************************************************
*/

`timescale 1ns/100ps

module tb_ex_1 ();

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam NB_DATA  = 3 ;
    localparam NB_SEL   = 2 ;
    localparam NB_OUT   = 6 ;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    /* Module inputs  */
    reg [NB_DATA -1:0] i_data_1 ;
    reg [NB_DATA -1:0] i_data_2 ;
    reg [NB_SEL  -1:0] i_sel    ;
    reg                i_rst_n  ;
    reg                i_clock  ;

    /* Module outputs */
    wire [NB_OUT  -1:0] o_data  ;
    wire                o_ovf   ;

/*
************************************************************************************************
*								       STIMULUS
************************************************************************************************
*/

    initial begin
        /* Initialize inputs */
        i_data_1 = {{NB_DATA-1{1'b0}} , 1'b1 };
        i_data_2 = {{NB_DATA-1{1'b0}} , 1'b1 };
        i_sel    = {NB_SEL/2 {2'h1}};
        i_rst_n  = 1'b1;
        i_clock  = 1'b0;
        
        /* Reset system */
        #4 i_rst_n = 1'b0;
        #4 i_rst_n = 1'b1;

        # 30 i_sel    = {NB_SEL/2 {2'h2}};

        #30 $finish();
    end

    always  begin
        #1 i_clock = ~i_clock;
    end

/*
************************************************************************************************
*								       MODULE INSTANCE
************************************************************************************************
*/

    ex_1 #(
        .NB_DATA    ( NB_DATA  ),
        .NB_SEL     ( NB_SEL   ),
        .NB_OUT     ( NB_OUT   )
    )
    DUT
    (
        .i_data_1   ( i_data_1 ),
        .i_data_2   ( i_data_2 ),
        .i_sel      ( i_sel    ), 
        .i_rst_n    ( i_rst_n  ),
        .i_clock    ( i_clock  ), 
        .o_data     ( o_data   ),
        .o_ovf      ( o_ovf    ) 
    );

endmodule  /* tb_ex_1 */
