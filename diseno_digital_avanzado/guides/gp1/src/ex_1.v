/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: ex_1.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 13 Sep. 2021
* Description 	: guide 1 - exercise 1
************************************************************************************************
*/

module ex_1 #(
        /* Parameters */
        parameter    NB_DATA  = 3 ,
        parameter    NB_SEL   = 2 ,
        parameter    NB_OUT   = 6
    )
    (
        /* Inputs */    
        input wire signed [NB_DATA -1:0] i_data_1 ,
        input wire signed [NB_DATA -1:0] i_data_2 ,
        input wire        [NB_SEL  -1:0] i_sel    , 
        input wire                       i_rst_n  ,
        input wire                       i_clock  , 

        /* Outputs */                
        output wire [NB_OUT  -1:0] o_data   ,
        output wire                o_ovf     
    );

/*
************************************************************************************************
*								      LOCAL PARAMETERS
************************************************************************************************
*/

    localparam NB_SUM = NB_OUT + 1;

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/
    reg signed [(NB_DATA + 1) -1 : 0] mux_out ;
    reg signed [ NB_SUM       -1 : 0] sum_out ;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    /* Mux */
    always @(*) begin
        case (i_sel)
            2'b00   : mux_out = {1'b0, i_data_2}    ;
            2'b01   : mux_out = i_data_1 + i_data_2 ;
            2'b10   : mux_out = {1'b0, i_data_1}    ;
            default : mux_out = {(NB_DATA+1){1'b0}} ;
        endcase
    end

    /* Feedback sum */
    always @(posedge i_clock or negedge i_rst_n) begin
        if(!i_rst_n)
            sum_out <= {NB_SUM{1'b0}};
        else begin
            sum_out <= mux_out + sum_out[NB_SUM-2 : 0]; 
        end
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/
    assign o_data = sum_out[NB_SUM-2 : 0];
    assign o_ovf  = sum_out[NB_SUM-1];

endmodule  /* ex_1 */
