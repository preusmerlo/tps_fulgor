/*
************************************************************************************************
*                                   DISEÑO DIGITAL AVANZADO 
*			        			        Fundacion Fulgor 
*
* Filename		: ex_4.v
* Programmer(s)	: Patricio Reus Merlo
* Created on	: 13 Sep. 2021
* Description 	: guide 1 - exercise 4
************************************************************************************************
*/

module ex_2 #(
        /* Parameters */
        parameter NB_IN  = 16 ,
        parameter NB_OUT = 16 ,
        parameter NB_SEL = 2 
    )
    (
        /* Inputs */    
        input wire signed  [NB_IN  -1:0] i_data_A ,
        input wire signed  [NB_IN  -1:0] i_data_B ,
        input wire         [NB_SEL -1:0] i_sel    ,
        
        /* Outputs */                
        output wire signed [NB_OUT -1:0] o_data_C  
    );

/*
************************************************************************************************
*								          VARIABLES
************************************************************************************************
*/

    reg [NB_OUT -1:0] data_C;

/*
************************************************************************************************
*								        ARCHITECTURE
************************************************************************************************
*/

    always @(*) begin
        case (i_sel)
            2'b00   : data_C = i_data_A + i_data_B ;
            2'b01   : data_C = i_data_A - i_data_B ;
            2'b10   : data_C = i_data_A | i_data_B ;
            2'b11   : data_C = i_data_A & i_data_B ;
            default : data_C = {NB_OUT{1'b0}}      ;
        endcase    
    end

/*
************************************************************************************************
*								        PORT MAPPING
************************************************************************************************
*/

    assign o_data_C = data_C;

endmodule  /* ex_2 */
